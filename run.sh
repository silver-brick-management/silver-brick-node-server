#!/bin/bash

# We're building a unique Worker ID, since every instance
# we spawn must be unique so we can avoid collisions

# WORKER_UUID=${WORKER_ID}_${RANDOM}_$(date '+%F-%T')
# WORKER_UUID=${WORKER_ID}

echo "-----------------------"
echo "Version - v${VERSION}"
echo "Instance time - $(date)"
echo "Execution Flag - ${EXECUTION_FLAG}"
echo "Worker config path - ${WORKER_CFG_PATH}"
echo "-----------------------"
echo
echo

node bin/main.js ${EXECUTION_FLAG} ${WORKER_CFG_PATH} 
