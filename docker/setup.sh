#!/bin/bash 

if [ "$EUID" -ne 0 ]
    then 
        echo "This script requires sudo privileges in order to run";
        exit 1;
fi

echo "Copying our service startup descriptor file into systemd's system location."
cp docker-silverbrick-startup.service   /etc/systemd/system

echo "Calling systemctl, to register docker-silverbrick-startup.service to launch at every boot"
systemctl enable docker-silverbrick-startup.service

echo "Copying over AWS credentials to be used by Docker"
mkdir -p /etc/systemd/system/docker.service.d/
cp aws-credentials.conf /etc/systemd/system/docker.service.d/aws-credentials.conf

echo "Reloading the daemon!"
systemctl daemon-reload

echo "Reloading the docker service!"
service docker restart

echo "Done."