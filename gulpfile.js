var gulp = require("gulp"),
    ts = require("gulp-typescript"),
    tsProject = ts.createProject("tsconfig.json", { typescript: require('typescript') }),
    tslint = require("gulp-tslint"),
    gulpUtil = require("gulp-util"),
    sourceMaps = require('gulp-sourcemaps'),
    path = require('path'),
    mocha = require('gulp-mocha');

gulp.task(
    "build", 
    ["lint"] /* Lint first before we declare the build successful */, 
    function () 
    {
        return tsProject.src()

            // Start the source maps plugin, so source maps can be
            // created once we transpile the typescript files
            .pipe(sourceMaps.init()) 
            
            // Transpile typescript to javascript
            .pipe(tsProject())

            // Write source maps to the current working directory 
            // (e.g. in this case, the ./bin directory)
            // 'sourceRoot'' is needed in order for the transpiler know where the 
            // typescript files are while generating the source maps. Without
            // the 'sourceRoot' declared, setting breakpoints in typescript won't 
            // work due to not being able to find the source maps. 'sourceRoot' is 
            // ran within the context of the destination path
            .pipe(sourceMaps.write(".", { sourceRoot: "../src/" }))
            
            // Write all of the working files into the ./bin directory
            .pipe(gulp.dest("bin"));
    });

gulp.task('test', function () {
    gulp.src('bin/test/**/*.js')
        .pipe(mocha({
            reporter: 'nyan',
            clearRequireCache: true,
            ignoreLeaks: false
        }));
});

gulp.task(
    "lint", 
    function() 
    {
        gulpUtil.log('There cannot be any lint errors, in order for the build to be declared successful.');
        gulpUtil.log('Therefore if you see any lint errors, please check/fix the typescript,');
        gulpUtil.log('and then try building again.');
        
        return gulp.src([
            "src/**/**.ts"
        ])

        // Lint errors/warnings are in prose, which is digestable
        // by the TsLint plugin in VS Code
        .pipe(tslint({
            formatter: "prose"
        }))

        // Return the report of errors/warnings thorugh stdout
        .pipe(tslint.report());
    });
