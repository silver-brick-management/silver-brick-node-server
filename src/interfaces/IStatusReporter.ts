// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { IConfigContext } from "./IConfigContext";

export declare type IncomingMessageCallbackType = (data: string) => void;

export interface IStatusReporter {

    SendStatus(message: string, channel?: string): void;

    RegisterIncomingMessageCallback(callback: IncomingMessageCallbackType): void;

    // Internal methods
    Initialize(configContext: IConfigContext): void;
}