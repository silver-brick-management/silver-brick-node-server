// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

export interface IJwtUser {
    readonly uid: string;
    readonly customerName: string;
    readonly email: string;
    readonly isAdmin: boolean;
    readonly stripeID: string;
    readonly org: IJwtOrg;
    readonly buildings: IJwtBuilding[];
    readonly phone?: string;
    propertyCodes?: number;
    readonly role?: string;
}

export interface IJwtOrgBuildingBase {
    readonly id: string;
    readonly isAdmin: boolean;
}

export interface IJwtOrg extends IJwtOrgBuildingBase {
    // Just need to extend the base. Might need to
    // diverge later
}

export interface IJwtBuilding extends IJwtOrgBuildingBase {
    readonly audiences: string[];
    readonly audienceNames: string[];
}
