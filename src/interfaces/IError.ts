// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

export interface IUnauthorizedError {
    readonly Name: string;
    readonly Code: string;
    readonly Status: number;
    readonly Message: string;
}

export interface JoinError {
    readonly Name: string;
    readonly Code: string;
    readonly Message: string;
}