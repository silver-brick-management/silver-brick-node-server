// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

export interface IConfigJsonData {
    readonly FriendlyName: string;

    readonly FirebaseAdminServiceAccountJsonPath: string;

    readonly FirebaseDBName: string;

    readonly FirebaseServiceAccountName: string;

    readonly FirebaseAPIKey: string;

    readonly FirebaseCloudMessagingAPIKey: string;

    readonly FirebaseCloudMessagingSenderID: string;

    readonly WorkerType: string;

    readonly MailgunAPIKey: string;

    readonly MailgunDomain: string;

    readonly StripePubKey: string;

    readonly StripeSecKey: string;

    readonly TwillioAccountSID: string;

    readonly TwillioAuthToken: string;

    readonly QuickBooksClientID: string;

    readonly QuickBooksClientSecret: string;

    readonly SlackChannel: string;

    readonly HttpServerPort: number;

    readonly JWTDbaSecret: string;

    readonly JWTLiveSecret: string;

    readonly JWTUserCPSecret: string;

    readonly JWTExpire: string;

    readonly JWTExpireCP: string;

    readonly MorganLogging: string;

    readonly SocketHandshakeTimeout: number;

    readonly SSLKeyPath: string;

    readonly SSLCertPath: string;

    readonly SSLSecret: string;

    readonly ShouldUseHTTPS: boolean;
}