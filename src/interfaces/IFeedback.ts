
export type FeedbackItem = {
    [id: string]: IFeedbackEntry;
};

export interface IFeedbackData {
    Feedback: string;
}

export interface IFeedbackEntry {
    Data: IFeedbackData;
    Timestamp: number;
    Type: string;
}