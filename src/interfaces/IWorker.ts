// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

export enum WorkerType {
    /*
        Here, we also version the specs as we can
        expect future iterations
    */
    UPSTREAM_V1,
    DOWNSTREAM_V1,
    ERROR_V1,
    RPC_V1,
    FEEDBACK_V1,
    API_REPORTER,
    NONE
}

export interface IWorker {

    readonly WorkerType: WorkerType;

    // Internal methods
    _initialize(): void;
}