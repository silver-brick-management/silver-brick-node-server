export interface IReportBuilder {
    toString(): string;
    Add(tag: string, obj: any): void;
}