// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { WorkerType } from "../interfaces/IWorker";

export enum ExecutionType {
    NONE,
    CREATE_WORKER,
    IMPORT_SCHEDULE,
    CREATE_HTTP_SERVER,
    GET_IN_PROGRESS_SCHEDULES
}

export enum ScenarioType {
    // Requires services to be up to work
    LIVE,
    // Run on mocked services for testing scenarios
    MOCK
}

export interface PropertyMap {
    [key: string]: string;
}

export interface IConfigContext {
    readonly ExecutionType: ExecutionType;
    // The service account is needed by firebase admin, in order for
    // the server instance to interact with the firebase database.
    readonly FirebaseAdminServiceAccountConfigJSONPath: string;

    // The firebase DB name we're interacting with
    readonly FirebaseDBName: string;

    // The identifying worker ID, unique to this server instance/process
    readonly FirebaseServiceAccountName: string;

    // The firebase API key we need in order to interact with the firebase DB
    readonly FirebaseAPIKey: string;

    readonly FirebaseDBUrl: string;

    readonly FirebaseStorageBucket: string;

    readonly FirebaseAuthDomain: string;

    readonly FirebaseCloudMessagingAPIKey: string;

    readonly FirebaseCloudMessagingSenderID: string;

    readonly MailgunDomain: string;

    readonly MailgunAPIKey: string;

    readonly TwillioAccountSID: string;

    readonly TwillioAuthToken: string;

    readonly StripePubKey: string;

    readonly StripeSecKey: string;

    readonly WorkerID: string;

    readonly WorkerType: WorkerType;

    readonly ConfigFriendlyName: string;

    readonly SlackChannel: string;

    readonly PropertyMap: PropertyMap;

    readonly QuickBooksClientID: string;

    readonly QuickBooksClientSecret: string;

    readonly HttpServerPort: number;

    readonly JWTDbaSecret: string;

    readonly JWTLiveSecret: string;

    readonly JWTUserCPSecret: string;

    readonly JWTExpire: string;

    readonly JWTExpireCP: string;

    readonly MorganLogging: string;

    readonly SocketHandshakeTimeout: number;

    readonly ScenarioType: ScenarioType;

    readonly SSLKeyPath: string;

    readonly SSLCertPath: string;

    readonly SSLSecret: string;

    readonly ShouldUseHTTPS: boolean;

    IsReady(): boolean;

    // Underscored methods indicate internal private methods, only to be used
    // inside classes implementing this interface and/or by the coupled factory
    _initialize(): void;
}