// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

export interface IFirebaseUser {
    readonly displayName: string;
    readonly email: string;
    readonly emailVerified?: boolean;
    readonly password: string;
    readonly photoURL?: string;
    readonly disabled?: boolean;
}

export interface IFirebaseConfig {
    readonly apiKey: string;
    readonly authDomain: string;
    readonly databaseURL: string;
    readonly projectId: string;
    readonly storageBucket: string;
    readonly messagingSenderId: string;
}