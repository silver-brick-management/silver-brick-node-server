
export interface IPotentialUser {
    readonly FirstName: string;
    readonly MiddleName: string;
    readonly LastName: string;
    readonly PersonID: string;

    IsEqual?(potentialUser: IPotentialUser): boolean;
}