var ALERT_TITLE = "This Booking is No Longer Active!";
var url = new URL(window.location.href);
var urlToken = JSON.stringify(url.searchParams.get("newToken"));
var phoneNumber = url.searchParams.get("phone");
let newStr = urlToken.replace(/\+/g, "%2B");
let newStr2 = newStr.replace(/['"]+/g, "");
console.log("Newstr", newStr2);

const serverCredentials = {
    phone: phoneNumber,
    id_token: newStr2
};
fetch('http://ec2-54-81-148-188.compute-1.amazonaws.com:3000/auth/v1/portal/authenticate/step1', 
{
    method: 'POST',
    headers: {
        'accept': 'text/plain; charset=utf-8',
        'content-type': 'application/json',
    },
    body: JSON.stringify(serverCredentials)
})
.then(function (response) {
    return response.json();
})
.then(function (serverResponse) {
    console.log("serverResponse", serverResponse);
    if (!serverResponse.success) {
        console.log("serverResponse false", serverResponse);
        setTimeout(function() {
            window.alert("This Booking is No Longer Active!");
        }, 1000);
    }
    else if (serverResponse.success) {
        console.log("serverResponse true", serverResponse);
        window.location.href = serverResponse.message;
    } else {
        setTimeout(function() {
            window.alert("This Booking is No Longer Active!");
        }, 1000);
    }
})
.catch(function (error) {
    setTimeout(function() {
            window.alert("Please click the text link that was sent to you");
    }, 1000);
});

if(document.getElementById) {
    window.alert = function(txt) {
        ALERT_TITLE = txt;
        createCustomAlert(txt);
    }
}


function createCustomAlert(txt) {
    var d = document;

    if(d.getElementById("modalContainer")) return;

    var mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
    mObj.id = "modalContainer";
    mObj.style.height = d.documentElement.scrollHeight + "px";

    var alertObj = mObj.appendChild(d.createElement("div"));
    alertObj.id = "alertBox";
    if(d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
    // alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth)/2 + "px";
    alertObj.style.visiblity="visible";

    var h1 = alertObj.appendChild(d.createElement("h1"));
    h1.appendChild(d.createTextNode(ALERT_TITLE));
    h1.align = "center";
    var msg = alertObj.appendChild(d.createElement("p"));
    //msg.appendChild(d.createTextNode(txt));
    msg.innerHTML = (txt === 'This Booking is No Longer Active!') ? 'The Booking was deleted or reassigned since you received this text' : 'Please click the original link in the text message you received.';
    msg.align = "center";
    var btn = alertObj.appendChild(d.createElement("a"));
    btn.id = "cancelBtn";
    btn.appendChild(d.createTextNode('Thanks'));
    btn.href = "#";
    btn.focus();
    btn.onclick = function() { removeCustomAlert(); window.close(); return false; }

    alertObj.style.display = "block";
}

function removeCustomAlert() {
    document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
}