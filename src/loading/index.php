<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="robots" content="noindex">
        <title>
            SilverBrick Portal
        </title>
        <meta name="viewport" content="viewport-fit=cover, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <meta name="format-detection" content="telephone=no" />
        <meta name="msapplication-tap-highlight" content="no" />
        <link rel="icon" type="image/x-icon" href="https://www.adamwolfson.com/silver.jpg" />
        <!-- <link type="text/css" rel="stylesheet" href="bootstrap.min.css"/> -->
        <link type="text/css" rel="stylesheet" href="css/style.css"/>
        <!-- <script src="mobile-detect.js"></script> -->
        <script src="js/appless.js?n=1"></script>
    </head>
    <body class="splash">
        <div style="width: 100%; height: 100%;">
            <img src="img/splash-rectangle.png" class="rectangle"/>
            <img src="img/splash-rectangle-1.png" class="rectangle1"/>
            <div class="group3" align="center">
                <img src="img/brand-rectangle-2@2x.png" class="rectangle2" />
                <div class="SilverBrick">
                    SilverBrick
                </div>
                <img src="img/brand-fill-4@2x.png" class="fill4"/>
            </div>
            <div class="spinner"></div>
        </div>
    </body>
</html>