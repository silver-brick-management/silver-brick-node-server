// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

declare namespace SocketIO {

    /**
	 * The client behind each socket (can have multiple sockets)
     * Extending for storing additional metadata
	 */
    interface Client {
        /**
         * Whether the connection has been authenticated
         */
        isAuthenticated: boolean;

        /**
         * Stores the time the connection was established
         */
        connectedAt: Date;

        /**
         * User info for easy access throughout the duration
         * of the connection. Cant import IUser into the declaration file
         * so have to type it as any and rely on casting during usage.
         * ALT is to duplicate the IUser interface here, but not a fan of that.
         */
        user: any;

        /**
         * The JWT token used to authenticate the connection
         */
        token: string;

        /**
         * Keeps track of rooms user joined.
         * Allows for proper leaving of room on disconnect
         */
        rooms: Map<string, string[]>;

        /**
         * Allowing client to be extensible
         */
        [key: string]: any;
    }
}
