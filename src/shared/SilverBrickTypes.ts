// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.text'; which can be found at the root of this
// project.
//
// *********************************************************

import { customers, ICard, cards } from "stripe";

/*
  Account Related
*/

export interface IUser {
    firstName: string;
    lastName: string;
    uid?: string;
    isAdmin: boolean;
    isVendor?: boolean;
    email: string;
    phone: string;
    fcmRegToken: string;
    osType: string;
    password?: string;
    photoURL: string;
    fullPhotoURL?: string;
    createdAt?: Date;
    lastSignedInAt?: Date;
    orgs?: IUserOrg;
    role: string;
    clients: IClient[];
    stripeID: string;
    address?: IAddress;
    lastActivity?: number;
    propertyCodes?: number;
    preferences?: IPreferences;
    unit?: string;
    isFirstTime?: boolean;
    isClockedIn?: boolean;
    calendarColor?: string;
    calendarTextColor?: string;
}

export interface ISilverBrickUser {
    firstName: string;
    lastName: string;
    uid?: string;
    isAdmin: boolean;
    isVendor?: boolean;
    email: string;
    phone: string;
    fcmRegToken: string;
    osType: string;
    password?: string;
    photoURL: string;
    fullPhotoURL?: string;
    createdAt?: Date;
    lastSignedInAt?: Date;
    orgs?: OrgToPermissionMap;
    role: string;
    clients: IClient[];
    stripeID: string;
    lastActivity?: number;
    address?: IAddress;
    propertyCodes?: number;
    preferences?: IPreferences;
    unit?: string;
    isFirstTime?: boolean;
    isClockedIn?: boolean;
    calendarColor?: string;
    calendarTextColor?: string;
}

export interface IPreferences {
    isFirstTime: boolean;
    hasPropertyCode: boolean;
    timeOfDay: any;
    endTimeOfDay: any;
    preferredDay: string;
    planID?: string;
    cardID?: string;
    subscriptionNewDate?: number;
}

export interface IClient {
    id: string;
    name: string;
    photoURL: string;
    email: string;
    active: boolean;
    scheduleInfo: IScheduleInfo[];
}

export interface IScheduleInfo {
    Address1: string;
    Address2: string;
    City: string;
    PostalCode: string;
    State?: string;
    coordinates: IUserLocation;
    note: string;
    time: ITime;
}

export interface IEmployee {
    id: string;
    name: string;
    last: string;
    goesby: string;
    address: string;
    rate: string;
    type: string;
    email: string;
    active: boolean;
    log: ILog[];
}

export interface IVendor {
    contract: boolean;
    photoURL?: string;
    name: string;
    address: IAddress;
    employees: IStaffProfile[];
    id?: string;
}

export interface ITimeCard {
    startTime: number;
    endTime?: number;
    location: ILocation;
    notes?: string;
    authorID: string;
    authorName: string;
    id?: string;
    date: number;
    year?: number;
    month?: number;
    day?: number;
}

export interface ILocation {
  lat: number;
  lng: number;
}

export interface ILogSchema {
    user: ISilverBrickUser;
    time: number;
    request: string;
    params: string;
}

export interface ILog {
    time: number;
    coords: IScheduleInfo;
    action: string;
}

export interface ITask {
    authorName: string;
    authorID: string;
    clientID: string;
    clientName: string;
    clientUnit: string;
    active: boolean;
    icon: string;
    status: string;
    deleted: boolean;
    notes: string;
    date: number;
    id?: string;
    type?: string;
    category: string;
    subCategory?: string;
    orgID?: string;
    buildingID?: string;
    buildingName: string;
    subject: string;
    description: string;
    availability?: string;
    messages?: IMessage[];
    photoURL?: string;
    photoGallery?: string[];
    files?: any[];
    lastUpdated?: number;
    progressStart?: number;
    progress?: ITime;
    address?: IAddress;
    updateAuthor?: string;
    updatedRole?: string;
    buildingAddress?: string;
    permissionToEnter?: boolean;
    havePets?: boolean;
    recurring?: boolean;
    startDate?: number;
    endDate?: number;
    durationAmount?: string;
    durationQuantity?: number;
    startTime?: string;
    endTime?: string;
    clockInTime?: number;
    scheduled?: boolean;
    scheduling?: boolean;
    clockOutTime?: number;
    repeats?: string;
    repeatInterval?: string;
    repeatDays?: number[];
    repeatQuantity?: number;
    monthlyConvention?: string;
    monthlyDays?: number[];
    completedDates?: string[];
    monthlyWeekDays?: number[];
    monthlyNWeekDays?: number[][];
    assignees?: IAssignee[];
    skipDates?: string[];
    location?: ILocation;
    taskType?: string;
}

export interface IAssignee {
    name: string;
    uid: string;
    date?: number;
}

export interface IHistory {
    bookingID: string;
    clientID: string;
    clientName: string;
    clientUnit: string;
    assignID: string;
    assignName?: string;
    date: number;
    serviceID?: string;
    serviceName?: string;
    buildingID: string;
    orgID: string;
}

export interface ITaskFeed {
    type: string;
    taskID: string;
    message: string;
    role?: string;
    timestamp: any;
    category?: string;
    subCategory?: string;
    orgID: string;
    id?: string;
    buildingID: string;
    buildingName?: string;
    clientUnit: string;
    photoURL?: string;
    authorName?: string;
    authorID?: string;
    status?: string;
    audience?: string;
    assignees?: IAssignee[];
    approved: boolean;
    approveDate?: number;
    approveAuthor?: string;
    approveAuthorID?: string;
    photoGallery?: string[];
    readReceipts?: IAssignee[];
}

export interface ITime {
    start?: number;
    startChecked?: boolean;
    finish?: number;
    finishChecked?: boolean;
}

export interface IAccountInfo extends customers.ICustomer {
    name: string;
}

/*
  Org Related
*/

export interface IUserOrgBuildingBase {
    id: string;
    name?: string;
    isAdmin: boolean;
}

export interface IUserOrg extends IUserOrgBuildingBase {
    buildings: IUserBuilding[] | any;
}

export interface IUserBuilding extends IUserOrgBuildingBase {
    [key: string]: any;
}

export interface IUserAudience {
    id: string;
    name: string;
}

export interface IOrgPermissionData {
    buildings: BuildingToPermissionMap;
    isAdmin: boolean;
}

export type OrgToPermissionMap = {
    [orgID: string]: IOrgPermissionData;
};

export type BuildingToPermissionMap = {
    [buildingID: string]: IBuildingPermissionData;
};

export interface IBuildingPermissionData {
    canCreate: boolean;
    isAdmin: false;
    audiences: AudienceToPermissionMap;
    isDBA?: boolean;
    isBillingContact?: boolean;
}

export type AudienceToPermissionMap = {
    [audienceID: string]: string;
};

export interface IOrgBasic {
    name: string;
    id: string;
    buildings: any[];
}

export interface IOrgSummary {
    name: string;
    id: string;
    buildings: IBuildingBasic;
}

export interface IBuildingBasic {
    [buildingID: number]: IBuildingBasicInfo;
}

export interface IBuilding {
    info: IBuildingInfo;
    config: IBuildingConfig;
    id: string;
    orgID?: string;
    landlords?: IUser[];
}
export interface IBuildingConfig {
    isEmailSendingEnabled: boolean;
    isSMSSendingEnabled: boolean;
    propertyCodes: string;
}

export interface IBuildingInfo {
    address: IAddress;
    name: string;
    phone: string;
    lastModified?: number;
    landlords?: IUser[];
    type?: string;
    billingContact?: IProfile;
}

export interface IBuildingBasicInfo {
    name: string;
    id: string;
    city?: string;
    state?: string;
    address?: any;
    orgID?: string;
    zip?: string | number;
    landlords?: IUser[];
    type?: string;
    billingContact?: IProfile;
}

export interface IOrgSimple {
    id: string;
    name: string;
    address: IAddress;
}

export interface IBuildingSearch {
    orgID: string;
    buildingID: string;
    address: IAddress;
    name: string;
    propertyCode: string;
}

export interface IBuildingSimple {
    id: string;
    name: string;
    address?: IAddress;
}
export interface IInfo {
    readonly address: IAddress;
    readonly name: string;
    readonly phone: string;
    readonly lastModified: number;
}

export interface INote {
    deleted: boolean;
    message: string;
    authorName: string;
    authorID: string;
    buildingID: string;
    orgID: string;
    id?: string;
    title: string;
    updated?: number;
    date: number;
}

export interface IOrgInfo extends IInfo {
    // Room for extras
    id: string;
    owner?: IProfile;
}

export interface IBuilding {
    info: IBuildingInfo;
    config: IBuildingConfig;
    id: string;
}
export interface IBuildingConfig {
    isEmailSendingEnabled: boolean;
    isSMSSendingEnabled: boolean;
    propertyCodes: string;
}

export interface IBuildingInfo {
    address: IAddress;
    name: string;
    phone: string;
    lastModified?: number;
    owner?: string;
    contactPerson?: IContactPerson;
}

export interface IContactPerson {
    firstName: string;
    lastName: string;
    email: string;
    phone?: string;
    role: string;
}

export interface IProfile {
    firstName?: string;
    email?: string;
    lastName: string;
    uid: string;
    photoURL?: string;
    address?: IAddress;
    phone?: string;
}

export interface IStaffProfile {
    firstName?: string;
    lastName: string;
    uid: string;
    photoURL?: string;
    role?: string;
    email?: string;
    isVendor?: boolean;
    phone?: string;
}

export interface IProfileUser {
    uid: string;
    address?: IAddress;
}

export interface IAddress {
    city: string;
    line1: string;
    line2: string;
    state: string;
    zip: string;
    country?: string;
}

export interface IUserLocation {
    latitude: number;
    longtitude: number;
}

export interface IPaymentCard extends cards.ISourceCreationOptionsExtended {
    name: string;
    number: string;
    exp_month: number;
    exp_year: number;
    cvc: string;
    address: IAddress;
    default?: boolean;
    cardID: string;
    customerName: string;
    brand: string;
}

/*
  Message Related
*/

export interface IMessage {
    text: string;
    authour: string;
    date: any;
    authorID: string;
    photoURL?: string;
    id?: string;
    type: string;
    role?: string;
}

/*
  Push Related
*/

export enum NotificationType {
    NEW_USER,
    NEW_MESSAGE,
    SERVICE_ASSIGNED,
    EDIT_SERVICE,
    CHAT_ROOM
}

export interface INotificationData {
    type: NotificationType;
    date: any;
    authorName: string;
    authorID?: string;
    photoURL?: string;
    title: string;
    message: string;
    trackingURL?: string;
    id?: string;
}

export interface IInAppToastData {
    message: string;
    data: string;
}

export interface IDeviceInfo {
    osType: string;
    fcmRegToken: string;
}
