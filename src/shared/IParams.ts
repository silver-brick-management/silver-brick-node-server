// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Local modules

export interface ICredentials {
    email: string;
    password: string;
}

export interface IServerCredentials {
    email: string;
    id_token: string;
}


export interface ITaskFeedJoin {
    orgID: string;
    buildingID: string;
    taskID: string;
}
