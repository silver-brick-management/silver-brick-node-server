// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { customers, ICard, cards } from "stripe";

import {
    ISilverBrickUser,
    IAssignee,
    IMessage,
    IPaymentCard,
    INotificationData,
    ILocation,
    IProfile,
    IProfileUser,
    IUser,
    IBuilding,
    IUserAudience,
    IUserBuilding,
    IUserOrg,
    IBuildingSearch,
    IVendor,
    IOrgBasic,
    IOrgInfo,
    ITaskFeed,
    INote,
    IAccountInfo,
    IPreferences,
    ITask,
    IBuildingInfo,
    IHistory,
    ITimeCard
} from "../shared/SilverBrickTypes";

/* COMMON */

export interface IServerResponseBase {
    success: boolean;
    description?: string;
}

export interface IServerResponseMessage extends IServerResponseBase {
    message: string;
}

export interface IServerResponseExisting extends IServerResponseBase {
    baseId: string;
}

export interface IServerResponseRefreshToken extends IServerResponseBase {
    accessToken: string;
}

export interface IServerResponseLogin extends IServerResponseRefreshToken {
    data: IUser;
}

export interface IServerResponseGetUser extends IServerResponseBase {
    data: IUser;
}

export interface IServerResponseAddOrUpdateOrGetUser
    extends IServerResponseBase {
    data: IUser;
}

export interface IServerResponseGetBuildingUsers extends IServerResponseBase {
    data: IUser[];
}

export interface IServerGetAccountInfo extends IServerResponseBase {
    data: customers.ICustomer;
}

export interface IServerAddPaymentCard extends IServerResponseBase {
    data: ICard;
}

export interface IServerDeletePaymentCard extends IServerResponseBase {
    data: string;
}

export interface IServerResponseGetCards extends IServerResponseBase {
    data: ICard[];
}

/*
  DBA and Admin-CP
*/
export interface IServerResponseGetUsers extends IServerResponseBase {
    data: IUser[];
    paging: IPaging;
}

export interface IServerResponsePatchOrDeleteUser extends IServerResponseBase {
    data: string;
}

export interface IServerResponseGetCalendarColors extends IServerResponseBase {
    data: any;
}

export interface IServerResponseGetAllOrgs extends IServerResponseBase {
    data: IOrgBasic[];
}

export interface IServerResponseCreateOrg extends IServerResponseBase {
    data: IOrgBasic;
}

export interface IServerResponseGetOrgInfo extends IServerResponseBase {
    data: IOrgInfo;
}

export interface IServerResponseGetBuildingInfo extends IServerResponseBase {
    data: IBuildingInfo;
}

export interface IServerResponseGetAllOrgs extends IServerResponseBase {
    data: IOrgBasic[];
}

export interface IServerResponseGetAllBuildings extends IServerResponseBase {
    data: IBuilding[];
}

export interface IServerResponseCreateOrg extends IServerResponseBase {
    data: IOrgBasic;
}

export interface IServerResponseGetOrgInfo extends IServerResponseBase {
    data: IOrgInfo;
}

export interface IServerResponseCreateBldg extends IServerResponseBase {
    data: IBuilding;
}

export interface IServerResponseGetTasks extends IServerResponseBase {
    data: ITask[];
}

export interface IServerResponseAddUpdateAssignTask extends IServerResponseBase {
    data: ITask;
}

export interface IServerResponseAssignTask extends IServerResponseBase {
    data: AssignTaskHelper;
}

export interface IServerResponseAddTimeCard extends IServerResponseBase {
    data: ITimeCard;
}

export interface IServerResponseGetTimeCards extends IServerResponseBase {
    data: ITimeCard[];
}

export interface GetTimeCardHelper {
    date: number;
    year?: number;
    month?: number;
    day?: number;
    uid: string;
}

export interface AddTimeCardHelper {
    date: number;
    timeCard: ITimeCard;
}

export interface AssignTaskHelper {
    userID: string;
    authorID: string;
    task: ITask;
    message?: string;
}

/* Helpers */

export interface NewSignUpHelper {
    wallet: cards.ISourceCreationOptionsExtended[];
    user: IUser;
}

export interface PostStatHelper {
    count: number;
    active: boolean;
    postID: string;
    users: string[];
}

export interface PostUserHelper {
    bio: string;
    username: string;
    avatar: string;
    timestamp?: number;
    id: string;
}

export interface ProfileCursor {
    userID: string;
    lastPost: string;
}

/* COMMON */

export interface IServerResponseBase {
    success: boolean;
    description?: string;
}

export interface IServerResponseMessage extends IServerResponseBase {
    message: string;
}

export interface IServerResponseRefreshToken extends IServerResponseBase {
    accessToken: string;
}

export interface IServerResponseCreateNewUser extends IServerResponseBase {
    data: ISilverBrickUser;
}

export interface IServerResponseUpdateProfile extends IServerResponseBase {
    data: ISilverBrickUser;
}

export interface IServerResponseGetProfileUser extends IServerResponseBase {
    data: IProfileUser;
}

export interface IServerResponseGetLovedUsers extends IServerResponseBase {
    data: PostUserHelper;
}

export interface IServerResponsePostStat extends IServerResponseBase {
    data: PostStatHelper;
}

export interface IServerResponseSearchUser extends IServerResponseBase {
    data: (ISilverBrickUser)[];
}

export interface IServerResponseGetProfile extends IServerResponseBase {
    data: ISilverBrickUser;
}

export interface IServerResponseUpdateProfile extends IServerResponseBase {
    data: ISilverBrickUser;
}

export interface IServerResponseUpdatePreferences extends IServerResponseBase {
    data: IPreferences;
}

export interface IServerResponseCheckUsername extends IServerResponseBase {
    data: boolean;
}

export interface IServerResponseAuthorizationBrainTree
    extends IServerResponseBase {
    data: string;
}

/*
    Order Related
*/

export interface IServerGetAccountInfo extends IServerResponseBase {
    data: customers.ICustomer;
}

export interface IServerAddPaymentCard extends IServerResponseBase {
    data: ICard;
}

export interface IServerDeletePaymentCard extends IServerResponseBase {
    data: string;
}

export interface IServerResponseGetCards extends IServerResponseBase {
    data: ICard[];
}

export interface IServerResponseGetStripeConnectID extends IServerResponseBase {
    data: string;
}

export interface IServerResponseRemoveFromCart extends IServerResponseBase {
    data: string;
}

export interface IServerResponseUpdateStripeConnect
    extends IServerResponseBase {
    data: string;
}

export interface IServerResponseOrderShipped extends IServerResponseBase {
    data: string;
}

export interface IServerResponseGetNotes extends IServerResponseBase {
    data: INote[];
}

export interface IServerResponseAddUpdateNote extends IServerResponseBase {
    data: INote;
}

export interface IServerResponseDeleteNote extends IServerResponseBase {
    data: string;
}

export interface IServerResponseAddUpdateUserLocation extends IServerResponseBase {
    data: ILocation;
}

export interface IServerResponseGetUserLocations extends IServerResponseBase {
    data: ILocation[];
}


/*
  Messages Related
*/

export interface IServerResponseGetMessageHistory extends IServerResponseBase {
    data: IGetMessageHistoryHelper;
}

export interface IServerResponseAddMessage extends IServerResponseBase {
    data: AddMessageHelper;
}

export interface IServerResponseAddChat extends IServerResponseBase {
    data: AddChatHelper;
}

export interface IServerResponseNewRoom extends IServerResponseBase {
    data: NewChatRoomHelper;
}

export interface IServerResponseGetChatRooms extends IServerResponseBase {
    data: UserChatRoom[];
}

export interface IServerResponseAssignBooking extends IServerResponseBase {
    data: IScheduleAssigner;
}

export interface IServerResponseGetVendors extends IServerResponseBase {
    data: IVendor[];
}

export interface IServerResponseAddOrUpdateVendor extends IServerResponseBase {
    data: IVendor;
}

export interface IServerResponseGetHistory extends IServerResponseBase {
    data: IHistory[];
}

export interface IServerResponseGetTaskHistory extends IServerResponseBase {
    data: ITaskFeed[];
}

export interface IServerResponseAddTaskHistory extends IServerResponseBase {
    data: ITaskFeed;
}

export interface IServerResponseAddorUpdateHistory extends IServerResponseBase {
    data: IHistory;
}

export interface IServerResponseBuildingSearch extends IServerResponseBase {
    data: IBuildingSearch[];
}

/*
  Notifications Related
*/

export interface IServerResponseGetNotifications extends IServerResponseBase {
    data: INotificationData[];
}

export interface IServerResponseAddNotification extends IServerResponseBase {
    data: INotificationData;
}

export interface IServerResponseChangePassword extends IServerResponseBase {
    data: boolean;
}

export interface IServerResponseGetTimeCardRoster extends IServerResponseBase {
    data: ITimeCardRoster;
}

export interface ProfileCursor {
    userID: string;
    lastPost: string;
}

export interface TrackCursor {
    userID: string;
    lastTrack: string;
}

/*
  Chat Helpers
*/

export interface IGetMessageHistoryHelper {
    messages: IMessage[];
    hasMoreMessages: boolean;
    cursor: string;
    isInitialLoad: boolean;
}
export interface IPaging {
    cursors: ICursors;
    previous: string;
    next: string;
}

export interface ICursors {
    prevCursor: string;
    nextCursor: string;
}

export interface AddMessageHelper {
    taskID: string;
    message: ITaskFeed;
}

export interface NewChatRoomHelper {
    message: IMessage;
    participants: MessageParticipant[];
    roomID?: string;
    authorID: string;
}

export interface UserChatRoom {
    users: MessageParticipant[];
    roomID: string;
    lastMessage: IMessage;
    authorID: string;
}

export interface MessageParticipant {
    uid: string;
    photoURL: string;
    authour: string;
    role?: string;
}

export interface AddChatHelper {
  authorID: string;
  roomID: string;
  participants: string[];
  message: IMessage;
}

export interface INewSubscription {
    date: number;
    planID: string;
    price?: number;
}

export interface IScheduleAssigner {
    scheduleID: string;
    assignees?: IAssignee[];
}

export interface ITimeCardToday {
    name: string;
    uid: string;
    cards: ITimeCard[];
}

export interface ITimeCardRoster {
    [key: string]: ITimeCardToday;
}

