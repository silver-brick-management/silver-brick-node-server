// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
//  This is the file where we will:
//  - Configure our Socket.io server
// *********************************************************

// Node modules
import * as http from "http";
import * as https from "https";
import { listen } from "socket.io";

// Local modules
import { AuthUtil } from "../../common/AuthUtil";
import { IJwtUser } from "../../interfaces/IAuth";
import { Constants } from "../../common/Constants";
import { UnauthorizedError } from "../../common/Error";
import { IUnauthorizedError } from "../../interfaces/IError";
import { IConfigContext } from "../../interfaces/IConfigContext";
import { IServerResponseMessage } from "../../shared/ResponseTypes";
import { ISilverBrickUser } from "../../shared/SilverBrickTypes";
import { MessagesV1Socket } from "./v1/MessageSocket";


export function ConfigureSocketio(hostedServer: https.Server | http.Server, configContext: IConfigContext): SocketIO.Server {

    // JWT Auth Guard
    const checkAuthToken = (authorization: string): Promise<IJwtUser> => {
        return new Promise<IJwtUser>((resolve, reject) => {
            let token: string;
            const parts = authorization.split(" ");

            if (parts.length === 2) {
                const scheme = parts[0];
                const credentials = parts[1];

                if (/^Bearer$/i.test(scheme)) {
                    token = credentials;
                }
                else {
                    reject(new UnauthorizedError("credentials_bad_scheme", { message: "Format is Authorization: Bearer [token]" }));
                }
            }
            else {
                reject(new UnauthorizedError("credentials_bad_format", { message: "Format is Authorization: Bearer [token]" }));
            }

            if (!token) {
                reject(new UnauthorizedError("credentials_required", { message: "No authorization token was found" }));
            }
            else {
                try {
                    const jwtData: IJwtUser = AuthUtil.VerifyJWTAccessToken(token, configContext.JWTLiveSecret);
                    resolve(jwtData);
                }
                catch (error) {
                    reject(new UnauthorizedError("invalid_token", error));
                }
            }
        });
    };

    const socketioServer: SocketIO.Server = listen(hostedServer);

    // Register all namespaces
    MessagesV1Socket.SetupSockets(socketioServer, configContext);
    console.log(Object.keys(socketioServer.nsps));

    // temp delete socket from namespace connected map
    Object.keys(socketioServer.nsps).forEach((channel) => {
        socketioServer.nsps[channel].on(Constants.SOCKET_CONNECT, (socket) => {
            // console.log("Socket client id: ", socket.client.id);
            // console.log("Socket authentication status in connect: ", socket.client.isAuthenticated);
            // console.log("Socket id: ", socket.id);
            if (!socket.client.isAuthenticated) {
                // console.log("Removing socket from ", channel);
                delete socketioServer.nsps[channel].connected[socket.id];
            }
        });
    });
    socketioServer.on(Constants.SOCKET_CONNECTION, (socket) => {
        socket.client.isAuthenticated = false;
        // console.log("A user connected: ", socket.id);

        // Give the client 5 seconds to authenticate
        const auth_timeout = setTimeout(() => {
            if (!socket.client.isAuthenticated) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: "UnauthorizedError",
                    description: "An authorization message was not received with 5 seconds"
                };
                socket.emit(Constants.SOCKET_UNAUTHORIZED, responseObject);
                // TODO: Log error
                const error: UnauthorizedError = new UnauthorizedError("authorization_timeout", { message: "An authorization message was not received with 5 seconds" });
                // console.log(`Default Channel: Error: ${error}`);
                // console.log("Disconnecting socket ", socket.id);
                // True closes the connection completel.
                // Set back to true once i figure out how to stop the client
                // From reconnecting
                socket.disconnect(true);
                return;
            }
        }, configContext.SocketHandshakeTimeout);

        socket.on(Constants.SOCKET_AUTHENTICATE, (token: string) => {
            // Authenticate emit received, so stop the timeout
            // Allows for processing auth without racing against the clock
            clearTimeout(auth_timeout);
            checkAuthToken(token)
                .then((userData: IJwtUser) => {
                    // The client object is global across all namespaces
                    // Storing infomation that will be shared while the socket is alive
                    socket.client.isAuthenticated = true;
                    socket.client.connectedAt = new Date();
                    socket.client.user = userData;
                    socket.client.token = token;
                    socket.client.rooms = new Map<string, string[]>(); // Init rooms
                    // Restore temporarily disabled connection
                    // Only need to restore the default namespace socket as
                    // auth can only occur on this namespace. If a user tries to
                    // connect to other namespaces without first connecting here to auth
                    // the whole connection will be rejected and killed
                    console.log("Restoring socket to ", socketioServer.sockets.name);
                    socketioServer.sockets.connected[socket.id] = socket;

                    console.info(`Socket ${socket.id} connected and authenticated`);
                    const responseObject: IServerResponseMessage = {
                        success: true,
                        message: Constants.SOCKET_AUTHENTICATED
                    };
                    socket.emit(Constants.SOCKET_AUTHENTICATED, responseObject);
                })
                .catch((error: IUnauthorizedError) => {
                    const responseObject: IServerResponseMessage = {
                        success: false,
                        message: error.Name,
                        description: error.Message
                    };
                    socket.emit(Constants.SOCKET_UNAUTHORIZED, responseObject);
                    // TODO: Log error
                    // console.log(`Default Channel: Error: ${error}`);
                    // console.log("Authentication failed. Token: ", token);
                    socket.disconnect(true);
                    return;
                });
        });

        // Disconnect listener
        socket.on(Constants.SOCKET_DISCONNECT, () => {
            // socket.disconnect(true);
            // console.info(`Socket ${socket.id} disconnected`);
        });
    });

    return socketioServer;
}

export function GlobalSocketErrorHandler(socket: SocketIO.Socket, type: string, error: Error = null) {
    let event: string = null;
    let description: string = null;

    // TODO: Move types into constant
    switch (type) {
        case "JoinTimeout":
            event = Constants.SOCKET_UNAUTHORIZED;
            description = "A join request was not received with 10 seconds";
            break;

        case "JoinNoParam":
            event = Constants.SOCKET_ERROR;
            description = "A user ID was not provided with the join request";
            break;

        case "JoinError":
            event = Constants.SOCKET_ERROR;
            description = "The join request failed for reason. Please try again";
            break;

        case "JoinDupe":
            event = Constants.SOCKET_ERROR;
            description = "The client has already joined this room";
            break;

        case Constants.ERROR_MESSAGE_BAD_DATA:
            event = Constants.SOCKET_ERROR;
            description = Constants.ERROR_DESCRIPTION_MISSING_REQS;
            break;

        default:
            event = Constants.SOCKET_ERROR;
            type = "UnauthorizedError";
            description = "Not authorized";
            break;

    }

    const responseObject: IServerResponseMessage = {
        success: false,
        message: type,
        description: description
    };
    socket.emit(event, responseObject);

    // TODO: Log error
    if (!error) {
        error = new UnauthorizedError(type, { message: description });
    }
    else {
        // Error object was passed in
        // Use that instead
    }

    console.log(`Default Channel: Error: ${error}`);
    console.log("Default Channel: Disconnecting socket ", socket.id);
    // True closes the connection completely.
    // Set back to true once i figure out how to stop the client
    // From reconnecting
    socket.disconnect(true);
    return;
}

export function JoinChannelRoom(socket: SocketIO.Socket, channel: string, room: string): boolean {
    // Init flag that determines if a user can join a room
    // used to ensure dupe joins are rejected
    let canJoinRoom = false;
    // Save joined room in the socket object so we can leave
    // on disconnect. Map channel to room(s)
    if (socket.client.rooms && socket.client.rooms.has(channel)) {
        // Not user's first room, append new room to room list
        const roomList: string[] = socket.client.rooms.get(channel);
        // Ensure we dont append dupes
        if (!roomList.includes(room)) {
            canJoinRoom = true;
            roomList.push(room);
            socket.client.rooms.set(channel, roomList);
        }
        else {
            // Do nothing, client requesting to be joining a room they already are in
        }
    }
    else {
        canJoinRoom = true;
        // This is the user's first room join
        socket.client.rooms.set(channel, [room]);
    }

    // Join room if user is not already in it
    if (canJoinRoom) {
        // Finally join the room to receive post updates
        socket.join(room);
        console.log(`${channel} Channel: User has joined room: ${room}`);
    }
    return canJoinRoom;
}

export function LeaveChannelRoom(socket: SocketIO.Socket, channel: string, room: string = null): void {
    // Validate channel exist in our cache
    if (socket.client.rooms && socket.client.rooms.has(channel)) {
        // Get the list of rooms the user has joined for a specific channel
        const roomList: string[] = socket.client.rooms.get(channel);
        if (!room) {
            // Specific room not specified, so we will remove the user from all rooms
            for (let innerRoom of roomList) {
                socket.leave(innerRoom);
            }
            // Remove channel from our caches
            socket.client.rooms.delete(channel);
        }
        else {
            // Remove & update the room list for the channel
            const newRoomList = roomList.filter((innerRoom) => {
                innerRoom !== room;
            });
            if (newRoomList.length === 0) {
                // Room list is empty
                // Remove channel from our caches
                socket.client.rooms.delete(channel);
            }
            else {
                // Room list is not empty
                // Update room list for the channel
                socket.client.rooms.set(channel, newRoomList);
            }
            // Remove user from only the room specified
            socket.leave(room);
        }
    }
    else {
        // User has not yet joined a room
    }
}