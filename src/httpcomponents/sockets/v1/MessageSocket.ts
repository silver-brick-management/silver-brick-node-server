// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.text', which is part of this source code package.
//
// *********************************************************

// Local modules
import * as firebaseAdmin from "firebase-admin";
import { Constants } from "../../../common/Constants";
import { UnauthorizedError } from "../../../common/Error";
import { IJwtUser } from "../../../interfaces/IAuth";
import { IConfigContext } from "../../../interfaces/IConfigContext";

import { JoinChannelRoom, LeaveChannelRoom } from "../SocketBase";
import { IServerResponseMessage, IServerResponseGetMessageHistory, IServerResponseAddChat, AddChatHelper, NewChatRoomHelper, IServerResponseNewRoom, UserChatRoom, IServerResponseGetChatRooms, MessageParticipant } from "../../../shared/ResponseTypes";
import { UserAdminApiInternal } from "../../../api/UserAdminApiInternal";
import { IMessage, IUser, INotificationData, NotificationType } from "../../../shared/SilverBrickTypes";
import { MessageApiInternal } from "../../../api/MessageApiInternal";
import { PushApiInternal } from "../../../api/PushApiInternal";
import { SilverBrickPushType } from "../../../common/ServerInternalTypes";

export enum MessageActions {
    GET_MESSAGE_HISTORY,
    GET_MESSAGE_HISTORY_SUCCESS,
    ADD_MESSAGE,
    ADD_MESSAGE_SUCCESS,
    NEW_CHAT_ROOM,
    NEW_CHAT_ROOM_SUCCESS,
    NEW_CHAT_ROOM_FAIL,
    GET_CHAT_ROOMS,
    GET_CHAT_ROOMS_SUCCESS
}

export class MessagesV1Socket {
    static SetupSockets(socketioServer: SocketIO.Server, configContext: IConfigContext): void {
        // Defining a message channel
        let messageChannel: SocketIO.Namespace = socketioServer.of(Constants.SOCKET_MESSAGES_CHANNEL);

        messageChannel.on(Constants.SOCKET_CONNECT, (socket: SocketIO.Socket) => {
            if (socket.client.isAuthenticated) {
                // Ack to client that everything is OK
                const responseObject: IServerResponseMessage = {
                    success: true,
                    message: Constants.SOCKET_AUTHENTICATED
                };
                socket.emit(Constants.SOCKET_AUTHENTICATED, responseObject);
                socket.client.roomID = null;
                /*
                 * BASIC CHANNEL SETUP
                 * Pull out user object for auth use in this function
                 */

                // Extract user object
                const user: IJwtUser = socket.client.user;

                /*
                 * JOIN LISTENER
                 * Adds socket user to a room in the channel
                 */
                socket.on(Constants.SOCKET_JOIN, async (room: string = null) => {
                    LeaveChannelRoom(socket, Constants.SOCKET_MESSAGES_CHANNEL);
                    const roomID: string = <string>room;
                    try {
                        let messageHistory: IMessage[] = [];
                        if (roomID) {
                            socket.client.roomID = roomID;
                            messageHistory = await MessageApiInternal.GetMessageHistory(roomID);
                        }
                        // Set default pagination values
                        let newCursor = null;
                        let hasMoreMessages = null;
                        // Calculate pagination params
                        if (null != messageHistory && messageHistory.length > 0) {
                            if (messageHistory.length === Constants.NUM_PAGINATION_LIMIT + Constants.NUM_LIMIT_BUFFER_ONE) {
                                // There is more messages to be loaded. Return a cursor to get more messages
                                // Remove first message from the messageHistory array
                                // Save key of first message in cursor
                                const firstMessage: IMessage = messageHistory.shift();
                                newCursor = firstMessage.id;
                                hasMoreMessages = true;
                            } else {
                                // There is no more message to load,
                                // cursor remains null, has more messages goes to false
                                hasMoreMessages = false;
                            }
                        }
                        const responseObject: IServerResponseGetMessageHistory = {
                            success: true,
                            data: {
                                hasMoreMessages: hasMoreMessages,
                                messages: messageHistory,
                                cursor: newCursor,
                                isInitialLoad: true
                            }
                        };
                        // Returns true if a room was joined and false if the user is asking to join a room they are already in
                        const didJoinRoom = JoinChannelRoom(socket, Constants.SOCKET_MESSAGES_CHANNEL, `${roomID}`);
                        if (didJoinRoom) {
                            socket.emit(Constants.SOCKET_JOINED, responseObject);
                        } else {
                            this.MessagesSocketErrorHandler(socket, "JoinDupe");
                        }
                    } catch (error) {
                        this.MessagesSocketErrorHandler(socket, "JoinError", error);
                    }
                });

                socket.on(MessageActions[MessageActions.GET_CHAT_ROOMS], async () => {
                    const user: IJwtUser = socket.client.user;
                    try {
                        console.log(`Messages Channel: On GET_CHAT_ROOMS`);
                        const rooms: UserChatRoom[] = await MessageApiInternal.GetChatRooms(user.uid);
                        console.log("Rooms ", rooms);
                        // Set default pagination values
                        const responseObject: IServerResponseGetChatRooms = {
                            success: true,
                            data: rooms
                        };

                        messageChannel.in(`${user.uid}`).emit(MessageActions[MessageActions.GET_CHAT_ROOMS_SUCCESS], responseObject);
                    } catch (error) {
                        this.MessagesSocketErrorHandler(socket, "GetError", error);
                    }
                });

                socket.on(MessageActions[MessageActions.GET_MESSAGE_HISTORY], async (room: string, lastCursor: string) => {
                    const cursor: string = <string>lastCursor || null;
                    const roomID: string = (null != socket.client.roomID) ? socket.client.roomID : room;
                    if (!roomID) {
                        this.MessagesSocketErrorHandler(socket, "GetError");
                    } else {
                    }
                    try {
                        const messageHistory: IMessage[] = await MessageApiInternal.GetMessageHistory(roomID, cursor);
                        // Set default pagination values
                        let newCursor = null;
                        console.log(`Messages Channel: On GET_MESSAGE_HISTORY`, roomID);

                        let hasMoreMessages = null;
                        // Calculate pagination params
                        if (messageHistory.length === Constants.NUM_PAGINATION_LIMIT + Constants.NUM_LIMIT_BUFFER_ONE) {
                            // There is more messages to be loaded. Return a cursor to get more messages
                            // Remove first message from the messageHistory array
                            // Save key of first message in cursor
                            const firstMessage: IMessage = messageHistory.shift();
                            newCursor = firstMessage.id;
                            hasMoreMessages = true;
                        } else {
                            // There is no more message to load,
                            // cursor remains null, has more messages goes to false
                            hasMoreMessages = false;
                        }
                        const responseObject: IServerResponseGetMessageHistory = {
                            success: true,
                            data: {
                                hasMoreMessages: hasMoreMessages,
                                messages: messageHistory,
                                cursor: newCursor,
                                isInitialLoad: false
                            }
                        };
                        messageChannel.in(`${user.uid}`).emit(MessageActions[MessageActions.GET_MESSAGE_HISTORY_SUCCESS], responseObject);
                    } catch (error) {
                        this.MessagesSocketErrorHandler(socket, "GetError", error);
                    }
                });

                socket.on(MessageActions[MessageActions.NEW_CHAT_ROOM], async (helper: NewChatRoomHelper) => {
                    const messageHelper: NewChatRoomHelper = <NewChatRoomHelper>helper;
                    if (!messageHelper) {
                        this.MessagesSocketErrorHandler(socket, "AddError");
                    } else {
                        // All params exist, continue.
                    }
                    try {
                        // let match: MessageParticipant = helper.participants.find((user: MessageParticipant) => {
                        //     return user.uid === Constants.LEO_UID;
                        // });
                        // if (null == match) {
                        //     const users: IUser = await UserAdminApiInternal.GetUser(Constants.LEO_UID);
                        //     let messagePart: MessageParticipant = {
                        //         photoURL: users.photoURL,
                        //         uid: users.uid,
                        //         authour: null != users.firstName && users.firstName !== "" ? `${users.firstName} ${users.lastName}` : users.email,
                        //         role: users.role
                        //     };
                        //     helper.participants.push(messagePart);
                        // }
                        console.log(`Messages Channel: On NEW_CHAT_ROOM`);
                        const newHelper: NewChatRoomHelper = await MessageApiInternal.NewChatRoom(messageHelper);
                        const responseObject: IServerResponseNewRoom = {
                            success: true,
                            data: newHelper
                        };
                        for (let nextUser of helper.participants) {
                            if (nextUser.uid !== helper.message.authorID) {
                                await PushApiInternal.SendMessagePayload(nextUser.uid, helper.message.authorID, helper.message.authour, helper.message.type === "photo" ? `${helper.message.authour} added a photo` : helper.message.text, newHelper.roomID, helper.message.type, user.org.id, user.buildings[0].id, SilverBrickPushType.NEW_MESSAGE);
                            }
                        }
                        let message: string = "";
                        message = `${ user.customerName } started a new chat`;
                        const notificationData: INotificationData = {
                            authorName: helper.message.authour,
                            title: `New chat`,
                            type: NotificationType.CHAT_ROOM,
                            date: firebaseAdmin.database.ServerValue.TIMESTAMP,
                            message: message,
                            authorID: user.uid
                        };
                        PushApiInternal.AddNotification(user.org.id, user.buildings[0].id, notificationData);
                        messageChannel.in(`${newHelper.message.authorID}`).emit(MessageActions[MessageActions.NEW_CHAT_ROOM_SUCCESS], responseObject);
                    } catch (error) {
                        this.MessagesSocketErrorHandler(socket, "GetError", error);
                    }
                });

                socket.on(MessageActions[MessageActions.ADD_MESSAGE], async (helper: AddChatHelper) => {
                    const messageHelper: AddChatHelper = <AddChatHelper>helper;
                    const roomID: string = (null != socket.client.roomID) ? socket.client.roomID : helper.roomID;
                    if (!messageHelper) {
                        this.MessagesSocketErrorHandler(socket, "AddError");
                    } else {
                        // All params exist, continue.
                    }
                    try {
                        // console.log("ADD_MESSAGE", helper.participants);
                        // helper.participants.push(helper.message.authorID);
                        console.log(`Messages Channel: On ADD_MESSAGE`, helper);
                        const newHelper: AddChatHelper = await MessageApiInternal.AddMessage(messageHelper, roomID);
                        const responseObject: IServerResponseAddChat = {
                            success: true,
                            data: newHelper
                        };
                        let messageParticipants: MessageParticipant[] = await MessageApiInternal.GetRoomUsers(roomID, helper.authorID);
                        for (let nextUser of messageParticipants) {
                            if (nextUser.uid !== helper.message.authorID) {
                                await PushApiInternal.SendMessagePayload(nextUser.uid, helper.authorID, helper.message.authour, helper.message.type === "photo" ? `${helper.message.authour} added a photo` : helper.message.text, roomID, helper.message.type, user.org.id, user.buildings[0].id, SilverBrickPushType.NEW_MESSAGE);
                            }
                        }
                        messageChannel.in(`${roomID}`).emit(MessageActions[MessageActions.ADD_MESSAGE_SUCCESS], responseObject);
                    } catch (error) {
                        this.MessagesSocketErrorHandler(socket, "GetError", error);
                    }
                });

                /*
                 * LEAVE LISTENER
                 * User has requested to leave a room
                 */
                socket.on(Constants.SOCKET_LEAVE, (buildingID: string) => {
                    // console.log(`Message Channel: Request to leave ${user.uid}`);
                    // Remove user from a room
                    LeaveChannelRoom(socket, Constants.SOCKET_MESSAGES_CHANNEL);
                });

                /*
                 * DISCONNECT LISTENER
                 * When a user leaves, this is what to do
                 * we should leave all the room the user joined
                 */
                socket.on(Constants.SOCKET_DISCONNECT, () => {
                    // console.log(`Message Channel: On disconnect ${user.uid}`);
                    // Remove user from rooms
                    LeaveChannelRoom(socket, Constants.SOCKET_MESSAGES_CHANNEL);
                });
            } else {
                // Every connection should start with the default namespace and authenticate there
                // if someone tries to connect to this first without authorizing. Disconnect immediately
                console.log("Message Channel: Disconnecting socket ", socket.id);
                socket.disconnect(true);
                return;
            }
        });
    }

    static MessagesSocketErrorHandler(socket: SocketIO.Socket, type: string, error: Error = null): void {
        let event: string = null;
        let description: string = null;

        switch (type) {
            case "JoinDupe": {
                event = Constants.SOCKET_JOIN;
                description = "User already joined this socket!";
                break;
            }

            case "JoinError": {
                event = Constants.SOCKET_JOIN;
                description = "Error joining this message channel";
                break;
            }

            case "GetError": {
                event = Constants.SOCKET_ERROR;
                description = "Unable to get messages";
                break;
            }

            case "AddError": {
                event = Constants.SOCKET_ERROR;
                description = "Unable to add message";
                break;
            }

            case "JoinError": {
                event = Constants.SOCKET_ERROR;
                description = "Unable to join channel";
                break;
            }

            default: {
                event = Constants.SOCKET_ERROR;
                type = "UnauthorizedError";
                description = "Not authorized";
                break;
            }
        }

        const responseObject: IServerResponseMessage = {
            success: false,
            message: type,
            description: description
        };
        socket.emit(event, responseObject);
        // TODO: Log error
        if (!error) {
            error = new UnauthorizedError(type, { message: description });
        } else {
            // Error object was passed in
            // Use that instead
        }

        console.log(`Messages Channel: Error: ${error}`);
        console.log("Messages Channel: Disconnecting socket ", socket.id);
        // True closes the connection completel.
        // Set back to true once i figure out how to stop the client
        // From reconnecting
        socket.disconnect(true);
        return;
    }
}

