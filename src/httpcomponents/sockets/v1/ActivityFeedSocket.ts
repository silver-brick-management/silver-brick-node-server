// // *********************************************************
// //
// // This file is subject to the terms and conditions defined in
// // file 'LICENSE.txt', which is part of this source code package.
// //
// // *********************************************************

// // Node modules
// import * as socketio from "socket.io";

// // Local modules
// import { IJwtUser } from "../../../interfaces/IAuth";
// import { Constants } from "../../../common/Constants";
// import { UnauthorizedError } from "../../../common/Error";
// import { IConfigContext } from "../../../interfaces/IConfigContext";
// import { JoinError } from "../../../interfaces/IError";

// import { ServiceApiInternal } from "../../../api/ServiceApiInternal";
// import { MessageApiInternal } from "../../../api/MessageApiInternal";
// import { UserApiInternal } from "../../../api/UserApiInternal";

// import { ISilverBrickUser, ITaskFeed, ITask } from "../../../shared/SilverBrickTypes";
// import { GlobalSocketErrorHandler, JoinChannelRoom, LeaveChannelRoom } from "../SocketBase";
// import {
//     IServerResponseMessage,
//     IServerResponseGetTaskHistory
// } from "../../../shared/ResponseTypes";
// import { ITaskFeedJoin } from "../../../shared/IParams";

// export enum ActivityFeedActions {
//     GET_TASK_ACTIVITY,
//     NEW_ACTIVITY_FEED,
//     NEW_ACTIVITY_FEED_SUCCESS,
//     NEW_ACTIVITY_FEED_FAIL,
// }

// export class ActivityFeedV1Sockets {
//     static SetupSockets(socketioServer: SocketIO.Server, configContext: IConfigContext): void {
//         // Defining a chat channel
//         let activityFeedChannel: SocketIO.Namespace = socketioServer.of(Constants.SOCKET_ACTIVITY_FEED_CHANNEL);

//         activityFeedChannel.on(Constants.SOCKET_CONNECT, (socket: SocketIO.Socket) => {
//             if (socket.client.isAuthenticated) {
//                 // Give the client 5 seconds to join a room
//                 const responseObject: IServerResponseMessage = {
//                     success: true,
//                     message: Constants.SOCKET_AUTHENTICATED
//                 };
//                 socket.emit(Constants.SOCKET_AUTHENTICATED, responseObject);


//                 socket.client.buildingID = null;
//                 socket.client.warnableID = null;

//                 const user: IJwtUser = socket.client.user;

//                 /*
//                 * JOIN LISTENER
//                 * Adds socket user to a room in the channel
//                 * Room = ORG + BUILDING + WARNABLE
//                 * Succesfull join returns object containing checkin objects
//                 */
//                 socket.on(Constants.SOCKET_JOIN, async (joinParam: IWarnableActivityFeedJoin) => {
//                     LeaveChannelRoom(socket, Constants.SOCKET_ACTIVITY_FEED_CHANNEL);

//                     // Remove user from rooms in case it failed in another call
//                     // Currently, a user should only ever be in one room (exception is warnable socket)
//                     const buildingID: string = joinParam.buildingID;
//                     const warnableID: string = joinParam.warnableID;

//                     if (!buildingID || !warnableID || !joinParam.buildingID || !joinParam.warnableID) {
//                         let error: JoinError = {
//                             Name: "JoinParam",
//                             Code: "Join Error",
//                             Message: "Param Missing -> buildingID " + buildingID + " warnableID " + warnableID + " joinParam.buildingID " + joinParam.buildingID + " joinParam.warnableID " + joinParam.warnableID
//                         };
//                         this.ActivitSocketErrorHandler(socket, "JoinError", error);
//                     }
//                     else {
//                         // All params there
//                     }

//                     try {
//                         // TODO. fix client also when this is done
//                         socket.client.buildingID = buildingID;
//                         socket.client.warnableID = warnableID;

//                         const activityFeed: IWarnableActivityFeedSummary[] = await WarnableApiInternal.GetWarnableActivityFeed(
//                             user.org.id,
//                             buildingID,
//                             warnableID
//                             );
//                         const responseObject: IServerResponseWarnableActivityFeed = {
//                             success: true,
//                             data: activityFeed
//                         };

//                         // Returns true if a room was joined and false if the user is asking to join a room they are already in
//                         const didJoinRoom = JoinChannelRoom(socket, Constants.SOCKET_ACTIVITY_FEED_CHANNEL, `${user.org.id}_${buildingID}_${warnableID}`);

//                         if (didJoinRoom) {
//                             // Pass back the checkin object so it can be populated
//                             socket.emit(Constants.SOCKET_JOINED, responseObject);
//                         }
//                         else {
//                             GlobalSocketErrorHandler(socket, "JoinDupe");
//                         }
//                     }
//                     catch (error) {
//                         console.log("Error", error);
//                         this.ActivitSocketErrorHandler(socket, error);
//                     }
//                 });

//                 socket.on((ActivityFeedActions[ActivityFeedActions.NEW_ACTIVITY_FEED]), async (activityFeed: IWarnableActivityFeedSummary) => {
//                     const buildingID: string = <string>activityFeed.buildingID;
//                     const warnableID: string = <string>activityFeed.warnableID;
//                     const addedActivityFeed: IWarnableActivityFeedSummary = <IWarnableActivityFeedSummary>activityFeed;
//                     console.log("Activity Feed Socket: NEW_ACTIVITY_FEED ", activityFeed, buildingID, warnableID, addedActivityFeed);
//                     if (!buildingID || !warnableID) {
//                         GlobalSocketErrorHandler(socket, Constants.ERROR_MESSAGE_BAD_DATA);
//                     }
//                     if (!activityFeed.authorID || !activityFeed.authorName) {
//                         activityFeed.authorID = user.uid;
//                         let userInfo: IWarnableUser = await UserApiInternal.GetUserInfo(user.uid);
//                         activityFeed.authorName = userInfo.firstName + " " + userInfo.lastName;
//                     }
//                     try {
//                         await WarnableApiInternal.NewActivityFeed(user.org.id, buildingID, warnableID, addedActivityFeed);
//                         if (addedActivityFeed.photoURL) {
//                             let activityImage: string = await ChatApiInternal.GetChatMessageImage(addedActivityFeed.photoURL);
//                             addedActivityFeed.photoURL = activityImage;
//                         }
//                         const responseObject: IServerResponseAddActivityFeedSuccess = {
//                             success: true,
//                             data: addedActivityFeed
//                         };
//                         // Pass back the chat history so it can be populated
//                         activityFeedChannel.in(`${user.org.id}_${buildingID}_${warnableID}`).emit(ActivityFeedActions[ActivityFeedActions.NEW_ACTIVITY_FEED_SUCCESS], responseObject);
//                         console.log("NEW_ACTIVITY_FEED_SUCCESS " + ` ${user.org.id}_${buildingID}_${warnableID}`, responseObject);
//                     }
//                     catch (error) {
//                         this.ActivitSocketErrorHandler(socket, "ActivityError", error);
//                     }
//                 });

//                 /*
//                 * LEAVE LISTENER
//                 * User has requested to leave a room
//                 */
//                 socket.on(Constants.SOCKET_LEAVE, () => {
//                     if (socket.client.isAuthenticated) {
//                         socket.client.buildingID = null;
//                         socket.client.warnableID = null;
//                     }
//                     else {
//                         // Nothing
//                     }
//                     LeaveChannelRoom(socket, Constants.SOCKET_ACTIVITY_FEED_CHANNEL);
//                 });

//                 /*
//                 * DISCONNECT LISTENER
//                 * When a user leaves, this is what to do
//                 * we should leave all the room the user joined
//                 */
//                 socket.on(Constants.SOCKET_DISCONNECT, () => {
//                     if (socket.client.isAuthenticated) {
//                         delete (socket.client.buildingID);
//                         delete (socket.client.warnableID);
//                     }
//                     else {
//                         // Nothing
//                     }
//                     LeaveChannelRoom(socket, Constants.SOCKET_ACTIVITY_FEED_CHANNEL);
//                 });
//             } else {

//                 const responseObject: IServerResponseMessage = {
//                     success: false,
//                     message: Constants.SOCKET_UNAUTHORIZED
//                 };
//                 socket.emit(Constants.SOCKET_UNAUTHORIZED, responseObject);
//                 // Every connection should start with the default namespace and authenticate there
//                 // if someone tries to connect to this first without authorizing. Disconnect immediately
//                 socket.disconnect(true);

//             }
//         });
//     }

//     static ActivitSocketErrorHandler(socket: SocketIO.Socket, type: string, error: any = null): void {
//         let event: string = null;
//         let description: string = null;

//         switch (type) {
//             case "ActivityError":
//             {
//                 event = Constants.SOCKET_ERROR;
//                 description = "Unable to get users to check in -->" + error.message;
//                 break;
//             }

//             case "UpdateStatusError":
//             {
//                 event = Constants.SOCKET_ERROR;
//                 description = "Unable to update status -->" + error.message;
//                 break;
//             }

//             case "AddError":
//             {
//                 event = Constants.SOCKET_ERROR;
//                 description = "Unable to add activity feed";
//                 break;
//             }

//             case "JoinError":
//             {
//                 event = Constants.SOCKET_ERROR;
//                 description = "Unable to join room -->" + error.Message;
//                 break;
//             }

//             case "WarnableClosed":
//             {
//                 event = Constants.SOCKET_ERROR;
//                 description = "This Warnable is not accepting Check In Changes because it is not active.";
//                 break;
//             }

//             default:
//             {
//                 event = Constants.SOCKET_ERROR;
//                 type = "UnauthorizedError";
//                 description = "Not authorized.";
//                 break;
//             }

//         }

//         const responseObject: IServerResponseMessage = {
//             success: false,
//             message: type,
//             description: description
//         };

//         socket.emit(event, responseObject);
//         // TODO: Log error
//         if (!error) {
//             error = new UnauthorizedError(type, { message: description });
//         }
//         else {
//             // Error object was passed in
//             // Use that instead
//         }

//         console.log(`Activity Channel: Error: ${error}`);
//         console.log("Activity Channel: Disconnecting socket ", socket.id);
//         // True closes the connection completel.
//         // Set back to true once i figure out how to stop the client
//         // From reconnecting
//         socket.disconnect(true);
//     }
// }