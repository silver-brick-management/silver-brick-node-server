// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// ## Org API object
//
// HTTP Verb  Route                           Description
//
// SECURE: All API routes should
// be secured by the authGuard (JWT)
// GET        /dba/v1/orgs             Get all of the orgs in the system
// GET        /dba/v1/orgs/:org_id    Get a single org item by org id
// POST       /dba/v1/orgs             Create a single org (Possible to do a batch request)
// PUT        /dba/v1/orgs/:org_id    Update a org with new info
// *********************************************************

// Node modules
import { parse, format } from "url";
import { RequestHandler } from "express-jwt";
import { Application, Router } from "express";
import * as firebaseAdmin from "firebase-admin";

// Local modules
import { HTTPUtil } from "../../../../common/HttpUtil";
import { AuthUtil } from "../../../../common/AuthUtil";
import { Constants } from "../../../../common/Constants";
import { NormalizedUser } from "../../../../factories/users/NormalizedUser"; // Replaced with interface for testing
import { IUser } from "../../../../shared/SilverBrickTypes";
import { IJwtUser } from "../../../../interfaces/IAuth";
import { SilverBrickAdminApiInternal } from "../../../../api/SilverBrickAdminApiInternal";
import { MessageApiInternal } from "../../../../api/MessageApiInternal";
import { ServiceApiInternal } from "../../../../api/ServiceApiInternal";
import { SilverBrickPushType } from "../../../../common/ServerInternalTypes";
import { TwilioWrapper } from "../../../../wrappers/TwilioWrapper";
import {
    IServerResponseMessage,
    IServerResponseGetAllOrgs,
    IServerResponseCreateOrg,
    IServerResponseGetOrgInfo,
    IServerResponseAssignBooking,
    IServerResponseGetNotifications,
    IServerResponseGetTasks,
    IServerResponseAssignTask,
    IServerResponseAddUpdateAssignTask,
    IServerResponseCreateBldg,
    UserChatRoom,
    IServerResponseGetVendors,
    IServerResponseAddOrUpdateVendor,
    IServerResponseGetChatRooms
} from "../../../../shared/ResponseTypes";
import { IOrgBasic, IVendor, IStaffProfile, NotificationType, ITask, INotificationData, IOrgInfo, IBuilding } from "../../../../shared/SilverBrickTypes";
import { PushApiInternal } from "../../../../api/PushApiInternal";

export function SetAdminCPV1OrgRoutes(
    app: Application,
    router: Router,
    authGuard: RequestHandler) {

    router.route("/messages/rooms/")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const chatRooms: UserChatRoom[] = await MessageApiInternal.GetChatRoomsDashboard();
                const responseObject: IServerResponseGetChatRooms = {
                    success: true,
                    data: chatRooms
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /messages/rooms/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });


    router.route("/bookings/all")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const bookings: ITask[] = await ServiceApiInternal.GetSchedule(orgID, buildingID);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /bookings/all/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/tenantBookings")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const tenantID: string = <string> req.query.tenant_id || null;
            if (!user || !buildingID || !orgID || !tenantID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const bookings: ITask[] = await ServiceApiInternal.GetTenantSchedule(orgID, buildingID, tenantID);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /bookings/tenant/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/staffBookings")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const staffID: string = <string> req.query.staff_id || null;
            if (!user || !buildingID || !orgID || !staffID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const bookings: ITask[] = await ServiceApiInternal.GetStaffSchedule(orgID, buildingID, staffID);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /bookings/staff/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/notifications")
        .put(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const notifications: INotificationData[] = await PushApiInternal.GetNotifications(orgID, buildingID);
                const responseObject: IServerResponseGetNotifications = {
                    success: true,
                    data: notifications
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /schedules/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });
    router.route("/vendors")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const vendors: IVendor[] = await SilverBrickAdminApiInternal.GetVendors(orgID, buildingID);
                const responseObject: IServerResponseGetVendors = {
                    success: true,
                    data: vendors
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /vendors/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });
}