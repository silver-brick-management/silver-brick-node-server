// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// ## User API object
//
// HTTP Verb  Route                           Description
//
// SECURE: All API routes should
// be secured by the authGuard (JWT)
// GET        /user-cp/v1/users/:user_id    Get a single user item by user id
// POST       /user-cp/v1/users             Create a single user (Possible to do a batch request)
// DELETE     /user-cp/v1/users/:user_id    Delete a single user (Possible to do a batch request)
// PUT        /user-cp/v1/users/:user_id    Update a user with new info
// *********************************************************

// Node modules
import { parse, format } from "url";
import { RequestHandler } from "express-jwt";
import { Application, Router } from "express";

// Local modules
import { HTTPUtil } from "../../../../common/HttpUtil";
import { Constants } from "../../../../common/Constants";
import { NormalizedUser } from "../../../../factories/users/NormalizedUser"; // Replaced with interface for testing
import { IBuildingInfo, ITask, IUser } from "../../../../shared/SilverBrickTypes";
import { SilverBrickAdminApiInternal } from "../../../../api/SilverBrickAdminApiInternal";
import { UserAdminApiInternal } from "../../../../api/UserAdminApiInternal";
import { ServiceApiInternal } from "../../../../api/ServiceApiInternal";

import { UserApiInternal } from "../../../../api/UserApiInternal";
import { MessageApiInternal } from "../../../../api/MessageApiInternal";
import { IJwtUser } from "../../../../interfaces/IAuth";
import {
    IServerResponseMessage,
    IServerResponseGetUsers,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponsePatchOrDeleteUser,
    IServerResponseGetBuildingUsers,
    IServerResponseGetBuildingInfo,
    IServerResponseGetTasks,
    UserChatRoom,
    IServerResponseGetChatRooms
} from "../../../../shared/ResponseTypes";

export function SetAdminCPV1UserRoutes(
    app: Application,
    router: Router,
    authGuard: RequestHandler) {

    // Secure routes for the user APIs
    router.route("/users")
        // #Create a new user
        // Accessed at POST http://localhost:*PORT*/api/v1/user-cp/users
        .post(authGuard, async (req, res) => {
            const userData: IUser = <IUser> req.body.data || null;

            try {
                // Create user
                const userObj: IUser = await UserAdminApiInternal.CreateUser(userData);
                const responseObject: IServerResponseAddOrUpdateOrGetUser = {
                    success: true,
                    data: userObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`POST /users/:id route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    // NOTE: Only GET utilizes email. Other resuests must be UID only
    router.route("/users/:uid_or_email")
        // #Get a user by ID or Email
        // Accessed at GET http://localhost:*PORT*/api/v1/user-cp/users/:user_id
        .get(authGuard, async (req, res) => {
            const uidOrEmail: string = <string> req.params.uid_or_email || null;

            if (!uidOrEmail) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            try {
                const userObj: IUser = await UserAdminApiInternal.GetUser(uidOrEmail);

                const responseObject: IServerResponseAddOrUpdateOrGetUser = {
                    success: true,
                    data: userObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /users/:uid_or_email route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })

        // #Update a user by ID and user object
        // Accessed at PUT http://localhost:*PORT*/api/v1/user-cp/users/:user_id
        .put(authGuard, async (req, res) => {
            const uid: string = <string> req.params.uid_or_email || null;
            if (!uid) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            const userData: IUser = <IUser> req.body.data;

            // Extra validation to ensure object matches param
            if (uid.toLowerCase() !== userData.uid.toLowerCase()) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISMATCH_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            try {
                // Update user
                const userObj: IUser = await UserAdminApiInternal.UpdateUser(userData);

                const responseObject: IServerResponseAddOrUpdateOrGetUser = {
                    success: true,
                    data: userObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`POST /users/:uid route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })

        // #Activate a user by ID
        // Accessed at PATCH http://localhost:*PORT*/api/v1/user-cp/users/:user_id
        .patch(authGuard, async (req, res) => {
            const uid: string = <string> req.params.uid_or_email || null;

            if (!uid) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            const patchAction: string = <string> req.body.data;

            try {
                let responseState: boolean = true;
                let responseMessage: string = Constants.SUCCESS_MESSAGE_ACTIVATE;
                let responseStatus: number = 200;
                let responseDesctiption: string = undefined;

                if (patchAction.toLowerCase() === Constants.PATCH_ACTIVATE_ACTION.toLowerCase()) {
                    await UserAdminApiInternal.ActivateUser(uid);
                }
                else if (patchAction.toLowerCase() === Constants.PATCH_DEACTIVATE_ACTION.toLowerCase()) {
                    await UserAdminApiInternal.DeactivateUser(uid);
                }
                else {
                    responseState = false;
                    responseMessage = Constants.ERROR_MESSAGE_BAD_DATA;
                    responseDesctiption = Constants.ERROR_DESCRIPTION_BAD_PATCH_ACTION;
                    responseStatus = 400;
                }

                const responseObject: IServerResponsePatchOrDeleteUser = {
                    success: responseState,
                    data: responseMessage,
                    description: responseDesctiption
                };
                // Set HTTP status code `200 successful`
                res.status(responseStatus);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`PATCH /users/:uid route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })

        // #Deactivate a user by ID
        // Accessed at DELETE http://localhost:*PORT*/api/v1/user-cp/users/:user_id
        .delete(authGuard, async (req, res) => {
            const uid: string = <string> req.params.uid_or_email || null;

            if (!uid) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            try {
                await UserAdminApiInternal.DeleteUser(uid);

                const responseObject: IServerResponsePatchOrDeleteUser = {
                    success: true,
                    data: Constants.SUCCESS_MESSAGE_DELETE
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`DELETE /users/:uid route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/buildingusers")
        // #Get all users under a building (supports pagination)
        // Accessed at GET http://localhost:*PORT*/api/v1/user-cp/buildingusers
        .get(authGuard, async (req, res) => {
            const buildingID: string = <string> req.query.building_id || null;
            const orgID: string = <string> req.query.org_id || null;
            console.log("buildingusers", buildingID, orgID);
            if (!buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const userObjArray: IUser[] = await UserAdminApiInternal.GetAllNonOrgAdminsUnderBuilding(orgID, buildingID);
                const responseObject: IServerResponseGetBuildingUsers = {
                    success: true,
                    data: userObjArray
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /users route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/buildingtenants")
        // #Get all users under a building (supports pagination)
        // Accessed at GET http://localhost:*PORT*/api/v1/user-cp/buildingusers
        .get(authGuard, async (req, res) => {
            const buildingID: string = <string> req.query.building_id || null;
            const orgID: string = <string> req.query.org_id || null;
            console.log("buildingtenants", buildingID, orgID);
            if (!buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const userObjArray: IUser[] = await UserApiInternal.GetAllTenants(orgID, buildingID);
                const responseObject: IServerResponseGetBuildingUsers = {
                    success: true,
                    data: userObjArray
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /users route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/buildinginfo")
        .get(authGuard, async (req, res) => {
            const buildingID: string = <string> req.query.building_id || null;
            const orgID: string = <string> req.query.org_id || null;
            console.log("buildinginfo", buildingID, orgID);
            if (!buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const buildingInfo: IBuildingInfo = await SilverBrickAdminApiInternal.GetBuildingInfo(orgID, buildingID);
                const responseObject: IServerResponseGetBuildingInfo = {
                    success: true,
                    data: buildingInfo
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /buildinginfo route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const buildingID: string = <string> req.query.building_id || null;
            const orgID: string = <string> req.query.org_id || null;
            const buildingInfo: IBuildingInfo = <IBuildingInfo> req.body.data || null;
            console.log("save buildinginfo", buildingID, orgID, buildingInfo);
            if (!buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const buildin: IBuildingInfo = await SilverBrickAdminApiInternal.SetBuildingInfo(orgID, buildingID, buildingInfo);
                const responseObject: IServerResponseGetBuildingInfo = {
                    success: true,
                    data: buildin
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /buildinginfo route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/tenantBookings")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const tenantID: string = <string> req.query.tenant_id || null;
            if (!user || !buildingID || !orgID || !tenantID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const bookings: ITask[] = await ServiceApiInternal.GetTenantSchedule(orgID, buildingID, tenantID);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /bookings/tenant/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });
}