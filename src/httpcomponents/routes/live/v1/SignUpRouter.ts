// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// ## User API object
//
// HTTP Verb  Route                           Description
//
// SECURE: All API routes should
// be secured by the authGuard (JWT)
// GET        /live/v1/users             Get all of the users in the system
// GET        /live/v1/users/:user_id    Get a single user item by user id
// *********************************************************

// Node modules
import { RequestHandler } from "express-jwt";
import { Application, Router } from "express";
import { IShippingInformation, customers, ICard, orders } from "stripe";
import * as firebaseAdmin from "firebase-admin";

// Local modules
import { IJwtUser } from "../../../../interfaces/IAuth";
import { Constants } from "../../../../common/Constants";
import { UserAdminApiInternal } from "../../../../api/UserAdminApiInternal";
import { SilverBrickAdminApiInternal } from "../../../../api/SilverBrickAdminApiInternal";
import { ServiceApiInternal } from "../../../../api/ServiceApiInternal";
import { UserApiInternal } from "../../../../api/UserApiInternal";
import { FirebaseAdminWrapper } from "../../../../wrappers/FirebaseAdminWrapper";
import { MailGunWrapper } from "../../../../wrappers/MailGunWrapper";
import { PushApiInternal } from "../../../../api/PushApiInternal";
import { EmailTemplateFactory, MailType, ISignUpEmailTemplateParams } from "../../../../factories/EmailTemplateFactory";
import { IEmailPredicate } from "../../../../common/ServerInternalTypes";
import { NormalizedUser } from "../../../../factories/users/NormalizedUser"; // Replaced with interface for testing
import { AuthUtil } from "../../../../common/AuthUtil";
import { IConfigContext } from "../../../../interfaces/IConfigContext";

import {
    ISilverBrickUser,
    IUser,
    IUserBuilding,
    IBuildingInfo,
    IBuildingSearch,
    IUserOrg,
    IOrgInfo,
    IPreferences
    } from "../../../../shared/SilverBrickTypes";
import {
    IServerResponseBuildingSearch,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponseLogin,
    IServerResponseMessage,
    IServerResponseUpdatePreferences,
    IServerResponseCheckUsername
} from "../../../../shared/ResponseTypes";
import { StripeWrapper } from "../../../../wrappers/StripeWrapper";
import { StatusOrchestrator } from "../../../../common/StatusOrchestrator";

export function SetLiveV1SignupRoutes(
    app: Application,
    router: Router,
    configContext: IConfigContext) {
    router.route("/signup")
        // #Get a user by ID
        // Accessed at GET http://localhost:*PORT*/live/v1/users/:user_id
        .post(async (req, resp) => {
            const user: IUser = <IUser>req.body || null;
            console.log("User", user);
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                resp.status(400);
                return resp.json(responseObject);
            }

            try {
                // console.log("User", user);
                let responseObject: IServerResponseAddOrUpdateOrGetUser = {
                    success: true,
                    data: null
                };
                await UserAdminApiInternal.CreateUser(user)
                .then(async (newUser: any) => {
                    if (null != newUser) {
                        console.log("Create user successful", newUser);
                        responseObject.data = newUser;
                        const newMessage: string = Constants.MESSAGE_TITLE_NEW_USER + "_" + Constants.MESSAGE_DESCRIPTION_NEW_USER;
                        // await PushApiInternal.SendNewUserPayload(newUser.uid, newMessage);
                        // Set HTTP status code `200 successful`
                        resp.status(200);
                        return resp.json(responseObject);
                    }
                })
                .catch((error) => {
                    console.log("Error", error.message);
                    const responseObject: IServerResponseMessage = {
                        success: false,
                        message: Constants.ERROR_MESSAGE_SERVER_ERROR,
                        description: error.message
                    };
                    // Set HTTP status code `400 Bad Request`
                    resp.status(500);
                    return resp.json(responseObject);
                });
            }
            catch (error) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR,
                    description: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `400 Bad Request`
                resp.status(500);
                return resp.json(responseObject);
            }
        });

    router.route("/googleSignup")
        // #Get a user by ID
        // Accessed at GET http://localhost:*PORT*/live/v1/users/:user_id
        .post(async (req, resp) => {
            const user: IUser = <IUser>req.body || null;
            console.log("User", user);
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                resp.status(400);
                return resp.json(responseObject);
            }

            try {
                // console.log("User", user);
                let responseObject: IServerResponseLogin = {
                    accessToken: null,
                    success: true,
                    data: null
                };
                await UserAdminApiInternal.CreateUserGoogleSignIn(user)
                .then(async (newUser: any) => {
                    if (null != newUser) {
                        console.log("Create user successful", newUser);
                        const newMessage: string = Constants.MESSAGE_TITLE_NEW_USER + "_" + Constants.MESSAGE_DESCRIPTION_NEW_USER;
                        // await PushApiInternal.SendNewUserPayload(newUser.uid, newMessage);
                        // Set HTTP status code `200 successful`
                        const accessToken: string = AuthUtil.GenerateJWTAccessToken(newUser, configContext.JWTLiveSecret, configContext.JWTExpire);
                        responseObject.data = newUser;
                        responseObject.accessToken = accessToken;

                        resp.status(200);
                        return resp.json(responseObject);
                    }
                })
                .catch((error) => {
                    console.log("Error", error.message);
                    const responseObject: IServerResponseMessage = {
                        success: false,
                        message: Constants.ERROR_MESSAGE_SERVER_ERROR,
                        description: error.message
                    };
                    // Set HTTP status code `400 Bad Request`
                    resp.status(500);
                    return resp.json(responseObject);
                });
            }
            catch (error) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR,
                    description: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `400 Bad Request`
                resp.status(500);
                return resp.json(responseObject);
            }
        });

    router.route("/propertyIndex")
        .put(async (req, resp) => {
            const searchStr: string = <string>req.body.data || null;
            console.log("propertyIndex", searchStr);
            if (!searchStr) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                resp.status(400);
                return resp.json(responseObject);
            }

            try {
                const buildingSearches: IBuildingSearch[] = await UserApiInternal.CheckAddress(searchStr);
                console.log("buildingSearches", buildingSearches);
                let responseObject: IServerResponseBuildingSearch = {
                    success: true,
                    data: buildingSearches
                };
                resp.status(200);
                return resp.json(responseObject);
            }
            catch (error) {
                console.log(`POST /propertyCodes route error: ${error}`);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR,
                    description: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `400 Bad Request`
                resp.status(500);
                return resp.json(responseObject);
            }
        });

    router.route("/propertyCodes")
        // #Get a user by ID
        // Accessed at GET http://localhost:*PORT*/live/v1/users/:user_id
        .put(async (req, resp) => {
            const user: IUser = <IUser>req.body || null;
            console.log("propertyCodes", user);
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                resp.status(400);
                return resp.json(responseObject);
            }

            try {
                // console.log("User", user);
                let orgBdlgID: string = await SilverBrickAdminApiInternal.GetOrgIDFromCode(String(user.propertyCodes));
                let orgID: string = orgBdlgID.split("@@", 2)[0];
                let buildingID: string = orgBdlgID.split("@@", 2)[1];
                const buildingMap: Map<string, string> = await SilverBrickAdminApiInternal.GetBuildingsInOrg(orgID);
                let buildings: IUserBuilding[] = [];
                let buildingInfo: IBuildingInfo = await SilverBrickAdminApiInternal.GetBuildingInfo(orgID, buildingID);
                if (orgID) {
                    let newBuilding: IUserBuilding = {
                        id: buildingID,
                        name: buildingInfo.name,
                        isAdmin: false
                    };
                    buildings.push(newBuilding);
                }

                const orgInfo: IOrgInfo = await SilverBrickAdminApiInternal.GetOrgInfo(orgID);
                let org: IUserOrg = {
                    id: orgID || null,
                    name: orgInfo.name || null,
                    isAdmin: false,
                    buildings: buildings
                };
                let orgsObj: any = {};
                if (org) {
                    let buildingsObj: any = {};

                    if (org.buildings) {
                        for (let building of org.buildings) {
                            let buildingObj: any = {
                                isAdmin: building.isAdmin
                            };
                            buildingsObj[building.id] = buildingObj;
                        }
                    }

                    let orgObj: any = {
                        isAdmin: org.isAdmin,
                        buildings: buildingsObj
                    };
                    orgsObj[org.id] = orgObj;
                }
                const usersPath: string = Constants.STR_USERS_FSLASH + Constants.STR_FSLASH_TENANTS_FSLASH + user.uid + Constants.STR_FSLASH + "orgs/";
                const propertyCodePath: string = Constants.STR_USERS_FSLASH + Constants.STR_FSLASH_TENANTS_FSLASH + user.uid + Constants.STR_FSLASH + "propertyCodes/";
                const hasPropertyCodePath: string = Constants.STR_USERS_FSLASH + Constants.STR_FSLASH_TENANTS_FSLASH + user.uid + Constants.STR_FSLASH + "preferences/hasPropertyCode/";
                await FirebaseAdminWrapper.GetInstance().SetValue(usersPath, orgsObj);
                await FirebaseAdminWrapper.GetInstance().SetValue(propertyCodePath, user.propertyCodes);
                await FirebaseAdminWrapper.GetInstance().SetValue(hasPropertyCodePath, true);
                user.orgs = org;
                user.preferences.hasPropertyCode = true;
                let responseObject: IServerResponseLogin = {
                    accessToken: null,
                    success: true,
                    data: user
                };
                resp.status(200);
                return resp.json(responseObject);
            }
            catch (error) {
                console.log(`POST /propertyCodes route error: ${error}`);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR,
                    description: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `400 Bad Request`
                resp.status(500);
                return resp.json(responseObject);
            }
        });

    router.route("/checkemail")
    // #Get a user by ID
    // Accessed at GET http://localhost:*PORT*/live/v1/users/:user_id
        .post(async (req, resp) => {
            const name: string = <string>req.body.data || null;
            if (!name) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `500 Server Error`
                resp.status(400);
                return resp.json(responseObject);
            }
            try {
                const exists: boolean = await UserApiInternal.CheckEmail(name);
                const responseObject: IServerResponseCheckUsername = {
                    success: true,
                    data: exists
                };
                // Set HTTP status code `200 successful`
                resp.status(200);
                return resp.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`POST /searchUserName route error: ${error}`);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                resp.status(500);
                return resp.json(responseObject);
            }
        });
}