// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// ## User API object
//
// HTTP Verb  Route                           Description
//
// SECURE: All API routes should
// be secured by the authGuard (JWT)
// PUT        /live/v1/account             Add a new Card
// *********************************************************

// Node modules
import { RequestHandler } from "express-jwt";
import { Application, Router } from "express";
import {
    customers,
    cards,
    charges,
    coupons,
    ICard,
    sources,
    IDeleteConfirmation,
    IList
} from "stripe";
import * as firebaseAdmin from "firebase-admin";

// Local modules
import { IJwtUser } from "../../../../interfaces/IAuth";
import { Constants } from "../../../../common/Constants";
import { UserApiInternal } from "../../../../api/UserApiInternal";
import { PushApiInternal } from "../../../../api/PushApiInternal";
import { StripeWrapper } from "../../../../wrappers/StripeWrapper";
import { SilverBrickAdminApiInternal } from "../../../../api/SilverBrickAdminApiInternal";
import { MessageApiInternal } from "../../../../api/MessageApiInternal";
import { ServiceApiInternal } from "../../../../api/ServiceApiInternal";
import { UserAdminApiInternal } from "../../../../api/UserAdminApiInternal";
import { SilverBrickPushType } from "../../../../common/ServerInternalTypes";
import {
    IServerResponseMessage,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponseGetAllOrgs,
    IServerResponseCreateOrg,
    IServerResponseGetOrgInfo,
    IServerResponseGetBuildingUsers,
    IServerResponseAssignBooking,
    IServerResponseCreateBldg,
    UserChatRoom,
    IServerResponseGetChatRooms,
    IServerAddPaymentCard,
    IServerDeletePaymentCard,
    IServerGetAccountInfo,
    IServerResponseGetTasks,
    IServerResponseAssignTask,
    IServerResponseAddUpdateAssignTask,
    IServerResponseGetCards
} from "../../../../shared/ResponseTypes";
import {
    IOrgBasic,
    NotificationType,
    IStaffProfile,
    IOrgInfo,
    INotificationData,
    IProfile,
    IBuilding,
    IUser,
    ITask
} from "../../../../shared/SilverBrickTypes";
import { StatusOrchestrator } from "../../../../common/StatusOrchestrator";

export function SetLiveV1AccountRoutes(
    app: Application,
    router: Router,
    authGuard: RequestHandler) {
    router.route("/account")
        .get(authGuard, async (req, res) => {
            const profile: IJwtUser = req.user;
            try {
                // Update user
                const customer: customers.ICustomer = await StripeWrapper.GetInstance().GetCustomer(profile.stripeID);

                const responseObject: IServerGetAccountInfo = {
                    success: true,
                    data: customer
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`Get /account route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const profile: IJwtUser = req.user;
            const customerUpdates: customers.ICustomerUpdateOptions = req.body || null;
            if (!customerUpdates) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `500 Server Error`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Update user

                const customer: customers.ICustomer = await StripeWrapper.GetInstance().UpdateCustomer(profile.stripeID, customerUpdates);

                const responseObject: IServerGetAccountInfo = {
                    success: true,
                    data: customer
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /account route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/profile/user/")
        .put(authGuard, async (req, res) => {
            const profile: IJwtUser = req.user;
            const userProfile:  IProfile = req.body.data || null;
            if (!userProfile) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `500 Server Error`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Update user

                let newUser = await UserApiInternal.UpdateProfile(userProfile);
                const responseObject: IServerResponseAddOrUpdateOrGetUser = {
                    success: true,
                    data: newUser
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /profile/user route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/account/cards")
        .get(authGuard, async (req, res) => {
            const profile: IJwtUser = req.user;
            if (!profile || !profile.stripeID || (profile.stripeID === "")) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `500 Server Error`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const customerCards: ICard[] = await StripeWrapper.GetInstance().GetPaymentInfo(profile.stripeID);
                const responseObject: IServerResponseGetCards = {
                    success: true,
                    data: customerCards
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /account/cards route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .post(authGuard, async (req, res) => {
            const profile: IJwtUser = req.user;
            const cardInfo: cards.ISourceCreationOptionsExtended = req.body.data || null;
            if (!cardInfo) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `500 Server Error`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Update user
                const newCardIinfo: ICard = await StripeWrapper.GetInstance().AddPaymentCard(profile.stripeID, profile.email, cardInfo);
                const responseObject: IServerAddPaymentCard = {
                    success: true,
                    data: newCardIinfo
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`POST /account/cards route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .patch(authGuard, async (req, res) => {
            const profile: IJwtUser = req.user;
            const cardInfo: string = req.body.data || null;
            if (!cardInfo) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `500 Server Error`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Update user

                const newCardIinfo: IDeleteConfirmation = await StripeWrapper.GetInstance().DeletePaymentCard(profile.stripeID, cardInfo);
                const responseObject: IServerDeletePaymentCard = {
                    success: true,
                    data: cardInfo
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`DELETE /account/cards route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    // router.route("/account/orders")
    //     .put(authGuard, async(req, res) => {
    //         const profile: IJwtUser = req.user;
    //         const orderInfo: IOrderCheckoutHelper = req.body.data || null;
    //         if (!orderInfo) {
    //             const responseObject: IServerResponseMessage = {
    //                 success: false,
    //                 message: Constants.ERROR_DESCRIPTION_MISSING_REQS
    //             };
    //             // Set HTTP status code `500 Server Error`
    //             res.status(400);
    //             return res.json(responseObject);
    //         }
    //         try {
    //             console.log("Submit Order", orderInfo);
    //             const completedOrders: IOrder = await OrderAdminApiInternal.CreateOrder(orderInfo.orders, profile.uid, profile.stripeID, profile.email
    //             , orderInfo.cardID, profile.customerName);
    //             const responseObject: IServerResponseCompletedOrder = {
    //                 success: true,
    //                 data: completedOrders
    //             };
    //             // Set HTTP status code `200 successful`
    //             res.status(200);
    //             return res.json(responseObject);
    //         }
    //         catch (error) {
    //             // TODO: Log error
    //             console.log(`PUT /account/orders route error: ${error}`);

    //             const responseObject: IServerResponseMessage = {
    //                 success: false,
    //                 message: Constants.ERROR_MESSAGE_SERVER_ERROR
    //             };
    //             // Set HTTP status code `500 Server Error`
    //             res.status(500);
    //             return res.json(responseObject);
    //         }
    //     });

    router.route("/buildings/:org_id/:building_id")
        .get(async (req, res) => {
            const buildingID: string = <string> req.params.building_id || null;
            const orgID: string = <string> req.params.org_id || null;
            try {
                // Create user
                const bldgObj: IBuilding = await SilverBrickAdminApiInternal.GetBuilding(orgID, buildingID);

                const responseObject: IServerResponseCreateBldg = {
                    success: true,
                    data: bldgObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /buildings/:org_id/:building_id route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/buildingusers")
        // #Get all users under a building (supports pagination)
        // Accessed at GET http://localhost:*PORT*/api/v1/user-cp/buildingusers
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            console.log("buildingusers", buildingID, orgID);
            if (!buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const userObjArray: IUser[] = await UserAdminApiInternal.GetAllNonOrgAdminsUnderBuilding(orgID, buildingID);
                const responseObject: IServerResponseGetBuildingUsers = {
                    success: true,
                    data: userObjArray
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /users route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/messages/rooms/")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const chatRooms: UserChatRoom[] = await MessageApiInternal.GetChatRooms(user.uid);
                const responseObject: IServerResponseGetChatRooms = {
                    success: true,
                    data: chatRooms
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /messages/rooms/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/vendorMessages/rooms/")
        .patch(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const tenantID: string = <string>req.body.data || null;
            if (!user || !buildingID || !orgID || !tenantID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const chatRooms: UserChatRoom[] = await MessageApiInternal.GetChatRooms(tenantID);
                const responseObject: IServerResponseGetChatRooms = {
                    success: true,
                    data: chatRooms
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /messages/rooms/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });


    router.route("/bookings/all")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const bookings: ITask[] = await ServiceApiInternal.GetSchedule(orgID, buildingID);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /bookings/all/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/tenantBookings")
        .post(authGuard, async (req, res) => {
            const profile: IJwtUser = req.user;
            const task: ITask = <ITask>req.body.data || null;
            try {
                // Create user
                task.updateAuthor = profile.customerName;
                task.lastUpdated = new Date(Date.now()).getTime();
                const newTask: ITask = await ServiceApiInternal.AddTask(task.orgID, task.buildingID, task.authorID, task);
                const responseObject: IServerResponseAddUpdateAssignTask = {
                    success: true,
                    data: newTask
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`POST /tenantBookings/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const profile: IJwtUser = req.user;
            const task:  ITask = req.body.data || null;
            if (!task) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `500 Server Error`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Update user
                // task.updateAuthor = profile.customerName;
                task.lastUpdated = new Date(Date.now()).getTime();
                let newTask: ITask = await ServiceApiInternal.UpdateTask(task.orgID, task.buildingID, profile.uid, task, profile.uid, profile.customerName);
                const responseObject: IServerResponseAddUpdateAssignTask = {
                    success: true,
                    data: newTask
                };
                // await StatusOrchestrator.GetInstance().SendStatus(`${ profile.customerName } has updated their request. ${ task.notes }`, "service-requests");
                const notificationData: INotificationData = {
                    authorName: profile.customerName,
                    title: `${ profile.customerName } has updated their services`,
                    type: NotificationType.EDIT_SERVICE,
                    date: new Date(Date.now()).getTime(),
                    message: `${ profile.customerName } has updated their request. ${ task.notes }.`,
                    authorID: profile.uid
                };
                // PushApiInternal.AddNotification(
                //     profile.org.id,
                //     profile.buildings[0].id,
                //     notificationData
                // );
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`PUT /services route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const tenantID: string = <string> req.query.tenant_id || null;
            if (!user || !buildingID || !orgID || !tenantID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const bookings: ITask[] = await ServiceApiInternal.GetTenantSchedule(orgID, buildingID, tenantID);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /bookings/tenant/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });
}
