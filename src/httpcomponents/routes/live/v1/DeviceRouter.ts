// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// ## User API object
//
// HTTP Verb  Route                           Description
//
// SECURE: All API routes should
// be secured by the authGuard (JWT)
// GET        /live/v1/users             Get all of the users in the system
// GET        /live/v1/users/:user_id    Get a single user item by user id
// *********************************************************

// Node modules
import { RequestHandler } from "express-jwt";
import { Application, Router } from "express";

// Local modules
import { IJwtUser } from "../../../../interfaces/IAuth";
import { Constants } from "../../../../common/Constants";
import { UserApiInternal } from "../../../../api/UserApiInternal";
import { ISilverBrickUser, IDeviceInfo } from "../../../../shared/SilverBrickTypes";
import {
    IServerResponseMessage,
    IServerResponseBase
} from "../../../../shared/ResponseTypes";

export function SetLiveV1DeviceRoutes(
    app: Application,
    router: Router,
    authGuard: RequestHandler) {

    // Secure routes for the user APIs
    router.route("/device")
        // #Update a user by ID and user object
        // Accessed at PUT http://localhost:*PORT*/live/v1/device/
        .put(authGuard, async (req, res) => {
            const profile: IJwtUser = req.user;

            const deviceInfo: IDeviceInfo = <IDeviceInfo>req.body;
            console.log("Device Info", JSON.stringify(deviceInfo));
            try {
                // Update user

                await UserApiInternal.UpdateUserDeviceInfo(profile.uid, deviceInfo);

                const responseObject: IServerResponseBase = {
                    success: true
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`POST /device route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .patch(authGuard, async (req, res) => {
            const profile: IJwtUser = req.user;

            const deviceInfo: IDeviceInfo = <IDeviceInfo>req.body;

            try {
                // Update user

                await UserApiInternal.DeleteUserDeviceInfo(profile.uid, deviceInfo);

                const responseObject: IServerResponseBase = {
                    success: true
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`POST /device route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });
}
