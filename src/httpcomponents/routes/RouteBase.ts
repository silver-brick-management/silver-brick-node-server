// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
//  This is the file where we will:
//  - Define the routes for the Server
// *********************************************************

// Node modules
import * as passport from "passport";
import * as expJwt from "express-jwt"; // for securing routes

import { Application, Router, Request, Response, NextFunction } from "express";

// Local modules
import { UnauthorizedError } from "../../common/Error";
import { SetAdminCPV1UserRoutes } from "./admin-cp/v1/UserRouter";
import { SetAdminCPV1OrgRoutes } from "./admin-cp/v1/OrgRouter";

import { SetDbaV1OrgRoutes } from "./dba/v1/OrgRouter";
import { SetDbaV1UserRoutes } from "./dba/v1/UserRouter";
import { IConfigContext } from "../../interfaces/IConfigContext";
import { SetAuthV1LiveRoutes } from "./auth/v1/AuthenticationLiveRouter";
import { SetAuthV1ControlPanelRoutes } from "./auth/v1/AuthenticationAdminCPRouter";
import { SetAuthV1DBARoutes } from "./auth/v1/AuthenticationDBARouter";
import { SetAuthV1PortalRoutes } from "./auth/v1/AuthenticationPortalRouter";
import { SetLiveV1SignupRoutes } from "./live/v1/SignUpRouter";
import { SetLiveV1DeviceRoutes } from "./live/v1/DeviceRouter";
import { SetDBAV1DeviceRoutes } from "./dba/v1/DeviceRouter";
import { SetLiveV1AccountRoutes } from "./live/v1/AccountRouter";
import { SetLiveV1SilverBrickPortalRoutes } from "./portal/v1/SilverBrickPortalRouter";
import { FirebaseAdminWrapper } from "../../wrappers/FirebaseAdminWrapper";

export function ConfigureRoutes(
    configContext: IConfigContext,
    app: Application,
    passport: passport.Passport) {

    // JWT authGuard
    const authGuardDba = expJwt({ secret: configContext.JWTLiveSecret });
    const authGuardLive = expJwt({ secret: configContext.JWTLiveSecret });
    const authGuardAdminCP = expJwt({ secret: configContext.JWTUserCPSecret });
    const authGuardPortal = expJwt({ secret: configContext.JWTLiveSecret });
    const authGuardFirebaseUser = function (req: Request, res: Response, next: NextFunction) {
        let token: string;
        console.log("authGuardFirebaseUser");
        if (req.headers && req.headers.authorization) {
            const parts = req.headers.authorization.split(" ");

            if (parts.length === 2) {
                const scheme = parts[0];
                const credentials = parts[1];

                if (/^Bearer$/i.test(scheme)) {
                    token = credentials;
                }
                else {
                    return next(new UnauthorizedError("credentials_bad_scheme", { message: "Format is Authorization: Bearer [token]" }));
                }
            }
            else {
                return next(new UnauthorizedError("credentials_bad_format", { message: "Format is Authorization: Bearer [token]" }));
            }
        }

        if (!token) {
            return next(new UnauthorizedError("credentials_required", { message: "No authorization token was found" }));
        }
        FirebaseAdminWrapper.GetInstance().VerifyIdToken(token)
            .then((uid) => {
                req.user.uid = uid;
                next();
            })
            .catch((error) => {
                return next(new UnauthorizedError("invalid_token", error));
            });
    };

    // Add headers
    app.use(function (req, res, next) {
        // Website you wish to allow to connect
        res.setHeader("Access-Control-Allow-Origin", "*");
        // Request methods you wish to allow
        res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
        // Request headers you wish to allow
        res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type,Authorization");
        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader("Access-Control-Allow-Credentials", "true");
        // Pass to next layer of middleware
        next();
    });

    // Get an instance of the express Router for Auth
    const authv1Router = Router();
    // All auth routes will be prefixed with /auth
    app.use("/auth/v1", authv1Router);

    // Get an instance of the express Router for Dba routes
    const dbav1Router = Router();
    // All dba routes will be prefixed with /dba
    app.use("/dba/v1", dbav1Router);

    // Get an instance of the express Router for Dba routes
    const adminCPv1Router = Router();
    // All dba routes will be prefixed with /dba
    app.use("/admin-cp/v1", adminCPv1Router);
    // Get an instance of the express Router for live routes (ionic 2)
    const livev1Router = Router();
    // All live routes will be prefixed with /live
    app.use("/live/v1", livev1Router);
    const portalV1Router = Router();
    // All appless workflows routes will be prefixed with /appless
    app.use("/portal/v1", portalV1Router);

    // #Authentication routes
    // Also pass in auth & admin middleware and Passport instance
    SetAuthV1LiveRoutes(configContext, app, authv1Router, passport, authGuardLive);
    SetAuthV1ControlPanelRoutes(configContext, app, authv1Router, passport, authGuardAdminCP);
    SetAuthV1DBARoutes(configContext, app, authv1Router, passport, authGuardDba);
    SetAuthV1PortalRoutes(configContext, app, authv1Router, passport);

    // #DBA Routes
    SetDbaV1UserRoutes(app, dbav1Router, authGuardDba);
    SetDbaV1OrgRoutes(app, dbav1Router, authGuardDba);
    SetDBAV1DeviceRoutes(app, dbav1Router, authGuardDba);

    // #Live Routes
    SetLiveV1SignupRoutes(app, livev1Router, configContext);
    SetLiveV1DeviceRoutes(app, livev1Router, authGuardLive);
    SetLiveV1AccountRoutes(app, livev1Router, authGuardLive);

    // #Admin-CP Routes
    SetAdminCPV1UserRoutes(app, adminCPv1Router, authGuardAdminCP);
    SetAdminCPV1OrgRoutes(app, livev1Router, authGuardAdminCP);
    // #Appless Routes
    SetLiveV1SilverBrickPortalRoutes(app, configContext, portalV1Router);

    // // // Route to handle all Angular requests (fowards calls to client)
    app.all("*", (req, res) => {
        // Load our public/index.html file
        // NOTE that the root is set to the parent of this folder, ie the app root **
        // res.redirect('http://localhost:8100/');
        res.sendFile("public/index.html", { root: __dirname + "/../../" });
    });

    // error handler for all the applications
    app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
        // Custom error handler for express
        let defaultMsg = err.message ? err.message : "Internal Server Error";
        let errorType = typeof err,
            code = 500,
            msg = { success: false, message: defaultMsg };

        switch (err.name) {
            case "UnauthorizedError":
                code = err.status;
                msg = { success: false, message: err.message ? err.message : "Unauthorized" };
                break;
            case "BadRequestError":
            case "UnauthorizedAccessError":
            case "NotFoundError":
                code = err.status;
                msg = { success: false, message: err.inner ? err.inner : "Error" };
                break;
            default:
                break;
        }

        return res.status(code).json(msg);
    });
}
