// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// ## User API object
//
// HTTP Verb  Route                           Description
//
// SECURE: All API routes should
// be secured by the authGuard (JWT)
// GET        /dba/v1/users             Get all of the users in the system
// GET        /dba/v1/users/:user_id    Get a single user item by user id
// POST       /dba/v1/users             Create a single user (Possible to do a batch request)
// DELETE     /dba/v1/users/:user_id    Delete a single user (Possible to do a batch request)
// PUT        /dba/v1/users/:user_id    Update a user with new info
// *********************************************************

// Node modules
import { parse, format } from "url";
import { RequestHandler } from "express-jwt";
import { Application, Router } from "express";

// Local modules
import { HTTPUtil } from "../../../../common/HttpUtil";
import { Constants } from "../../../../common/Constants";
import { NormalizedUser } from "../../../../factories/users/NormalizedUser"; // Replaced with interface for testing
import { IUser, ITask, ITimeCard } from "../../../../shared/SilverBrickTypes";
import { MessageApiInternal } from "../../../../api/MessageApiInternal";
import { UserAdminApiInternal } from "../../../../api/UserAdminApiInternal";
import { UserApiInternal } from "../../../../api/UserApiInternal";
import { ServiceApiInternal } from "../../../../api/ServiceApiInternal";
import { IJwtUser } from "../../../../interfaces/IAuth";
import {
    UserChatRoom,
    AddTimeCardHelper,
    IServerResponseGetChatRooms,
    IServerResponseMessage,
    IServerResponseGetCalendarColors,
    IServerResponseGetTimeCards,
    IServerResponseAddTimeCard,
    GetTimeCardHelper,
    ITimeCardRoster,
    IServerResponseGetTimeCardRoster,
    IServerResponseGetUsers,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponsePatchOrDeleteUser,
    IServerResponseChangePassword,
    IServerResponseGetBuildingUsers
} from "../../../../shared/ResponseTypes";
import { SilverBrickAdminApiInternal } from "../../../../api/SilverBrickAdminApiInternal";
import { MailGunWrapper } from "../../../../wrappers/MailGunWrapper";
import { EmailTemplateFactory, MailType, ISignUpEmailTemplateParams } from "../../../../factories/EmailTemplateFactory";
import { IEmailPredicate } from "../../../../common/ServerInternalTypes";
import { FirebaseAdminWrapper } from "../../../../wrappers/FirebaseAdminWrapper";
import { TwilioWrapper } from "../../../../wrappers/TwilioWrapper";
import { StatusOrchestrator } from "../../../../common/StatusOrchestrator";

export function SetDbaV1UserRoutes(
    app: Application,
    router: Router,
    authGuard: RequestHandler) {

    // Secure routes for the user APIs
    router.route("/users")
        // #Create a new user
        // Accessed at POST http://localhost:*PORT*/api/v1/dba/users
        .post(authGuard, async (req, res) => {
            const userData: IUser = <IUser> req.body.data || null;
            try {
                // Create user
                const userObj: IUser = await UserAdminApiInternal.CreateUser(userData);
                const emailPredicate: IEmailPredicate = {
                    Email: "leo@silverbrickmanagement.com",
                    FirstName: "Leo"
                };
                const userEmailPredicate: IEmailPredicate = {
                    Email: userObj.email,
                    FirstName: userObj.firstName
                };
                const adamEmailPredicate: IEmailPredicate = {
                    Email: "adam@adamwolfson.com",
                    FirstName: "Adam"
                };
                let buildingInfo = await SilverBrickAdminApiInternal.GetBuilding(userObj.orgs.id, userObj.orgs.buildings[0].id);
                const emailParams: ISignUpEmailTemplateParams = EmailTemplateFactory.CreateSignUpEmailTemplateParams(`You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Building Portal`, `${ userObj.orgs.name }`, `${ buildingInfo.info.name }`, `https://portal.silverbrickmanagement.com?customerEmail=${userData.email}`, null);
                const userEmailParams: ISignUpEmailTemplateParams = EmailTemplateFactory.CreateSignUpEmailTemplateParams(`You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Brick Building Portal`, `${ userObj.orgs.name }`, `${ buildingInfo.info.name }`, `https://portal.silverbrickmanagement.com?customerEmail=${userData.email}`, null);
                const adamEmailParams: ISignUpEmailTemplateParams = EmailTemplateFactory.CreateSignUpEmailTemplateParams(`You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Brick Building Portal`, `${ userObj.orgs.name }`, `${ buildingInfo.info.name }`, `https://portal.silverbrickmanagement.com?customerEmail=${userData.email}`, null);
                // console.log("emailParams", emailParams, adamEmailParams);
                await MailGunWrapper.GetInstance().Send([emailPredicate], `You're Invited: To the Silver Brick Building Portal`, userEmailParams, MailType.SIGNUP);
                await MailGunWrapper.GetInstance().Send([userEmailPredicate], `You're Invited: To the Silver Brick Building Portal`, userEmailParams, MailType.SIGNUP);
                await MailGunWrapper.GetInstance().Send([adamEmailPredicate], `You're Invited: To the Silver Brick Building Portal`, userEmailParams, MailType.SIGNUP);
                const TinyURL = require("tinyurl");
                if ((null != userData.phone) && (userData.phone.trim() !== "")) {
                    TinyURL.shorten(`https://portal.silverbrickmanagement.com?customerEmail=${userData.email}`, async function(res: any) {
                        await TwilioWrapper.GetInstance().SendSMS(userData.phone, `You're Invited: To the Silver Brick Building Portal. You can access it here: ${ res }`); 
                    });
                }
                if (userData.role === "Landlord") {
                    let allTasks: ITask[] = await ServiceApiInternal.GetRecurringTasksMultiBldg();
                    for (let task of allTasks) {
                        await ServiceApiInternal.UpdateTaskLandlord(task.orgID, task.buildingID, task.authorID, task, task.authorID, task.authorName);
                    }
                }

                if (userData.role === "Staff") {
                    if ((null != userData.calendarColor) && (null != userData.calendarTextColor)) {
                        let calendarObj: any = {
                            primary: userData.calendarColor,
                            secondary: userData.calendarTextColor
                        };
                        let calendarColorPath: string = Constants.STR_USERS_FSLASH + Constants.STR_CALENDAR_COLORS_FSLASH + userData.uid + Constants.STR_FSLASH;
                        await FirebaseAdminWrapper.GetInstance().SetValue(calendarColorPath, calendarObj);
                    }
                }
                const responseObject: IServerResponseAddOrUpdateOrGetUser = {
                    success: true,
                    data: userObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`POST /users/:id route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("CreateUser Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })

        // #Get multiple users (supports pagination)
        // Accessed at GET http://localhost:*PORT*/api/v1/dba/users
        .get(authGuard, async (req: any, res) => {
            const oldPrevCursor: string = <string> req.query.prev_cursor || null;
            const oldNextCursor: string = <string> req.query.next_cursor || null;
            const limit: number = <number> parseInt(req.query.limit) || Constants.NUM_PAGINATION_LIMIT;
            const isForward: boolean = ((req.query.is_forward || "true") === "true");
            const cursor: string = isForward ? oldNextCursor : oldPrevCursor;

            try {
                // Prepare response values
                let newPrevCursor: string = null;
                let newNextCursor: string = null;
                let fullPrevLink: string = null;
                let fullNextLink: string = null;
                const userObjArray: IUser[] = await UserAdminApiInternal.GetAllUsers(cursor, limit, isForward);

                // Update response values based on request
                if (isForward) {
                    // The old next cursor is now the new prev cursor
                    newPrevCursor = oldNextCursor;
                    if (userObjArray.length === (limit + Constants.NUM_LIMIT_BUFFER_ONE)) {
                        const lastItem: IUser = userObjArray.pop();
                        newNextCursor = lastItem.uid;
                    }
                }
                else {
                    // The old prev cursor is now the new next cursor
                    newNextCursor = oldPrevCursor;
                    if (userObjArray.length === (limit + Constants.NUM_LIMIT_BUFFER_TWO)) {
                        userObjArray.shift();
                        newPrevCursor = userObjArray[0].uid;
                    }
                    // Always remove the last item as its just a cursor anchor
                    userObjArray.pop();
                }

                // Build full previous link
                if (newPrevCursor) {
                    let queryString = Constants.STR_QUESTION + Constants.STR_LIMIT + Constants.STR_EQUAL + limit.toString() +
                        Constants.STR_AND + Constants.STR_IS_BACKWARD +
                        Constants.STR_AND + Constants.STR_PREV_CURSOR + Constants.STR_EQUAL + newPrevCursor;
                    if (newNextCursor) {
                        queryString = queryString + Constants.STR_AND + Constants.STR_NEXT_CURSOR + Constants.STR_EQUAL + newNextCursor;
                    }

                    fullPrevLink = HTTPUtil.GetFullUrl(req, queryString);
                }

                // Build full next link
                if (newNextCursor) {
                    let queryString = Constants.STR_QUESTION + Constants.STR_LIMIT + Constants.STR_EQUAL + limit.toString() +
                        Constants.STR_AND + Constants.STR_IS_FORWARD +
                        Constants.STR_AND + Constants.STR_NEXT_CURSOR + Constants.STR_EQUAL + newNextCursor;
                    if (newPrevCursor) {
                        queryString = queryString + Constants.STR_AND + Constants.STR_PREV_CURSOR + Constants.STR_EQUAL + newPrevCursor;
                    }

                    fullNextLink = HTTPUtil.GetFullUrl(req, queryString);
                }

                const responseObject: IServerResponseGetUsers = {
                    success: true,
                    data: userObjArray,
                    paging: {
                        cursors: {
                            prevCursor: newPrevCursor,
                            nextCursor: newNextCursor
                        },
                        previous: fullPrevLink,
                        next: fullNextLink
                    }
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /users route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("GetAllUsers Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    // router.route("/userimport")
    //     // #Import an array of new users
    //     // Accessed at POST http://localhost:*PORT*/api/v1/dba/userimport
    //     .post(authGuard, async (req, res) => {
    //         const userData: IUser[] = <IUser[]> req.body.data || null;
    //         if (!userData) {
    //             // Some of the required data is missing
    //             const responseObject: IServerResponseMessage = {
    //                 success: false,
    //                 message: Constants.ERROR_MESSAGE_BAD_DATA,
    //                 description: Constants.ERROR_DESCRIPTION_MISSING_REQS
    //             };
    //             // Set HTTP status code `400 Bad Request`
    //             res.status(400);
    //             return res.json(responseObject);
    //         }
    //         else {
    //             try {
    //                 let userImported: IUser[] = [];
    //                 const importAction = userData.forEach(async (user: IUser) => {
    //                     console.log("Mass User Import", user, userData.length);
    //                     // Create user
    //                     const userObj: IUser = await UserAdminApiInternal.CreateUser(user);
    //                     userImported.push(userObj);
    //                 });
    //                 await importAction;
    //                 const responseObject: IServerResponseImportUsers = {
    //                     success: true,
    //                     data: userImported
    //                 };
    //                 // Set HTTP status code `200 successful`
    //                 res.status(200);
    //                 return res.json(responseObject);
    //             }
    //             catch (error) {
    //                 // TODO: Log error
    //                 console.log(`POST /userimport route error: ${error}`);

    //                 const responseObject: IServerResponseMessage = {
    //                     success: false,
    //                     message: Constants.ERROR_MESSAGE_SERVER_ERROR
    //                 };
    //                 // Set HTTP status code `500 Server Error`
    //                 res.status(500);
    //                 return res.json(responseObject);
    //             }
    //         }
    //     });

    // NOTE: Only GET utilizes email. Other resuests must be UID only
    router.route("/users/:uid_or_email")
        // #Get a user by ID or Email
        // Accessed at GET http://localhost:*PORT*/api/v1/dba/users/:user_id
        .get(authGuard, async (req, res) => {
            const uidOrEmail: string = <string> req.params.uid_or_email || null;

            if (!uidOrEmail) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            try {
                const userObj: IUser = await UserAdminApiInternal.GetUser(uidOrEmail);

                const responseObject: IServerResponseAddOrUpdateOrGetUser = {
                    success: true,
                    data: userObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /users/:uid_or_email route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("Get User Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })

        // #Update a user by ID and user object
        // Accessed at PUT http://localhost:*PORT*/api/v1/dba/users/:user_id
        .put(authGuard, async (req, res) => {
            const uid: string = <string> req.params.uid_or_email || null;
            if (!uid) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            const userData: IUser = <IUser>req.body.data;

            // Extra validation to ensure object matches param
            if (uid.toLowerCase() !== userData.uid.toLowerCase()) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISMATCH_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            try {
                // Update user
                const userObj: IUser = await UserAdminApiInternal.UpdateUser(userData);
                if (userData.role === "Staff") {
                    if ((null != userData.calendarColor) && (null != userData.calendarTextColor)) {
                        let calendarObj: any = {
                            primary: userData.calendarColor,
                            secondary: userData.calendarTextColor
                        };
                        let calendarColorPath: string = Constants.STR_USERS_FSLASH + Constants.STR_CALENDAR_COLORS_FSLASH + userData.uid + Constants.STR_FSLASH;
                        await FirebaseAdminWrapper.GetInstance().SetValue(calendarColorPath, calendarObj);
                    }
                }
                const responseObject: IServerResponseAddOrUpdateOrGetUser = {
                    success: true,
                    data: userObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`PUT /users/:uid route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("UpdateUser Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })

        // #Activate a user by ID
        // Accessed at PATCH http://localhost:*PORT*/api/v1/dba/users/:user_id
        .patch(authGuard, async (req, res) => {
            const uid: string = <string> req.params.uid_or_email || null;

            if (!uid) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            const patchAction: string = <string> req.body.data;

            try {
                let responseState: boolean = true;
                let responseMessage: string = Constants.SUCCESS_MESSAGE_ACTIVATE;
                let responseStatus: number = 200;
                let responseDesctiption: string = undefined;

                if (patchAction.toLowerCase() === Constants.PATCH_ACTIVATE_ACTION.toLowerCase()) {
                    await UserAdminApiInternal.ActivateUser(uid);
                }
                else if (patchAction.toLowerCase() === Constants.PATCH_DEACTIVATE_ACTION.toLowerCase()) {
                    await UserAdminApiInternal.DeactivateUser(uid);
                }
                else {
                    responseState = false;
                    responseMessage = Constants.ERROR_MESSAGE_BAD_DATA;
                    responseDesctiption = Constants.ERROR_DESCRIPTION_BAD_PATCH_ACTION;
                    responseStatus = 400;
                }

                const responseObject: IServerResponsePatchOrDeleteUser = {
                    success: responseState,
                    data: responseMessage,
                    description: responseDesctiption
                };
                // Set HTTP status code `200 successful`
                res.status(responseStatus);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`PATCH /users/:uid route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })

        // #Deactivate a user by ID
        // Accessed at DELETE http://localhost:*PORT*/api/v1/dba/users/:user_id
        .delete(authGuard, async (req, res) => {
            const uid: string = <string> req.params.uid_or_email || null;

            if (!uid) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            try {
                await UserAdminApiInternal.DeleteUser(uid);

                const responseObject: IServerResponsePatchOrDeleteUser = {
                    success: true,
                    data: Constants.SUCCESS_MESSAGE_DELETE
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`DELETE /users/:uid route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/buildingusers")
        // #Get all users under a building (supports pagination)
        // Accessed at GET http://localhost:*PORT*/api/v1/dba/buildingusers
        .get(authGuard, async (req, res) => {
            const buildingID: string = <string> req.query.building_id || null;
            const orgID: string = <string> req.query.org_id || null;
            // console.log("buildingusers", buildingID, orgID);
            if (!buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const userObjArray: IUser[] = await UserAdminApiInternal.GetAllNonOrgAdminsUnderBuilding(orgID, buildingID);


                const responseObject: IServerResponseGetBuildingUsers = {
                    success: true,
                    data: userObjArray
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /users route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/buildingtenants")
        // #Get all users under a building (supports pagination)
        // Accessed at GET http://localhost:*PORT*/api/v1/user-cp/buildingusers
        .get(authGuard, async (req, res) => {
            const buildingID: string = <string> req.query.building_id || null;
            const orgID: string = <string> req.query.org_id || null;
            if (!buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const userObjArray: IUser[] = await UserApiInternal.GetAllTenants(orgID, buildingID);
                const responseObject: IServerResponseGetBuildingUsers = {
                    success: true,
                    data: userObjArray
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /buildingtenants route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .patch(authGuard, async (req, res) => {
            try {
                const userObjArray: IUser[] = await UserApiInternal.GetAllTenantsAllBuildings();
                const responseObject: IServerResponseGetBuildingUsers = {
                    success: true,
                    data: userObjArray
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`PATCH /buildingtenants route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/landlordTenants")
        .get(authGuard, async (req, res) => {
            const profile: IJwtUser = <IJwtUser>req.user || null;
            try {
                let user: IUser = await UserAdminApiInternal.GetUser(profile.uid);
                const userObjArray: IUser[] = await UserApiInternal.GetAllTenantsSomeBldg([user.orgs]);
                const responseObject: IServerResponseGetBuildingUsers = {
                    success: true,
                    data: userObjArray
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /landlordBuildingTenants route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/landlords")
    // #Get all users under a building (supports pagination)
    // Accessed at GET http://localhost:*PORT*/api/v1/user-cp/buildingusers
    .get(authGuard, async (req, res) => {
        try {
            // console.log("GetAllLandlordsAllBuildings");
            const userObjArray: IUser[] = await UserApiInternal.GetAllLandlordsAllBuildings();
            const responseObject: IServerResponseGetBuildingUsers = {
                success: true,
                data: userObjArray
            };
            // Set HTTP status code `200 successful`
            res.status(200);
            return res.json(responseObject);
        }
        catch (error) {
            // TODO: Log error
            console.log(`GET /landlords route error: ${error}`);

            const responseObject: IServerResponseMessage = {
                success: false,
                message: Constants.ERROR_MESSAGE_SERVER_ERROR
            };
            // Set HTTP status code `500 Server Error`
            res.status(500);
            return res.json(responseObject);
        }
    });

    // Secure routes for the user APIs
    router.route("/setNewPassword/:uid_or_email")
        // #Create a new user
        // Accessed at POST http://localhost:*PORT*/api/v1/user-cp/users
        .put(authGuard, async (req, res) => {
            const uid: string = <string> req.params.uid_or_email || null;
            if (!uid) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            const userData: IUser = <IUser>req.body.data;

            // Extra validation to ensure object matches param
            if (uid.toLowerCase() !== userData.uid.toLowerCase()) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISMATCH_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            try {
                // Update user
                const userObj: IUser = await UserAdminApiInternal.UpdateUser(userData);
                let buildingInfo = await SilverBrickAdminApiInternal.GetBuilding(userObj.orgs.id, userObj.orgs.buildings[0].id);
                const map: string = await UserApiInternal.GetUserMap(uid);
                let uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + uid + Constants.STR_FSLASH_IS_FIRSTIME_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(uidPath, false);
                await StatusOrchestrator.GetInstance().SendStatus(`A new user has accepted the invite! \n\nDetails: ${ userObj.firstName } ${ userObj.lastName }. \n\nRole: ${ userObj.role }. \n\nBuilding: ${ buildingInfo.info.name }.`, "user-signups");
                const responseObject: IServerResponseAddOrUpdateOrGetUser = {
                    success: true,
                    data: userObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`PUT /users/:uid route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/appMessages/rooms/")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const chatRooms: UserChatRoom[] = await MessageApiInternal.GetChatRooms(user.uid);
                const responseObject: IServerResponseGetChatRooms = {
                    success: true,
                    data: chatRooms
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /messages/rooms/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/timeCard")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const card: ITimeCard[] = await UserApiInternal.GetTimeCard(user.uid);
                const responseObject: IServerResponseGetTimeCards = {
                    success: true,
                    data: card
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /timeCard/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const helper: GetTimeCardHelper = <GetTimeCardHelper>req.body.data || null;
            // console.log("timeCard helper", helper);
            if (!user || !helper) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const cards: ITimeCard[] = await UserApiInternal.GetTimeCards(helper.date, helper.uid, helper.year, helper.month, helper.day);
                const responseObject: IServerResponseGetTimeCards = {
                    success: true,
                    data: cards
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PUT /timeCard/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .post(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const helper: AddTimeCardHelper = <AddTimeCardHelper>req.body.data || null;
            if (!user || !helper) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const card: ITimeCard = await UserApiInternal.AddTimeCard(helper.timeCard);
                const responseObject: IServerResponseAddTimeCard = {
                    success: true,
                    data: card
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /timeCard/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .patch(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const helper: AddTimeCardHelper = <AddTimeCardHelper>req.body.data || null;
            if (!user || !helper) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const card: ITimeCard = await UserApiInternal.UpdateTimeCard(helper.timeCard);
                const responseObject: IServerResponseAddTimeCard = {
                    success: true,
                    data: card
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /timeCard/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/allTimeCards")
        .put(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const helper: GetTimeCardHelper = <GetTimeCardHelper>req.body.data || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const card: ITimeCardRoster = await UserApiInternal.GetTimeCardsForToday(helper.year, helper.month, helper.day);
                const responseObject: IServerResponseGetTimeCardRoster = {
                    success: true,
                    data: card
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /allTimeCards route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/userInvite")
        .post(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const userRef: IUser = <IUser>req.body.data || null;
            if (!userRef) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let userObj: IUser = await UserAdminApiInternal.GetUser(userRef.uid);
                const emailPredicate: IEmailPredicate = {
                    Email: "leo@silverbrickmanagement.com",
                    FirstName: "Leo"
                };
                const userEmailPredicate: IEmailPredicate = {
                    Email: userObj.email,
                    FirstName: userObj.firstName
                };
                const adamEmailPredicate: IEmailPredicate = {
                    Email: "adam@adamwolfson.com",
                    FirstName: "Adam"
                };
                let buildingInfo = await SilverBrickAdminApiInternal.GetBuilding(userObj.orgs.id, userObj.orgs.buildings[0].id);
                const emailParams: ISignUpEmailTemplateParams = EmailTemplateFactory.CreateSignUpEmailTemplateParams(`You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Building Portal`, `${ userObj.orgs.name }`, `${ buildingInfo.info.name }`, `https://portal.silverbrickmanagement.com?customerEmail=${userObj.email}`, null);
                const userEmailParams: ISignUpEmailTemplateParams = EmailTemplateFactory.CreateSignUpEmailTemplateParams(`You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Brick Building Portal`, `${ userObj.orgs.name }`, `${ buildingInfo.info.name }`, `https://portal.silverbrickmanagement.com?customerEmail=${userObj.email}`, null);
                const adamEmailParams: ISignUpEmailTemplateParams = EmailTemplateFactory.CreateSignUpEmailTemplateParams(`You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Brick Building Portal`, `You're Invited: To the Silver Brick Building Portal`, `${ userObj.orgs.name }`, `${ buildingInfo.info.name }`, `https://portal.silverbrickmanagement.com?customerEmail=${userObj.email}`, null);
                // console.log("emailParams", emailParams, adamEmailParams);
                // await MailGunWrapper.GetInstance().Send([emailPredicate], `You're Invited: To the Silver Brick Building Portal`, userEmailParams, MailType.SIGNUP);
                await MailGunWrapper.GetInstance().Send([userEmailPredicate], `You're Invited: To the Silver Brick Building Portal`, userEmailParams, MailType.SIGNUP);
                await MailGunWrapper.GetInstance().Send([adamEmailPredicate], `You're Invited: To the Silver Brick Building Portal`, userEmailParams, MailType.SIGNUP);
                const TinyURL = require("tinyurl");
                if ((null != userObj.phone) && (userObj.phone.trim() !== "")) {
                    TinyURL.shorten(`https://portal.silverbrickmanagement.com?customerEmail=${userObj.email}`, async function(res: any) {
                        await TwilioWrapper.GetInstance().SendSMS(userObj.phone, `You're Invited: To the Silver Brick Building Portal. You can access it here: ${ res }`); 
                    });
                }
                const responseObject: IServerResponseAddOrUpdateOrGetUser = {
                    success: true,
                    data: userObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /userInvite route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/calendarColors")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // Create user
                const colors: any = await UserApiInternal.GetCalendarColors();
                const responseObject: IServerResponseGetCalendarColors = {
                    success: true,
                    data: colors
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /calendarColors/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });
}