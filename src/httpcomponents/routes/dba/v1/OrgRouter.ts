// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file "LICENSE.txt", which is part of this source code package.
//
// ## Org API object
//
// HTTP Verb  Route                           Description
//
// SECURE: All API routes should
// be secured by the authGuard (JWT)
// GET        /dba/v1/orgs             Get all of the orgs in the system
// GET        /dba/v1/orgs/:org_id    Get a single org item by org id
// POST       /dba/v1/orgs             Create a single org (Possible to do a batch request)
// PUT        /dba/v1/orgs/:org_id    Update a org with new info
// *********************************************************

// Node modules
import { parse, format } from "url";
import { RequestHandler } from "express-jwt";
import { Application, Router } from "express";
import * as firebaseAdmin from "firebase-admin";

// Local modules
import { HTTPUtil } from "../../../../common/HttpUtil";
import { ArrayUtil } from "../../../../common/ArrayUtil";
import { AuthUtil } from "../../../../common/AuthUtil";
import { Constants } from "../../../../common/Constants";
import { NormalizedUser } from "../../../../factories/users/NormalizedUser"; // Replaced with interface for testing
import { IUser } from "../../../../shared/SilverBrickTypes";
import { IJwtUser } from "../../../../interfaces/IAuth";
import { SilverBrickAdminApiInternal } from "../../../../api/SilverBrickAdminApiInternal";
import { MessageApiInternal } from "../../../../api/MessageApiInternal";
import { UserAdminApiInternal } from "../../../../api/UserAdminApiInternal";
import { UserApiInternal } from "../../../../api/UserApiInternal";
import { ServiceApiInternal } from "../../../../api/ServiceApiInternal";
import { PushApiInternal } from "../../../../api/PushApiInternal";
import { FirebaseAdminWrapper } from "../../../../wrappers/FirebaseAdminWrapper";
import { MailGunWrapper } from "../../../../wrappers/MailGunWrapper";
import { EmailTemplateFactory, MailType, INewTaskEmailTemplateParams } from "../../../../factories/EmailTemplateFactory";
import { IEmailPredicate } from "../../../../common/ServerInternalTypes";
import { SilverBrickPushType } from "../../../../common/ServerInternalTypes";
import { TwilioWrapper } from "../../../../wrappers/TwilioWrapper";
import { ITaskFeedJoin } from "../../../../shared/IParams";
import {
    IServerResponseMessage,
    IServerResponseGetAllBuildings,
    IServerResponseGetAllOrgs,
    IServerResponseGetNotes,
    IServerResponseAddUpdateNote,
    IServerResponseDeleteNote,
    IServerResponseCreateOrg,
    IServerResponseGetOrgInfo,
    IServerResponseGetTaskHistory,
    IServerResponseAssignBooking,
    IServerResponseGetVendors,
    IServerResponseAddOrUpdateVendor,
    IServerResponseGetNotifications,
    IServerResponseCreateBldg,
    UserChatRoom,
    AssignTaskHelper,
    IServerResponseAddTaskHistory,
    IServerResponseGetTasks,
    IServerResponseAssignTask,
    IServerResponseAddUpdateAssignTask,
    IServerResponseGetChatRooms,
    AddMessageHelper
} from "../../../../shared/ResponseTypes";
import {
    IOrgBasic,
    IVendor,
    INote,
    IBuildingBasicInfo,
    IOrgInfo,
    IBuilding,
    ITaskFeed,
    IAssignee,
    IStaffProfile,
    ITask,
    INotificationData,
    NotificationType
} from "../../../../shared/SilverBrickTypes";
import { StatusOrchestrator } from "../../../../common/StatusOrchestrator";

export function SetDbaV1OrgRoutes(app: Application, router: Router, authGuard: RequestHandler) {
    // Secure routes for the user APIs
    router.route("/orgs")
        // // #Create a new org
        // // Accessed at POST http://localhost:*PORT*/api/v1/dba/orgs
        .post(authGuard, async (req, res) => {
            const orgData: IOrgInfo = <IOrgInfo>req.body.data || null;

            try {
                const orgObj: IOrgBasic = await SilverBrickAdminApiInternal.CreateOrg(orgData);

                const responseObject: IServerResponseCreateOrg = {
                    success: true,
                    data: orgObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`POST /orgs/:id route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })

        // #Get multiple users (supports pagination)
        // Accessed at GET http://localhost:*PORT*/api/v1/dba/users
        .get(authGuard, async (req, res) => {
            try {
                // Prepare response values
                const orgArray: IOrgBasic[] = await SilverBrickAdminApiInternal.GetOrgs();
                const responseObject: IServerResponseGetAllOrgs = {
                    success: true,
                    data: ArrayUtil.Transform(orgArray)
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /orgs route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .patch(authGuard, async (req, res) => {
            try {
                // Prepare response values
                const orgArray: IOrgBasic[] = await SilverBrickAdminApiInternal.GetOrgs();
                for (let org of orgArray) {
                    let keys: string[] = (null != org.buildings) ? Object.keys(org.buildings) : [];
                    let count: number = 0;
                    let orgBuildings: any = org.buildings;
                    for (let building of keys) {
                        let newBuiding: IBuildingBasicInfo = {
                            id: building,
                            orgID: org.id,
                            name: orgBuildings[building],
                            landlords: []
                        };
                        orgBuildings[building] = newBuiding;
                        count = count + 1;
                        org.buildings = ArrayUtil.Transform(orgBuildings);
                    }
                }
                const responseObject: IServerResponseGetAllOrgs = {
                    success: true,
                    data: ArrayUtil.Transform(orgArray)
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /orgs route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            try {
                // Prepare response values
                const orgArray: any[] = await SilverBrickAdminApiInternal.GetOrgsAllBuildings();
                // for (let building of orgArray) {
                //     let landlords: IUser[] = await UserApiInternal.GetAllLandlords(building.orgID, building.id);
                //     if ((null != landlords) && (landlords.length > 0)) {
                //         building.landlords = landlords;
                //     }
                //     else {
                //         building.landlords = [];
                //     }
                // }
                const responseObject: IServerResponseGetAllBuildings = {
                    success: true,
                    data: orgArray
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PATCH /orgs route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    // // NOTE: Only GET utilizes email. Other resuests must be UID only
    router
        .route("/orgs/:org_id")
        // #Get a user by ID or Email
        // Accessed at GET http://localhost:*PORT*/api/v1/dba/users/:user_id
        .get(authGuard, async (req, res) => {
            const orgID: string = <string>req.params.org_id || null;
            if (!orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            try {
                const orgObj: IOrgInfo = await SilverBrickAdminApiInternal.GetOrgInfo(orgID);
                const responseObject: IServerResponseGetOrgInfo = {
                    success: true,
                    data: orgObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PUT /orgs/:org_id route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const orgID: string = <string>req.params.org_id || null;
            const orgInfo: IOrgInfo = <IOrgInfo>req.body.data || null;
            if (!orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            try {
                const orgObj: IOrgBasic = await SilverBrickAdminApiInternal.UpdateOrgInfo(orgID, orgInfo);
                const responseObject: IServerResponseCreateOrg = {
                    success: true,
                    data: orgObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /orgs/:org_id route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .delete(authGuard, async (req, res) => {
            const orgID: string = <string> req.params.org_id || null;
            if (!orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }

            try {
                await SilverBrickAdminApiInternal.DeleteOrg(orgID);
                const responseObject: IServerResponseMessage = {
                    success: true,
                    message: orgID
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`DELETE /orgs route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    // Secure routes for the user APIs
    router
        .route("/buildings/:org_id")
        // // #Create a new org
        // // Accessed at POST http://localhost:*PORT*/api/v1/dba/orgs
        .post(authGuard, async (req, res) => {
            const bldgData: IBuilding = <IBuilding>req.body.data || null;
            const orgID: string = <string>req.params.org_id || null;
            try {
                const bldgObj: IBuilding = await SilverBrickAdminApiInternal.CreateBuilding(orgID, bldgData);

                const responseObject: IServerResponseCreateBldg = {
                    success: true,
                    data: bldgObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`POST /buildings/:org_id route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const bldgData: IBuilding = <IBuilding>req.body.data || null;
            const orgID: string = <string>req.params.org_id || null;
            try {
                const bldgObj: IBuilding = await SilverBrickAdminApiInternal.UpdateBuilding(orgID, bldgData);

                const responseObject: IServerResponseCreateBldg = {
                    success: true,
                    data: bldgObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`POST /buildings/:org_id route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/buildings/:org_id/:building_id")
        .get(authGuard, async (req, res) => {
            const buildingID: string = <string>req.params.building_id || null;
            const orgID: string = <string>req.params.org_id || null;
            try {
                const bldgObj: IBuilding = await SilverBrickAdminApiInternal.GetBuilding(orgID, buildingID);

                const responseObject: IServerResponseCreateBldg = {
                    success: true,
                    data: bldgObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /buildings/:org_id/:building_id route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/messages/rooms/")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const chatRooms: UserChatRoom[] = await MessageApiInternal.GetChatRoomsDashboard();
                const responseObject: IServerResponseGetChatRooms = {
                    success: true,
                    data: chatRooms
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /messages/rooms/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/bookings/all")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let bookings: ITask[] = await ServiceApiInternal.GetSchedule(orgID, buildingID);
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /bookings/all/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/tenantBookings")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const tenantID: string = <string>req.query.tenant_id || null;
            if (!user || !buildingID || !orgID || !tenantID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let bookings: ITask[] = await ServiceApiInternal.GetTenantSchedule(orgID, buildingID, tenantID);
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /bookings/tenant/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    /*************************************************************
    Updating task messages
    ************************************************************/
    router.route("/taskHistory")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const history: ITaskFeed[] = await ServiceApiInternal.GetAllHistory();
                const responseObject: IServerResponseGetTaskHistory = {
                    success: true,
                    data: history
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /taskHistory route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .patch(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const helper: AddMessageHelper = <AddMessageHelper>req.body.data || null;
            if (!user || !helper) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let oldTask: ITaskFeed = helper.message;
                let task: ITask = await ServiceApiInternal.GetTask(oldTask.taskID);
                let receiverIDs: string[] = [];
                const leoPredicate: IEmailPredicate = {
                    Email: "leo@silverbrickmanagement.com",
                    FirstName: "Leo"
                };
                const adamPredicate: IEmailPredicate = {
                    Email: "adam@silverbrickmanagement.com",
                    FirstName: "Adam"
                };
                if (oldTask.type === "insert_photo") {
                    if ((user.role === "Staff") && (!user.isAdmin)) {
                        if (task.recurring) {
                            oldTask.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                        } else {
                            oldTask.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
                        }
                    } else if (user.role === "Landlord") {
                        oldTask.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                    }
                }
                const emailParams: INewTaskEmailTemplateParams = EmailTemplateFactory.CreateNewTaskEmailTemplateParams(`New Message on Task: ${ task.subject } (${ task.buildingName })`, `New Message on Task: ${ task.subject } (${ task.buildingName })`, `New Message on Task: ${ task.subject } (${ task.buildingName })`, `${ task.subject }`, `${ task.authorName }`, `${ task.buildingName }`, null);
                let receiverPredicates: IEmailPredicate[] = [];
                let receiverNames: string[] = [];
                let allAdmins: IUser[] = await UserAdminApiInternal.GetAllAdmins();
                for (let adm of allAdmins) {
                    receiverNames.push(`${ adm.firstName } ${ adm.lastName }`);
                    receiverIDs.push(`${ adm.uid }`);
                }
                let assigneeIDs: string[] = [];
                switch (oldTask.audience) {
                    case Constants.STR_AUDIENCE_LANDLORD_ONLY:
                    {
                        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(oldTask.orgID, oldTask.buildingID);
                        for (let landlord of landlords) {
                            if (user.uid !== landlord.uid) {
                                receiverNames.push(`${ landlord.firstName } ${ landlord.lastName }`);
                                receiverIDs.push(`${ landlord.uid }`);
                                const emailPredicate: IEmailPredicate = {
                                    Email: landlord.email,
                                    FirstName: landlord.firstName
                                };
                                receiverPredicates.push(emailPredicate);
                            }
                        }
                        break;
                    }

                    case Constants.STR_AUDIENCE_STAFF_ONLY:
                    {
                        // receiverIDs.push(Constants.LEO_UID);
                        receiverNames.push("Leo Amer");
                        if (null != task.assignees) {
                            task.assignees.forEach((assignee: IAssignee) => {
                                if ((user.uid !== assignee.uid) && (assignee.uid !== Constants.LEO_UID)) {
                                    receiverIDs.push(assignee.uid);
                                    assigneeIDs.push(assignee.uid);
                                }
                            });
                        }
                        break;
                    }

                    case Constants.STR_AUDIENCE_TENANT_AND_STAFF:
                    {
                        // receiverIDs.push(Constants.LEO_UID);
                        receiverNames.push("Leo Amer");
                        if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                            receiverIDs.push(task.clientID);
                            receiverNames.push(task.clientName);
                            if (task.clientID.length > 20) {
                                let clientRef: IUser = await UserAdminApiInternal.GetUser(task.clientID);
                                if (null != clientRef) {
                                    const emailPredicate: IEmailPredicate = {
                                        Email: clientRef.email,
                                        FirstName: clientRef.firstName
                                    };
                                    receiverPredicates.push(emailPredicate);
                                }
                            }
                        }
                        if (null != task.assignees) {
                            task.assignees.forEach((assignee: IAssignee) => {
                                if ((user.uid !== assignee.uid) && (assignee.uid !== Constants.LEO_UID)) {
                                    receiverIDs.push(assignee.uid);
                                    assigneeIDs.push(assignee.uid);
                                }
                            });
                        }
                        break;
                    }

                    case Constants.STR_AUDIENCE_TENANT_AND_LANDLORD:
                    {
                        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(oldTask.orgID, oldTask.buildingID);
                        if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                            receiverIDs.push(task.clientID);
                            receiverNames.push(task.clientName);
                            if (task.clientID.length > 20) {
                                let clientRef: IUser = await UserAdminApiInternal.GetUser(task.clientID);
                                if (null != clientRef) {
                                    const emailPredicate: IEmailPredicate = {
                                        Email: clientRef.email,
                                        FirstName: clientRef.firstName
                                    };
                                    receiverPredicates.push(emailPredicate);
                                }
                            }
                        }
                        for (let landlord of landlords) {
                            if (user.uid !== landlord.uid) {
                                receiverNames.push(`${ landlord.firstName } ${ landlord.lastName }`);
                                receiverIDs.push(`${ landlord.uid }`);
                                const emailPredicate: IEmailPredicate = {
                                    Email: landlord.email,
                                    FirstName: landlord.firstName
                                };
                                receiverPredicates.push(emailPredicate);
                            }
                        }
                        break;
                    }

                    case Constants.STR_AUDIENCE_TENANT_ONLY:
                    {
                        if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                            receiverIDs.push(task.clientID);
                            receiverNames.push(task.clientName);
                            if (task.clientID.length > 20) {
                                let clientRef: IUser = await UserAdminApiInternal.GetUser(task.clientID);
                                if (null != clientRef) {
                                    const emailPredicate: IEmailPredicate = {
                                        Email: clientRef.email,
                                        FirstName: clientRef.firstName
                                    };
                                    receiverPredicates.push(emailPredicate);
                                }
                            }
                        }
                        break;
                    }
                    
                    default:
                    {
                        break;
                    }
                }
                helper.message.approveDate = new Date(Date.now()).getTime();
                helper.message.approveAuthor = user.customerName;
                helper.message.approveAuthorID = user.uid;
                let newTask: ITaskFeed = await ServiceApiInternal.ApproveHistory(helper.message.orgID, helper.message.buildingID, helper.message);
                let count: number = 0;
                let msg: string = `Updates (${ user.role }): ${ oldTask.message }`;
                // for (let uid of receiverIDs) {
                //     if (uid === task.clientID) {
                //         let role: string = (oldTask.role === "Staff") ? "Silver Brick Team" : oldTask.role;
                //         msg = `Updates (${ role }): ${ oldTask.message }`;
                //     }
                //     else {
                //         msg = `(${ oldTask.authorName }): ${ oldTask.message }`;
                //     }
                //     await PushApiInternal.SendServiceMessagePayload(uid, user.customerName, msg, task.id, helper.message.orgID, helper.message.buildingID, SilverBrickPushType.REQUEST_MESSAGE);
                // }
                await MailGunWrapper.GetInstance().Send(receiverPredicates, `New Photos available on Task: ${ task.subject } (${ task.buildingName })`, emailParams, MailType.NEW_MESSAGE);
                const responseObject: IServerResponseAddTaskHistory = {
                    success: true,
                    data: newTask
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PATCH /taskHistory route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .post(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const helper: AddMessageHelper = <AddMessageHelper>req.body.data || null;
            if (!user || !helper) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let oldTask: ITaskFeed = helper.message;
                let task: ITask = await ServiceApiInternal.GetTask(oldTask.taskID);
                console.log("oldTask", oldTask, task);
                oldTask.timestamp = new Date(Date.now()).getTime();
                oldTask.authorName = user.customerName;
                oldTask.role = user.role;
                oldTask.authorID = user.uid;
                let receiverIDs: string[] = [];
                const leoPredicate: IEmailPredicate = {
                    Email: "leo@silverbrickmanagement.com",
                    FirstName: "Leo"
                };
                const adamPredicate: IEmailPredicate = {
                    Email: "adam@silverbrickmanagement.com",
                    FirstName: "Adam"
                };
                if (oldTask.type === "insert_photo") {
                    if ((user.role === "Staff") && (!user.isAdmin)) {
                        if (task.recurring) {
                            oldTask.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                        } else {
                            oldTask.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
                        }
                    } else if (user.role === "Landlord") {
                        oldTask.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                    }
                }
                const emailParams: INewTaskEmailTemplateParams = EmailTemplateFactory.CreateNewTaskEmailTemplateParams(`New Message on Task: ${ task.subject } (${ task.buildingName })`, `New Message on Task: ${ task.subject } (${ task.buildingName })`, `New Message on Task: ${ task.subject } (${ task.buildingName })`, `${ task.subject }`, `${ task.authorName }`, `${ task.buildingName }`, null);
                let receiverPredicates: IEmailPredicate[] = [];
                let receiverNames: string[] = [];
                let allAdmins: IUser[] = await UserAdminApiInternal.GetAllAdmins();
                for (let adm of allAdmins) {
                    receiverNames.push(`${ adm.firstName } ${ adm.lastName }`);
                    receiverIDs.push(`${ adm.uid }`);
                }
                let assigneeIDs: string[] = [];
                let isSenderStaff: boolean = false;
                if ((user.role === "Staff") || (user.isAdmin) || (user.role === "Landlord")) {
                    isSenderStaff = true;
                }
                if ((null != oldTask.approved) && (oldTask.approved)) {
                    if (isSenderStaff) {
                        switch (oldTask.audience) {
                            case Constants.STR_AUDIENCE_LANDLORD_ONLY:
                            {
                                let landlords: IUser[] = await UserApiInternal.GetAllLandlords(oldTask.orgID, oldTask.buildingID);
                                for (let landlord of landlords) {
                                    if (user.uid !== landlord.uid) {
                                        receiverNames.push(`${ landlord.firstName } ${ landlord.lastName }`);
                                        receiverIDs.push(`${ landlord.uid }`);
                                        const emailPredicate: IEmailPredicate = {
                                            Email: landlord.email,
                                            FirstName: landlord.firstName
                                        };
                                        receiverPredicates.push(emailPredicate);
                                    }
                                }
                                break;
                            }

                            case Constants.STR_AUDIENCE_STAFF_ONLY:
                            {
                                // // receiverIDs.push(Constants.LEO_UID);
                                // receiverNames.push("Leo Amer");
                                if (null != task.assignees) {
                                    task.assignees.forEach((assignee: IAssignee) => {
                                        if ((user.uid !== assignee.uid) && (assignee.uid !== Constants.LEO_UID)) {
                                            receiverIDs.push(assignee.uid);
                                            assigneeIDs.push(assignee.uid);
                                        }
                                    });
                                }
                                break;
                            }

                            case Constants.STR_AUDIENCE_TENANT_AND_STAFF:
                            {
                                // // receiverIDs.push(Constants.LEO_UID);
                                // receiverNames.push("Leo Amer");
                                if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                                    receiverIDs.push(task.clientID);
                                    receiverNames.push(task.clientName);
                                    if (task.clientID.length > 20) {
                                        let clientRef: IUser = await UserAdminApiInternal.GetUser(task.clientID);
                                        if (null != clientRef) {
                                            const emailPredicate: IEmailPredicate = {
                                                Email: clientRef.email,
                                                FirstName: clientRef.firstName
                                            };
                                            receiverPredicates.push(emailPredicate);
                                        }
                                    }
                                }
                                if (null != task.assignees) {
                                    task.assignees.forEach((assignee: IAssignee) => {
                                        if ((user.uid !== assignee.uid) && (assignee.uid !== Constants.LEO_UID)) {
                                            receiverIDs.push(assignee.uid);
                                            assigneeIDs.push(assignee.uid);
                                        }
                                    });
                                }
                                break;
                            }

                            case Constants.STR_AUDIENCE_TENANT_AND_LANDLORD:
                            {
                                let landlords: IUser[] = await UserApiInternal.GetAllLandlords(oldTask.orgID, oldTask.buildingID);
                                if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                                    receiverIDs.push(task.clientID);
                                    receiverNames.push(task.clientName);
                                    if (task.clientID.length > 20) {
                                        let clientRef: IUser = await UserAdminApiInternal.GetUser(task.clientID);
                                        if (null != clientRef) {
                                            const emailPredicate: IEmailPredicate = {
                                                Email: clientRef.email,
                                                FirstName: clientRef.firstName
                                            };
                                            receiverPredicates.push(emailPredicate);
                                        }
                                    }
                                }
                                for (let landlord of landlords) {
                                    if (user.uid !== landlord.uid) {
                                        receiverNames.push(`${ landlord.firstName } ${ landlord.lastName }`);
                                        receiverIDs.push(`${ landlord.uid }`);
                                        const emailPredicate: IEmailPredicate = {
                                            Email: landlord.email,
                                            FirstName: landlord.firstName
                                        };
                                        receiverPredicates.push(emailPredicate);
                                    }
                                }
                                break;
                            }

                            case Constants.STR_AUDIENCE_TENANT_ONLY:
                            {
                                if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                                    receiverIDs.push(task.clientID);
                                    receiverNames.push(task.clientName);
                                    if (task.clientID.length > 20) {
                                        let clientRef: IUser = await UserAdminApiInternal.GetUser(task.clientID);
                                        if (null != clientRef) {
                                            const emailPredicate: IEmailPredicate = {
                                                Email: clientRef.email,
                                                FirstName: clientRef.firstName
                                            };
                                            receiverPredicates.push(emailPredicate);
                                        }
                                    }
                                }
                                break;
                            }
                            
                            default:
                            {
                                break;
                            }
                        }

                    }
                    else {
                        // receiverIDs.push(Constants.LEO_UID);
                        receiverNames.push("Leo Amer");
                        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(oldTask.orgID, oldTask.buildingID);
                        if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                            receiverIDs.push(task.clientID);
                            receiverNames.push(task.clientName);
                        }
                        for (let landlord of landlords) {
                            if (user.uid !== landlord.uid) {
                                receiverNames.push(`${ landlord.firstName } ${ landlord.lastName }`);
                                receiverIDs.push(`${ landlord.uid }`);
                                const emailPredicate: IEmailPredicate = {
                                    Email: landlord.email,
                                    FirstName: landlord.firstName
                                };
                                receiverPredicates.push(emailPredicate);
                            }
                        }

                        if (null != task.assignees) {
                            task.assignees.forEach((assignee: IAssignee) => {
                                if ((user.uid !== assignee.uid) && (assignee.uid !== Constants.LEO_UID)) {
                                    receiverIDs.push(assignee.uid);
                                    assigneeIDs.push(assignee.uid);
                                }
                            });
                        }
                    }
                }
                else {
                    // receiverIDs.push(Constants.LEO_UID);
                    receiverNames.push("Leo Amer");
                }
                const bookings: ITaskFeed = await ServiceApiInternal.RecordActivity(helper.message.orgID, helper.message.buildingID, helper.message);
                await ServiceApiInternal.UpdateTaskStatus(oldTask.orgID, oldTask.buildingID, user.uid, helper.message, task.clientID);
                let count: number = 0;
                let msg: string = `Updates (${ user.role }): ${ oldTask.message }`;
                for (let uid of receiverIDs) {
                    if (uid === task.clientID) {
                        let role: string = (oldTask.role === "Staff") ? "Silver Brick Team" : oldTask.role;
                        msg = `Updates (${ role }): ${ oldTask.message }`;
                    }
                    else {
                        msg = `(${ oldTask.authorName }): ${ oldTask.message }`;
                    }
                    await PushApiInternal.SendServiceMessagePayload(uid, user.customerName, msg, task.id, helper.message.orgID, helper.message.buildingID, SilverBrickPushType.REQUEST_MESSAGE);
                }
                await MailGunWrapper.GetInstance().Send(receiverPredicates, `New Message on Task: ${ task.subject } (${ task.buildingName })`, emailParams, MailType.NEW_MESSAGE);
                const responseObject: IServerResponseAddTaskHistory = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`POSR /taskHistory route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const taskID: ITaskFeedJoin = <ITaskFeedJoin>req.body.data || null;
            if (!user || !taskID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let role: string = "";
                if (user.isAdmin) {
                    role = "super_admin";
                }
                else {
                    role = user.role.toLowerCase();
                }
                let bookings: ITaskFeed[] = await ServiceApiInternal.GetHistory(taskID.orgID, taskID.taskID, role, user.uid);
                let landlordBookings: ITaskFeed[] = [];
                if (((user.role.toLowerCase()) === "landlord") || ((user.role.toLowerCase()) === "tenant")) {
                    for (let feed of bookings) {
                        if (feed.role === "Staff") {
                            feed.authorName = "Silver Brick";
                            landlordBookings.push(feed);
                        }
                    }
                }
                const responseObject: IServerResponseGetTaskHistory = {
                    success: true,
                    data: ((user.role.toLowerCase() === "landlord") || (user.role.toLowerCase() === "tenant")) ? landlordBookings : bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PUT /taskHistory route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/updateHistory")
        .post(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const helper: AddMessageHelper = <AddMessageHelper>req.body.data || null;
            if (!user || !helper) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let newTask: ITaskFeed = await ServiceApiInternal.UpdateHistory(helper.message.orgID, helper.message.buildingID, helper.message);
                const responseObject: IServerResponseAddTaskHistory = {
                    success: true,
                    data: newTask
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PATCH /taskHistory route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/closedTasks")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let bookings: ITask[] = await ServiceApiInternal.GetClosedTasksMultiBldg();
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /closedTasks/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/allBookings")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let bookings: ITask[] = await ServiceApiInternal.GetTasksMultiBldg();
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /allBookings/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>req.body.data.buildingID || null;
            const orgID: string = <string>req.body.data.orgID || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let bookings: ITask[] = await ServiceApiInternal.GetTasks(orgID, buildingID);
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PUT /allBookings/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/deleteTasks")
        .post(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const taskRef: ITask = <ITask> req.body.data || null;
            console.log("Deleting task", taskRef, user.uid);
            if (!taskRef) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                console.log("Deleting task", user.uid, taskRef);
                await ServiceApiInternal.DeleteTask(taskRef.orgID, taskRef.buildingID, taskRef);
                const responseObject: IServerResponseMessage = {
                    success: true,
                    message: ""
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`DELETE /allBookings/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/recurringTasks")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let bookings: ITask[] = await ServiceApiInternal.GetRecurringTasksMultiBldg();
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /recurringTasks/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>req.body.buildingID || null;
            const orgID: string = <string>req.body.orgID || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let bookings: ITask[] = await ServiceApiInternal.GetRecurringTasks(orgID, buildingID);
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PUT /recurringTasks/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .patch(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let bookings: ITask[] = await ServiceApiInternal.GetStaffRecurringSchedule(user.uid);
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PATCH /recurringTasks/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/staffBookings")
        .post(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const task: ITask = <ITask>req.body.data || null;
            try {
                task.lastUpdated = new Date(Date.now()).getTime();
                task.updateAuthor = user.customerName;
                task.updatedRole = user.role;
                task.scheduling = false;
                if (null == task.scheduled) {
                    task.scheduled = false;
                }
                task.skipDates = [];
                let orgInfo = await SilverBrickAdminApiInternal.GetBuildingInfo(task.orgID, task.buildingID);
                if (null != orgInfo) {
                    task.address = orgInfo.address;
                }
                let newTask: ITask = await ServiceApiInternal.AddTask(task.orgID, task.buildingID, task.authorID, task);
                const leoPredicate: IEmailPredicate = {
                    Email: "leo@silverbrickmanagement.com",
                    FirstName: "Leo"
                };
                const adamPredicate: IEmailPredicate = {
                    Email: "adam@silverbrickmanagement.com",
                    FirstName: "Adam"
                };
                const emailParams: INewTaskEmailTemplateParams = EmailTemplateFactory.CreateNewTaskEmailTemplateParams(`New Task at ${ newTask.buildingName } by ${ newTask.authorName }`, `New Task at ${ newTask.buildingName } by ${ newTask.authorName }`, `New Task at ${ newTask.buildingName } by ${ newTask.authorName }`, `${ task.subject }`, `${ newTask.authorName }`, `${ newTask.buildingName }`, null);
                let receiverIDs: string[] = [ newTask.clientID ];
                let receiverPredicates: IEmailPredicate[] = [];
                if (newTask.clientID.length < 20) {
                    let clientRef: IUser = await UserAdminApiInternal.GetUser(newTask.clientID);
                    let clientPredicate: IEmailPredicate = {
                        Email: clientRef.email,
                        FirstName: clientRef.firstName
                    };
                    receiverPredicates.push(clientPredicate);
                }
                if ((user.uid === task.clientID) || ((null != task) && (null != task.recurring) && (task.recurring))) {
                    receiverIDs = receiverIDs.filter((uid: string) => {
                        return (uid !== user.uid);
                    });
                }
                let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
                for (let landlord of landlords) {
                    if (user.uid !== landlord.uid) {
                        receiverIDs.push(`${ landlord.uid }`);
                    }
                }
                let message: string = "";
                for (let uid of receiverIDs) {
                    if (uid === task.clientID) {
                        message = `A ${ task.category } request was added for you.`;
                    }
                    else {
                        message = `A ${ task.category } request for ${ task.clientName } was added.`;
                    }
                    await PushApiInternal.SendNewServicePayload(uid, user.customerName, message, newTask.id, task.orgID, task.buildingID, SilverBrickPushType.NEW_REQUEST);
                }
                await MailGunWrapper.GetInstance().Send(receiverPredicates, `New Task at ${ newTask.buildingName } by ${ newTask.authorName }`, emailParams, MailType.NEW_TASK);
                const responseObject: IServerResponseAddUpdateAssignTask = {
                    success: true,
                    data: newTask
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`POST /staffBookings/ route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("AddTask Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const task: ITask = <ITask>req.body.data || null;
            try {
                // task.lastUpdated = new Date(Date.now()).getTime();
                task.updateAuthor = user.customerName;
                task.updatedRole = user.role;
                console.log("UpdateTask", task);
                const newTask: ITask = await ServiceApiInternal.UpdateTask(task.orgID, task.buildingID, task.authorID, task, user.uid, user.customerName);
                let receiverIDs: string[] = [];
                let assigneeIDs: string[] = [];
                if (user.uid !== Constants.LEO_UID) {
                    // receiverIDs.push(Constants.LEO_UID);
                }
                if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                    receiverIDs.push(task.clientID);
                }
                let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
                for (let landlord of landlords) {
                    // receiverIDs.push(`${ landlord.uid }`);
                }
                if (null != task.assignees) {
                    task.assignees.forEach((assignee: IAssignee) => {
                        if (user.uid !== assignee.uid) {
                            receiverIDs.push(assignee.uid);
                            assigneeIDs.push(assignee.uid);
                        }
                    });
                }
                let message: string = "";
                for (let uid of receiverIDs) {
                    if (assigneeIDs.includes(uid)) {
                        message = `Your assigned request for ${ task.clientName } was updated by ${ user.customerName }`;
                    }
                    else {
                        message = `The ${ task.category } request for ${ task.clientName } was updated.`;
                    }
                    await PushApiInternal.SendUpdateServicePayload(uid, user.customerName, message, newTask.id, task.orgID, task.buildingID, SilverBrickPushType.REQUEST_UPDATED);
                }
                // await PushApiInternal.SendAssignServicePayload(Constants.LEO_UID, user.customerName, `${ task.clientName }\'s ${ (task.category === "Maintenance Request") ? task.subCategory : task.category } request was updated by ${ task.updateAuthor }.`, newTask.id, "N/A", "N/A", task.orgID, task.buildingID, SilverBrickPushType.NEW_REQUEST);
                // await PushApiInternal.SendAssignServicePayload(task.clientID, task.clientName, `Your ${ (task.category === "Maintenance Request") ? task.subCategory : task.category } request for ${ task.clientName } was updated.`, newTask.id, "N/A", "N/A", task.orgID, task.buildingID, SilverBrickPushType.NEW_REQUEST);
                const responseObject: IServerResponseAddUpdateAssignTask = {
                    success: true,
                    data: newTask
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PUT /staffBookings/ route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("UpdateTask Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .patch(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const helper: AssignTaskHelper = <AssignTaskHelper>req.body.data || null;
            try {
                // helper.task.lastUpdated = new Date(Date.now()).getTime();
                helper.task.updateAuthor = user.customerName;
                helper.task.updatedRole = user.role;
                helper.task.status = "Assigned";
                await ServiceApiInternal.AssignTask(helper.task.orgID, helper.task.buildingID, helper.task, user.uid, user.customerName, helper.message);
                let newFeed: ITaskFeed = {
                    type: "assign",
                    taskID: helper.task.id,
                    message: ((null != helper.message) && (helper.message !== "")) ? helper.message : `This task was assigned on ${ new Date(Date.now()).toDateString() }`,
                    timestamp: new Date(Date.now()).getTime(),
                    orgID: helper.task.orgID,
                    audience: Constants.STR_AUDIENCE_STAFF_ONLY,
                    category: helper.task.category,
                    subCategory: helper.task.category,
                    role: helper.task.updatedRole,
                    buildingID: helper.task.buildingID,
                    buildingName: helper.task.buildingName,
                    approved: false,
                    clientUnit: helper.task.clientUnit,
                    authorName: user.customerName,
                    authorID: user.uid,
                    status: "Assigned",
                    assignees: (null != helper.task.assignees) ? helper.task.assignees : []
                };
                let receiverIDs: string[] = [];
                let assigneeIDs: string[] = [];

                if (null != helper.task.assignees) {
                    helper.task.assignees.forEach((assignee: IAssignee) => {
                        if (user.uid !== assignee.uid) {
                            receiverIDs.push(assignee.uid);
                            assigneeIDs.push(assignee.uid);
                        }
                    });
                }
                if ((user.uid !== Constants.LEO_UID) && (!assigneeIDs.includes(Constants.LEO_UID))) {
                    // receiverIDs.push(Constants.LEO_UID);
                }
                let message: string = "";
                for (let uid of receiverIDs) {
                    if (uid === helper.task.clientID) {
                        message = `Your ${ helper.task.category } request has been assigned to a Silver Brick team member`;
                    }
                    else if (assigneeIDs.includes(uid)) {
                        message = ((null != helper.message) && (helper.message !== "")) ? helper.message : `An ${ helper.task.category } request for ${ helper.task.clientName } was assigned to you.`;
                    }
                    else {
                        message = `An ${ helper.task.category } request for ${ helper.task.clientName } was assigned to a Silver Brick team member.`;
                    }
                    await PushApiInternal.SendAssignServicePayload(uid, user.customerName, message, helper.task.id, helper.task.orgID, helper.task.buildingID, SilverBrickPushType.SERVICE_ASSIGN);
                }
                await ServiceApiInternal.UpdateTaskStatus(helper.task.orgID, helper.task.buildingID, user.uid, newFeed, helper.task.clientID);
                const responseObject: IServerResponseAddUpdateAssignTask = {
                    success: true,
                    data: helper.task
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PATCH /staffBookings/ route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("AssignTask Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const staffID: string = <string> req.query.staff_id || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let bookings: ITask[] = await ServiceApiInternal.GetStaffSchedule(orgID, buildingID, staffID);
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /staffBookings/ route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("staffBookings Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/staffHistory")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            if (!user) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const bookings: ITaskFeed[] = await ServiceApiInternal.GetStaffHistory(user.uid);
                const responseObject: IServerResponseGetTaskHistory = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /staffHistory/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/completeRecurring")
        .patch(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const task: ITask = <ITask>req.body.data || null;
            if (!user || !buildingID || !orgID || !task) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // task.lastUpdated = new Date(Date.now()).getTime();
                task.updateAuthor = user.customerName;
                task.updatedRole = user.role;
                if (null == task.progress) {
                    task.progress = {};
                }
                task.progress.finish = new Date(Date.now()).getTime();
                task.progress.finishChecked = true;
                task.clockOutTime = new Date(Date.now()).getTime();
                let date: Date = new Date(Date.now());
                await StatusOrchestrator.GetInstance().SendStatus(`The ${ task.subject } for ${ task.buildingName } was completed on ${date.toDateString()} by ${user.customerName}`, "task-completion");
                await ServiceApiInternal.CompleteTask(orgID, buildingID, task, user.customerName, user.uid);
                const responseObject: IServerResponseAssignBooking = {
                    success: true,
                    data: {
                        scheduleID: task.id,
                        assignees: (null != task.assignees) ? task.assignees : []
                    }
                };
                
                let week = `${date.getMonth()}/${date.getFullYear()}`;
                let newFeed: ITaskFeed = {
                    type: "update",
                    taskID: task.id,
                    message: `${ user.customerName } marked ${ task.subject } as complete`,
                    timestamp: new Date(Date.now()).getTime(),
                    category: task.category,
                    subCategory: task.category,
                    orgID: orgID,
                    buildingID: task.buildingID,
                    buildingName: task.buildingName,
                    clientUnit: task.clientUnit,
                    audience: Constants.STR_AUDIENCE_TENANT_AND_STAFF,
                    approved: (user.isAdmin) ? true : false,
                    authorName: task.authorName,
                    role: task.updatedRole,
                    authorID: task.authorID,
                    status: "Completed",
                    assignees: (null != task.assignees) ? task.assignees : []
                };
                
                let receiverIDs: string[] = [];
                if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                    receiverIDs.push(task.clientID);
                }
                if (user.uid !== Constants.LEO_UID) {
                    // receiverIDs.push(Constants.LEO_UID);
                }

                if (null != task.assignees) {
                    task.assignees.forEach((assignee: IAssignee) => {
                        if (user.uid !== assignee.uid) {
                            receiverIDs.push(assignee.uid);
                        }
                    });
                }

                // let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
                // for (let landlord of landlords) {
                //     if (user.uid !== landlord.uid) {
                //         receiverIDs.push(`${ landlord.uid }`);
                //     }
                // }
                await ServiceApiInternal.UpdateTaskStatus(task.orgID, task.buildingID, user.uid, newFeed, task.clientID, task.completedDates);
                let message: string = "";
                for (let uid of receiverIDs) {
                    message = `The ${ task.subject } was completed on ${date.toDateString()} by a Silver Brick team member`;
                    await PushApiInternal.SendCompleteServicePayload(uid, user.customerName, message, task.id, task.orgID, task.buildingID, SilverBrickPushType.SERVICE_COMPLETE);
                }
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                await StatusOrchestrator.GetInstance().SendStatus("CompleteTask Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                console.log(`PATCH /completeRecurring/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/schedules")
        .patch(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const task: ITask = <ITask>req.body.data || null;
            if (!user || !buildingID || !orgID || !task) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // task.lastUpdated = new Date(Date.now()).getTime();
                task.updateAuthor = user.customerName;
                task.updatedRole = user.role;
                if (null == task.progress) {
                    task.progress = {};
                }
                task.progress.finish = new Date(Date.now()).getTime();
                task.progress.finishChecked = true;
                await ServiceApiInternal.CompleteTask(orgID, buildingID, task, user.customerName, user.uid);
                const responseObject: IServerResponseAssignBooking = {
                    success: true,
                    data: {
                        scheduleID: task.id,
                        assignees: task.assignees
                    }
                };
                let date: Date = new Date(Date.now());
                let week = `${date.getMonth()}/${date.getFullYear()}`;
                let newFeed: ITaskFeed = {
                    type: "complete",
                    taskID: task.id,
                    message: `${ user.customerName } marked this request as complete`,
                    timestamp: new Date(Date.now()).getTime(),
                    category: task.category,
                    subCategory: task.category,
                    orgID: orgID,
                    buildingID: task.buildingID,
                    buildingName: task.buildingName,
                    clientUnit: task.clientUnit,
                    audience: Constants.STR_AUDIENCE_TENANT_AND_STAFF,
                    approved: (user.isAdmin) ? true : false,
                    authorName: task.authorName,
                    role: task.updatedRole,
                    authorID: task.authorID,
                    status: "Completed",
                    assignees: (null != task.assignees) ? task.assignees : []
                };
                let receiverIDs: string[] = [];
                if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                    receiverIDs.push(task.clientID);
                }
                if (user.uid !== Constants.LEO_UID) {
                    // receiverIDs.push(Constants.LEO_UID);
                }

                if (null != task.assignees) {
                    task.assignees.forEach((assignee: IAssignee) => {
                        if (user.uid !== assignee.uid) {
                            receiverIDs.push(assignee.uid);
                        }
                    });
                }

                // let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
                // for (let landlord of landlords) {
                //     if (user.uid !== landlord.uid) {
                //         receiverIDs.push(`${ landlord.uid }`);
                //     }
                // }
                let message: string = "";
                for (let uid of receiverIDs) {
                    if (uid === task.clientID) {
                        message = `Your request was completed on ${date.toDateString()} by a Silver Brick team member`;
                    }
                    else {
                        message = `The ${ task.category } was completed on ${date.toDateString()} by a Silver Brick team member`;
                    }
                    await PushApiInternal.SendCompleteServicePayload(uid, user.customerName, message, task.id, task.orgID, task.buildingID, SilverBrickPushType.SERVICE_COMPLETE);
                }
                await ServiceApiInternal.UpdateTaskStatus(task.orgID, task.buildingID, user.uid, newFeed, task.clientID), task.completedDates;
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                await StatusOrchestrator.GetInstance().SendStatus("CompleteTask Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                console.log(`PATCH /schedules/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const task: ITask = <ITask>req.body.data || null;
            if (!user || !buildingID || !orgID || !task) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                // task.lastUpdated = new Date(Date.now()).getTime();
                task.updateAuthor = user.customerName;
                task.updatedRole = user.role;
                if (null == task.progress) {
                    task.progress = {};
                }
                task.progress.finish = new Date(Date.now()).getTime();
                task.progress.finishChecked = true;
                await ServiceApiInternal.UnCompleteTask(orgID, buildingID, task, user.customerName, user.uid);
                const responseObject: IServerResponseAssignBooking = {
                    success: true,
                    data: {
                        scheduleID: task.id,
                        assignees: task.assignees
                    }
                };
                let date: Date = new Date(Date.now());
                let week = `${date.getMonth()}/${date.getFullYear()}`;
                let newFeed: ITaskFeed = {
                    type: "update",
                    taskID: task.id,
                    message: `${ user.customerName } re-opened this request`,
                    timestamp: new Date(Date.now()).getTime(),
                    category: task.category,
                    subCategory: task.category,
                    orgID: orgID,
                    buildingID: task.buildingID,
                    buildingName: task.buildingName,
                    clientUnit: task.clientUnit,
                    audience: Constants.STR_AUDIENCE_TENANT_AND_STAFF,
                    approved: (user.isAdmin) ? true : false,
                    authorName: task.authorName,
                    role: task.updatedRole,
                    authorID: task.authorID,
                    status: "Assigned",
                    assignees: (null != task.assignees) ? task.assignees : []
                };
                let receiverIDs: string[] = [];
                if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                    receiverIDs.push(task.clientID);
                }
                if (user.uid !== Constants.LEO_UID) {
                    // receiverIDs.push(Constants.LEO_UID);
                }

                if (null != task.assignees) {
                    task.assignees.forEach((assignee: IAssignee) => {
                        if (user.uid !== assignee.uid) {
                            receiverIDs.push(assignee.uid);
                        }
                    });
                }

                // let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
                // for (let landlord of landlords) {
                //     if (user.uid !== landlord.uid) {
                //         receiverIDs.push(`${ landlord.uid }`);
                //     }
                // }
                let message: string = "";
                for (let uid of receiverIDs) {
                    if (uid === task.clientID) {
                        message = `Your request was re-opened on ${date.toDateString()} by a Silver Brick team member`;
                    }
                    else {
                        message = `The ${ task.category } was re-opened on ${date.toDateString()} by a Silver Brick team member`;
                    }
                    // await PushApiInternal.SendCompleteServicePayload(uid, user.customerName, message, task.id, task.orgID, task.buildingID, SilverBrickPushType.SERVICE_REOPENED);
                }
                await ServiceApiInternal.UpdateTaskStatus(task.orgID, task.buildingID, user.uid, newFeed, task.clientID);
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PATCH /schedules/ route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("Re-Open Task Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/notifications")
        .put(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const notifications: INotificationData[] = await PushApiInternal.GetNotifications(orgID, buildingID);
                const responseObject: IServerResponseGetNotifications = {
                    success: true,
                    data: notifications
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /schedules/ route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("GetNotifications Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/vendors")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const vendors: IVendor[] = await SilverBrickAdminApiInternal.GetVendors(orgID, buildingID);
                const responseObject: IServerResponseGetVendors = {
                    success: true,
                    data: vendors
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /vendors/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .post(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            const vendor: IVendor = <IVendor>req.body.data || null;
            if (!user || !buildingID || !orgID || !vendor) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const vendors: IVendor = await SilverBrickAdminApiInternal.AddVendor(orgID, buildingID, vendor);
                const responseObject: IServerResponseAddOrUpdateVendor = {
                    success: true,
                    data: vendors
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`POST /vendors/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/progressTasks")
        .patch(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const task: ITask = <ITask>req.body.data || null;
            const buildingID: string = <string>user.buildings[0].id || null;
            const orgID: string = <string>user.org.id || null;
            if (!user || !task) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                if (null == task.progress) {
                    task.progress = {};
                }
                task.progress.start = new Date(Date.now()).getTime();
                task.progress.startChecked = true;
                task.clockInTime = new Date(Date.now()).getTime();
                const services: ITask = await ServiceApiInternal.ProgressTask(orgID, buildingID, task, user.customerName, user.uid);
                const responseObject: IServerResponseAddUpdateAssignTask = {
                    success: true,
                    data: services
                };
                let date: Date = new Date(Date.now());
                let week = `${date.getMonth()}/${date.getFullYear()}`;
                let newFeed: ITaskFeed = {
                    type: "update",
                    taskID: task.id,
                    message: `${ user.customerName } marked this task as in progress`,
                    timestamp: new Date(Date.now()).getTime(),
                    category: task.category,
                    subCategory: task.category,
                    orgID: orgID,
                    buildingID: task.buildingID,
                    buildingName: task.buildingName,
                    clientUnit: task.clientUnit,
                    audience: Constants.STR_AUDIENCE_TENANT_AND_STAFF,
                    approved: (user.isAdmin) ? true : false,
                    authorName: task.authorName,
                    role: task.updatedRole,
                    authorID: task.authorID,
                    status: "In Progress",
                    assignees: (null != task.assignees) ? task.assignees : []
                };
                let receiverIDs: string[] = [];
                if ((null != task) && (null != task.recurring) && (!task.recurring)) {
                    receiverIDs.push(task.clientID);
                }
                if (user.uid !== Constants.LEO_UID) {
                    // receiverIDs.push(Constants.LEO_UID);
                }

                let message: string = "";
                for (let uid of receiverIDs) {
                    if (uid === task.clientID) {
                        message = `Your request has been started on ${date.toDateString()} by ${ user.customerName }`;
                    }
                    else {
                        message = `The ${ task.category } was started on ${date.toDateString()} by a Silver Brick team member`;
                    }
                    await PushApiInternal.SendProgressServicePayload(uid, user.customerName, message, task.id, task.orgID, task.buildingID, SilverBrickPushType.SERVICE_PROGRESS);
                }
                await ServiceApiInternal.UpdateTaskStatus(task.orgID, task.buildingID, user.uid, newFeed, task.clientID);
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /services/ route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("Progress Task Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/buildingNotes")
        .get(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const buildingID: string = <string>req.query.building_id || null;
            const orgID: string = <string>req.query.org_id || null;
            if (!user || !buildingID || !orgID) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const notes: INote[] = await ServiceApiInternal.GetNotes(orgID, buildingID);
                const responseObject: IServerResponseGetNotes = {
                    success: true,
                    data: notes
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /buildingNotes/ route error: ${error}`);
                await StatusOrchestrator.GetInstance().SendStatus("buildingNotes Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .post(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const note: INote = <INote>req.body.data || null;
            if (!user || !note) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const newNote: INote = await ServiceApiInternal.AddNote(note.orgID, note.buildingID, note);
                const responseObject: IServerResponseAddUpdateNote = {
                    success: true,
                    data: note
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`POST /buildingNotes/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const user: IJwtUser = <IJwtUser>req.user || null;
            const note: INote = <INote>req.body.data || null;
            if (!user || !note) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                const newNote: INote = await ServiceApiInternal.UpdateNote(note.orgID, note.buildingID, note);
                const responseObject: IServerResponseAddUpdateNote = {
                    success: true,
                    data: newNote
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`PUT /buildingNotes/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });

    router.route("/landlordTasks")
        .get(authGuard, async (req, res) => {
            const profile: IJwtUser = <IJwtUser>req.user || null;
            if (!profile) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let user: IUser = await UserAdminApiInternal.GetUser(profile.uid);
                let bookings: ITask[] = await ServiceApiInternal.GetTasksSomeBldgLandlord([user.orgs]);
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`GET /landlordTasks/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        })
        .put(authGuard, async (req, res) => {
            const profile: IJwtUser = <IJwtUser>req.user || null;
            if (!profile) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_UID
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            try {
                let user: IUser = await UserAdminApiInternal.GetUser(profile.uid);
                let bookings: ITask[] = await ServiceApiInternal.GetCompletedTasksSomeBldgLandlord([user.orgs]);
                bookings = ArrayUtil.SortTasksByDate(bookings);
                const responseObject: IServerResponseGetTasks = {
                    success: true,
                    data: bookings
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            } catch (error) {
                // TODO: Log error
                console.log(`PUT /landlordTasks/ route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });
}

