// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// ## Auth object
//
// HTTP Verb  Route Description
//
// SECURE
// POST       */auth/v1/appless/login* Route to login
//
// *********************************************************

// Node modules
import * as passport from "passport";

import { sign } from "jsonwebtoken";
import { RequestHandler } from "express-jwt";
import { Application, Router } from "express";

// Local modules
import { AuthUtil } from "../../../../common/AuthUtil";
import { Constants } from "../../../../common/Constants";
import { IConfigContext } from "../../../../interfaces/IConfigContext";
import { NormalizedUser } from "../../../../factories/users/NormalizedUser";
import { UserApiInternal } from "../../../../api/UserApiInternal";
import { UserAdminApiInternal } from "../../../../api/UserAdminApiInternal";
import {
    IServerResponseMessage, IServerResponseGetProfile,
    IServerResponseLogin, IServerResponseRefreshToken
} from "../../../../shared/ResponseTypes";

export function SetAuthV1PortalRoutes(
    configContext: IConfigContext,
    app: Application,
    router: Router,
    passport: passport.Passport) {

    // Public route for login API
    router.route("/portal/authenticate/step1")
        // #Aquire your access and refresh token
        // Login up a user
        .post((req, res, next) => {
            // Call `authenticate()` from within the route handler, rather than
            // as a route middleware. This gives the callback access to the `req`
            // and `res` object through closure.
            req.body.context = Constants.STR_PORTAL;
            // If authentication fails, `user` will be set to `false`. If an
            // exception occured, `err` will be set. `info` contains a message
            // set within the Local Passport strategy.
            passport.authenticate("portal-first", (err: any, user: any, info: any) => {
                if (err) {
                    return next(err);
                }

                // If no user is returned...
                if (!user) {
                    let responseObject: IServerResponseMessage = {
                        success: false,
                        message: info.message
                    };
                    // Set HTTP status code `401 Unauthorized`
                    res.status(401);
                    return res.json(responseObject);
                }

                // Use login function exposed by Passport to generate
                // JWT token
                req.login(user, { session: false }, async (err) => {
                    if (err) {
                        return next(err);
                    }
                    // If no user is returned...
                    if ((!user) || (!user.user)) {
                        console.log("No User!");
                        let responseObject: IServerResponseMessage = {
                            success: false,
                            message: info.message
                        };
                        // Set HTTP status code `401 Unauthorized`
                        res.status(401);
                        return res.json(responseObject);
                    }
                    else {
                        UserApiInternal.SaveLastActivityDate(user.uid);
                        // Generate access token
                        // Create response object (Could JSON.stringify);
                        const accessToken: string = AuthUtil.GenerateJWTApplessAccessToken(user.user, configContext.JWTLiveSecret, configContext.JWTExpire);
                        // Set HTTP status code `200 OK`
                        let url: string = `https://portal.silverbrickmanagement.com/main/?${ accessToken }&phoneNumber=${ user.user.phone }&bookingID=${ user.bookingID }`;
                        let responseObject: IServerResponseMessage = {
                            success: true,
                            message: url
                        };
                        return res.json(responseObject);
                    }
                });
            }) (req, res, next);
        });

    // Public route for login API
    router.route("/portal/authenticate")
        // #Aquire your access and refresh token
        // Login up a user
        .post((req, res, next) => {
            // Call `authenticate()` from within the route handler, rather than
            // as a route middleware. This gives the callback access to the `req`
            // and `res` object through closure.
            req.body.context = Constants.STR_PORTAL;
            // If authentication fails, `user` will be set to `false`. If an
            // exception occured, `err` will be set. `info` contains a message
            // set within the Local Passport strategy.
            passport.authenticate("portal-second", (err: any, user: NormalizedUser, info: any) => {
                if (err) {
                    return next(err);
                }

                // If no user is returned...
                if (!user) {
                    let responseObject: IServerResponseMessage = {
                        success: false,
                        message: info.message
                    };
                    // Set HTTP status code `401 Unauthorized`
                    res.status(401);
                    return res.json(responseObject);
                }

                // Use login function exposed by Passport to generate
                // JWT token
                req.login(user, { session: false }, (err) => {
                    if (err) {
                        return next(err);
                    }

                    // Generate access token
                    // Create response object (Could JSON.stringify)
                    let responseObject: IServerResponseLogin = {
                        success: true,
                        accessToken: AuthUtil.GenerateJWTApplessAccessToken(user, configContext.JWTLiveSecret, configContext.JWTExpire),
                        data: user
                    };
                    // Set HTTP status code `200 OK`
                    res.status(200);
                    return res.json(responseObject);
                });
            }) (req, res, next);
        });
}