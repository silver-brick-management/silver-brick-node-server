// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// ## Auth object
//
// HTTP Verb  Route                     Description
//
// UNSECURE
// POST       */auth/v1/dba/login*          Route to login
//
// SECURE
// GET        */auth/v1/dba/profile*        Get user data from session object in
//                                          Node
// GET        */auth/v1/refreshtoken*       Route to test if the user is logged in
//                                          or not
// *********************************************************

// Node modules
import * as passport from "passport";

import { sign } from "jsonwebtoken";
import { RequestHandler } from "express-jwt";
import { Application, Router } from "express";

// Local modules
import { AuthUtil } from "../../../../common/AuthUtil";
import { Constants } from "../../../../common/Constants";
import { IConfigContext } from "../../../../interfaces/IConfigContext";
import { UserApiInternal } from "../../../../api/UserApiInternal";
import { NormalizedUser } from "../../../../factories/users/NormalizedUser";
import { UserAdminApiInternal } from "../../../../api/UserAdminApiInternal";
import {
    IServerResponseMessage,
    IServerResponseGetProfile,
    IServerResponseLogin,
    IServerResponseRefreshToken,
    IServerResponseGetUser
} from "../../../../shared/ResponseTypes";
import { IUser } from "../../../../shared/SilverBrickTypes";
import { StatusOrchestrator } from "../../../../common/StatusOrchestrator";
import { IStatusReporter, IncomingMessageCallbackType } from "../../../../interfaces/IStatusReporter";

export function SetAuthV1ControlPanelRoutes(
    configContext: IConfigContext,
    app: Application,
    router: Router,
    passport: passport.Passport,
    authGuard: RequestHandler) {

    // Public route for login API
    router.route("/admin-cp/login")
        // #Aquire your access and refresh token
        // Accessed at POST http://localhost:*PORT*/auth/v1/login
        // Login up a user
        .post((req, res, next) => {
            // Call `authenticate()` from within the route handler, rather than
            // as a route middleware. This gives the callback access to the `req`
            // and `res` object through closure.

            // Set login context to force isDBA check
            req.body.context = Constants.STR_CP;
            // If authentication fails, `user` will be set to `false`. If an
            // exception occured, `err` will be set. `info` contains a message
            // set within the Local Passport strategy.
            passport.authenticate("local-login", async (err: any, user: NormalizedUser, info: any) => {
                // console.log("Login", user);
                if (err) {
                    return next(err);
                }
                // If no user is returned...
                if (!user) {
                    let responseObject: IServerResponseMessage = {
                        success: false,
                        message: info.message
                    };
                    // Set HTTP status code `401 Unauthorized`
                    res.status(401);
                    return res.json(responseObject);
                }

                // Use login function exposed by Passport to generate
                // JWT token
                req.login(user, { session: false }, async (err) => {
                    if (err) {
                        return next(err);
                    }
                    UserApiInternal.SaveLastActivityDate(user.uid);
                    // Generate access token
                    // Create response object (Could JSON.stringify)
                    let responseObject: IServerResponseLogin = {
                        success: true,
                        accessToken: AuthUtil.GenerateJWTAccessToken(user, configContext.JWTLiveSecret, configContext.JWTExpire),
                        data: user
                    };
                    // Set HTTP status code `200 OK`
                    res.status(200);
                    // res.setHeader('Content-Type', 'application/json');
                    return res.json(responseObject);
                });

            }) (req, res, next);
        });

    // Refreshes the access token before or after expiration
    // by using the refresh token
    router.route("/admin-cp/refreshtoken")
        // #Get a new access token with the refresh token
        // Accessed at GET http://localhost:*PORT*/auth/v1/refreshtoken
        .post((req, res, next) => {
            // Call `authenticate()` from within the route handler, rather than
            // as a route middleware. This gives the callback access to the `req`
            // and `res` object through closure.

            // If authentication fails, `user` will be set to `false`. If an
            // exception occured, `err` will be set. `info` contains a message
            // set within the Local Passport strategy.
            passport.authenticate("cp-refresh", { session: false }, (err: any, user: NormalizedUser, info: any) => {

                if (err) {
                    return next(err);
                }

                // If no user is returned...
                if (!user) {
                    // Create response object (Could JSON.stringify)
                    let responseObject: IServerResponseMessage = {
                        success: false,
                        message: info.message
                    };
                    // Set HTTP status code `401 Unauthorized`
                    res.status(401);
                    // Return the info message
                    return res.json(responseObject);
                }

                // ALL Successful

                // Generate access token
                let responseObject: IServerResponseRefreshToken = {
                    success: true,
                    accessToken: AuthUtil.GenerateJWTAccessToken(user, configContext.JWTUserCPSecret, configContext.JWTExpireCP),
                };
                // Set HTTP status code `200 OK`
                res.status(200);
                return res.json(responseObject);

            }) (req, res, next);
        });

    // Get user information
    router.route("/admin-cp/profile")
        // #Get a user's own profile info
        // Accessed at GET http://localhost:*PORT*/auth/v1/user
        .get(authGuard, async (req, res) => {
            // If no user is returned from authGuard...
            if (!req.user) {
                let responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_NO_USER
                };
                // Set HTTP status code `401 Unauthorized`
                res.status(401);
                return res.json(responseObject);
            }

            try {
                let userObj: NormalizedUser = await UserAdminApiInternal.GetUser(req.user.uid);

                let responseObject: IServerResponseGetUser = {
                    success: true,
                    data: userObj
                };
                // Set HTTP status code `200 successful`
                res.status(200);
                return res.json(responseObject);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /auth/profile route error: ${error}`);

                let responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });
}