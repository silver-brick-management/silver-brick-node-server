// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// ## WarnableAppless API object
//
// HTTP Verb  Route                           Description
//
// SECURE: All API routes should
// be secured by the authGuard (JWT)
// GET        /live/v1/warnable          Get Warnable info
// *********************************************************

// Node modules
import { RequestHandler } from "express-jwt";
import { Application, Router } from "express";

// Local modules
import { IJwtUser } from "../../../../interfaces/IAuth";
import { Constants } from "../../../../common/Constants";
import { AuthUtil } from "../../../../common/AuthUtil";

import { ServiceApiInternal } from "../../../../api/ServiceApiInternal";
import { IDeviceInfo, IUser } from "../../../../shared/SilverBrickTypes";
import {
    IServerResponseUpdateProfile,
    IServerResponseAddOrUpdateOrGetUser, IServerResponseMessage,
    IServerResponseBase
} from "../../../../shared/ResponseTypes";
import { IConfigContext } from "../../../../interfaces/IConfigContext";

export function SetLiveV1SilverBrickPortalRoutes(
    app: Application,
    configContext: IConfigContext,
    router: Router) {
    // Secure routes for the user APIs
    router.route("/portal/:urlToken")
        // #Get multiple users
        // Accessed at GET http://localhost:*PORT*/live/v1/users
        .get(async (req, res) => {
            const urlToken: string = <string>req.params.urlToken || null;
            if (!urlToken) {
                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_BAD_DATA,
                    description: Constants.ERROR_DESCRIPTION_MISSING_REQS
                };
                // Set HTTP status code `400 Bad Request`
                res.status(400);
                return res.json(responseObject);
            }
            else {
                // All params are provided, continue
            }

            try {
                let newStr = urlToken.replace(/\+/g, "%2B");
                let newStr2 = newStr.replace(/['"]+/g, "");
                let parts: string[] = newStr2.split("phoneNumber=", 2);
                let url = `https://portal.SilverBrick.io/index.php?newToken=${ parts[0]}phone=${parts[1] }`;
                console.log("URL for PWA ", url);
                // Set HTTP status code `200 successful`
                return res.redirect(url);
            }
            catch (error) {
                // TODO: Log error
                console.log(`GET /live/groups route error: ${error}`);

                const responseObject: IServerResponseMessage = {
                    success: false,
                    message: Constants.ERROR_MESSAGE_SERVER_ERROR
                };
                // Set HTTP status code `500 Server Error`
                res.status(500);
                return res.json(responseObject);
            }
        });
}
