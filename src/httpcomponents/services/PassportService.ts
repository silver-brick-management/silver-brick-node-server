// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
//  This is the file where we will:
//  - Configure PassportJS
// *********************************************************

// Node modules
import * as passport from "passport";

import { Request } from "express";
import { randomBytes } from "crypto";
import { Strategy as LocalStrategy } from "passport-local";
import { Strategy as JwtStrategy, ExtractJwt, VerifiedCallback } from "passport-jwt";

// Local modules
import { Constants } from "../../common/Constants";
import { StringUtil } from "../../common/StringUtil";
import { AuthUtil } from "../../common/AuthUtil";
import { UnauthorizedError } from "../../common/Error";
import { IConfigContext } from "../../interfaces/IConfigContext";
import { FirebaseAdminWrapper } from "../../wrappers/FirebaseAdminWrapper";
import { UserAdminApiInternal } from "../../api/UserAdminApiInternal";
import { ServiceApiInternal } from "../../api/ServiceApiInternal";
import { IUserBuilding } from "../../shared/SilverBrickTypes";

export function ConfigurePassport(
    configContext: IConfigContext,
    passport: passport.Passport) {

    // Named strategy for handling login (context allows for DBA vs Live)
    // By default, if there is no name, it would just be called 'local'
    passport.use("local-login", new LocalStrategy({
            // By default, the local strategy uses email and password
            usernameField : "email",
            passwordField : "id_token",
            // Allow the entire request to be passed back to the callback
            passReqToCallback : true
        },
        (req, email, id_token, done) => {
        // ## Data Checks
        // If the length of the email string is too long/short,
        // invoke verify callback

        // If the length of the email string is too long/short,
        // invoke verify callback
        if (!StringUtil.IsOutofBounds(email, Constants.NUM_EMAIL_MIN_LENGTH, Constants.NUM_EMAIL_MAX_LENGTH)) {
            // ### Verify Callback
            // Invoke `done` with `false` to indicate authentication
            // failure
            return done(null,
                false,
                // Return info message object
                { message: Constants.ERROR_MESSAGE_BAD_EMAIL_LENGTH }
            );
        }

        // If the string is not a valid email...
        if (!StringUtil.IsValidEmail(email)) {
            // ### Verify Callback
            // Invoke `done` with `false` to indicate authentication
            // failure
            return done(null,
                false,
                // Return info message object
                { message: Constants.ERROR_MESSAGE_BAD_EMAIL }
            );
        }

        // Validate firebase id_token (JWT) and get user's id
        FirebaseAdminWrapper.GetInstance().VerifyIdToken(id_token)
        .then((uid) => {
            return UserAdminApiInternal.GetUser(uid);
        })
        .then(async (user) => {
            // If no user is found, return a message
            if (!user) {
                return done(null,
                    false,
                    { message: Constants.ERROR_MESSAGE_NO_USER }
                );
            }
            // if (req.body.context === Constants.STR_DBA) {
            //     // If the user is found but is not DBA
            //     if (!user.isAdmin) {
            //         return done(null,
            //             false,
            //             { message: Constants.ERROR_MESSAGE_UNAUTHORIZED });
            //     }
            // }
            // else if (req.body.context === Constants.STR_CP) {
            //     // If the user is found but is not DBA
            //     // If the user is found but is not DBA
            //     let isDBA: boolean = false;
            //     user.orgs.buildings.forEach((building: IUserBuilding) => {
            //         if (building.isAdmin) {
            //             isDBA = true;
            //             console.log("Is Building DBA: ", building.id + " " + isDBA);
            //         } else if (user.orgs.isAdmin) {
            //             isDBA = true;
            //             console.log("Is Org DBA: ", user.orgs.id + " " + isDBA);
            //         }
            //     });

            //     if (!isDBA) {
            //         return done(null,
            //             false,
            //             { message: Constants.ERROR_MESSAGE_UNAUTHORIZED });
            //     }
            // }
            // else if (req.body.context === Constants.STR_LIVE) {
            //     // Continue, no additional checks
            // }
            // else {
            //     // No context provided. Error out
            //     return done(null,
            //         false,
            //         { message: Constants.ERROR_MESSAGE_UNAUTHORIZED });
            // }

            // Otherwise all is well; return successful user
            return done(null, user);
        })
        .catch((error) => {
            return done(new UnauthorizedError("firebase_token_unverified", error));
        });
    }));

    // # JWT Refresh
    // Checks isDBA
    // By default, if there is no name, it would just be called 'jwt'
    passport.use("dba-refresh", new JwtStrategy({
            secretOrKey : configContext.JWTDbaSecret,
            jwtFromRequest : ExtractJwt.fromAuthHeaderWithScheme("Bearer"),
            ignoreExpiration: true,
            // Allow the entire request to be passed back to the callback
            passReqToCallback : true
        },
        (req: Request, payload: any, done: VerifiedCallback) => {
        // Verify id_token is sent
        if (!req.body.id_token) {
            // ### Verify Callback
            // Invoke `done` with `false` to indicate authentication
            // failure
            return done(null,
                false,
                // Return info message object
                { message: Constants.ERROR_MESSAGE_NO_TOKEN }
            );
        }

        // Validate firebase id_token (JWT) and get user's id
        FirebaseAdminWrapper.GetInstance().VerifyIdToken(req.body.id_token)
        .then((uid) => {
            return UserAdminApiInternal.GetUser(uid);
        })
        .then((user) => {
            // If no user is found, return a message
            if (!user) {
                return done(null,
                    false,
                    { message: Constants.ERROR_MESSAGE_NO_USER }
                );
            }

            // Extra validation around uid
            // Protects against stolen id_token
            if (payload.uid !== user.uid) {
                return done(null,
                    false,
                    { message: Constants.ERROR_MESSAGE_UNAUTHORIZED });
            }

            // If the user is found but is not DBA
            // if (!user.isAdmin) {
            //     return done(null,
            //         false,
            //         { message: Constants.ERROR_MESSAGE_UNAUTHORIZED });
            // }
            // Otherwise all is well; return successful user
            return done(null, user);
        })
        .catch((error) => {
            return done(new UnauthorizedError("firebase_token_unverified", error));
        });
    }));

    // # JWT Refresh
    // Checks isCP
    // By default, if there is no name, it would just be called 'jwt'
    passport.use("admin-refresh", new JwtStrategy({
            secretOrKey : configContext.JWTUserCPSecret,
            jwtFromRequest : ExtractJwt.fromAuthHeaderWithScheme("Bearer"),
            ignoreExpiration: false,
            // Allow the entire request to be passed back to the callback
            passReqToCallback : true
        },
        (req: Request, payload: any, done: VerifiedCallback) => {
        // Verify id_token is sent
        if (!req.body.id_token) {
            // ### Verify Callback
            // Invoke `done` with `false` to indicate authentication
            // failure
            return done(null,
                false,
                // Return info message object
                { message: Constants.ERROR_MESSAGE_NO_TOKEN }
            );
        }

        // Validate firebase id_token (JWT) and get user's id
        FirebaseAdminWrapper.GetInstance().VerifyIdToken(req.body.id_token)
        .then((uid) => {
            return UserAdminApiInternal.GetUser(uid);
        })
        .then((user) => {
            // If no user is found, return a message
            if (!user) {
                return done(null,
                    false,
                    { message: Constants.ERROR_MESSAGE_NO_USER }
                );
            }

            // Extra validation around uid
            // Protects against stolen id_token
            if (payload.uid !== user.uid) {
                return done(null,
                    false,
                    { message: Constants.ERROR_MESSAGE_UNAUTHORIZED });
            }

            // If the user is found but is not DBA
            let isDBA: boolean = false;
            user.orgs.buildings.forEach((building: IUserBuilding) => {
                if (building.isAdmin) {
                    isDBA = true;
                } else if (user.orgs.isAdmin) {
                    isDBA = true;
                }
            });

            if (!isDBA) {
                return done(null,
                    false,
                    { message: Constants.ERROR_MESSAGE_UNAUTHORIZED });
            }

            // Otherwise all is well; return successful user
            return done(null, user);
        })
        .catch((error) => {
            return done(new UnauthorizedError("firebase_token_unverified", error));
        });
    }));

    // # JWT Refresh
    // Does not check isDBA
    // By default, if there is no name, it would just be called 'jwt'
    passport.use("live-refresh", new JwtStrategy({
            secretOrKey : configContext.JWTLiveSecret,
            jwtFromRequest : ExtractJwt.fromAuthHeaderWithScheme("Bearer"),
            ignoreExpiration: true,
            // Allow the entire request to be passed back to the callback
            passReqToCallback : true
        },
        (req: Request, payload: any, done: VerifiedCallback) => {
        // Verify id_token is sent
        if (!req.body.id_token) {
            // ### Verify Callback
            // Invoke `done` with `false` to indicate authentication
            // failure
            return done(null,
                false,
                // Return info message object
                { message: Constants.ERROR_MESSAGE_NO_TOKEN }
            );
        }

        // Validate firebase id_token (JWT) and get user's id
        FirebaseAdminWrapper.GetInstance().VerifyIdToken(req.body.id_token)
        .then((uid) => {
            return UserAdminApiInternal.GetUser(uid);
        })
        .then((user) => {
            // If no user is found, return a message
            if (!user) {
                return done(null,
                    false,
                    { message: Constants.ERROR_MESSAGE_NO_USER }
                );
            }

            // Extra validation around uid
            // Protects against stolen id_token
            if (payload.uid !== user.uid) {
                return done(null,
                    false,
                    { message: Constants.ERROR_MESSAGE_UNAUTHORIZED });
            }

            // If the user is found but is not active

            // Otherwise all is well; return successful user
            return done(null, user);
        })
        .catch((error) => {
            return done(new UnauthorizedError("firebase_token_unverified", error));
        });
    }));

    passport.use("portal-first", new LocalStrategy({
            // By default, the local strategy uses email and password
            usernameField : "phone",
            passwordField : "id_token",
            // Allow the entire request to be passed back to the callback
            passReqToCallback : true
        },
        async (req, phone, id_token, done) => {
        // Validate our self generated id_token and get user's id
        let response: any = {
            user: null,
            bookingID: null,
            serviceID: null
        };
        AuthUtil.VerifyLoginHint(phone, id_token)
        .then(async (result: string) => {
            console.log("Result ", result);
            let newParts: string[] = result.split("+___", 8);
            let orgID: string = newParts[5];
            let buildingID: string = newParts[6];
            let serviceID: string = newParts[3];
            let bookingID: string = newParts[2];
            let userID: string = newParts[7];
            response.bookingID = bookingID;
            response.serviceID = serviceID;
            console.log("orgID ", orgID, buildingID, bookingID);
            userID = userID;
            const bookingPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_SCHEDULES_FSLASH + buildingID + Constants.STR_FSLASH + bookingID;
            return UserAdminApiInternal.GetUser(userID);
        })
        .then((user) => {
            response.user = user;
            // Otherwise all is well; return successful user
            return done(null, response);
        })
        .catch((error) => {
            return done(new UnauthorizedError("Booking not available", "This booking is unavailable"));
        });
    }));

    passport.use("portal-second", new LocalStrategy({
            // By default, the local strategy uses email and password
            usernameField : "phone",
            passwordField : "id_token",
            // Allow the entire request to be passed back to the callback
            passReqToCallback : true
        },
        async (req, phone, id_token, done) => {
        // Validate our self generated id_token and get user's id
        AuthUtil.VerifyWebJWTAccessToken(id_token, configContext.JWTLiveSecret)
        .then((user) => {
            return UserAdminApiInternal.GetUser(user.uid);
        })
        .then((user) => {
            // If no user is found, return a message
            if (!user) {
                return done(null,
                    false,
                    { message: Constants.ERROR_MESSAGE_NO_USER }
                );
            }
            // Otherwise all is well; return successful user
            return done(null, user);
        })
        .catch((error) => {
            return done(new UnauthorizedError("firebase_token_unverified", error));
        });
    }));
}