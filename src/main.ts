// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as http from "http";
import * as https from "https";
import * as fs from "fs";
import * as firebaseAdmin from "firebase-admin";

// Local modules
import { Logger } from "./common/Logger";
import { HTTPUtil } from "./common/HttpUtil";
import { ArrayUtil } from "./common/ArrayUtil";
import { Constants } from "./common/Constants";
import { IWorker } from "./interfaces/IWorker";
import { ConfigureSocketio } from "./httpcomponents/sockets/SocketBase";

import { ExpressWrapper } from "./wrappers/ExpressWrapper";
import { IStatusReporter } from "./interfaces/IStatusReporter";
import { StatusOrchestrator } from "./common/StatusOrchestrator";
import { GCloudStorageWrapper } from "./wrappers/GCloudStorageWrapper";
import { FirebaseAdminWrapper } from "./wrappers/FirebaseAdminWrapper";
import { IOrgInfo, IBuilding, ITask, IAssignee, IOrgBasic } from "./shared/SilverBrickTypes";
import { MailGunWrapper } from "./wrappers/MailGunWrapper";
import { PushWrapper } from "./wrappers/PushWrapper";
import { TwilioWrapper } from "./wrappers/TwilioWrapper";
import { StripeWrapper } from "./wrappers/StripeWrapper";
import { FirebaseWorkerFactory } from "./factories/FirebaseWorkerFactory";
import { FirebaseStorageWrapper } from "./wrappers/FirebaseStorageWrapper";
import { IConfigContext, ExecutionType, ScenarioType } from "./interfaces/IConfigContext";
import { ConfigContextFactory, ConfigContextType } from "./factories/ConfigContextFactory";
import { UserAdminApiInternal } from "./api/UserAdminApiInternal";
import { ServiceApiInternal } from "./api/ServiceApiInternal";
import { SilverBrickAdminApiInternal } from "./api/SilverBrickAdminApiInternal";

async function _main(): Promise<void> {
    try {
        // Retrieve the config context, containing all of the info needed to communicate with firebase
        // and FCM
        let configContext: IConfigContext = ConfigContextFactory.CreateInstance(ConfigContextType.CLI);

        Logger.SetConfigContext(configContext);

        if (configContext.IsReady()) {

            switch (configContext.ExecutionType) {
                case ExecutionType.CREATE_WORKER:
                    {
                        // Initialize firebase with the config
                        FirebaseAdminWrapper.GetInstance().Initialize(configContext);
                        await StatusOrchestrator.GetInstance().Initialize(configContext);

                        let worker: IWorker = FirebaseWorkerFactory.CreateInstance(
                            configContext);

                        // After the CreateInstance, the worker should now be running
                        break;
                    }

                case ExecutionType.CREATE_HTTP_SERVER:
                    {
                        // Initialize firebase with the config
                        FirebaseAdminWrapper.GetInstance().Initialize(configContext);
                        await StatusOrchestrator.GetInstance().Initialize(configContext);
                        // Initialize google cloud storage with the config
                        GCloudStorageWrapper.GetInstance().Initialize(configContext);
                        await StripeWrapper.GetInstance().Initialize(configContext.StripePubKey, configContext.StripeSecKey);
                        await TwilioWrapper.GetInstance().Initialize(configContext.TwillioAccountSID, configContext.TwillioAuthToken);
                        await MailGunWrapper.GetInstance().Initialize(configContext.MailgunAPIKey, configContext.MailgunDomain);

                        // let list: string[] = [];
                        // let tasks: ITask[] = await ServiceApiInternal.GetRecurringTasksMultiBldg();
                        // let taskAction = tasks.forEach(async (task: ITask) => {
                        //     if (null == task.photoGallery) {
                        //         list.push(task.buildingName);
                        //     }
                        // });
                        // await taskAction;
                        // setTimeout(() => {
                        //     fs.writeFile("need.json", JSON.stringify(list), (err) => {
                        //         console.log("Error", err);
                        //     });
                        // }, 4000);
                        /************************************************************************
                         * 
                         * ASSIGN TASKS
                         * 
                         ************************************************************************/
                        // let tasks: ITask[] = JSON.parse(fs.readFileSync("./assign.json", "utf8"));
                        // for (let task of tasks) {
                        //     task.assignees = [ { uid: "TrFWc5DSPsfncZUq8accyFr2eBn1", name: "Chris Bartley" }, { "name": "Tyrone Deliotte", "uid": "cHI6y6PWePaxkGwhgGmyTycgFhw1" } ];
                        //     await ServiceApiInternal.AssignTask(task.orgID, task.buildingID, task, task.authorID, task.authorName);
                        // }

                        /************************************************************************
                         * 
                         * MATCH ASSIGNED TASKS
                         * 
                         ************************************************************************/
                        // let tasks: any[] = JSON.parse(fs.readFileSync("./chris.json", "utf8"));
                        // let allTask: ITask[] = await ServiceApiInternal.GetRecurringTasksMultiBldg();
                        // let newTasks: ITask[] = [];
                        // let nonTasks: ITask[] = [];
                        // let matchAction = tasks.forEach((task: any) => {
                        //     let match: ITask = allTask.find((tsk: ITask) => {
                        //         let uid: string = `${ tsk.startTime.replace(" ", "") }_${ tsk.endTime.replace(" ", "") }_${ tsk.subject.trim() }`;
                        //         // console.log("uid", uid);
                        //         // console.log("task.match", task.match.replace("_ ", "_"));
                        //         return (task.match.replace("_ ", "_") === uid);
                        //     });
                        //     if (null != match) {
                        //         newTasks.push(match);
                        //         // tasks = tasks.filter((tks: any) => {
                        //         //     return (tks.id !== match.id);
                        //         // });
                        //         console.log("newTasks", newTasks);
                        //     }
                        //     else {
                        //         nonTasks.push(task);
                        //     }
                        // });
                        // await matchAction;
                        // fs.writeFile("assign.json", JSON.stringify(newTasks), (err) => {
                        //     console.log("Error", err);
                        // });

                        // if ((null != nonTasks) && (nonTasks.length > 0)) {
                        //     fs.writeFile("noassign.json", JSON.stringify(nonTasks), (err) => {
                        //         console.log("Error", err);
                        //     });
                        // }

                        /************************************************************************
                         * 
                         * IMPORT TASKS
                         * 
                         ************************************************************************/
                        // let tasks: any[] = JSON.parse(fs.readFileSync("./trash.json", "utf8"));
                        // let newTasks: ITask[] = [];
                        // let noID: ITask[] = [];
                        // let allOrgs: any[] = await SilverBrickAdminApiInternal.GetOrgsAllBuildings();
                        // // let taskAction = tasks.forEach(async (task: any) => {
                        // for (let task of tasks) {
                        //     // let newTask = <ITask>task;
                        //     // if ((null != newTask.buildingID) && (null != newTask.orgID)) {
                        //     //     let newT = await ServiceApiInternal.AddTask(newTask.orgID, newTask.buildingID, newTask.authorID, newTask);
                        //     //     console.log("task Added", newT);
                        //     // }
                        //     let taskNames: string[] = [];
                        //     let taskDays: number[] = [];
                        //     // if (task.Sunday !== "") {
                        //     //     taskNames.push(task.Sunday);
                        //     //     taskDays.push(0);
                        //     // }
                        //     // if (task.Monday !== "") {
                        //     //     taskNames.push(task.Monday);
                        //     //     taskDays.push(1);
                        //     // }
                        //     // if (task.Tuesday !== "") {
                        //     //     taskNames.push(task.Tuesday);
                        //     //     taskDays.push(2);
                        //     // }
                        //     // if (task.Wednesday !== "") {
                        //     //     taskNames.push(task.Wednesday);
                        //     //     taskDays.push(3);
                        //     // }
                        //     // if (task.Thursday !== "") {
                        //     //     taskNames.push(task.Thursday);
                        //     //     taskDays.push(4);
                        //     // }
                        //     // if (task.Friday !== "") {
                        //     //     taskNames.push(task.Friday);
                        //     //     taskDays.push(5);
                        //     // }
                        //     // if (task.Saturday !== "") {
                        //     //     taskNames.push(task.Saturday);
                        //     //     taskDays.push(6);
                        //     // }
                        //     let count: number = 0;
                        //     // let taskAction = taskNames.forEach(async (taskName: string) => {
                        //     // for (let taskName of taskNames) {
                        //         let newTask: ITask = {
                        //             authorName: "Nathan Deliotte",
                        //             authorID: "qfD6dkLEyrUf3JzXfIScK68XaBh2",
                        //             clientID: "",
                        //             clientName: task.buildingName,
                        //             clientUnit: "",
                        //             active: true,
                        //             icon: "",
                        //             status: "Assigned",
                        //             deleted: false,
                        //             notes: "",
                        //             date: new Date(Date.now()).getTime(),
                        //             category: "Recurring Task",
                        //             updatedRole: "Staff",
                        //             buildingName: task.buildingName,
                        //             subject: task.description,
                        //             description: task.description,
                        //             lastUpdated:  new Date(Date.now()).getTime(),
                        //             updateAuthor: "Nathan Deliotte",
                        //             buildingAddress: task.buildingName,
                        //             recurring: true,
                        //             startDate: new Date(Date.now()).getTime(),
                        //             durationAmount: "year(s)",
                        //             durationQuantity: 2,
                        //             scheduled: true,
                        //             scheduling: false,
                        //             repeats: "Custom",
                        //             repeatInterval: "Weekly",
                        //             // monthlyConvention: "Day of month",
                        //             repeatQuantity: 1,
                        //             // monthlyDays: [Number(task.monthlyDays) - 1]
                        //             monthlyWeekDays: [Number(task.monthlyWeekDays)]
                        //         };
                        //         let match: any = allOrgs.find((org: any) => {
                        //             return (org.name.toLowerCase().trim() === task.buildingName.toLowerCase().trim());
                        //         });
                        //         console.log("match", match);
                        //         if (null != match) {
                        //             newTask.orgID = match.orgID;
                        //             newTask.buildingID = match.id;
                        //             newTask.clientID = match.id;
                        //         }
                        //         newTask.assignees = [ { "name": task.assignee, "uid": task.assignID } ];
                        //         newTask.startTime = task.startTime;
                        //         newTask.endTime = task.endTime;
                        //         // newTasks.push(newTask);
                        //         if ((null != newTask.buildingID) && (null != newTask.orgID)) {
                        //             newTasks.push(newTask);
                        //             let newT = await ServiceApiInternal.AddTask(newTask.orgID, newTask.buildingID, newTask.authorID, newTask);
                        //             console.log("task Added", newTask);
                        //         }
                        //         else {
                        //             noID.push(newTask);
                        //             console.log("task no ID", newTask);
                        //         }
                        //         count = count + 1;
                        //     // }
                        //     // await taskAction;
                        // }
                        // // await taskAction;
                        // setTimeout(() => {
                        //     fs.writeFile("noid.json", JSON.stringify(noID), (err) => {
                        //         console.log("Error", err);
                        //     });

                        //     fs.writeFile("tasks.json", JSON.stringify(newTasks), (err) => {
                        //         console.log("Error", err);
                        //     });
                        // }, 4000);


                        /************************************************************************
                         * 
                         * IMPORT JOBBER TASKS
                         * 
                         ************************************************************************/
                        // let tasks: ITask[] = JSON.parse(fs.readFileSync("./blank.json", "utf8"));
                        // let taskAction = tasks.forEach(async (task: ITask) => {
                        //     if ((null != task.orgID) && (task.orgID !== "") && (null != task.buildingID) && (task.buildingID !== "")) {
                        //         let newT =  await ServiceApiInternal.AddTask(task.orgID, task.buildingID, task.authorID, task);
                        //         console.log("task Added", task);
                        //     }
                        // });

                        // let tasks: any[] = JSON.parse(fs.readFileSync("./blank2.json", "utf8"));
                        // let newTasks: ITask[] = [];
                        // let nonTasks: ITask[] = [];
                        // let allOrgs: any[] = await SilverBrickAdminApiInternal.GetOrgsAllBuildings();
                        // let assignTasks: any[] = JSON.parse(fs.readFileSync("./assign.json", "utf8"));
                        // let taskAction = tasks.forEach(async (task: ITask) => {
                        //     let newTask: ITask = task;
                        //     let uidOne: string = `${ task.startTime.replace(" ", "") }_${ task.endTime.replace(" ", "") }_${ task.subject.trim() }`;
                        //     let matchAssign: ITask = assignTasks.find((tsk: ITask) => {
                        //         let uid: string = `${ tsk.startTime.replace(" ", "") }_${ tsk.endTime.replace(" ", "") }_${ tsk.subject.trim() }`;
                        //         // console.log("uid", uid);
                        //         // console.log("task.match", task.match.replace("_ ", "_"));
                        //         return (uidOne === uid);
                        //     });
                        //     if (null != matchAssign) {
                        //         newTask.assignees = [ { uid: "TrFWc5DSPsfncZUq8accyFr2eBn1", name: "Chris Bartley" }, { "name": "Tyrone Deliotte", "uid": "cHI6y6PWePaxkGwhgGmyTycgFhw1" } ];
                        //         // tasks = tasks.filter((tks: any) => {
                        //         //     return (tks.id !== match.id);
                        //         // });
                        //     }
                        //     else {
                        //         newTask.assignees = [ { uid: "TrFWc5DSPsfncZUq8accyFr2eBn1", name: "Chris Bartley" } ];
                        //         nonTasks.push(task);
                        //     }
                        //     let startParts: string[] = task.startTime.split(":", 2);
                        //     let startMinutes: string = startParts[1].slice(0, 2);
                        //     let startPM: string = startParts[1].slice(2, 4);
                        //     let startHours: number = (startPM === "PM") ? Number(startParts[0]) + 12 : Number(startParts[0]);

                        //     let endParts: string[] = task.endTime.split(":", 2);
                        //     let endMinutes: string = endParts[1].slice(0, 2);
                        //     let endPM: string = endParts[1].slice(2, 4);
                        //     let endHours: number = (endPM === "PM") ? Number(endParts[0]) + 12 : Number(endParts[0]);
                        //     task.startTime = `${ startHours }:${startMinutes}`;
                        //     task.endTime = `${ endHours }:${endMinutes}`;
                        //     // let newT =  await ServiceApiInternal.AddTask(task.orgID, task.buildingID, task.authorID, task);
                        //     // console.log("task Added", task);

                        //     let match: any = allOrgs.find((org: any) => {
                        //         return (org.name === task.clientName);
                        //     });
                        //     if (null != match) {
                        //         newTask.orgID = match.orgID;
                        //         newTask.buildingID = match.id;
                        //         newTask.clientID = match.id;
                        //     }
                        //     newTask.monthlyWeekDays = [ Number(newTask.monthlyWeekDays) ];
                            
                        //     newTask.updatedRole = "Staff";
                        //     newTasks.push(newTask);
                        //     // console.log("newTask", newTask);
                        // });
                        // await taskAction;
                        // fs.writeFile("blank.json", JSON.stringify(newTasks), (err) => {
                        //     console.log("Error", err);
                        // });


                        /************************************************************************
                         * 
                         * IMPORT ORGS/BUILDINGS
                         * 
                         ************************************************************************/
                        // let orgs: any[] = JSON.parse(fs.readFileSync("./clients.json", "utf8"));
                        // for (let org of orgs) {
                        //     let newOrg: IOrgInfo = {
                        //         name: org.name,
                        //         phone: "",
                        //         address: {
                        //             line1: org.line1,
                        //             line2: org.line2,
                        //             city: org.city,
                        //             state: org.state,
                        //             zip: org.zip
                        //         },
                        //         id: "",
                        //         lastModified: 0
                        //     };
                        //     let newerOrg: IOrgBasic = await SilverBrickAdminApiInternal.CreateOrg(newOrg);
                        //     let newBuilding: IBuilding = {
                        //         info: {
                        //             name: org.name,
                        //             phone: "",
                        //             address: {
                        //                 line1: org.line1,
                        //                 line2: org.line2,
                        //                 city: org.city,
                        //                 state: org.state,
                        //                 zip: org.zip
                        //             },
                        //             type: "Residential",
                        //             owner: org.name
                        //         },
                        //         config: {
                        //             isEmailSendingEnabled: false,
                        //             isSMSSendingEnabled: false,
                        //             propertyCodes: ""
                        //         },
                        //         id: "",
                        //         orgID: newerOrg.id
                        //     };
                        //     SilverBrickAdminApiInternal.CreateBuilding(newerOrg.id, newBuilding);
                        //     console.log("newerOrg", newerOrg, newBuilding);
                        // }
                        // let allOrgs: IOrgInfo[] = await SilverBrickAdminApiInternal.GetOrgsInfo();
                        // let buildings: any[] = JSON.parse(fs.readFileSync("./building.json", "utf8"));
                        // console.log("buildings", buildings.length);
                        // let buildingAction = buildings.forEach(async (building: any) => {
                        //     let newBuilding: IBuilding = {
                        //         info: {
                        //             name: building.name,
                        //             phone: "",
                        //             address: {
                        //                 line1: building.line1,
                        //                 line2: building.line2,
                        //                 city: building.city,
                        //                 state: building.state,
                        //                 zip: building.zip
                        //             },
                        //             type: "Residential",
                        //             owner: building.owner
                        //         },
                        //         config: {
                        //             isEmailSendingEnabled: false,
                        //             isSMSSendingEnabled: false,
                        //             propertyCodes: ""
                        //         },
                        //         id: ""
                        //     };
                        //     let matchOrg: IOrgInfo = allOrgs.find((org: IOrgInfo) => {
                        //         return (org.name.toLowerCase() === newBuilding.info.owner.toLowerCase());
                        //     });
                        //     if (null != matchOrg) {
                        //         newBuilding.orgID = matchOrg.id;
                        //         SilverBrickAdminApiInternal.CreateBuilding(matchOrg.id, newBuilding);
                        //     }
                        // });
                        // await buildingAction;
                        let expressWrapperInstance = ExpressWrapper.GetInstance();
                        expressWrapperInstance.Initialize(configContext);
                        let expressApp = expressWrapperInstance.GetApp();
                        let hostedServer: http.Server | https.Server = null;
                        if (configContext.ShouldUseHTTPS) {
                            hostedServer = https.createServer(
                            {
                                key: fs.readFileSync(configContext.SSLKeyPath),
                                cert: fs.readFileSync(configContext.SSLCertPath),
                                passphrase: configContext.SSLSecret
                            },
                            expressApp);
                        }
                        else {
                            console.warn("WARNING!!! HTTP mode detected! NOTE THAT THIS MODE SHOULD NOT BE USED IN PRODUCTION!");
                            hostedServer = http.createServer(
                                expressApp
                            );
                        }
                        // Integrate socket.io
                        let socketIoServerRef: SocketIO.Server = ConfigureSocketio(hostedServer, configContext);
                        // Set the http port
                        let port: number | string | boolean = HTTPUtil.NormalizePort(configContext.HttpServerPort || Constants.DEFAULT_HTTP_PORT);
                        // Run http server
                        hostedServer.listen(port);
                        // Event listener for HTTP server "listening" event.
                        hostedServer.on("listening", () => {
                            let addr = hostedServer.address();
                            let bind = (typeof addr === "string") ? `Pipe ${addr}` : `Port ${addr.port}`;
                            console.log(`Listening on ${bind}`);
                        });

                        // Event listener for HTTP server "error" event.
                        hostedServer.on("error", (error: NodeJS.ErrnoException) => {
                            if (error.syscall !== "listen") {
                                throw error;
                            }
                            let bind = (typeof port === "string") ? `Pipe ${port}` : `Port ${port}`;
                            // Handle specific listen errors with friendly messages
                            switch (error.code) {
                                case Constants.EACCES:
                                    console.error(`${bind} requires elevated privileges`);
                                    process.exit(1);
                                    break;
                                case Constants.EADDRINUSE:
                                    console.error(`${bind} is already in use`);
                                    process.exit(1);
                                    break;
                                default:
                                    throw error;
                            }
                        });
                        // Error handler to ensure socket issues dont freeze the server
                        process.on("uncaughtException", function (exception: any) {
                            // handle or ignore error
                            console.log("[TRACE] Http server uncaughtException: ", exception);
                        });

                        // The http server should now be running
                        break;
                    }
                default:
                    throw new Error("Unexpected Execution Type hit!");
            }
        }
        else {
            Logger.Status("Exiting gracefully since the config context isn't ready...");
        }
    }
    catch (error) {
        Logger.Error("Service critically failed with: " + error);
    }
}
_main();