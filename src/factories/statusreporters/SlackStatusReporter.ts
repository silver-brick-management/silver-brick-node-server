// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { IStatusReporter, IncomingMessageCallbackType } from "../../interfaces/IStatusReporter";
import { Constants } from "../../common/Constants";
import { IConfigContext } from "../../interfaces/IConfigContext";

let SlackBot = require("slackbots");

export interface ISlackMessagePayload {
    type: string;
    text: string;
}

export class SlackStatusReporter implements IStatusReporter {

    _slackBot: any;
    _incomingCallbacks: IncomingMessageCallbackType[];
    _targetChannel: string;

    constructor() {
        // Nothing to do
        this._incomingCallbacks = [];
        this._slackBot = null;
        this._targetChannel = null;
    }

    HandleIncomingMessage(data: ISlackMessagePayload) {
        // Only 'message' types from Slack are valid
        // messages
        if (Constants.SLACK_EVENT_MESSAGE_NAME === data.type) {
            for (let callback of this._incomingCallbacks) {
                // Invoke registered callbacks
                callback(data.text);
            }
        }
        else {
            // Nothing to do
        }
    }

    Initialize(configContext: IConfigContext): void {
        this._targetChannel = configContext.SlackChannel;
        const botName: string = "silverbrick";
        const botToken: string = "xoxb-1829915228566-1860525943216-e4HgATurfeVS3G59TYZ8UGdn";

        this._slackBot = new SlackBot({
            token: "xoxb-1829915228566-1860525943216-e4HgATurfeVS3G59TYZ8UGdn", // Add a bot https://my.slack.com/services/new/bot and put the token
            name: "silverbrick",
            icon_url: Constants.DEFAULT_SLACK_BOT_ICON
        });

        let self = this;
        this._slackBot.on(
            Constants.SLACK_EVENT_MESSAGE_NAME,
            function(data: ISlackMessagePayload) {
                self.HandleIncomingMessage(data);
            });
    }

    SendStatus(message: string, channel: string = null): void {
         let params: any = {
            as_user: false
        };
        params["icon_url"] = Constants.DEFAULT_SLACK_BOT_ICON;
        try {
            // Best-effort
            this._slackBot.postMessageToChannel(
                (null === channel) ? this._targetChannel : channel,
                message,
                params);

        } catch (error) {
            // We use console.log here, so we don't have circular dependencies between the Logger and this Reporter
            // For these issues, we should inspect the docker logs directly on the server
            console.log("SlackStatusReporter, Error occurred while sending: " + error);
        }
    }

    RegisterIncomingMessageCallback(callback: IncomingMessageCallbackType) {
        this._incomingCallbacks.push(callback);
    }
}