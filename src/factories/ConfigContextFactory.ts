// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { CLIConfigContext } from "./configcontexts/CLIConfigContext";
import { MockConfigContext } from "./configcontexts/MockConfigContext";
import { IConfigContext } from "../interfaces/IConfigContext";

export enum ConfigContextType {
    CLI,
    MOCK
}

export class ConfigContextFactory {

    static CreateInstance(contextType: ConfigContextType): IConfigContext {
        let contextInstance: IConfigContext = null;

        switch (contextType) {
            case ConfigContextType.CLI:
                contextInstance = new CLIConfigContext();
                break;

            case ConfigContextType.MOCK:
                contextInstance = new MockConfigContext();
                break;

            default:
                throw new Error("Undefined ConfigContextType passed in");
        }

        contextInstance._initialize();

        return contextInstance;
    }
}