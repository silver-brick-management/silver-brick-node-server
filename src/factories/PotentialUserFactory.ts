import { IPotentialUser } from "../interfaces/IPotentialUser";
import { BasicPotentialUser } from "./users/BasicPotentialUser";

export enum PotentialUserType {
    BASIC
}

export class PotentialUserFactory {
    static CreateInstance(
        tempUserType: PotentialUserType,
        firstName: string,
        middleName: string,
        lastName: string,
        personID: string): IPotentialUser {

        let potentialUserInstance: IPotentialUser = null;

        switch (tempUserType) {
            case PotentialUserType.BASIC:
                potentialUserInstance = new BasicPotentialUser(firstName, middleName, lastName, personID);
                break;

            default:
                throw new Error("Unexpected Temp User Type! TempUserType - " + tempUserType);
        }

        return potentialUserInstance;
    }
}