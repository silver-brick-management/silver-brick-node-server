// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { IStatusReporter } from "../interfaces/IStatusReporter";
import { SlackStatusReporter } from "./statusreporters/SlackStatusReporter";
import { IConfigContext } from "../interfaces/IConfigContext";

export enum StatusReporterType {
    SLACK
}

export class StatusReporterFactory {

    static CreateInstance(statusReporterType: StatusReporterType, configContext: IConfigContext) {
        let reporterInstance: IStatusReporter = null;

        switch (statusReporterType) {
            case StatusReporterType.SLACK:
            {
                reporterInstance = new SlackStatusReporter();
                break;
            }

            default:
            {
                throw new Error("Invalid type was passed in!");
            }

        }

        reporterInstance.Initialize(configContext);

        return reporterInstance;
    }
}