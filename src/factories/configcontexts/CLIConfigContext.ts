// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as fs from "fs";
import * as uuid from "node-uuid";

// Local modules
import { Logger } from "../../common/Logger";
import { JSONUtil } from "../../common/JsonUtil";
import { Constants } from "../../common/Constants";
import { GCloudStorageWrapper } from "../../wrappers/GCloudStorageWrapper";
import { WorkerType, IWorker } from "../../interfaces/IWorker";
import { IConfigJsonData } from "../../interfaces/IConfigJsonData";
import { IConfigContext, ScenarioType, ExecutionType, PropertyMap } from "../../interfaces/IConfigContext";

declare type ArgHandlerCallback = (self: CLIConfigContext, args: string[]) => void;

interface ArgEntry {
    HandlerFunc: ArgHandlerCallback;
    ExecutionType: ExecutionType;
    HelpMessage: string[];
}

interface ArgBehaviorMap {
    [flag: string]: ArgEntry;
}

export class CLIConfigContext implements IConfigContext {

    /*
        Argument Handling
    */
    handleImageUpload(self: CLIConfigContext, args: string[]) {
        let configFilePath: string = args[0];
        let inputFilePath: string = args[1];

        if ((null == configFilePath) ||
            (null == inputFilePath)) {
            self.ShowHelp();
        }
        else {
            let jsonData: IConfigJsonData = null;

            try {
                // Load the JSON config file
                jsonData =  JSONUtil.ExpandJsonObject(JSON.parse(fs.readFileSync(configFilePath, "utf8")));
            }
            catch (e) {
                Logger.Error(e);
                self.ShowHelp();
                jsonData = null;
            }

            if (null !== jsonData) {
                Logger.Status("Loaded Config: " + jsonData.FriendlyName);

                try {
                    self.populatePropertiesFromJson(jsonData);

                    // Validation step: Only util configs (with no worker declaration can be defined)
                    if (WorkerType.NONE !== self._workerType) {
                        throw new Error("Passed in config cannot be a worker config, please use a non-worker config for this utility");
                    }
                    self._propertyMap[Constants.UPLOAD_IMAGE_PROPERTY_INPUT_FILE] = inputFilePath;
                    self._isReady = true;
                }
                catch (error) {
                    Logger.Error(error);
                }
            }
            else {
                Logger.Error("JsonData is null. This config context has not been initialized successfully.");
            }
        }
    }

    handleWorkerOrHttpStackArgs(self: CLIConfigContext, args: string[]) {
        let configFilePath: string = args[0];

        if (null === configFilePath) {
            self.ShowHelp();
        }
        else {
            let jsonData: IConfigJsonData = null;

            try {
                // Load the JSON config file
                jsonData = JSONUtil.ExpandJsonObject(JSON.parse(fs.readFileSync(configFilePath, "utf8")));

                if (null == jsonData) {
                    throw new Error("JSON data is null! File - " + configFilePath);
                }
            }
            catch (e) {
                Logger.Error(e);
                self.ShowHelp();
                jsonData = null;
            }

            if (null !== jsonData) {
                Logger.Status("Loaded Config: " + jsonData.FriendlyName);
                console.log("Details", jsonData);

                try {
                    self.populatePropertiesFromJson(jsonData);
                    self._isReady = true;
                }
                catch (e) {
                    Logger.Error(e);
                }
            }
            else {
                Logger.Error("JsonData is null. This config context has not been initialized successfully.");
            }
        }
    }

    private _argBehaviorMap: ArgBehaviorMap = {
        "-W": {
            HandlerFunc: this.handleWorkerOrHttpStackArgs,
            ExecutionType: ExecutionType.CREATE_WORKER,
            HelpMessage: [
                "    Execute a worker instance.",
                "",
                "    Usage:",
                "        node bin/main.js -W <config file path>"
            ]
        },
        "-H": {
            HandlerFunc: this.handleWorkerOrHttpStackArgs,
            ExecutionType: ExecutionType.CREATE_HTTP_SERVER,
            HelpMessage: [
                "    Start the API HTTP express server.",
                "",
                "    Usage:",
                "        node bin/main.js -H <config file path>"
            ]
        }
    };

    private _isReady: boolean;

    private _firebaseAdminServiceAccountJsonPath: string;

    private _configFriendlyName: string;

    private _firebaseDBName: string;

    private _firebaseServiceAccountName: string;

    private _firebaseAPIKey: string;

    private _firebaseCloudMessagingAPIKey: string;

    private _firebaseCloudMessagingSenderID: string;

    private _mailgunAPIKey: string;

    private _mailgunDomain: string;

    private _twillioAccountSID: string;

    private _twillioAuthToken: string;

    private _quickBooksClientID: string;

    private _quickBooksClientSecret: string;

    private _stripePubKey: string;

    private _stripeSecKey: string;

    private _workerType: WorkerType;

    private _workerUuid: string;

    private _propertyMap: PropertyMap;

    private _execType: ExecutionType;

    private _slackChannel: string;

    private _httpServerPort: number;

    private _JWTDbaSecret: string;

    private _JWTLiveSecret: string;

    private _JWTUserCPSecret: string;

    private _JWTExpireCP: string;

    private _JWTExpire: string;

    private _MorganLogging: string;

    private _SocketHandshakeTimeout: number;

    private _sslKeyPath: string;

    private _sslCertPath: string;

    private _sslSecret: string;

    private _shouldUseHTTPS: boolean;

    get ShouldUseHTTPS(): boolean {
        return this._shouldUseHTTPS;
    }

    get SSLKeyPath(): string {
        return this._sslKeyPath;
    }

    get SSLCertPath(): string {
        return this._sslCertPath;
    }

    get SSLSecret(): string {
        return this._sslSecret;
    }

    get MailgunAPIKey(): string {
        return this._mailgunAPIKey;
    }

    get MailgunDomain(): string {
        return this._mailgunDomain;
    }

    get TwillioAccountSID(): string {
        return this._twillioAccountSID;
    }

    get TwillioAuthToken(): string {
        return this._twillioAuthToken;
    }

    get StripePubKey(): string {
        return this._stripePubKey;
    }

    get StripeSecKey(): string {
        return this._stripeSecKey;
    }

    get ExecutionType(): ExecutionType {
        return this._execType;
    }

    get WorkerType(): WorkerType {
        return this._workerType;
    }

    get ScenarioType(): ScenarioType {
        return ScenarioType.LIVE;
    }

    get FirebaseAdminServiceAccountConfigJSONPath(): string {
        return this._firebaseAdminServiceAccountJsonPath;
    }

    get ConfigFriendlyName(): string {
        return this._configFriendlyName;
    }

    get FirebaseDBName(): string {
        return this._firebaseDBName;
    }

    get FirebaseServiceAccountName(): string {
        return this._firebaseServiceAccountName;
    }

    get FirebaseAPIKey(): string {
        return this._firebaseAPIKey;
    }

    get FirebaseStorageBucket(): string {
        return "silver-brick.appspot.com";
    }

    get FirebaseAuthDomain(): string {
        return "silver-brick.firebaseapp.com";
    }

    get FirebaseDBUrl(): string {
        return "https://" + this.FirebaseDBName + ".firebaseio.com";
    }

    get WorkerID(): string {
        return this.FirebaseServiceAccountName + "_" + this._workerUuid;
    }

    get FirebaseCloudMessagingAPIKey(): string {
        return this._firebaseCloudMessagingAPIKey;
    }

    get FirebaseCloudMessagingSenderID(): string {
        return this._firebaseCloudMessagingSenderID;
    }

    get SlackChannel(): string {
        return this._slackChannel;
    }

    get HttpServerPort(): number {
        return this._httpServerPort;
    }

    get JWTDbaSecret(): string {
        return this._JWTDbaSecret;
    }

    get JWTLiveSecret(): string {
        return this._JWTLiveSecret;
    }

    get JWTExpire(): string {
        return this._JWTExpire;
    }

    get JWTExpireCP(): string {
        return this._JWTExpireCP;
    }

    get JWTUserCPSecret(): string {
        return this._JWTUserCPSecret;
    }

    get QuickBooksClientID(): string {
        return this._quickBooksClientID;
    }

    get QuickBooksClientSecret(): string {
        return this.QuickBooksClientSecret;
    }

    get MorganLogging(): string {
        return this._MorganLogging;
    }

    get SocketHandshakeTimeout(): number {
        return this._SocketHandshakeTimeout;
    }

    /*
        Optional, useful when we want to dictate arguments
        for non-worker flags
    */
    get PropertyMap(): PropertyMap {
        return this._propertyMap;
    }

    constructor() {
        // Nothing to construct
        this._firebaseAdminServiceAccountJsonPath = null;
        this._firebaseDBName = null;
        this._firebaseServiceAccountName = null;
        this._firebaseAPIKey = null;
        this._firebaseCloudMessagingSenderID = null;
        this._configFriendlyName = null;
        this._isReady = false;
        this._workerUuid = null;
        this._propertyMap = {};
        this._execType = ExecutionType.NONE;
        this._slackChannel = null;
        this._httpServerPort = null;
        this._JWTDbaSecret = null;
        this._JWTLiveSecret = null;
        this._JWTExpire = null;
        this._JWTExpireCP = null;
        this._MorganLogging = null;
        this._SocketHandshakeTimeout = null;
        this._shouldUseHTTPS = true;
        this._stripePubKey = null;
        this._stripeSecKey = null;
        this._mailgunAPIKey = null;
        this._quickBooksClientID = null;
        this._quickBooksClientSecret = null;
        this._mailgunDomain = null;
    }

    private ShowHelp() {
        Logger.Info("Pantry Callback - 1.0!");
        Logger.Info("----");
        for (let temp in this._argBehaviorMap) {
            let argBehavior = this._argBehaviorMap[temp];
            Logger.Info(temp + ":");
            for (let temp2 of argBehavior.HelpMessage) {
                Logger.Info(temp2);
            }
            Logger.Info("");
        }
        Logger.Info("");
    }

    IsReady(): boolean {
        return this._isReady;
    }

    _initialize(): void {
        // Retrieve arguments from process
        let argList: string[] = process.argv;

        /*
            First param is always node, second param is always the passed in script to run.
            Any params afterwards we will parse and save. The third parameter represents the initial flag indicating behavior.

        */
        if (3 > argList.length) {
            this.ShowHelp();
            return;
        }

        // e.g. node main.js -W, and we're looking for the -W flag
        let argFlag: string = argList[2];

        let argBehaviorEntry: ArgEntry = this._argBehaviorMap[argFlag];

        if (null != argBehaviorEntry) {
            // Argument is valid!
            argBehaviorEntry.HandlerFunc(this, argList.slice(3) /* Scope down the argument list, to what are actually viable candidates */);

            // Assign execution type so the the code path knows, which destiny to follow
            this._execType = argBehaviorEntry.ExecutionType;
        }
        else {
            // Invalid flag found!
            Logger.Info("Invalid flag passed in! Flag - " + argFlag);
            this.ShowHelp();
            return;
        }
    }

    protected populatePropertiesFromJson(jsonData: IConfigJsonData) {
        this._firebaseAdminServiceAccountJsonPath = jsonData.FirebaseAdminServiceAccountJsonPath;
        this._firebaseAPIKey = jsonData.FirebaseAPIKey;
        this._firebaseCloudMessagingAPIKey = jsonData.FirebaseCloudMessagingAPIKey;
        this._firebaseCloudMessagingSenderID = jsonData.FirebaseCloudMessagingSenderID;
        this._firebaseDBName = jsonData.FirebaseDBName;
        this._firebaseServiceAccountName = jsonData.FirebaseServiceAccountName;
        this._configFriendlyName = jsonData.FriendlyName;
        this._mailgunAPIKey = (null != jsonData.MailgunAPIKey) ? jsonData.MailgunAPIKey : null;
        this._mailgunDomain = (null != jsonData.MailgunDomain) ? jsonData.MailgunDomain : null;
        this._stripePubKey = jsonData.StripePubKey;
        this._stripeSecKey = jsonData.StripeSecKey;
        this._twillioAccountSID = jsonData.TwillioAccountSID;
        this._twillioAuthToken = jsonData.TwillioAuthToken;
        this._slackChannel = jsonData.SlackChannel;
        this._quickBooksClientID = jsonData.QuickBooksClientID;
        this._quickBooksClientSecret = jsonData.QuickBooksClientSecret;
        this._httpServerPort = (null != jsonData.HttpServerPort) ? jsonData.HttpServerPort : Constants.DEFAULT_HTTP_PORT;
        this._JWTDbaSecret = (null != jsonData.JWTDbaSecret) ? jsonData.JWTDbaSecret : Constants.DEFAULT_JWT_TOKEN;
        this._JWTLiveSecret = (null != jsonData.JWTLiveSecret) ? jsonData.JWTLiveSecret : Constants.DEFAULT_JWT_TOKEN;
        this._JWTUserCPSecret = (null != jsonData.JWTUserCPSecret) ? jsonData.JWTUserCPSecret : Constants.DEFAULT_JWT_TOKEN;
        this._JWTExpireCP = (null != jsonData.JWTExpireCP) ? jsonData.JWTExpireCP : Constants.DEFAULT_JWT_CP_EXPIRATION;
        this._JWTExpire = (null != jsonData.JWTExpire) ? jsonData.JWTExpire : Constants.DEFAULT_JWT_EXPIRATION;
        this._MorganLogging = jsonData.MorganLogging;
        this._SocketHandshakeTimeout = (null != jsonData.SocketHandshakeTimeout) ? jsonData.SocketHandshakeTimeout : Constants.DEFAULT_SOCKET_HANDSHAKE_TIMEOUT;
        this._sslCertPath = (null != jsonData.SSLCertPath) ? jsonData.SSLCertPath : null;
        this._sslKeyPath = (null != jsonData.SSLKeyPath) ? jsonData.SSLKeyPath : null;
        this._sslSecret = (null != jsonData.SSLSecret) ? jsonData.SSLSecret : null;
        this._shouldUseHTTPS = (null != jsonData.ShouldUseHTTPS) ? jsonData.ShouldUseHTTPS : true /* HTTPS should always be on in production */;
        // Create a unique ID for this worker instance
        this._workerUuid = uuid.v4();

        if (null != jsonData.WorkerType) {
            switch (jsonData.WorkerType) {
                // Firebase queue workers
                case Constants.SPEC_DOWNSTREAM_WORKER_V1:
                {
                    this._workerType = WorkerType.DOWNSTREAM_V1;
                    break;
                }

                case Constants.SPEC_FATAL_ERROR_V1:
                {
                    this._workerType = WorkerType.ERROR_V1;
                    break;
                }

                case Constants.SPEC_UPSTREAM_WORKER_V1:
                {
                    this._workerType = WorkerType.UPSTREAM_V1;
                    break;
                }

                case Constants.SPEC_RPC_WORKER_V1:
                {
                    this._workerType = WorkerType.RPC_V1;
                    break;
                }

                // Basic firebase worker
                case Constants.SPEC_FEEDBACK_WORKER_V1:
                {
                    this._workerType = WorkerType.FEEDBACK_V1;
                    break;
                }

                case Constants.SPEC_API_WORKER_V1:
                {
                    this._workerType = WorkerType.API_REPORTER;
                    break;
                }

                default:
                {
                    throw new Error("Unknown spec type!");
                }
            }
        }
        else {
            // This path can be hit if we're not spawning workers
            this._workerType = WorkerType.NONE;
        }

        for (let prop in this) {
            // Using the '==' comparator so we can match 'undefined' definitions
            if (null == (<any>this)[prop]) {
                throw new Error(
                    "Property '" + prop + "' is null in the config file! Please check your config file to make sure none of the properties are null or undefined.");
            }
        }
    }
}