import { IConfigContext, ScenarioType, ExecutionType, PropertyMap } from "../../interfaces/IConfigContext";
import { WorkerType, IWorker } from "../../interfaces/IWorker";

export class MockConfigContext implements IConfigContext {
    get ExecutionType(): ExecutionType {
        return ExecutionType.NONE;
    }

    get FirebaseAdminServiceAccountConfigJSONPath(): string {
        return null;
    }

    get FirebaseDBName(): string {
        return null;
    }

    get FirebaseServiceAccountName(): string {
        return null;
    }

    get FirebaseAPIKey(): string {
        return null;
    }

    get FirebaseStorageBucket(): string {
        return null;
    }

    get FirebaseAuthDomain(): string {
        return null;
    }

    get FirebaseDBUrl(): string {
        return null;
    }

    get FirebaseCloudMessagingAPIKey(): string {
        return null;
    }

    get FirebaseCloudMessagingSenderID(): string {
        return null;
    }

    get MailgunDomain(): string {
        return null;
    }

    get MailgunAPIKey(): string {
        return null;
    }

    get TwillioAccountSID(): string {
        return null;
    }

    get TwillioAuthToken(): string {
        return null;
    }

    get StripePubKey(): string {
        return null;
    }

    get StripeSecKey(): string {
        return null;
    }

    get WorkerID(): string {
        return null;
    }

    get WorkerType(): WorkerType {
        return null;
    }

    get ConfigFriendlyName(): string {
        return null;
    }

    get SlackChannel(): string {
        return null;
    }

    get PropertyMap(): PropertyMap {
        return null;
    }

    get HttpServerPort(): number {
        return null;
    }

    get JWTDbaSecret(): string {
        return null;
    }

    get JWTLiveSecret(): string {
        return null;
    }

    get JWTExpire(): string {
        return null;
    }

    get JWTExpireCP(): string {
        return null;
    }

    get JWTUserCPSecret(): string {
        return null;
    }

    get MorganLogging(): string {
        return null;
    }

    get QuickBooksClientID(): string {
        return null;
    }

    get QuickBooksClientSecret(): string {
        return null;
    }

    get SocketHandshakeTimeout(): number {
        return null;
    }

    get ScenarioType(): ScenarioType {
        return ScenarioType.MOCK;
    }

    get SSLCertPath(): string {
        return null;
    }

    get SSLKeyPath(): string {
        return null;
    }

    get SSLSecret(): string {
        return null;
    }

    get ShouldUseHTTPS(): boolean {
        return null;
    }

    IsReady(): boolean {
        return true;
    }

    _initialize(): void {
    }
}