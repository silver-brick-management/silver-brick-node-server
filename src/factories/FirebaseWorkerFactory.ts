// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

/*
    Interfaces
*/
import { IWorker, WorkerType } from "../interfaces/IWorker";
import { IConfigContext } from "../interfaces/IConfigContext";
import { FirebaseAdminWrapper } from "../wrappers/FirebaseAdminWrapper";

/*
    Workers
*/
import { UpstreamWorker } from "./workers/UpstreamWorker";
import { DownstreamWorker } from "./workers/DownstreamWorker";
import { RpcWorker } from "./workers/RpcWorker";
import { ErrorWorker } from "./workers/ErrorWorker";
import { FeedbackWorker } from "./workers/FeedbackWorker";
import { Constants } from "../common/Constants";
import { Logger } from "../common/Logger";
import { FirebaseSpecEntryData, SpecEntry } from "../common/ServerInternalTypes";

/*
    Constants
*/
export class FirebaseWorkerFactory {

    static CreateInstance(configContext: IConfigContext): IWorker {

        let workerInstance: IWorker = null;

        switch (configContext.WorkerType) {
            case WorkerType.DOWNSTREAM_V1:
                workerInstance = new DownstreamWorker(
                    configContext);
                break;

            case WorkerType.UPSTREAM_V1:
                workerInstance = new UpstreamWorker(
                    configContext);
                break;

            case WorkerType.ERROR_V1:
                workerInstance = new ErrorWorker(
                    configContext);
                break;

            case WorkerType.RPC_V1:
                workerInstance = new RpcWorker(
                    configContext);
                break;

            case WorkerType.FEEDBACK_V1:
                workerInstance = new FeedbackWorker(
                    configContext);
                break;

            default:
                throw new Error("Unexpected spec type!");
        }

        workerInstance._initialize();

        return workerInstance;
    }
}