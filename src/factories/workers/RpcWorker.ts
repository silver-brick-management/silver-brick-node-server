// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { FirebaseQueueWorker, FirebaseQueueRootType } from "./FirebaseQueueWorker";
import { FirebaseAdminWrapper } from "../../wrappers/FirebaseAdminWrapper";
import { SpecEntry, FirebaseSpecEntryData, FirebaseQueueTaskPayload } from "../../common/ServerInternalTypes";
import { Constants } from "../../common/Constants";
import { Logger } from "../../common/Logger";
import { IConfigContext } from "../../interfaces/IConfigContext";

interface IRpcPayload {
    readonly CallbackID: string;
    readonly MethodName: string;
}

export class RpcWorker extends FirebaseQueueWorker {

    protected _specEntry: SpecEntry = new SpecEntry(
        Constants.SPEC_RPC_WORKER_V1,
                new FirebaseSpecEntryData(
                    Constants.RPC_START,
                    Constants.RPC_IN_PROGRESS,
                    null /* We're done here, no other state to go to */,
                    Constants.FATAL_ERROR_START,
                    Constants.ONE_MINUTE_IN_MILLISECONDS,
                    0));

    constructor(configContext: IConfigContext) {
        super(configContext, FirebaseQueueRootType.RPC);

        let self: RpcWorker = this;
        this.invokedCallback = (data: any, progress: string, resolve: any, reject: any) => {
            self.invokedCallbackWithSelf(
                self,
                data,
                progress,
                resolve,
                reject);
        };
    }

    _initialize(): void {
        super._initialize();

        Logger.Status("Initialized Rpc Worker!");
/*
        this.addTask(new FirebaseQueueTaskPayload(
            this._specEntry.Name,
            {
                DeviceID: "123",
                MethodName: "foo"
            },
            Constants.RPC_START));*/
    }

    protected invokedCallbackWithSelf(selfRef: RpcWorker, data: any, progress: string, resolve: any, reject: any) {

        let rpcPayload: IRpcPayload = data.Data;
        // Process queue event

        this.GetQueueRef()
            .child("out/" + rpcPayload.CallbackID)
            .set(
                {
                    Version: 1,
                    ResultCode: 0 /* S_OK */,
                    ErrorContext: null,
                    Payload: "Hello World!"
                });

        // Set the ready flag last
        this.GetQueueRef()
            .child("out/" + rpcPayload.CallbackID + "/status")
            .set(
                {
                    Ready: true
                });

        resolve(data);
    }
}