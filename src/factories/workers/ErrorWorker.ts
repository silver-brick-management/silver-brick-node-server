// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { IConfigContext } from "../../interfaces/IConfigContext";
import { FirebaseQueueWorker, FirebaseQueueRootType } from "./FirebaseQueueWorker";
import { SpecEntry, FirebaseSpecEntryData } from "../../common/ServerInternalTypes";
import { Constants } from "../../common/Constants";
import { Logger } from "../../common/Logger";

export class ErrorWorker extends FirebaseQueueWorker {

    protected _specEntry: SpecEntry = new SpecEntry(
            Constants.SPEC_FATAL_ERROR_V1,
            new FirebaseSpecEntryData(
                Constants.FATAL_ERROR_START,
                Constants.FATAL_IN_PROGRESS,
                null,
                null,
                Constants.ONE_MINUTE_IN_MILLISECONDS,
                0));

    constructor(configContext: IConfigContext) {
        super(configContext, FirebaseQueueRootType.PUSH);
    }

    _initialize(): void {
        super._initialize();
        Logger.Status("Initialized Error Worker: " + this._configContext.WorkerID);
    }

    protected invokedCallback(data: string, progress: string, resolve: any, reject: any) {
        super.invokedCallback(data, progress, resolve, reject);

        Logger.PayloadData("ERROR", JSON.stringify(data));

        resolve(null);
    }
}
