// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

/*
    Load non-typescript modules
*/
let Queue = require("firebase-queue");

/*
    Load typescript modules
*/
import { IConfigContext } from "../../interfaces/IConfigContext";
import { IWorker, WorkerType } from "../../interfaces/IWorker";
import { Constants } from "../../common/Constants";
import { Logger } from "../../common/Logger";
import { FirebaseAdminWrapper } from "../../wrappers/FirebaseAdminWrapper";
import { FirebaseSpecEntryData, SpecEntry, FirebaseQueueTaskPayload } from "../../common/ServerInternalTypes";
import { StatusOrchestrator } from "../../common/StatusOrchestrator";

/*
    Class definitions
*/

export class FirebaseWorker implements IWorker {
    readonly WorkerType: WorkerType;

    protected _configContext: IConfigContext;

    constructor(configContext: IConfigContext) {
        this._configContext = configContext;
    }

    protected HandleCancelRequest(): void {
        Logger.Status("Finished shutdown, exiting");

        process.exit(0);
    }

    _initialize(): void {
        /*
            Registering callbacks
        */

        // So our 'on' callbacks, can have access to 'this'
        let self = this;

        // Listen for CTRL-C/process kill, aka. the cancel request
        process.on(
            Constants.SIGINT,
            () => {
                Logger.Status("FirebaseWorker: Cancel request detected");

                self.HandleCancelRequest();
            });

        StatusOrchestrator.GetInstance().RegisterIncomingMessageCallback(
            (data: string) => {
                this.HandleIncomingMessage(this._configContext, data);
            });
    }

    protected HandleIncomingMessage(configContext: IConfigContext, data: string): void {
        switch (data) {
            case "ping":
            {
                Logger.Status("\n    ---> " + configContext.WorkerID + " is online! AKA: " + configContext.ConfigFriendlyName);
                break;
            }

            case "hi":
            case "hello":
            {
                Logger.Status("\n    ---> Hi!");
                break;
            }

            default:
            {
                break;
            }

        }
    }
}
