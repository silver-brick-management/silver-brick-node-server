// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { FirebaseQueueWorker, FirebaseQueueRootType } from "./FirebaseQueueWorker";
import { SpecEntry, FirebaseSpecEntryData, ISilverBrickTaskMessagePayloadV1, ISilverBrickNewServicePayloadV1, SilverBrickPushPredicate, ISilverBrickServiceAssignPayloadV1, ServiceAssignPushPredicate, ISilverBrickMessagesPayloadV1, IUpstreamFCMPayload, PushSound, PushPriority, NormalizedUserInfo } from "../../common/ServerInternalTypes";
import { Constants } from "../../common/Constants";
import { Logger, SessionLogBuilder, LogLevel, ReportBuilder } from "../../common/Logger";
import { IConfigContext, ScenarioType } from "../../interfaces/IConfigContext";
import { IReportBuilder } from "../../interfaces/IReportBuilder";
import { PushWrapper, IPushResponseResult } from "../../wrappers/PushWrapper";
import { MailGunWrapper } from "../../wrappers/MailGunWrapper";
import { StripeWrapper } from "../../wrappers/StripeWrapper";
import { MessagesPayLoadHandler } from "./handlers/MessagesPayLoadHandler";
import { ServicesPayloadHandler } from "./handlers/ServicesPayloadHandler";

import { NodeGcmSenderFactory } from "../../wrappers/factories/NodeGcmSenderFactory";

export class DownstreamWorker extends FirebaseQueueWorker {
    private _pushWrapperInstance: PushWrapper;
    private _mailGunInstance: MailGunWrapper;
    private _stripeInstance: StripeWrapper;

    protected _specEntry: SpecEntry = new SpecEntry(Constants.SPEC_DOWNSTREAM_WORKER_V1, new FirebaseSpecEntryData(Constants.DOWNSTREAM_START, Constants.DOWNSTREAM_IN_PROGRESS, null /* We're done here, no other state to go to */, Constants.FATAL_ERROR_START, Constants.ONE_MINUTE_IN_MILLISECONDS, 0));

    constructor(configContext: IConfigContext) {
        super(configContext, FirebaseQueueRootType.PUSH);

        this._pushWrapperInstance = null;
        this._mailGunInstance = null;
        this._stripeInstance = null;

        // Here, we're taking advantage of closure, so we can properly
        // reference the 'this' reference. Without this call pattern,
        // this would always be undefined in the callback.
        let self: DownstreamWorker = this;

        this.invokedCallback = (data: any, progress: string, resolve: any, reject: any) => {
            // Invoking the real call back, now
            self.invokedCallbackWithSelf(data)
                .catch(error => {
                    Logger.CatchError(error);
                })
                .then(() => {
                    // Best-effort, always resolve, despite errors so that
                    // the callback isn't destructed
                    resolve();
                });
        };
    }

    _initialize(): void {
        super._initialize();

        this._pushWrapperInstance = PushWrapper.CreateInstance(this._configContext.FirebaseCloudMessagingAPIKey, ScenarioType.LIVE);

      //  this._mailGunInstance = MailGunWrapper.GetInstance().CreateInstance(this._configContext.MailgunAPIKey, this._configContext.MailgunDomain);

        this._stripeInstance = StripeWrapper.CreateInstance(this._configContext.StripePubKey, this._configContext.StripeSecKey);

        Logger.Status("Initialized Downstream Worker: " + this._configContext.WorkerID);
    }

    /*
        Handlers
    */
    protected async invokedCallbackWithSelf(data: any) {
        let upstreamPayload: IUpstreamFCMPayload = <IUpstreamFCMPayload>data;

        let reportBuilder: IReportBuilder = ReportBuilder.CreateInstance("~~~ Downstream Push Report ~~~");

        reportBuilder.Add("Push Type", upstreamPayload.Type);
        reportBuilder.Add("Message ID", upstreamPayload.MessageID);

        try {
            switch (upstreamPayload.Type) {
                case Constants.TYPE_MESSAGE_PAYLOAD_V1:
                {
                    let data: ISilverBrickMessagesPayloadV1 = <ISilverBrickMessagesPayloadV1>upstreamPayload.Data;
                    await MessagesPayLoadHandler.HandlePayload(
                        this._pushWrapperInstance,
                        null,
                        reportBuilder,
                        data,
                        data.AuthorID);
                    break;
                }

                case Constants.TYPE_SERVICE_COMPLETE_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_ASSIGN_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_PROGRESS_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_UPDATED_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_REOPENED_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_NEW_PAYLOAD_V1:
                {
                    let data: ISilverBrickServiceAssignPayloadV1 = <ISilverBrickServiceAssignPayloadV1>upstreamPayload.Data;
                    await ServicesPayloadHandler.HandlePayload(
                        this._pushWrapperInstance,
                        null,
                        reportBuilder,
                        data,
                        upstreamPayload.UserID);
                    break;
                }

                case Constants.TYPE_SERVICE_MESSAGE_PAYLOAD_V1:
                {
                    let data: ISilverBrickTaskMessagePayloadV1 = <ISilverBrickTaskMessagePayloadV1>upstreamPayload.Data;
                    await ServicesPayloadHandler.HandlePayload(
                        this._pushWrapperInstance,
                        null,
                        reportBuilder,
                        data,
                        upstreamPayload.UserID);
                    break;
                }

                default:
                {
                    throw new Error("Invalid payload type found! Type: " + upstreamPayload.Type);
                }
            }
        } catch (error) {
            reportBuilder.Add("ERROR", JSON.stringify(error));
        } finally {
            Logger.Status(reportBuilder.toString());
        }
    }
}