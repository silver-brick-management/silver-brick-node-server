// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************
/*
    This class is responsible for handling upstream pushes from devices (via XMPP).
    Once the upstream push is received, a new task is created for the Downstream
    Worker to work on, to decide who the downstream push gets sent to
*/

import { FirebaseQueueWorker, FirebaseQueueRootType } from "./FirebaseQueueWorker";
import {
    SpecEntry,
    FirebaseSpecEntryData,
    FirebaseQueueTaskPayload,
    ISilverBrickTaskMessagePayloadV1,
    IUpstreamFCMPayload,
    ISilverBrickPayloadV1,
    SilverBrickPushPredicate
} from "../../common/ServerInternalTypes";
import { Logger, SessionLogBuilder, LogLevel, ReportBuilder } from "../../common/Logger";
import { Constants } from "../../common/Constants";
import { IConfigContext } from "../../interfaces/IConfigContext";
import { IReportBuilder } from "../../interfaces/IReportBuilder";
import { UserApiInternal } from "../../api/UserApiInternal";
import { ISilverBrickUser } from "../../shared/SilverBrickTypes";

export class UpstreamWorker extends FirebaseQueueWorker {

    protected _specEntry: SpecEntry = new SpecEntry(
        Constants.SPEC_UPSTREAM_WORKER_V1,
        new FirebaseSpecEntryData(
            Constants.UPSTREAM_START,
            Constants.UPSTREAM_IN_PROGRESS,
            Constants.DOWNSTREAM_START,
            Constants.FATAL_ERROR_START,
            Constants.ONE_MINUTE_IN_MILLISECONDS,
            0));

    constructor(configContext: IConfigContext) {
        super(configContext, FirebaseQueueRootType.PUSH);

        // Here, we're taking advantage of closure, so we can properly
        // reference the 'this' reference. Without this call pattern,
        // this would always be undefined in the callback.
        let self: UpstreamWorker = this;

        this.invokedCallback = (data: any, progress: string, resolve: any, reject: any) => {

            // Invoking the real call back, now
            self.invokedCallbackWithSelf(
                data)
            .then((data) => {
                resolve(data);
            })
            .catch((error) => {
                Logger.Error("Error occurred in upstream worker! Error: " + error);

                // Best-effort, we call resolve here beause we don't want the callback to destruct
                // becaus of a thrown error
                resolve();
            });
        };
    }

    _initialize(): void {
        super._initialize();

        Logger.Status("Initialized Upstream Worker: " + this._configContext.WorkerID);
    }

    private async _IsUserValid(userID: string, reportBuilder: IReportBuilder, logBuilder: SessionLogBuilder = null): Promise<Boolean> {
        if (userID) {
            let userInfo: ISilverBrickUser = await UserApiInternal.GetUserInfo(userID, logBuilder);
            let returnVal: Boolean = (null != userInfo);

            if (returnVal) {
                reportBuilder.Add("User Name", userInfo.firstName + " " + userInfo.lastName);
                reportBuilder.Add("User E-mail", userInfo.email);
            }
            if (logBuilder) {
                logBuilder.Log("Is the requesting user properly registered? Return Value: " + returnVal + " UserID: " + userID);
            }

            return returnVal;
        }
        else {
            if (logBuilder) {
                logBuilder.Log("_IsUserValid, the passed in user ID is null or undefined");
            }
            return false;
        }
    }

    protected async invokedCallbackWithSelf(data: any): Promise<any> {
        let reportBuilder: IReportBuilder = ReportBuilder.CreateInstance("~~~~ Upstream Push Report ~~~~");

        try {
            let upstreamPayload: IUpstreamFCMPayload = <IUpstreamFCMPayload> data;

            if (null != data) {
                Logger.Info("** START: Upstream Payload Data: **");
                Logger.Info(JSON.stringify(data));
                Logger.Info("** END: Upstream Payload Data **");
            }
            else {
                console.log("Payload is null?!");
            }

            reportBuilder.Add("User ID", upstreamPayload.UserID);
            reportBuilder.Add("FCM Reg Token", upstreamPayload.FromDeviceID);
            reportBuilder.Add("Message ID",  upstreamPayload.MessageID);
            reportBuilder.Add("Payload Timestamp", upstreamPayload.Timestamp);
            reportBuilder.Add("Payload type", upstreamPayload.Type);

            switch (upstreamPayload.Type) {
                case Constants.TYPE_SERVICE_MESSAGE_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_COMPLETE_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_ASSIGN_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_PROGRESS_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_PROGRESS_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_REOPENED_PAYLOAD_V1:
                case Constants.TYPE_SERVICE_NEW_PAYLOAD_V1:
                case Constants.TYPE_MESSAGE_PAYLOAD_V1:
                case Constants.TYPE_SilverBrick_V1:
                {
                    let userID: string = upstreamPayload.UserID;

                    /*
                        Payload validation business logic
                    */
                    if (userID) {
                        let isUserValid = await this._IsUserValid(userID, reportBuilder);
                        if (isUserValid) {
                            reportBuilder.Add("Is User Valid?", "The User is valid!!!");

                            // Resolve, successfully and pass along the upstream payload
                            return data;
                        }
                        else {
                            let errMessage: string = "UserID isn't valid! UserID: " + userID;

                            reportBuilder.Add("Error", errMessage);

                            throw new Error(errMessage);
                        }
                    }
                    else {
                        let errMessage: string = "The UserID in the upstream payload is null, so we failed payload validation";

                        reportBuilder.Add("Error", errMessage);

                        throw new Error(errMessage);
                    }
                }

                default:
                {
                    throw new Error("Invalid upstream payload type!");
                }
            }
        }
        catch (error) {
            Logger.Error("Error occurred in upstream worker! Error: " + error);
        }
        finally {
            Logger.Status(reportBuilder.toString());
        }
    }
}
