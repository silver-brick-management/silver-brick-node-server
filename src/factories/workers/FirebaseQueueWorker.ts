// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

/*
    Load non-typescript modules
*/
let Queue = require("firebase-queue");

import * as firebase from "firebase";

/*
    Load typescript modules
*/
import { IConfigContext } from "../../interfaces/IConfigContext";
import { IWorker, WorkerType } from "../../interfaces/IWorker";
import { Constants } from "../../common/Constants";
import { Logger } from "../../common/Logger";
import { FirebaseAdminWrapper } from "../../wrappers/FirebaseAdminWrapper";
import { FirebaseSpecEntryData, SpecEntry, FirebaseQueueTaskPayload } from "../../common/ServerInternalTypes";
import { StatusOrchestrator } from "../../common/StatusOrchestrator";
import { FirebaseWorker } from "./FirebaseWorker";
/*
    Internal types
*/
declare type FirebaseQueueCallbackType = (data: string, progress: string, resolve: any, reject: any) => void;

export enum FirebaseQueueRootType {
    PUSH,
    RPC
}

/*
    Class definitions
*/

export class FirebaseQueueWorker extends FirebaseWorker {
    private _queueRefNode: firebase.database.Reference;

    private _queue: any;

    protected _specEntry: SpecEntry = null;

    protected _queueRootType: FirebaseQueueRootType;

    constructor(configContext: IConfigContext, queueRootType: FirebaseQueueRootType) {
        super(configContext);

        this._queueRootType = queueRootType;
    }

    private completionHandler(error: Error) {
        if (null !== error) {
            Logger.Error("ERROR! " + error.message);
        }
        else {
            // Success, nothing to do
        }
    }

    InitializeQueueSpec() {
        if (null !== this._specEntry) {
            /*  Creates a JSON object with all of the queue specs, that should
                ultimately be written to the <firebase>/<queue path>/specs
                For example:
                {
                    upstream_worker: ...,
                    downstream_worker:...,
                    etc.
                }
            */
            let specCollection: any = {};
            specCollection[this._specEntry.Name] = this._specEntry.Data;

            // Create the spec collection under <firebase DB>/queue/specs
            this.GetQueueRef()
                .child(Constants.STR_SPECS)
                .update(
                    specCollection,
                    this.completionHandler);
        }
    }

    protected invokedCallback(data: any, progress: string, resolve: any, reject: any) {
        // Since the usage pattern requires classes to extned from
        // this worker class, do not reoslve/reject within this scope.
    }

    addTask(taskPayload: FirebaseQueueTaskPayload) {
        this.GetQueueRef()
        .child(Constants.STR_TASKS)
        .push(taskPayload);
    }

    protected GetQueueRef(): firebase.database.Reference {
        // Return the active queue reference node used by the worker
        return this._queueRefNode;
    }

    protected HandleCancelRequest(): void {
        Logger.Status("FirebaseQueueWorker: Cancel request detected, starting queue shutdown");

        this._queue.shutdown().then(
            () => {
                Logger.Status("Finished queue shutdown, exiting");

                process.exit(0);
            });
    }

    _initialize(): void {
        /*
            Registering callbacks
        */

        // Listen for CTRL-C/process kill, aka. the cancel request
        super._initialize();

        // Get the queue reference under the firebase DB tree
        switch (this._queueRootType) {

            case FirebaseQueueRootType.RPC:
                this._queueRefNode = FirebaseAdminWrapper.GetInstance().GetRpcQueueRef();
                break;

            case FirebaseQueueRootType.PUSH:
                this._queueRefNode = FirebaseAdminWrapper.GetInstance().GetPushQueueRef();
                break;

            default:
                throw new Error("Unexpected queue root type!");
        }

        this.InitializeQueueSpec();

        {
            // One worker for now
            let queueOptions: any = {
                specId: this._specEntry.Name,
                numWorkers: 4
            };

            this._queue = new Queue(
                this._queueRefNode,
                queueOptions,
                this.invokedCallback);
        }
    }
}
