// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file "LICENSE.txt", which can be found at the root of this
// project.
//
// *********************************************************

import { NewMessagePushPredicate, ISilverBrickMessagesPayloadV1, PushSound, PushPriority } from "../../../common/ServerInternalTypes";
import { Constants } from "../../../common/Constants";
import { Logger } from "../../../common/Logger";
import { NormalizedUserInfo } from "../../../common/ServerInternalTypes";

import { UserApiInternal } from "../../../api/UserApiInternal";
import { MessageApiInternal } from "../../../api/MessageApiInternal";
import { UserAdminApiInternal } from "../../../api/UserAdminApiInternal";

import { IPushResponseResult, PushWrapper } from "../../../wrappers/PushWrapper";
import { MailGunWrapper } from "../../../wrappers/MailGunWrapper";
import { IReportBuilder } from "../../../interfaces/IReportBuilder";
import { ISilverBrickUser, IUser } from "../../../shared/SilverBrickTypes";
import { UserChatRoom, MessageParticipant } from "../../../shared/ResponseTypes";
import { ArrayUtil } from "../../../common/ArrayUtil";

export class MessagesPayLoadHandler {
    public static async GetDevicesForFollowers(upstreamData: ISilverBrickMessagesPayloadV1, payloadUserID: string, reportBuilder: IReportBuilder): Promise<NewMessagePushPredicate> {
        const userRef: ISilverBrickUser = await UserApiInternal.GetUserInfo(payloadUserID);
        reportBuilder.Add(Constants.REPORT_USER_NAME, `${userRef.firstName} ${userRef.lastName}`);
        let payloadUser: IUser = await UserAdminApiInternal.GetUser(payloadUserID);
        // Get devices under this payload
        let users: MessageParticipant[] = await MessageApiInternal.GetRoomUsers(upstreamData.RoomID, upstreamData.SenderID);
        let userIDs: string[] = [];
        let userNames: string[] = [];
        for (let user of users) {
            if (upstreamData.AuthorID !== user.uid) {
                userIDs.push(user.uid);
                userNames.push(user.authour);
            }
        }
        let userDevicePairs: NormalizedUserInfo[] = await UserApiInternal.GetDevices(userIDs, [], reportBuilder);
        console.log("Devices ", userDevicePairs);

        const pushTitle: string = `Direct Message from ${upstreamData.AuthorName}:`;
        let pushPredicate: NewMessagePushPredicate = new NewMessagePushPredicate(
            userDevicePairs,
            pushTitle,
            upstreamData.PostType,
            true,
            Date.now(),
            upstreamData.Message);
        return pushPredicate;
    }

    /*
      Public methods
    */
    public static async HandlePayload(pushInstance: PushWrapper, mailGunInstance: MailGunWrapper, reportBuilder: IReportBuilder, upstreamData: ISilverBrickMessagesPayloadV1, payloadUserID: string): Promise<void> {
        const followerPushPredicate: NewMessagePushPredicate = await this.GetDevicesForFollowers(upstreamData, payloadUserID, reportBuilder);

        let responseBodies: IPushResponseResult[] = await pushInstance.SendPush(
            followerPushPredicate.Title,
            followerPushPredicate.Description,
            {
                PostType: Constants.TYPE_MESSAGE_PAYLOAD_V1,
                Timestamp: Date.now(),
                RoomID: upstreamData.RoomID
            },
            followerPushPredicate.UserDevicePair,
            PushSound.DEFAULT,
            PushPriority.HIGH,
            reportBuilder);

        for (let responseBody of responseBodies) {
            Logger.PushResponseResult(responseBody);
        }
    }
}

