// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file "LICENSE.txt", which can be found at the root of this
// project.
//
// *********************************************************

import { ServiceAssignPushPredicate, ISilverBrickNewServicePayloadV1, ISilverBrickServiceAssignPayloadV1, ISilverBrickTaskMessagePayloadV1, SilverBrickPushType, PushSound, PushPriority } from "../../../common/ServerInternalTypes";
import { Constants } from "../../../common/Constants";
import { Logger } from "../../../common/Logger";
import { NormalizedUserInfo } from "../../../common/ServerInternalTypes";

import { UserApiInternal } from "../../../api/UserApiInternal";
import { UserAdminApiInternal } from "../../../api/UserAdminApiInternal";
import { ServiceApiInternal } from "../../../api/ServiceApiInternal";

import { IPushResponseResult, PushWrapper } from "../../../wrappers/PushWrapper";
import { MailGunWrapper } from "../../../wrappers/MailGunWrapper";
import { IReportBuilder } from "../../../interfaces/IReportBuilder";
import { ISilverBrickUser, IAssignee, IUser, ITask } from "../../../shared/SilverBrickTypes";
import { UserChatRoom, MessageParticipant } from "../../../shared/ResponseTypes";
import { ArrayUtil } from "../../../common/ArrayUtil";

export class ServicesPayloadHandler {
    public static async GetDevicesForFollowers(upstreamData: ISilverBrickServiceAssignPayloadV1 | ISilverBrickNewServicePayloadV1 | ISilverBrickTaskMessagePayloadV1, payloadUserID: string, reportBuilder: IReportBuilder): Promise<ServiceAssignPushPredicate> {
        // Get devices under this payload
        const task: ITask = await ServiceApiInternal.GetTask(upstreamData.TaskID);
        let payloadUser: IUser = await UserAdminApiInternal.GetUser(payloadUserID);
        let userIDs: string[] = [];
        let userNames: string[] = [];
        let assigneeIDs: string[] = [];
        if (null != task.assignees) {
            task.assignees.forEach((assignee: IAssignee) => {
                assigneeIDs.push(assignee.uid);
            });
        }
        let userDevicePairs: NormalizedUserInfo[] = await UserApiInternal.GetDevices([payloadUserID, ...assigneeIDs], [], reportBuilder);
        let pushTitle: string = ``;
        if (upstreamData.PostType === SilverBrickPushType.REQUEST_MESSAGE) {
            if (payloadUserID === task.clientID) {
                pushTitle = `New Message in your request`;
            }
            else {
                pushTitle = `(${ task.buildingName }: ${ task.category } Request`;
            }
        }
        else {
            if (payloadUserID === task.clientID) {
                switch (upstreamData.PostType) {
                    case SilverBrickPushType.NEW_REQUEST:
                    {
                        pushTitle = `New Request`;
                        break;
                    }

                    case SilverBrickPushType.NEW_REQUEST:
                    {
                        pushTitle = `Request Re-Opened`;
                        break;
                    }

                    case SilverBrickPushType.SERVICE_ASSIGN:
                    {
                        pushTitle = `Request Assigned`;
                        break;
                    }

                    case SilverBrickPushType.SERVICE_COMPLETE:
                    {
                        pushTitle = `Request Complete`;
                        break;
                    }
                    
                    default:
                    {
                        pushTitle = `Your ${ task.category } request for ${ task.clientName } was updated`;
                        break;
                    }
                }
            }
            else {
                switch (upstreamData.PostType) {
                    case SilverBrickPushType.NEW_REQUEST:
                    {
                        pushTitle = `A new ${ task.category } request for ${ task.clientName } was submitted`;
                        break;
                    }

                    case SilverBrickPushType.SERVICE_ASSIGN:
                    {
                        if (!assigneeIDs.includes(payloadUserID)) {
                            pushTitle = `New Request`;
                        }
                        else {
                            pushTitle = `Request Assigned`;
                        }
                        break;
                    }

                    case SilverBrickPushType.REQUEST_UPDATED:
                    {
                        if (!assigneeIDs.includes(payloadUserID)) {
                            pushTitle = `Updated Request`;
                        }
                        else {
                            pushTitle = `Request Updated`;
                        }
                        break;
                    }

                    case SilverBrickPushType.REQUEST_UPDATED:
                    {
                        pushTitle = `Request Re-Opened`;
                        break;
                    }

                    case SilverBrickPushType.SERVICE_COMPLETE:
                    {
                        pushTitle = `Request Complete`;
                        break;
                    }
                    
                    default:
                    {
                        pushTitle = `The ${ task.category } request for ${ task.clientName } was updated by ${ upstreamData.AuthorName }`;
                        break;
                    }
                }
            }
        }
        reportBuilder.Add(Constants.REPORT_USER_NAME, `${payloadUser.firstName} ${payloadUser.lastName}`);
        reportBuilder.Add("Task Info", `${ task.subject } for ${ task.clientName }`);
        let pushPredicate: ServiceAssignPushPredicate = new ServiceAssignPushPredicate(
            userDevicePairs,
            pushTitle,
            upstreamData.PostType,
            true,
            Date.now(),
            upstreamData.Message);
        return pushPredicate;
    }

    /*
      Public methods
  */
    public static async HandlePayload(pushInstance: PushWrapper, mailGunInstance: MailGunWrapper, reportBuilder: IReportBuilder, upstreamData: ISilverBrickServiceAssignPayloadV1 | ISilverBrickTaskMessagePayloadV1 | ISilverBrickNewServicePayloadV1, payloadUserID: string): Promise<void> {
        const followerPushPredicate: ServiceAssignPushPredicate = await this.GetDevicesForFollowers(upstreamData, payloadUserID, reportBuilder);
        let postType: string = Constants.TYPE_SERVICE_ASSIGN_PAYLOAD_V1;
        switch (upstreamData.PostType) {
            case SilverBrickPushType.SERVICE_ASSIGN:
            {
                postType = Constants.TYPE_SERVICE_ASSIGN_PAYLOAD_V1;
                break;
            }

            case SilverBrickPushType.SERVICE_COMPLETE:
            {
                postType = Constants.TYPE_SERVICE_COMPLETE_PAYLOAD_V1;
                break;
            }

            case SilverBrickPushType.SERVICE_REOPENED:
            {
                postType = Constants.TYPE_SERVICE_REOPENED_PAYLOAD_V1;
                break;
            }

            case SilverBrickPushType.REQUEST_UPDATED:
            {
                postType = Constants.TYPE_SERVICE_UPDATED_PAYLOAD_V1;
                break;
            }

            case SilverBrickPushType.NEW_REQUEST:
            {
                postType = Constants.TYPE_SERVICE_NEW_PAYLOAD_V1;
                break;
            }

            case SilverBrickPushType.SERVICE_PROGRESS:
            {
                postType = Constants.TYPE_SERVICE_PROGRESS_PAYLOAD_V1;
                break;
            }
            
            default:
            {
                break;
            }
        }
        let responseBodies: IPushResponseResult[] = await pushInstance.SendPush(
            followerPushPredicate.Title,
            followerPushPredicate.Description,
            {
                PostType: postType,
                Timestamp: Date.now(),
                TaskID: upstreamData.TaskID
            },
            followerPushPredicate.UserDevicePair,
            PushSound.DEFAULT,
            PushPriority.HIGH,
            reportBuilder);

        for (let responseBody of responseBodies) {
            Logger.PushResponseResult(responseBody);
        }
    }
}

