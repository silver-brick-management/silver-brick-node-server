// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { FirebaseWorker } from "./FirebaseWorker";
import { IConfigContext } from "../../interfaces/IConfigContext";
import { FirebaseAdminWrapper, FirebaseSubscribeType } from "../../wrappers/FirebaseAdminWrapper";
import { Logger } from "../../common/Logger";
import { FeedbackItem, IFeedbackEntry } from "../../interfaces/IFeedback";
import { MailGunWrapper } from "../../wrappers/MailGunWrapper";
import { UserApiInternal } from "../../api/UserApiInternal";

export class FeedbackWorker extends FirebaseWorker {

    private _mailGunInstance: MailGunWrapper;

    constructor(configContext: IConfigContext) {
        super(configContext);
    }

    async HandleDataSnapshot(snapshot: firebase.database.DataSnapshot) {
        let feedbackMap: FeedbackItem = <FeedbackItem>snapshot.val();
        let userID: string = snapshot.key;


    }

    _initialize(): void {
        super._initialize();

        FirebaseAdminWrapper.GetInstance().Subscribe("/feedback",
            FirebaseSubscribeType.CHILD_ADDED,
            async (snapshot: firebase.database.DataSnapshot) => {
                // try {
                //     await this.HandleDataSnapshot(snapshot);
                // }
                // catch (error) {
                //     Logger.CatchError(error);
                // }
            });
    }
}