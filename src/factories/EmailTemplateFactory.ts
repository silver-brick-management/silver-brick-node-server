// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { IUserOrg, IPreferences } from "../shared/SilverBrickTypes";
import { Constants } from "../common/Constants";

export interface IBasicEmailTemplateParams {
    IsBasicTemplate: boolean;
    IsSignUpEmailTemplate: boolean;
    Intro: string;
    Body: string;
    Conclusion: string;
    FirstName?: string;
    Type: MailType;
}

export interface ISignUpEmailTemplateParams {
    IsBasicTemplate: boolean;
    IsSignUpEmailTemplate: boolean;
    Intro: string;
    Title: string;
    Body: string;
    Conclusion: string;
    FirstName?: string;
    OrgName: string;
    BuildingName: string;
    Link: string;
    Type: MailType;
}

export interface INewTaskEmailTemplateParams {
    IsNewTask: boolean;
    Intro: string;
    Title: string;
    Body: string;
    Conclusion: string;
    FirstName?: string;
    Message: string;
    Author: string;
    BuildingName: string;
    Type: MailType;
}


export enum MailType {
    BASIC,
    SIGNUP,
    SERVICE,
    NEW_TASK,
    NEW_MESSAGE
}

export class EmailTemplateFactory {
    static CreateBasicEmailTemplateParams(
        intro: string,
        body: string,
        conclusion: string): IBasicEmailTemplateParams {
        return {
            IsBasicTemplate: true,
            IsSignUpEmailTemplate: false,
            Intro: intro,
            Body: body,
            Conclusion: conclusion,
            Type: MailType.BASIC
        };
    }

    static CreateSignUpEmailTemplateParams(
        intro: string,
        title: string,
        body: string,
        orgName: string,
        buildingName: string,
        link: string,
        conclusion: string): ISignUpEmailTemplateParams {
        return {
            IsBasicTemplate: true,
            IsSignUpEmailTemplate: false,
            Intro: intro,
            Body: body,
            Title: title,
            Conclusion: conclusion,
            Type: MailType.SIGNUP,
            OrgName: orgName,
            BuildingName: buildingName,
            Link: link
        };
    }

    static CreateNewTaskEmailTemplateParams(
        intro: string,
        title: string,
        body: string,
        message: string,
        author: string,
        buildingName: string,
        conclusion: string): INewTaskEmailTemplateParams {
        return {
            IsNewTask: true,
            Intro: intro,
            Body: body,
            Title: title,
            Author: author,
            Conclusion: conclusion,
            Type: MailType.NEW_TASK,
            Message: message,
            BuildingName: buildingName
        };
    }

    static CreateNewMessageEmailTemplateParams(
        intro: string,
        title: string,
        body: string,
        message: string,
        author: string,
        buildingName: string,
        conclusion: string): INewTaskEmailTemplateParams {
        return {
            Author: author,
            IsNewTask: false,
            Intro: intro,
            Body: body,
            Title: title,
            Conclusion: conclusion,
            Type: MailType.NEW_MESSAGE,
            Message: message,
            BuildingName: buildingName
        };
    }
}