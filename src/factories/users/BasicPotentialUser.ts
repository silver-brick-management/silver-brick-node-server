import { IPotentialUser } from "../../interfaces/IPotentialUser";

export class BasicPotentialUser implements IPotentialUser {
    constructor(firstName: string, middleName: string, lastName: string, personID: string) {
        this._firstName = firstName;
        this._middleName = middleName;
        this._lastName = lastName;
        this._personID = personID;
    }

    _personID: string;
    get PersonID() {
        return this._personID || null;
    }

    _firstName: string;
    get FirstName() {
        return this._firstName || null;
    }

    _lastName: string;
    get LastName() {
        return this._lastName || null;
    }

    _middleName: string;
    get MiddleName() {
        return this._middleName || null;
    }

    IsEqual(potentialUser: IPotentialUser): boolean {
            /* Person ID comparisons */
        return (((null == this.PersonID) && (null == potentialUser.PersonID)) ||
            (this.PersonID === potentialUser.PersonID)) &&
            /* First Name comparisons */
            (((null == this.FirstName) && (null == potentialUser.FirstName)) ||
            (this.FirstName === potentialUser.FirstName)) &&
            /* Middle Name comparisons */
            (((null == this.MiddleName) && (null == potentialUser.MiddleName)) ||
            (this.MiddleName === potentialUser.MiddleName)) &&
            /* Last Name comparisons */
            (((null == this.LastName) && (null == potentialUser.LastName)) ||
            (this.LastName === potentialUser.LastName));
    }

    toJSON(): IPotentialUser {
        return {
            FirstName: this.FirstName,
            MiddleName: this.MiddleName,
            LastName: this.LastName,
            PersonID: this.PersonID
        };
    }
}