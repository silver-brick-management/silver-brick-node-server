// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Local modules
import { Constants } from "../../common/Constants";
import { StringUtil } from "../../common/StringUtil";
import {
    IUser,
    IUserOrg,
    ISilverBrickUser,
    IClient,
    IAddress,
    IUserBuilding,
    IProfile,
    IStaffProfile,
    IUserLocation,
    IPreferences
} from "../../shared/SilverBrickTypes";

export class NormalizedUser implements IUser {
    private _uid: string;
    private _firstName: string;
    private _lastName: string;
    private _address: IAddress;
    private _isAdmin: boolean;
    private _isVendor: boolean;
    private _clients: IClient[];
    private _role: string;
    private _email: string;
    private _phone: string;
    private _fcmRegToken: string;
    private _osType: string;
    private _photoURL: string;
    private _stripeID: string;
    private _orgs: IUserOrg;
    private _fullPhotoURL: string;
    private _lastSignedInAt: Date;
    private _createdAt: Date;
    private _propertyCodes: number;
    private _preferences: IPreferences;
    private _isFirstTime: boolean;
    private _lastActivity: number;
    private _isClockedIn: boolean;
    private _unit: string;
    private _calendarColor: string;
    private _calendarTextColor: string;

    constructor(uid: string,
                firstName: string,
                lastName: string,
                photoURL: string = null,
                email: string,
                phone: string,
                isAdmin: boolean,
                role: string,
                orgs: IUserOrg = null,
                lastSignedInAt: Date = null,
                createdAt: Date = null,
                fcmRegToken: string = null,
                address: IAddress = null,
                clients: IClient[] = null,
                osType: string = null,
                stripeID: string = null,
                propertyCodes: number = null,
                preferences: IPreferences = null,
                isVendor: boolean = null,
                isFirstTime: boolean = null,
                lastActivity: number = null,
                isClockedIn: boolean = null,
                unit: string = null,
                calendarColor: string = null,
                calendarTextColor: string = null) {

        if (!uid || !email) {
            throw new Error("Parameters cannot be null! userID: " +
                uid +
                ", Email: " +
                email);
        }
        this._uid = uid;
        this._orgs = orgs;
        this._firstName = firstName;
        this._lastName = lastName;
        this._email = email;
        this._phone = phone;
        this._isAdmin = isAdmin;
        this._createdAt = createdAt;
        this._lastSignedInAt = lastSignedInAt;
        this._osType = osType;
        this._stripeID = stripeID;
        this._preferences = preferences;
        this._photoURL = photoURL;
        this._clients = clients;
        this._fcmRegToken = fcmRegToken;
        this._address = address;
        this._role = role;
        this._propertyCodes = propertyCodes;
        this._isVendor = isVendor;
        this._calendarColor = calendarColor;
        this._lastActivity = lastActivity;
        this.isFirstTime = isFirstTime;
        this._isClockedIn = isClockedIn;
        this._unit = unit;
        this._calendarTextColor = calendarTextColor;
    }

    get uid(): string {
        return this._uid;
    }

    set uid(uid: string) {
        this._uid = uid;
    }

    get firstName(): string {
        return this._firstName;
    }

    set firstName(username: string) {
        this._firstName = StringUtil.ToNameCase(username);
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName(username: string) {
        this._lastName = StringUtil.ToNameCase(username);
    }

    get email(): string {
        return this._email;
    }

    set email(email: string) {
        this._email = StringUtil.ToNameCase(email);
    }

    get phone(): string {
        return this._phone;
    }

    set phone(phone: string) {
        this._phone = phone;
    }

    get photoURL(): string {
        return this._photoURL;
    }

    set photoURL(photoURL: string) {
        this._photoURL = photoURL || "";
    }

    get address(): IAddress {
        return this._address;
    }

    set address(address: IAddress) {
        this._address = address || null;
    }

    get isAdmin(): boolean {
        return this._isAdmin;
    }

    set isAdmin(isAdmin: boolean) {
        this._isAdmin = isAdmin || false;
    }

    get orgs(): IUserOrg {
        return this._orgs;
    }

    set orgs(or: IUserOrg) {
        this._orgs = or;
    }

    get fcmRegToken(): string {
        return this._fcmRegToken;
    }

    set fcmRegToken(tokens: string) {
        this._fcmRegToken = tokens || "";
    }

    get osType(): string {
        return this._osType;
    }

    set osType(type: string) {
        this._osType = type || "iOS";
    }

    get stripeID(): string {
        return this._stripeID;
    }

    set stripeID(stripeID: string) {
        this._stripeID = stripeID;
    }

    get role(): string {
        return this._role;
    }

    set role(pos: string) {
        this._role = pos;
    }

    get clients(): IClient[] {
        return this._clients;
    }

    set clients(ind: IClient[]) {
        this._clients = ind;
    }

    get fullPhotoURL(): string {
        return this._fullPhotoURL;
    }

    set fullPhotoURL(full: string) {
        this._fullPhotoURL = full;
    }

    get calendarColor(): string {
        return this._calendarColor;
    }

    set calendarColor(full: string) {
        this._calendarColor = full;
    }

    get calendarTextColor(): string {
        return this._calendarTextColor;
    }

    set calendarTextColor(full: string) {
        this._calendarTextColor = full;
    }

    get unit(): string {
        return this._unit;
    }

    set unit(unit: string) {
        this._unit = unit;
    }

    get lastSignedInAt(): Date {
        return this._lastSignedInAt;
    }

    set lastSignedInAt(lastSignedInAt: Date) {
        this._lastSignedInAt = lastSignedInAt;
    }

    get createdAt(): Date {
        return this._createdAt;
    }

    set createdAt(createdAt: Date) {
        this._createdAt = createdAt;
    }

    get propertyCodes(): number {
        return this._propertyCodes;
    }

    set propertyCodes(code: number) {
        this._propertyCodes = code;
    }

    get preferences(): IPreferences {
        return this._preferences;
    }

    set preferences(pref: IPreferences) {
        this._preferences = pref;
    }

    get isVendor(): boolean {
        return this._isVendor;
    }

    set isVendor(vendor: boolean) {
        this._isVendor = vendor || false;
    }

    get isClockedIn(): boolean {
        return this._isClockedIn;
    }

    set isClockedIn(isClockedIn: boolean) {
        this._isClockedIn = isClockedIn;
    }

    get isFirstTime(): boolean {
        return this._isFirstTime;
    }

    set isFirstTime(newValue: boolean) {
        this._isFirstTime = newValue || false;
    }

    get lastActivity(): number {
        return this._lastActivity;
    }

    set lastActivity(code: number) {
        this._lastActivity = code;
    }

    IsBuildingsEqual(newBuildings: IUserBuilding[]): boolean {
        console.log("IsBuildingsEqual", newBuildings);
        if (!newBuildings) {
            return false;
        }

        if (newBuildings.length !== this.orgs.buildings.length) {
            return false;
        }

        if (!(newBuildings instanceof Array)) {
            return false;
        }

        // Extract building ids from the org in this object
        let oldBuildingIDs: string[] = this.orgs.buildings.map((oldBuilding: IUserBuilding, index: number) => {
            return oldBuilding.id;
        });

        return newBuildings.every((newBuilding, index) => {
            return (oldBuildingIDs.indexOf(newBuilding.id) > - 1);
        });
    }

    GetProfileData(): IStaffProfile {
        return {
            firstName: this.firstName,
            lastName: this.lastName,
            uid: this.uid,
            role: this.role
        };
    }

    GetDatabaseData(): ISilverBrickUser {
        // Serialize orgs
        let orgsObj: any = {};

        if (this.orgs) {
            let buildingsObj: any = {};
            if (this.orgs.buildings) {
                for (let building of this.orgs.buildings) {
                    let buildingObj: any = {
                        isAdmin: building.isAdmin
                    };
                    if (null != building.isBillingContact) {
                        buildingObj.isBillingContact = building.isBillingContact;
                    }
                    buildingsObj[building.id] = buildingObj;
                }
            }
            let orgObj: any = {
                isAdmin: this.orgs.isAdmin,
                buildings: buildingsObj
            };
            orgsObj[this.orgs.id] = orgObj;
        }
        let userBase: IStaffProfile = this.GetProfileData();
        let userExtra: any = {
            email: this.email,
            isAdmin: this.isAdmin,
            fcmRegToken: this.fcmRegToken,
            stripeID: this.stripeID,
            osType: this.osType,
            role: this.role,
            address: this.address,
            photoURL: this.photoURL,
            isVendor: this.isVendor,
            orgs: orgsObj
        };

        if (null != this.calendarColor) {
            userExtra.calendarColor = this.calendarColor;
        }

        if (null != this.calendarTextColor) {
            userExtra.calendarTextColor = this.calendarTextColor;
        }

        if (null != this.clients) {
            userExtra.clients = this.clients;
        }

        if (null != this.isFirstTime) {
            userExtra.isFirstTime = this.isFirstTime;
        }

        if (null != this.lastActivity) {
            userExtra.lastActivity = this.lastActivity;
        }

        if (null != this.isClockedIn) {
            userExtra.isClockedIn = this.isClockedIn;
        }

        if (null != this.unit) {
            userExtra.unit = this.unit;
        }

        if ((null != this.role) && (null != this.propertyCodes) && (this.role.toLowerCase() === "tenant")) {
            userExtra.propertyCodes = this.propertyCodes;
            userExtra.preferences = this.preferences;
        }

        let userFull: ISilverBrickUser = Object.assign(userBase, userExtra);
        return userFull;
    }

    // Returns a JSON object ready for firebase /user node
    toJSON(): IUser {
        let userBase: ISilverBrickUser = this.GetDatabaseData();

        let userExtra: any = {
            uid: this.uid,
            createdAt: this.createdAt,
            lastSignedInAt: this.lastSignedInAt
        };

        let userFull: IUser = Object.assign(userBase, userExtra);

        return userFull;
    }
}