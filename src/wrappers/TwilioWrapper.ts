// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// *********************************************************

// Node Modules
import * as morgan from "morgan"; // Log requests to the console
import * as express from "express";
let twilio = require("twilio");

// Loca Modules
import { Constants } from "../common/Constants";
import { Logger } from "../common/Logger";
import { IReportBuilder } from "../interfaces/IReportBuilder";
import { ISilverBrickUser } from "../shared/SilverBrickTypes";
import { MailGunWrapper } from "./MailGunWrapper";
import { StringUtil } from "../common/StringUtil";

export interface ITwilioResponseEntry {
    MessageID: string;
    Error: string;
    User: string;
    Success: boolean;
}

export interface ITwilioResponseResults {
    messageCount: number;
    phoneNumber: any;
}

export interface ITwilioMap {
    [phone: string]: ITwilioResponseResults;
}

export class TwilioWrapper {
    private _twillioInstance: any;
    private static _twillioWrapperInstance: TwilioWrapper = null;
    private _messagesSent: ITwilioMap = {};
    private constructor() {
        this._twillioInstance = null;
    }

    static CreateInstance(twillioAccountSID: string, twillioApiKey: string) {
        console.log("Twilio", twillioAccountSID, twillioApiKey);
        let twilloRef: TwilioWrapper = new TwilioWrapper();
        twilloRef.Initialize(twillioAccountSID, twillioApiKey);
        console.log("Twillio Created", twillioApiKey, twillioAccountSID);
        return twilloRef;
    }

    static GetInstance(): TwilioWrapper {
        if (null === this._twillioWrapperInstance) {
            this._twillioWrapperInstance = new TwilioWrapper();
        }
        else {
            // Already initialized, nothing more to do here
        }

        return this._twillioWrapperInstance;
    }

    Initialize(twillioAccountSID: string, twillioApiKey: string): void {
        console.log("Twilio", twillioAccountSID, twillioApiKey);
        this._twillioInstance = require("twilio")(twillioAccountSID, twillioApiKey);
        this._messagesSent = {};
        // this._twillioInstance.messaging.services("MG4dbed88eb4a5f2103e9ddc5054cff14d")
        //         .phoneNumbers
        //         .each((phoneNumbers: any) => {
        //             this._messagesSent[phoneNumbers.phone_number] = { phoneNumber: phoneNumbers.phone_number, messageCount: 0 };
        //         });
        // console.log("Messages", this._messagesSent);

        // this._twillioInstance.messaging.services
        // .create({
        //    statusCallback: "http://requestb.in/1234abcd",
        //    friendlyName: "Alert.Church Messaging Service"
        //  })
        // .then((service: any) => console.log(service.sid))
        // .done();
    }

    PurchaseNumber(): void {
        if (null != this._twillioInstance) {
            this._twillioInstance.availablePhoneNumbers("US")
            .local.list({
                areaCode: "646",
            })
            .then((data: any) => {
                const numbers = data[0];
                return this._twillioInstance.incomingPhoneNumbers.create({
                    phoneNumber: numbers.phoneNumber
                });
            })
            .then((purchasedNumber: any) => {
                console.log(purchasedNumber.sid);
                this._twillioInstance.messaging.services("MG4dbed88eb4a5f2103e9ddc5054cff14d")
                .phoneNumbers
                .create({ phoneNumberSid: purchasedNumber.sid })
                .then((phone_number: any) => console.log(phone_number.sid))
                .done();
            })
            .catch((error: any) => {
                console.log("Error Buying a number!", error);
            });
        }
        else {
            let twillioInstance = new twilio("ACa748a53835d2dd25eed689076d24b89d", "13386ee90f248f43b42de04b6691e73f");
            console.log("Twillio null!", "ACa748a53835d2dd25eed689076d24b89d", "13386ee90f248f43b42de04b6691e73f");
            twillioInstance.availablePhoneNumbers("US")
            .local.list({
                areaCode: "646",
            })
            .then((data: any) => {
                const numbers = data[0];
                return this._twillioInstance.incomingPhoneNumbers.create({
                    phoneNumber: numbers.phoneNumber
                });
            })
            .then((purchasedNumber: any) => {
                console.log(purchasedNumber.sid);
                this._twillioInstance.messaging.services("MG4dbed88eb4a5f2103e9ddc5054cff14d")
                .phoneNumbers
                .create({ phoneNumberSid: purchasedNumber.sid })
                .then((phone_number: any) => console.log(phone_number.sid))
                .done();
            })
            .catch((error: any) => {
                console.log("Error Buying a number!", error);
            });
        }
    }

    RemoveNumber(numbers: string): void {
        if (null != this._twillioInstance) {
            this._twillioInstance.messaging.services("MG4dbed88eb4a5f2103e9ddc5054cff14d")
                .phoneNumbers(numbers)
                .remove()
                .then((phone_number: any) => console.log(phone_number.sid))
                .done();
        }
        else {
            let twillioInstance = new twilio("ACa748a53835d2dd25eed689076d24b89d", "13386ee90f248f43b42de04b6691e73f");
            console.log("Twillio null!", "ACa748a53835d2dd25eed689076d24b89d", "13386ee90f248f43b42de04b6691e73f");
            twillioInstance.messaging.services("MG4dbed88eb4a5f2103e9ddc5054cff14d")
                .phoneNumbers(numbers)
                .remove()
                .then((phone_number: any) => console.log(phone_number.sid))
                .done();
        }
    }

    async SendSMS(num: string, msg: string, reportBuilder: IReportBuilder = null, mailGunWrapper: MailGunWrapper = null): Promise<ITwilioResponseEntry> {
        return new Promise<ITwilioResponseEntry>(
            (resolve, reject) => {
            let response: ITwilioResponseEntry = {
                MessageID: StringUtil.GenerateUUID(),
                Error: null,
                User: num,
                Success: null
            };
            console.log("Sending message to: " + num + " Msg: " + msg);
            if (null != this._twillioInstance) {
                this._twillioInstance.messages.create({
                    to: num,
                    messagingServiceSid: "MG4dbed88eb4a5f2103e9ddc5054cff14d",
                    body: msg || "N/A"
                })
                .then((message: any) => {
                    console.log("Message Status ", ((null != message.status) ? message.status : "null"));
                    if ((message.status !== "error") && (message.status !== "failed")) {
                        response.Success = true;
                        resolve(response);
                    }
                    else {
                        response.Success = false;
                        response.Error = message.error_message;
                        resolve(response);
                    }
                })
                .catch((error: any) => {
                    console.log("Error", error);
                    response.Error = error;
                    response.Success = false;
                    reject(response);
                });
            }
            else {
                let twillioInstance = new twilio("ACa748a53835d2dd25eed689076d24b89d", "13386ee90f248f43b42de04b6691e73f");
                console.log("Twillio null!", "ACa748a53835d2dd25eed689076d24b89d", "13386ee90f248f43b42de04b6691e73f");
                twillioInstance.messages.create({
                    to: num,
                    messagingServiceSid: "MG4dbed88eb4a5f2103e9ddc5054cff14d",
                    body: msg || "N/A"
                })
                .then((message: any) => {
                    console.log("Message Status ", ((null != message.status) ? message.status : "null"));
                    if ((message.status !== "error") && (message.status !== "failed")) {
                        response.Success = true;
                        resolve(response);
                    }
                    else {
                        response.Success = false;
                        response.Error = message.error_message;
                        resolve(response);
                    }
                })
                .catch((error: any) => {
                    console.log("Error", error);
                    response.Error = error;
                    response.Success = false;
                    reject(response);
                });
            }
        });
    }
}