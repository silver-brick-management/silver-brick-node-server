// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as Stripe from "stripe";
import {
    IShippingInformation,
    customers,
    cards,
    charges,
    orders,
    coupons,
    ICard,
    IListOptions,
    sources,
    IList,
    subscriptions,
    subscriptionItems,
    plans
} from "stripe";
import { Constants } from "../common/Constants";
import { ISilverBrickUser } from "../shared/SilverBrickTypes";

export class StripeWrapper {

    private _stripeInstance: Stripe;
    private static _stripeWrapperInstance: StripeWrapper = null;
    private static _initialized: boolean = false;

    private constructor() {
        this._stripeInstance = null;
    }

    static CreateInstance(stripePubKey: string, stripeSecKey: string): StripeWrapper  {
        let stripeRef: StripeWrapper = new StripeWrapper();

        stripeRef.Initialize(stripePubKey, stripeSecKey);
        // console.log("Stripe Created", stripePubKey, stripeSecKey);
        this._initialized = true;
        return stripeRef;
    }

    static GetInstance(): StripeWrapper {
        if (null === this._stripeWrapperInstance) {
            this._stripeWrapperInstance = new StripeWrapper();
        }
        return this._stripeWrapperInstance;
    }

    Initialize(stripePubKey: string, stripeSecKey: string): void {
        this._stripeInstance = new Stripe(stripeSecKey, "2018-02-28");
        this._stripeInstance.setApiVersion("2018-02-28");
        // console.log("Stripe Created", stripePubKey, stripeSecKey);
    }

    async CreateCustomer(email: string, address: IShippingInformation): Promise<string> {
        let customData: Stripe.customers.ICustomerCreationOptions = {
            email: email,
            shipping: address
        };
        let newCustomer: Stripe.customers.ICustomer = null;
        try {
            newCustomer = await this._stripeInstance.customers.create(customData);
        }
        catch (error) {
            console.log("Stripe CreateCustomer Error ", error);
            throw Error("Stripe CreateCustomer Error " + error);
        }
        return newCustomer.id;
    }

    async UpdateCustomer(stripeID: string, customer: customers.ICustomerUpdateOptions): Promise<customers.ICustomer> {
        const updatedCustomer: Stripe.customers.ICustomer = await this._stripeInstance.customers.update(stripeID, customer);
        try {
            const cardList: IList<ICard> = await this._stripeInstance.customers.listCards(stripeID);
            cardList.data.forEach(async (card: ICard) => {
                let cardOptions: cards.ICardUpdateOptions = {
                    address_city: customer.shipping.address.city,
                    address_line1: customer.shipping.address.line1,
                    address_line2: customer.shipping.address.line2,
                    address_state: customer.shipping.address.state,
                    address_zip: customer.shipping.address.postal_code
                };
                await this._stripeInstance.customers.updateCard(updatedCustomer.id, card.id, cardOptions);
            });
        }
        catch (error) {
            console.log("Stripe UpdateCustomer Error ", error);
            throw Error("Stripe UpdateCustomer Error " + error);
        }
        return updatedCustomer;
    }

    async DeleteCustomer(stripeID: string): Promise<void> {
        try {
            await this._stripeInstance.customers.del(stripeID);
        }
        catch (error) {
            console.log("Stripe Wrapper Deleting Org Error: " + error);
            throw new Error("Stripe Wrapper Deleting Org Error: " + error);
        }

    }

    async GetCustomer(stripeID: string): Promise<customers.ICustomer> {
        let customer: Stripe.customers.ICustomer = null;
        try {
            customer = await this._stripeInstance.customers.retrieve(stripeID);
        }
        catch (error) {
            console.log("Stripe GetCustomer Error ", error);
            throw Error("Stripe GetCustomer Error " + error);
        }
        return customer;
    }

    async GetPaymentInfo(stripeID: string): Promise<ICard[]> {
        let cardInfo: ICard[] = [];
        try {
            const cardData: IList<ICard> = await this._stripeInstance.customers.listCards(stripeID);
            cardInfo = cardData.data;
        }
        catch (error) {
            console.log("Stripe GetPaymentInfo Error ", error);
            throw Error("Stripe GetPaymentInfo Error " + error);
        }
        return cardInfo;
    }

    async ChangeDefaultMethod(stripeID: string, cardID: string): Promise<cards.ICard> {
        let update: cards.ICardUpdateOptions = {
            default_for_currency: true
        };
        try {
            const card: cards.ICard = await this._stripeInstance.customers.updateSource(stripeID, cardID, update);
            return card;
        }
        catch (error) {
            console.log("Stripe ChangeDefaultMethod Error ", error);
            throw Error("Stripe ChangeDefaultMethod Error " + error);
        }
    }

    async AddPaymentCard(stripeID: string, name: string, cardInfo: cards.ISourceCreationOptionsExtended): Promise<ICard> {
        const customer: customers.ICustomer = await this._stripeInstance.customers.retrieve(stripeID);
        let customData: cards.ISourceCreationOptionsExtended = {
            name: name,
            number: cardInfo.number,
            object: cardInfo.object,
            exp_month: cardInfo.exp_month,
            exp_year: cardInfo.exp_year,
            cvc: cardInfo.cvc,
        };
        if (null != customer.shipping.address) {
            customData.address_city = customer.shipping.address.city;
            customData.address_line1 = customer.shipping.address.line1;
            customData.address_line2 = customer.shipping.address.line2;
            customData.address_state = customer.shipping.address.state;
            customData.address_zip = customer.shipping.address.postal_code;
            customData.address_country = customer.shipping.address.country;
        }
        const newCustomer: Stripe.ICard = await this._stripeInstance.customers.createCard(stripeID, {card: customData});
        return newCustomer;
    }

    async DeletePaymentCard(stripeID: string, cardID: string): Promise<Stripe.IDeleteConfirmation> {
        const deleteConfirm: Stripe.IDeleteConfirmation = await this._stripeInstance.customers.deleteCard(stripeID, cardID);
        return deleteConfirm;
    }

    async ChargeCard(stripeID: string, email: string, cardID: string, price: number, size: string): Promise<charges.ICharge> {
        let newCharge: charges.ICharge = null;
        const chargeOptions: charges.IChargeCreationOptions = {
            amount: Math.round(price * 100),
            currency: "USD",
            capture: false,
            customer: stripeID,
            receipt_email: email,
            description: `SilverBrick cleaning for a ${size} apartment` + price,
            source: cardID
        };
        // console.log("Charge", chargeOptions);
        try {
            newCharge = await this._stripeInstance.charges.create(chargeOptions);
        }
        catch (error) {
            console.log("Stripe Order Creation Error ", error);
            throw Error("Stripe Order Creation Error " + error);
        }
        return newCharge;
    }

    async CreateCoupon(stripeID: string): Promise<string> {
        console.log("Referral Code", stripeID);
        let newCoupon: string = null;
        let options: coupons.ICouponCreationOptions = {
            duration: "forever",
            percent_off: 10
        };
        try {
            let coupon = await this._stripeInstance.coupons.create(options);
            newCoupon = coupon.id;
            console.log("New Coupon", newCoupon, coupon);
            return newCoupon;
        }
        catch (error) {
            console.log("Error Creating Coupon Code! ", error);
            throw new Error ("Error Creating Coupon Code!" + error);
        }
    }

    async DeleteCoupon(couponID: string): Promise<void> {
        try {
            let coupon = await this._stripeInstance.coupons.del(couponID);
            console.log("Deleted Coupon", coupon);
        }
        catch (error) {

        }
    }

    async GetSubscriptions(stripeID: string): Promise<subscriptions.ISubscription[]> {
        let subs: subscriptions.ISubscription[] = [];
        try {
            let list = await this._stripeInstance.customers.listSubscriptions(stripeID);
            subs = list.data;
            console.log("GetSubscriptions", subs);
        }
        catch (error) {
            console.log("Error GetSubscriptions! ", error);
            throw new Error ("Error GetSubscriptions!" + error);
        }
        return subs;
    }

    async CreateSubscription(stripeID: string, planType: string, cardID: string, cvc: string, numbers: string, couponCode: string = null): Promise<subscriptions.ISubscription> {
        let newSubscription: subscriptions.ISubscription = null;
        let card: cards.ICard = await this._stripeInstance.customers.retrieveCard(stripeID, cardID);
        let source: sources.ISourceCreationOptions = {
            object: "card",
            exp_month: card.exp_month,
            exp_year: card.exp_year,
            cvc: cvc,
            number: numbers
        };
        let subscriptionOptions: subscriptions.ISubscriptionCreationOptions = {
            customer: stripeID,
            plan: (planType === Constants.BASE_PLAN_ID) ? Constants.BASE_PLAN_ID : Constants.PREMIUM_PLAN_ID,
            // plan: planType,
            source: source,
            coupon: couponCode
        };
        // console.log("Options", subscriptionOptions);
        try {
            newSubscription = await this._stripeInstance.subscriptions.create(subscriptionOptions);
            // console.log("New Subscription", newSubscription, newSubscription.id);
            return newSubscription;
        }
        catch (error) {
            console.log("Stripe Subscription Creation Error ", error);
            throw Error("Stripe Subscription Creation Error " + error);
        }
    }

    async UpdateSubscription(stripeID: string, planID: string, newPlanID: string, cardID: string, cvc: string, numbers: string): Promise<subscriptions.ISubscription> {
        let updatedSubscription: subscriptions.ISubscription = null;
        let card: cards.ICard = await this._stripeInstance.customers.retrieveCard(stripeID, cardID);
        let updateOptions: subscriptions.ISubscriptionUpdateOptions = {
            plan: newPlanID,
            proration_date: new Date(Date.now()).getDate()
        };
        try {
            await this._stripeInstance.customers.updateSubscription(stripeID, planID, updateOptions);
        }
        catch (error) {
            console.log("Stripe Subscription Update Error ", error);
            throw Error("Stripe Subscription Update Error " + error);
        }
        return updatedSubscription;
    }

    async CancelSubscription(stripeID: string, planID: string): Promise<void> {
        try {
            await this._stripeInstance.customers.cancelSubscription(stripeID, planID);
        }
        catch (error) {
            console.log("Stripe Subscription Cancel Error ", error);
            throw Error("Stripe Subscription Cancel Error " + error);
        }
    }

    async DeleteSubscription(stripeID: string, planID: string): Promise<subscriptions.ISubscription> {
        let oldSubscription: subscriptions.ISubscription = null;
        try {
            oldSubscription = await this._stripeInstance.customers.cancelSubscription(stripeID, planID);
            return oldSubscription;
        }
        catch (error) {
            console.log("Stripe Subscription Deletion Error ", error);
            throw Error("Stripe Subscription Deletion Error " + error);
        }
    }
}