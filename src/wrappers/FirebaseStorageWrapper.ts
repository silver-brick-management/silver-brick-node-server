// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as firebase from "firebase";

// Local modules
import { Constants } from "../common/Constants";
import { IFirebaseConfig } from "../interfaces/IFirebase";
import { IConfigContext } from "../interfaces/IConfigContext";

export class FirebaseStorageWrapper {

    private static _firebaseStorageWrapperInstance: FirebaseStorageWrapper = null;

    private _isInitialized: boolean;

    private _configContext: IConfigContext;

    private _firebaseAppInstance: firebase.app.App;

    private constructor() {
        this._isInitialized = false;
    }

    // FirebaseStorageWrapper needs to be initialized before it can
    // be useful
    Initialize(configContext: IConfigContext): void {
        if (!this._isInitialized) {
            this._configContext = configContext;

            const firebaseConfig: IFirebaseConfig = {
                apiKey: configContext.FirebaseAPIKey,
                authDomain: configContext.FirebaseAuthDomain,
                databaseURL: configContext.FirebaseDBUrl,
                projectId: "silver-brick",
                storageBucket: configContext.FirebaseStorageBucket,
                messagingSenderId: configContext.FirebaseCloudMessagingSenderID
            };

            // Initialize firebase
            this._firebaseAppInstance = firebase.initializeApp(firebaseConfig);

            this._isInitialized = true;
        }
        else {
            // Already initialized
        }
    }

    UnInitialize(): void {
        if (null != this._firebaseAppInstance) {
            this._firebaseAppInstance.delete();
            this._firebaseAppInstance = null;
        }
        else {
            console.warn("No firebase app instance to uninitialize!");
        }
    }

    static GetInstance(): FirebaseStorageWrapper {
        if (null === this._firebaseStorageWrapperInstance) {
            this._firebaseStorageWrapperInstance = new FirebaseStorageWrapper();
        }
        else {
            // Already initialized, nothing more to do here
        }

        return this._firebaseStorageWrapperInstance;
    }


    /* Firebase storage. We should move to the google-cloud/storage sdk */

    GetStorageRef(path: string): firebase.storage.Reference {
        if (!this._isInitialized) {
            throw new Error("FirebaseStorageWrapper hasn't been initialized yet!");
        }

        return firebase.storage().ref(path);
    }

    GetStorageDownloadUrl(referencePath: string): Promise<string> {
        if (!referencePath) {
            throw new Error("referencePath cannot be null!");
        }

        return new Promise<string>(
            (resolve, reject) => {
                try {
                    this.GetStorageRef(referencePath)
                        .getDownloadURL()
                        .then((url: string) => {
                            resolve(url);
                        })
                        .catch((error: any) => {
                            reject(error);
                        });
                }
                catch (error) {
                    reject(error);
                }
            });
    }

    GetStorageMetadata(referencePath: string): Promise<any> {
        if (!referencePath) {
            throw new Error("referencePath cannot be null!");
        }

        return new Promise<any>(
            (resolve, reject) => {
                try {
                    this.GetStorageRef(referencePath)
                        .getMetadata()
                        .then((data: any) => {
                            resolve(data);
                        })
                        .catch((error: any) => {
                            reject(error);
                        });
                }
                catch (error) {
                    reject(error);
                }
            });
    }
}
