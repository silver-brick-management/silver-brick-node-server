// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import { Bucket, File, Storage, FileMetadata, ApiResponse, UploadOptions } from "@google-cloud/storage";

// Local modules
import { Constants } from "../common/Constants";
import { IFirebaseConfig } from "../interfaces/IFirebase";
import { IConfigContext } from "../interfaces/IConfigContext";

// Cant figure out how to properly leverage tyings file
let storage = require("@google-cloud/storage");

export class GCloudStorageWrapper {

    private static _gcloudStorageWrapperInstance: GCloudStorageWrapper = null;

    private _isInitialized: boolean;

    private _configContext: IConfigContext;

    private _gcloudStorageInstance: Storage;

    private _gcloudBucketInstance: Bucket;

    private constructor() {
        this._isInitialized = false;
    }

    // GCloudStorageWrapper needs to be initialized before it can
    // be useful
    Initialize(configContext: IConfigContext): void {
        if (!this._isInitialized) {
            this._configContext = configContext;

            // Initialize gcloud storage
            this._gcloudStorageInstance = storage({
                projectId: "silver-brick",
                keyFilename: configContext.FirebaseAdminServiceAccountConfigJSONPath
            });
            // Reference our firebase bucket
            this._gcloudBucketInstance = this._gcloudStorageInstance.bucket(configContext.FirebaseStorageBucket);

            this._isInitialized = true;
        }
        else {
            // Already initialized
        }
    }

    UnInitialize(): void {
        if (null != this._gcloudBucketInstance) {
            this._gcloudBucketInstance.delete();
            this._gcloudBucketInstance = null;
            this._gcloudStorageInstance = null;
        }
        else {
            console.warn("No gcloud bucket instance to uninitialize!");
        }
    }

    static GetInstance(): GCloudStorageWrapper {
        if (null === this._gcloudStorageWrapperInstance) {
            this._gcloudStorageWrapperInstance = new GCloudStorageWrapper();
        }
        else {
            // Already initialized, nothing more to do here
        }

        return this._gcloudStorageWrapperInstance;
    }


    /* GCloud storage. We should move to the google-cloud/storage sdk */

    GetFile(path: string): File {
        if (!this._isInitialized) {
            throw new Error("GCloudStorageWrapper hasn't been initialized yet!");
        }

        return this._gcloudBucketInstance.file(path);
    }

    GetDownloadUrl(referencePath: string): Promise<string> {
        if (!referencePath) {
            throw new Error("referencePath cannot be null!");
        }

        return new Promise<string>(
            (resolve, reject) => {
                try {
                    this.GetFile(referencePath)
                        .getSignedUrl({
                            action: "read",
                            expires: "03-17-2055",
                        })
                        .then((url: string[]) => {
                            resolve(url[0]);
                        })
                        .catch((error: any) => {
                            reject(error);
                        });
                }
                catch (error) {
                    reject(error);
                }
            });
    }

    GetMetadata(referencePath: string): Promise<FileMetadata> {
        if (!referencePath) {
            throw new Error("referencePath cannot be null!");
        }

        return new Promise<FileMetadata>(
            (resolve, reject) => {
                try {
                    this.GetFile(referencePath)
                        .getMetadata()
                        .then((data: [FileMetadata, ApiResponse]) => {
                            resolve(data[0]);
                        })
                        .catch((error: any) => {
                            reject(error);
                        });
                }
                catch (error) {
                    reject(error);
                }
            });
    }

    Upload(path: string, data: string, format: string, contentType: string): Promise<string> {
        return new Promise<string>(
            (resolve, reject) => {
                try {
                    this.Upload(path, data, format, contentType)
                    .then((value: string) => {
                        console.log("String");
                        resolve(value);
                    })
                    .catch((error: any) => {
                        console.log("Upload Error");
                        reject(error);
                    });
                }
                catch (error) {
                    reject(error);
                }
            });
    }
}
