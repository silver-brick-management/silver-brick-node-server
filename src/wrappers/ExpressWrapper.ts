// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// *********************************************************

// Node Modules
import * as morgan from "morgan"; // Log requests to the console
import * as express from "express";
import * as bodyParser from "body-parser"; // Pull information from HTML POST
import * as cookieParser from "cookie-parser"; // Pull information from cookies
import * as methodOverride from "method-override"; // Simulate DELETE and PUT

import { Passport } from "passport";

// Local modules
import { Constants } from "../common/Constants";
import { IConfigContext } from "../interfaces/IConfigContext";
import { ConfigureRoutes } from "../httpcomponents/routes/RouteBase";
import { ConfigurePassport } from "../httpcomponents/services/PassportService";

// Creates and configures an ExpressJS web server.
export class ExpressWrapper {

    private static _expressWrapperInstance: ExpressWrapper = null;

    private _isInitialized: boolean;

    private _configContext: IConfigContext;
    // ref to Express instance
    private _appInstance: express.Application = null;

    // Run configuration methods on the Express instance.
    private constructor() {
        this._isInitialized = false;
    }

    // ExpressApp needs to be initialized before it can
    // be useful
    Initialize(configContext: IConfigContext): void {
        if (!this._isInitialized) {
            this._configContext = configContext;
            // Initialize express
            this._appInstance = express();
            this.configureMiddleware();

            this._isInitialized = true;
        }
        else {
            // Already initialized
        }
    }

    GetApp(): express.Application {
        if (this._isInitialized) {
            return this._appInstance;
        }
        else {
            throw new Error("ExpressApp has not been initialized yet");
        }
    }

    static GetInstance(): ExpressWrapper {
        if (null === this._expressWrapperInstance) {
            this._expressWrapperInstance = new ExpressWrapper();
        }
        else {
            // Already initialized, nothing more to do here
        }

        return this._expressWrapperInstance;
    }

    // Configure Express middleware.
    private configureMiddleware(): void {
        // Read cookies (needed for authentication)
        this._appInstance.use(cookieParser());
        // Get all data/stuff of the body (POST) parameters
        // Parse application/json
        this._appInstance.use(bodyParser.json());
        // Parse application/vnd.api+json as json
        this._appInstance.use(bodyParser.json({ type: "application/vnd.api+json" }));
        // Parse application/x-www-form-urlencoded
        this._appInstance.use(bodyParser.urlencoded({ extended: true }));
        // Override with the X-HTTP-Method-Override header in the request. Simulate DELETE/PUT
        this._appInstance.use(methodOverride("X-HTTP-Method-Override"));

        // Log every request to the console if dev or test
        if (Constants.MORGAN_DEV === this._configContext.MorganLogging) {
            this._appInstance.use(morgan(Constants.ENV_DEVELOPMENT));
        }

        // Startup the passport middleware
        let passportInstance = new Passport();
        this._appInstance.use(passportInstance.initialize());
        // Pass Passport configuration our PassportJS instance
        ConfigurePassport(this._configContext, passportInstance);
        // Pass in instances of the express app, router, and passport
        ConfigureRoutes(this._configContext, this._appInstance, passportInstance);

        // Set the static files location /public/img will be /img for users
        this._appInstance.use(express.static(__dirname + "/public"));
    }
}