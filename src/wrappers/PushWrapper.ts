import { Logger } from "../common/Logger";
import { NormalizedUserInfo, PushSound, PushPriority, OperatingSystemType } from "../common/ServerInternalTypes";
import { ArrayUtil } from "../common/ArrayUtil";
import { ISilverBrickUser } from "../shared/SilverBrickTypes";
import { IReportBuilder } from "../interfaces/IReportBuilder";
import { IResponseBody, Message, INodeGcmSender } from "./interfaces/INodeGcmSender";
import { NodeGcmSenderFactory } from "./factories/NodeGcmSenderFactory";
import { ScenarioType } from "../interfaces/IConfigContext";
import { Constants } from "../common/Constants";

export interface IPushResponseEntry {
    MessageID: string;
    RegistrationID: string;
    Error: string;
    User: ISilverBrickUser;
    Username: string;
    Uid: string;
}

export interface IPushResponseResult {
    OperatingSystem: string;
    Success: number;
    Failure: number;
    CanonicalIds: number;
    MulticastId: number;

    FailedPushes: IPushResponseEntry[];
    SuccessfulPushes: IPushResponseEntry[];
}

interface OsTypeToUserInfoPair {
    OsType: OperatingSystemType;
    DeviceIDs: string[];
    UserInfo: NormalizedUserInfo[];
}

export class PushWrapper {

    private _gcmSender: INodeGcmSender;

    private _isInitialized: boolean;

    private constructor() {
        this._isInitialized = false;
    }

    static CreateInstance(firebaseCloudMessagingApiKey: string, scenarioType: ScenarioType) {
        let instanceVal: PushWrapper = new PushWrapper();
        instanceVal._initialize(firebaseCloudMessagingApiKey, scenarioType);
        return instanceVal;
    }

    private _initialize(firebaseCloudMessagingApiKey: string, scenarioType: ScenarioType): void {
        if (!this._isInitialized) {
            this._gcmSender = NodeGcmSenderFactory.CreateInstance(firebaseCloudMessagingApiKey, scenarioType);

            this._isInitialized = true;
        }
        else {
            throw Error("Invalid call pattern, already initialized!");
        }
    }

    private _sendPush(payload: any, deviceIDs: string[]): Promise<IResponseBody> {
        return new Promise<IResponseBody>((resolve, reject) => {
            let fcmMessage = new Message(payload);

            this._gcmSender.send(
                fcmMessage,
                {
                    registrationTokens: deviceIDs
                },
                function (err, response: IResponseBody) {
                    if (err) {
                        console.log("_sendPush: Error ", err);
                        reject(err);
                    }
                    else {
                        resolve(response);
                    }
                });
        });
    }

    private _sortNormalizedUserInfoByDeviceTypes(userInfoArr: NormalizedUserInfo[]): OsTypeToUserInfoPair[] {
        let osTypeToUserInfoPair: OsTypeToUserInfoPair[] = [];
        type OsTypeToUserInfoPairArrMap = {
            [osTypeStr: string]: OsTypeToUserInfoPair
        };
        let map: OsTypeToUserInfoPairArrMap = {};
        for (let userInfo of userInfoArr) {
            if ((null == userInfo.DeviceTuple) || (0 === userInfo.DeviceTuple.length)) {
                // No devices to push to!
                continue;
            }
            for (let deviceTuple of userInfo.DeviceTuple) {
                if (null == map[deviceTuple.OsType]) {
                    map[deviceTuple.OsType] = {
                        OsType: deviceTuple.OsType,
                        DeviceIDs: [],
                        UserInfo: []
                    };
                }
                map[deviceTuple.OsType].DeviceIDs.push(deviceTuple.DeviceID);
                map[deviceTuple.OsType].UserInfo.push(userInfo);
            }
        }
        let resultArr: OsTypeToUserInfoPair[] = [];
        for (let key of Object.keys(map)) {
            resultArr.push(map[key]);
        }

        return resultArr;
    }

    private _mergeResponseBody(responseBodyArr: IResponseBody[]) {
        let responseBody: IResponseBody = {
            success: 0,
            failure: 0,
            canonical_ids: 0,
            multicast_id: 0,
            results: []
        };

        for (let response of responseBodyArr) {
            responseBody.success += response.success;
            responseBody.failure += response.failure;
            responseBody.canonical_ids += response.canonical_ids;
            responseBody.multicast_id += (null != response.multicast_id) ? response.multicast_id : 0;
            responseBody.results = responseBody.results.concat((null != response.results) ? response.results : [/* Append nothing */]);
        }

        return responseBody;
    }

    async SendPush(
        _title: string,
        _message: string,
        _data: any,
        userInfoArr: NormalizedUserInfo[],
        soundType: PushSound,
        priorityLevel: PushPriority,
        reportBuilder: IReportBuilder = null): Promise<IPushResponseResult[]> {
        console.log("SendPush Push Wrapper");
        let responseBody: IPushResponseResult[] = [];
        let osTypeToUserInfoPairArr: OsTypeToUserInfoPair[] = this._sortNormalizedUserInfoByDeviceTypes(userInfoArr);

        if ((null == osTypeToUserInfoPairArr) ||
            (0 === osTypeToUserInfoPairArr.length)) {
            return [
                {
                    OperatingSystem: OperatingSystemType[OperatingSystemType.NONE],
                    Success: 0,
                    Failure: 0,
                    CanonicalIds: 0,
                    MulticastId: 0,

                    FailedPushes: [],
                    SuccessfulPushes: []
                }
            ];
        }
        else {
            // Continue on, since we actually have real pushes to work with
        }

        for (let osTypeToUserInfoPair of osTypeToUserInfoPairArr) {
            if ((null == osTypeToUserInfoPair.DeviceIDs) ||
                (0 === osTypeToUserInfoPair.DeviceIDs.length)) {
                Logger.Info("PushWrapper: Device ID array is empty, no devices to push to!");
                console.log("PushWrapper: Device ID array is empty, no devices to push to!");
                responseBody.push({
                            OperatingSystem: OperatingSystemType[osTypeToUserInfoPair.OsType],
                            Success: 0,
                            Failure: 0,
                            CanonicalIds: 0,
                            MulticastId: 0,
                            FailedPushes: [],
                            SuccessfulPushes: []
                        });
                continue;
            }
            else {
                let _priorityLevelStr: string = null;
                switch (priorityLevel) {
                    case PushPriority.HIGH:
                    {
                        _priorityLevelStr = "high";
                        break;
                    }

                    default:
                    {
                        throw new Error("Invalid priority level found!");
                    }

                }

                let _soundTypeStr: string = null;
                let _soundTypeExtStr: string = null;
                switch (soundType) {
                    case PushSound.DEFAULT:
                    {
                        _soundTypeStr = "default";
                        break;
                    }

                    default:
                    {
                        console.log("Invalid sound type found!");
                        throw new Error("Invalid sound type found!");
                    }

                }

                Logger.UserDevicePairs(userInfoArr);
                console.log("PushWrapper: Pairs ", userInfoArr.length);
                let messagePayload: any = null;

                switch (osTypeToUserInfoPair.OsType) {
                    case OperatingSystemType.ANDROID:
                        messagePayload = {
                            timeToLive: (60 * 5 /* 5 minutes for TTL */),
                            priority: _priorityLevelStr,
                            data: {
                                title: _title,
                                body: _message,
                                icon: "Increment",
                                sound: _soundTypeStr /* Android doesn't need the ext */,
                                "force-start": 1 /* Enable the app to be opened in background, even though force closed */
                            },
                            contentAvailable: true
                        };

                        // Union the data to build the android payload
                        for (let key in _data) {
                            messagePayload.data[key] = _data[key];
                        }
                        break;

                    case OperatingSystemType.IOS:
                    default:
                        messagePayload = {
                            timeToLive: (60 * 5 /* 5 minutes for TTL */),
                            priority: _priorityLevelStr,
                            data: _data,
                            notification: {
                                title: _title,
                                body: _message,
                                icon: "Increment",
                                sound: _soundTypeStr
                            },
                            contentAvailable: true
                        };
                        break;
                }

                try {
                    let fcmMessage = new Message();
                    let responseBodyArr: IResponseBody[] = [];

                    let chunkList: string[][] = ArrayUtil.DivideIntoChunks<string>(osTypeToUserInfoPair.DeviceIDs, 1000);
                    for (let chunk of chunkList) {
                        try {
                            let responseTemp: IResponseBody = await this._sendPush(
                                messagePayload,
                                chunk);
                            responseBodyArr.push(responseTemp);
                        }
                        catch (error) {
                            // Best-effort
                        console.log("Error sending push: diving to chunks ", error);
                        Logger.CatchError(error);
                        }
                    }

                    let response: IResponseBody = this._mergeResponseBody(responseBodyArr);

                    Logger.Info("Push payload sent down:" + JSON.stringify(messagePayload));
                    console.log("Push payload sent down:" + JSON.stringify(messagePayload));
                    let adaptedResponseBody: IPushResponseResult = PushWrapper.adaptResponseBody(response, osTypeToUserInfoPair.UserInfo, osTypeToUserInfoPair.OsType);

                    PushWrapper.reportResult(osTypeToUserInfoPair.OsType, adaptedResponseBody, reportBuilder);

                    responseBody.push(adaptedResponseBody);
                }
                catch (error) {
                    console.log("Error sending push", error);
                    Logger.Error(error);
                }
            }
        }
        return responseBody;
    }

    private static reportResult(osType: OperatingSystemType, adaptedResponseBody: IPushResponseResult, reportBuilder: IReportBuilder) {
        if (null != reportBuilder) {
            reportBuilder.Add("Push Successes (Count)", OperatingSystemType[osType] + " - " +  adaptedResponseBody.Success);
            reportBuilder.Add("Push Failures (Count)", OperatingSystemType[osType] + " - " + adaptedResponseBody.Failure);

            if (null != adaptedResponseBody.FailedPushes) {
                for (let entry of adaptedResponseBody.FailedPushes) {
                    reportBuilder.Add("Failed Pushes", {
                        OsType: OperatingSystemType[osType],
                        Uid: entry.Uid,
                        Name: entry.User.firstName + " " + entry.User.lastName,
                        Email: (null != entry.User) ? entry.User.email : "NaN",
                        fcmRegToken: (null != entry.RegistrationID) ? entry.RegistrationID : ("<failed FCM token from user> " + ((null != entry.User) ? entry.User.fcmRegToken : "null")),
                        Error: (null != entry.Error) ? entry.Error : "NaN"
                    });                }
            }
            else {
                // Null, nothing to do
            }

            if (null != adaptedResponseBody.SuccessfulPushes) {
                for (let entry of adaptedResponseBody.SuccessfulPushes) {
                    reportBuilder.Add("Successful Pushes", {
                        OsType: OperatingSystemType[osType],
                        Uid: entry.Uid,
                        Name: entry.User.firstName + " " + entry.User.lastName,
                        Email: (null != entry.User) ? entry.User.email : "NaN",
                        fcmRegToken: (null != entry.RegistrationID) ? entry.RegistrationID : (((null != entry.User) ? entry.User.fcmRegToken : "null")),
                    });
                }
            }
            else {
                // No successfull pushes available!
            }
        }
        else {
            // Nothing to do
        }
    }

    private static adaptResponseBody(responseSrc: IResponseBody, userDevicePair: NormalizedUserInfo[], osType: OperatingSystemType): IPushResponseResult {
        let responseResult: IPushResponseResult = {
            OperatingSystem: OperatingSystemType[osType],
            Success: responseSrc.success,
            Failure: responseSrc.failure,
            CanonicalIds: responseSrc.canonical_ids,
            MulticastId: responseSrc.multicast_id,
            FailedPushes: [/* Empty */],
            SuccessfulPushes: [/* Empty */]
        };

        if (userDevicePair.length !== responseSrc.results.length) {
            console.log("adaptResponseBody", "Sent user device pairs should match the length of the resulting array!");
            throw new Error("Sent user device pairs should match the length of the resulting array!");
        }

        for (let resultIndex in responseSrc.results) {
            let result = responseSrc.results[resultIndex];
            if (null == result.error) {
                responseResult.SuccessfulPushes.push({
                    MessageID: result.message_id,
                    RegistrationID: result.registration_id,
                    Error: result.error,
                    User: userDevicePair[resultIndex].User,
                    Username: userDevicePair[resultIndex].User.firstName + " " + userDevicePair[resultIndex].User.lastName,
                    Uid: userDevicePair[resultIndex].UserID
                });
            }
            else {
                // Failure push
                responseResult.FailedPushes.push({
                    MessageID: result.message_id,
                    RegistrationID: result.registration_id,
                    Error: result.error,
                    User: userDevicePair[resultIndex].User,
                    Username: userDevicePair[resultIndex].User.firstName + " " + userDevicePair[resultIndex].User.lastName,
                    Uid: userDevicePair[resultIndex].UserID
                });
            }
        }

        return responseResult;
    }
}