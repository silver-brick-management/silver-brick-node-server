import * as xml2js from "xml2js";
import * as fs from "fs";

export type ConvertableXmlString = xml2js.convertableToString;

export class XmlToJsonWrapper {

    static async ConvertXmlFileToJsonFile(srcXmlFilePath: string, tgtJsonFilePath: string) {
        let bufferedContent: ConvertableXmlString = fs.readFileSync(srcXmlFilePath);
        let jsonContent = await this.ConvertXmlStringToJsonString(bufferedContent);
        fs.writeFileSync(tgtJsonFilePath, JSON.stringify(jsonContent, null, 4));
    }

    static ConvertXmlStringToJsonString(xmlContent: ConvertableXmlString): Promise<string> {
        return new Promise<string>(
            (resolve, reject) => {
                try {
                    let parser = new xml2js.Parser({explicitArray : false});
                    parser.parseString(xmlContent,
                    (err: any, result: any) => {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(result);
                        }
                    });
                }
                catch (error) {
                    reject(error);
                }
            });
    }
}