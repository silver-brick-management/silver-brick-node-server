import { INodeGcmSender } from "../interfaces/INodeGcmSender";
import { LiveNodeGcm } from "./nodegcmsender/LiveNodeGcmSender";
import { MockNodeGcm } from "./nodegcmsender/MockNodeGcmSender";
import { ScenarioType } from "../../interfaces/IConfigContext";

export class NodeGcmSenderFactory {
    static CreateInstance(firebaseApiKey: string, scenarioType: ScenarioType): INodeGcmSender {
        let instance: INodeGcmSender = null;
        switch (scenarioType) {
            case ScenarioType.LIVE:
                instance = new LiveNodeGcm(firebaseApiKey);
                break;

            case ScenarioType.MOCK:
                instance = new MockNodeGcm();
                break;

            default:
                throw new Error("Invalid Mode!");
        }

        instance._initialize();

        return instance;
    }
}