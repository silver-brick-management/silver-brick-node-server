import * as gcm from "node-gcm";
import { INodeGcmSender } from "../../interfaces/INodeGcmSender";

export class LiveNodeGcm implements INodeGcmSender {
    private _gcmSender: gcm.Sender;
    private _isInitialized: boolean;
    private _firebaseCloudMessagingApiKey: string;

    constructor(firebaseCloudMessagingApiKey: string) {
        this._isInitialized = false;
        this._firebaseCloudMessagingApiKey = firebaseCloudMessagingApiKey;
    }

    _initialize(): void {
        if (!this._isInitialized) {
            if (!this._firebaseCloudMessagingApiKey) {
                throw new Error("Firebase cloud messaging key cannot be null or empty string!");
            }

            this._gcmSender = new gcm.Sender(this._firebaseCloudMessagingApiKey);

            this._isInitialized = true;
        }
        else {
            throw Error("Invalid call pattern, already initialized!");
        }
    }

    send(message: gcm.Message, registrationIds: string | string[] | gcm.IRecipient, callback: (err: any, resJson: gcm.IResponseBody) => void): void {
        this._gcmSender.send(message, registrationIds, callback);
    }
}