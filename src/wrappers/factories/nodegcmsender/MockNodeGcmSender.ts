import * as gcm from "node-gcm";
import { INodeGcmSender, IRecipient } from "../../interfaces/INodeGcmSender";
import { ArrayUtil } from "../../../common/ArrayUtil";

export class MockNodeGcm implements INodeGcmSender {
    static readonly MARK_SUCCEEDED: string = "mark_success";
    static readonly MARK_FAILED: string = "mark_failed";

    private _isInitialized: boolean;

    constructor() {
        this._isInitialized = false;
    }

    public _initialize() {
        if (!this._isInitialized) {
            this._isInitialized = true;
        }
        else {
            throw Error("Invalid call pattern, already initialized!");
        }
    }

    private _didDeviceSucceed(registrationID: string) {
        switch (registrationID) {
            case MockNodeGcm.MARK_SUCCEEDED:
                return true;

            case MockNodeGcm.MARK_FAILED:
                return false;

            default:
                throw new Error("Unexpected registration ID!");
        }
    }

    send(message: gcm.Message, registrationIds: string | string[] | gcm.IRecipient, callback: (err: any, resJson: gcm.IResponseBody) => void): void {
        let _success: number = null;
        let _failure: number = null;
        let _count: number = null;

        if (null != <IRecipient>registrationIds) {
            let recipient: IRecipient = <IRecipient> registrationIds;
            let actualIDs: string [] = recipient.registrationTokens;
            let tempSuccess: number = 0;
            let tempFailures: number = 0;

            _count = actualIDs.length;

            // Check if array
            for (let id of actualIDs) {
                if (this._didDeviceSucceed(id)) {
                    tempSuccess++;
                }
                else {
                    tempFailures++;
                }
            }
            _success = tempSuccess;
            _failure = tempFailures;
        }
        else {
            throw new Error("Unexpected param!");
        }

        let resultsArr: any[] = [];
        // Build results
        for (let i = 0; i < _success; i++) {

            resultsArr.push({
                message_id: MockNodeGcm.MARK_SUCCEEDED,
                registration_id: MockNodeGcm.MARK_SUCCEEDED,
                error: null
            });
        }

        for (let i = 0; i < _failure; i++) {

            resultsArr.push({
                message_id: MockNodeGcm.MARK_FAILED,
                registration_id: MockNodeGcm.MARK_FAILED,
                error: "RANDOM ERROR"
            });
        }

        resultsArr = ArrayUtil.ShuffleInPlace<any>(resultsArr);

        callback(null, {
            success: _success,
            failure: _failure,
            canonical_ids: _count,
            multicast_id: _count,
            results: resultsArr
        });
    }
}