// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as firebaseAdmin from "firebase-admin";

// Local modules
import { Constants } from "../common/Constants";
import { IFirebaseUser } from "../interfaces/IFirebase";
import { IConfigContext } from "../interfaces/IConfigContext";

declare type FirebaseDataSnapshotType = (snapshot: firebaseAdmin.database.DataSnapshot) => any;

export enum FirebaseSubscribeType {
    NONE,
    VALUE,
    CHILD_ADDED,
    CHILD_CHANGED
}

export class FirebaseAdminWrapper {

    private static _firebaseAdminWrapperInstance: FirebaseAdminWrapper = null;

    private _isInitialized: boolean;

    private _configContext: IConfigContext;

    private _firebaseAdminInstance: firebaseAdmin.app.App;

    private constructor() {
        this._isInitialized = false;
    }

    // FirebaseAdminWrapper needs to be initialized before it can
    // be useful
    Initialize(configContext: IConfigContext): void {
        if (!this._isInitialized) {
            this._configContext = configContext;

            const firebaseConfig: any = {
                databaseURL: configContext.FirebaseDBUrl,
                credential: firebaseAdmin.credential.cert(configContext.FirebaseAdminServiceAccountConfigJSONPath),
                databaseAuthVariableOverride: {
                    uid: configContext.FirebaseServiceAccountName
                }
            };

            // Initialize firebase
            this._firebaseAdminInstance = firebaseAdmin.initializeApp(firebaseConfig);

            this._isInitialized = true;
        }
        else {
            // Already initialized
        }
    }

    UnInitialize(): void {
        if (null != this._firebaseAdminInstance) {
            this._firebaseAdminInstance.delete();
            this._firebaseAdminInstance = null;
        }
        else {
            console.warn("No firebase app instance to uninitialize!");
        }
    }


    /* Firebase database */

    GetRef(path: string): firebaseAdmin.database.Reference {
        if (!this._isInitialized) {
            throw new Error("FirebaseAdminWrapper hasn't been initialized yet!");
        }

        return firebaseAdmin.database().ref(path);
    }

    GetPushQueueRef(): firebaseAdmin.database.Reference {
        if (!this._isInitialized) {
            throw new Error("FirebaseAdminWrapper hasn't been initialized yet!");
        }

        return this.GetRef("queue/push");
    }

    GetRpcQueueRef(): firebaseAdmin.database.Reference {
        if (!this._isInitialized) {
            throw new Error("FirebaseAdminWrapper hasn't been initialized yet!");
        }

        return this.GetRef("queue/rpc");
    }

    static GetInstance(): FirebaseAdminWrapper {
        if (null === this._firebaseAdminWrapperInstance) {
            this._firebaseAdminWrapperInstance = new FirebaseAdminWrapper();
        }
        else {
            // Already initialized, nothing more to do here
        }

        return this._firebaseAdminWrapperInstance;
    }

    private getKeyValuePairs<V>(data: any): Map<string, V> {
        if (!data) {
            throw new Error("data parameter cannot be null!");
        }

        let dataMap: Map<string, V> = new Map<string, V>();

        Object.keys(data).forEach(key => {
            dataMap.set(key, data[key]);
        });

        return dataMap;
    }

    async DoesRefExist(path: string): Promise<boolean> {
        let val: firebaseAdmin.database.DataSnapshot = await this.GetValue(path);
        return val.exists();
    }

    Subscribe(
        referencePath: string,
        subscribeType: FirebaseSubscribeType,
        callback: FirebaseDataSnapshotType,
        cancelCallback?: FirebaseDataSnapshotType): void {

        let subscribeTypeStr: firebaseAdmin.database.EventType = null;

        switch (subscribeType) {
            case FirebaseSubscribeType.VALUE:
                subscribeTypeStr = "value";
                break;

            case FirebaseSubscribeType.CHILD_ADDED:
                subscribeTypeStr = "child_added";
                break;

            case FirebaseSubscribeType.CHILD_CHANGED:
                subscribeTypeStr = "child_changed";
                break;

            default:
                throw new Error("Unexpected subscribe type! - " + FirebaseSubscribeType[subscribeType]);

        }

        this.GetRef(referencePath).on(
            subscribeTypeStr,
            callback,
            cancelCallback);
    }

    GetValue(referencePath: string): Promise<firebaseAdmin.database.DataSnapshot> {
        if (!referencePath) {
            throw new Error("referencePath cannot be null!");
        }

        return this.GetRef(referencePath).once(Constants.STR_VALUE);
    }

    UpdateValue(referencePath: string, value: Object): Promise<void> {
        if (!referencePath) {
            throw new Error("referencePath cannot be null!");
        }

        return this.GetRef(referencePath).update(value);
    }

    SetValue(referencePath: string, value: Object): Promise<void> {
        if (!referencePath) {
            throw new Error("referencePath cannot be null!");
        }

        return this.GetRef(referencePath).set(value);
    }

    GetPushKey(referencePath: string): string {
        if (!referencePath) {
            throw new Error("referencePath cannot be null!");
        }

        return this.GetRef(referencePath).push().key;
    }

    PushValue(referencePath: string, value: Object): Promise<firebaseAdmin.database.Reference> {
        if (!referencePath) {
            throw new Error("referencePath cannot be null!");
        }

        return <Promise<firebaseAdmin.database.Reference>> this.GetRef(referencePath).push(value);
    }

    DeleteValue(referencePath: string): Promise<void> {
        if (!referencePath) {
            throw new Error("referencePath cannot be null!");
        }

        return this.GetRef(referencePath).remove();
    }


    /* Firebase auth */

    GetUserByUid(uid: string): Promise<firebaseAdmin.auth.UserRecord> {
        if (!uid) {
            throw new Error("Uid cannot be null!");
        }

        return this._firebaseAdminInstance.auth().getUser(uid);
    }

    GetUserByEmail(email: string): Promise<firebaseAdmin.auth.UserRecord> {
        if (!email) {
            throw new Error("Email cannot be null!");
        }

        return this._firebaseAdminInstance.auth().getUserByEmail(email);
    }

    CreateUser(user: IFirebaseUser): Promise<firebaseAdmin.auth.UserRecord> {
        if (!user) {
            throw new Error("User cannot be null!");
        }

        return this._firebaseAdminInstance.auth().createUser(user);
    }

    UpdateUser(uid: string, obj: any): Promise<firebaseAdmin.auth.UserRecord> {
        if (!uid) {
            throw new Error("Uid cannot be null!");
        }

        if (!obj) {
            throw new Error("User cannot be null!");
        }

        return this._firebaseAdminInstance.auth().updateUser(uid, obj);
    }

    DeleteUser(uid: string): Promise<void> {
        if (!uid) {
            throw new Error("User id cannot be null!");
        }

        return this._firebaseAdminInstance.auth().deleteUser(uid);
    }

    VerifyIdToken(idToken: string): Promise<string> {
        if (!idToken) {
            throw new Error("User id cannot be null!");
        }

        return new Promise<string>(
            (resolve, reject) => {
                try {
                    this._firebaseAdminInstance.auth().verifyIdToken(idToken)
                        .then((decodedToken: firebaseAdmin.auth.DecodedIdToken) => {
                            resolve(decodedToken.uid);
                        })
                        .catch((error: Error) => {
                            reject(error);
                        });
                }
                catch (error) {
                    reject(error);
                }
            });
    }
}