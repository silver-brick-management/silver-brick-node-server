import * as gcm from "node-gcm";

export type IResponseBody = gcm.IResponseBody;
export class Message extends gcm.Message {}
export type IRecipient = gcm.IRecipient;

export interface INodeGcmSender {
    send(message: Message, registrationIds: string | string[] | IRecipient, callback: (err: any, resJson: IResponseBody) => void): void;

    _initialize(): void;
}