// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************
import { IConfigContext } from "../interfaces/IConfigContext";
import { Logger } from "../common/Logger";
import { IBasicEmailTemplateParams, ISignUpEmailTemplateParams, INewTaskEmailTemplateParams, MailType } from "../factories/EmailTemplateFactory";
import { IEmailPredicate } from "../common/ServerInternalTypes";
import * as nunjucks from "nunjucks";

interface EmailResultResponse {
    Body: string;
    Error: string;
}

export class MailGunWrapper {

    private _mailgunInstance: any;
    private static _mailGunWrapperInstance: MailGunWrapper = null;

    private constructor() {
        this._mailgunInstance = null;
    }

    CreateInstance(mailgunAPIKey: string, mailgunDomain: string): MailGunWrapper {
        let mailgunRef: MailGunWrapper = new MailGunWrapper();

        mailgunRef.Initialize(mailgunAPIKey, mailgunDomain);
        console.log("Mailgun Initiazed", mailgunAPIKey, mailgunDomain);
        return mailgunRef;
    }

    static GetInstance(): MailGunWrapper {
        if (null === this._mailGunWrapperInstance) {
            this._mailGunWrapperInstance = new MailGunWrapper();
        }
        return this._mailGunWrapperInstance;
    }

    Initialize(mailgunAPIKey: string, mailgunDomain: string): void {
        console.log("Initialize Mailgun", mailgunAPIKey, mailgunDomain);
        this._mailgunInstance = require("mailgun-js")(
            {
                apiKey: mailgunAPIKey,
                domain: mailgunDomain
            });
    }

    private async sendSingleEmail(email: string, data: any): Promise<EmailResultResponse> {
        return new Promise<EmailResultResponse>(
            (resolve, reject) => {
                try {
                    if (null != this._mailgunInstance) {
                        this._mailgunInstance.messages().send(
                            data,
                            function (error: any, body: any) {
                                resolve({ Body: body, Error: error });
                            });
                    }
                    else {
                        console.log("_mailgunInstance.messages is undefined!");
                        let newMailGunRef: MailGunWrapper = new MailGunWrapper();
                        this._mailgunInstance = newMailGunRef.Initialize(
                            "7739575a22de905e545702aced5ac504-db4df449-ad206833",
                            "silverbrickmanagement.com");
                        this._mailgunInstance.messages().send(
                            data,
                            function (error: any, body: any) {
                                resolve({ Body: body, Error: error });
                            });
                    }
                }
                catch (error) {
                    reject(error);
                }
            });
    }

    async Send(
        emailPredicateArr: IEmailPredicate[],
        subjectStr: string,
        renderParams: IBasicEmailTemplateParams | ISignUpEmailTemplateParams | INewTaskEmailTemplateParams,
        type: MailType,
        fromEmail: string = "accounts@silverbrickmanagement.com") {
        Logger.Info("~~~START: MailGunWrapper~~~");
        Logger.Info("E-mail Subject: " + subjectStr);
        Logger.Info("E-mail Body: " + JSON.stringify(renderParams));
        Logger.Info("~~~END: MailGunWrapper~~~");

        type ResultResponseMap = {
            [key: string]: EmailResultResponse[];
        };
        let resultResponseMap: ResultResponseMap = {};
        let emailCount: number = 0;

        const emailFunction = emailPredicateArr.forEach(async (emailPredicate: IEmailPredicate) => {
            if (emailPredicate.FirstName) {
                renderParams.FirstName = emailPredicate.FirstName;
            }
            else {
                // NTD
            }
            try {
                let renderedEmail: string = null;
                switch (type) {
                    case MailType.BASIC:
                    {
                        renderedEmail = nunjucks.render(
                            "./templates/basic.html",
                            renderParams);
                        break;
                    }

                    case MailType.SIGNUP:
                    {
                        renderedEmail = nunjucks.render(
                            "./templates/sign-up.html",
                            renderParams);
                        break;
                    }

                    case MailType.SERVICE:
                    {
                        renderedEmail = nunjucks.render(
                            "./templates/service-request.html",
                            renderParams);
                        break;
                    }

                    case MailType.NEW_TASK:
                    {
                        renderedEmail = nunjucks.render(
                            "./templates/alert.html",
                            renderParams);
                        break;
                    }

                    case MailType.NEW_MESSAGE:
                    {
                        renderedEmail = nunjucks.render(
                            "./templates/message.html",
                            renderParams);
                        break;
                    }

                    default:
                    {
                        renderedEmail = nunjucks.render(
                            "./templates/basic.html",
                            renderParams);
                        break;
                    }
                }
                let data = {
                    from: "SilverBrick Alerts <accounts@silverbrickmanagement.com>",
                    to: emailPredicate.Email,
                    subject: subjectStr,
                    html: renderedEmail
                };
                emailCount = emailCount + 1;
                console.log("Emails Sent from Wrapper ", emailCount);
                let resultResponse: EmailResultResponse = await this.sendSingleEmail(emailPredicate.Email, data);
                if (null != resultResponseMap[emailPredicate.Email]) {
                    Logger.Status("!!WARNING, duplicate e-mail found in list!! Email: " + emailPredicate);
                    resultResponseMap[emailPredicate.Email].push(resultResponse);
                }
                else {
                    resultResponseMap[emailPredicate.Email] = [resultResponse];
                }
            }
            catch (error) {
                Logger.Error("Best-effort, but there was an error sending the e-mail. Error - " + error);
            }
        });
        await emailFunction;
        console.log("Post Loop: Emails Sent from Wrapper ", emailCount);
        Logger.Info("Mailgun Results: " + JSON.stringify(resultResponseMap));
    }
}