// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as firebaseAdmin from "firebase-admin";

// Local modules

// Wrapper Modules
import { FirebaseAdminWrapper } from "../wrappers/FirebaseAdminWrapper";

// Api Modules
import { UserApiInternal } from "./UserApiInternal";

// Util Modules
import { StringUtil } from "../common/StringUtil";

// Type Related Modules
import { Constants } from "../common/Constants";
import {
    ISilverBrickServiceAssignPayloadV1,
    ServiceAssignPushPredicate,
    NormalizedUserInfo,
    OperatingSystemType,
    DeviceInfoTuple,
    ISilverBrickPayloadV1,
    ISilverBrickNewServicePayloadV1,
    IUpstreamFCMPayload,
    ISilverBrickMessagesPayloadV1,
    ISilverBrickTaskMessagePayloadV1,
    NewMessagePushPredicate,
    SilverBrickPushType,
    SilverBrickPushPredicate
} from "../common/ServerInternalTypes";
import { ISilverBrickUser, INotificationData } from "../shared/SilverBrickTypes";
import { MessageParticipant } from "../shared/ResponseTypes";

export class PushApiInternal {
    public static async AddNotification(orgID: string, buildingID: string, notification: INotificationData): Promise<INotificationData> {
        try {
            const notificationPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_NOTIFICATIONS_FSLASH + buildingID + Constants.STR_FSLASH;
            const notRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(notificationPath, notification);
            const notificationID: string = notRef.key;
            notification.id = notificationID;
            const newNotPath = notificationPath + notificationID + Constants.STR_FSLASH + "id/";
            await FirebaseAdminWrapper.GetInstance().SetValue(newNotPath, notificationID);
        } catch (error) {
            // console.log("Error: ", error);
            throw new Error(error);
        }
        return notification;
    }

    public static async GetNotifications(orgID: string, buildingID: string): Promise<INotificationData[]> {
        let notifications: INotificationData[] = [];
        const notificationPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_NOTIFICATIONS_FSLASH + buildingID + Constants.STR_FSLASH;
        const notRef: firebaseAdmin.database.Query = await FirebaseAdminWrapper.GetInstance()
            .GetRef(notificationPath)
            .orderByKey()
            .limitToFirst(15);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(notificationPath)) {
            const notSnap: firebaseAdmin.database.DataSnapshot = await notRef.once(Constants.STR_VALUE);
            if (notSnap) {
                notSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                    let newNot: INotificationData = <INotificationData>innerSnapshot.val();
                    notifications.push(newNot);
                    return false;
                });
            }
            return notifications.reverse();
        }
        else {
            return notifications;
        }
    }

    public static async GetSenderDeviceToken(userID: string): Promise<string> {
        if (!userID) {
            throw new Error("No User ID!");
        } else {
            // UserID exists
        }

        let fcmRegToken: string = "";
        let snapShot: firebaseAdmin.database.DataSnapshot = null;
        // Get user data from the custom db
        const map: string = await UserApiInternal.GetUserMap(userID);
        if (null != map) {
            const uidPath: string = Constants.STR_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + `${userID}` + Constants.STR_FSLASH_FCMTOKEN_FSLASH;
            if (FirebaseAdminWrapper.GetInstance().DoesRefExist(uidPath)) {
                snapShot = await FirebaseAdminWrapper.GetInstance().GetValue(uidPath);

                if (snapShot.val()) {
                    fcmRegToken = snapShot.val();
                }
            } else {
                fcmRegToken = "No FCM Token!";
            }
        }
        return fcmRegToken;
    }

    static platformUsertoNormalizedUserInfo(userID: string, User: ISilverBrickUser) {
        let osType: OperatingSystemType = OperatingSystemType.OTHER;

        return NormalizedUserInfo.CreateInstance(userID, User, [User.email], [DeviceInfoTuple.CreateInstance(User.fcmRegToken, OperatingSystemType.IOS)]);
    }

    public static async SendMessagePayload(userID: string, authorID: string, userName: string, message: string, roomID: string, messageType: string, orgID: string, buildingID: string, type: SilverBrickPushType): Promise<void> {
        if (!userID || !message || !userName || !roomID || !orgID || !buildingID) {
            throw new Error("Missing Params: " + " UserID " + userID + " message " + message + " roomID " + roomID);
        } else {
            // All params valid
        }
        return this.SendUpstreamPayloadViaFirebaseQueue(
            Constants.TYPE_MESSAGE_PAYLOAD_V1,
            <ISilverBrickMessagesPayloadV1>{
                SenderID: authorID,
                RoomID: roomID,
                AuthorName: userName,
                AuthorID: authorID,
                Message: message,
                PostType: type,
                MessageType: messageType,
                BuildingID: buildingID,
                OrgID: orgID
            },
            userID
        );
    }

    public static async SendNewServicePayload(userID: string, userName: string, message: string, taskID: string, orgID: string, buildingID: string, type: SilverBrickPushType): Promise<void> {
        if (!userID || !message || !userName || !orgID || !buildingID) {
            throw new Error("Missing Params: " + " UserID " + userID + " message " + message + " taskID " + taskID);
        } else {
            // All params valid
        }
        return this.SendUpstreamPayloadViaFirebaseQueue(
            Constants.TYPE_SERVICE_NEW_PAYLOAD_V1,
            <ISilverBrickNewServicePayloadV1>{
                SenderID: userID,
                TaskID: taskID,
                AuthorName: userName,
                Message: message,
                PostType: type,
                BuildingID: buildingID,
                OrgID: orgID
            },
            userID
        );
    }

    public static async SendAssignServicePayload(userID: string, userName: string, message: string, bookingID: string, orgID: string, buildingID: string, type: SilverBrickPushType): Promise<void> {
        if (!userID || !message || !userName || !orgID || !buildingID) {
            throw new Error("Missing Params: " + " UserID " + userID + " message " + message + " bookingID " + bookingID);
        } else {
            // All params valid
        }
        return this.SendUpstreamPayloadViaFirebaseQueue(
            Constants.TYPE_SERVICE_ASSIGN_PAYLOAD_V1,
            <ISilverBrickServiceAssignPayloadV1>{
                SenderID: userID,
                TaskID: bookingID,
                AuthorName: userName,
                Message: message,
                PostType: type,
                BuildingID: buildingID,
                OrgID: orgID
            },
            userID
        );
    }

    public static async SendCompleteServicePayload(
        userID: string,
        userName: string,
        message: string,
        taskID: string,
        orgID: string,
        buildingID: string,
        type: SilverBrickPushType): Promise<void> {
        if (!userID || !message || !userName || !orgID || !buildingID) {
            throw new Error("Missing Params: " + " UserID " + userID + " message " + message + " taskID " + taskID);
        } else {
            // All params valid
        }
        return this.SendUpstreamPayloadViaFirebaseQueue(
            Constants.TYPE_SERVICE_COMPLETE_PAYLOAD_V1,
            <ISilverBrickServiceAssignPayloadV1>{
                SenderID: userID,
                TaskID: taskID,
                AuthorName: userName,
                Message: message,
                PostType: type,
                BuildingID: buildingID,
                OrgID: orgID
            },
            userID
        );
    }

    public static async SendReOpenServicePayload(
        userID: string,
        userName: string,
        message: string,
        taskID: string,
        orgID: string,
        buildingID: string,
        type: SilverBrickPushType): Promise<void> {
        if (!userID || !message || !userName || !orgID || !buildingID) {
            throw new Error("Missing Params: " + " UserID " + userID + " message " + message + " taskID " + taskID);
        } else {
            // All params valid
        }
        return this.SendUpstreamPayloadViaFirebaseQueue(
            Constants.TYPE_SERVICE_REOPENED_PAYLOAD_V1,
            <ISilverBrickServiceAssignPayloadV1>{
                SenderID: userID,
                TaskID: taskID,
                AuthorName: userName,
                Message: message,
                PostType: type,
                BuildingID: buildingID,
                OrgID: orgID
            },
            userID
        );
    }

    public static async SendUpdateServicePayload(
        userID: string,
        userName: string,
        message: string,
        taskID: string,
        orgID: string,
        buildingID: string,
        type: SilverBrickPushType): Promise<void> {
        if (!userID || !message || !userName || !orgID || !buildingID) {
            throw new Error("Missing Params: " + " UserID " + userID + " message " + message + " taskID " + taskID);
        } else {
            // All params valid
        }
        return this.SendUpstreamPayloadViaFirebaseQueue(
            Constants.TYPE_SERVICE_UPDATED_PAYLOAD_V1,
            <ISilverBrickServiceAssignPayloadV1>{
                SenderID: userID,
                TaskID: taskID,
                AuthorName: userName,
                Message: message,
                PostType: type,
                BuildingID: buildingID,
                OrgID: orgID
            },
            userID
        );
    }

    public static async SendProgressServicePayload(
        userID: string,
        userName: string,
        message: string,
        taskID: string,
        orgID: string,
        buildingID: string,
        type: SilverBrickPushType): Promise<void> {
        if (!userID || !message || !userName || !orgID || !buildingID) {
            throw new Error("Missing Params: " + " UserID " + userID + " message " + message + " taskID " + taskID);
        } else {
            // All params valid
        }
        return this.SendUpstreamPayloadViaFirebaseQueue(
            Constants.TYPE_SERVICE_PROGRESS_PAYLOAD_V1,
            <ISilverBrickServiceAssignPayloadV1>{
                SenderID: userID,
                TaskID: taskID,
                AuthorName: userName,
                Message: message,
                PostType: type,
                BuildingID: buildingID,
                OrgID: orgID
            },
            userID
        );
    }

    public static async SendServiceMessagePayload(
        userID: string,
        userName: string,
        message: string,
        taskID: string,
        orgID: string,
        buildingID: string,
        type: SilverBrickPushType): Promise<void> {
        if (!userID || !message || !userName || !orgID || !buildingID) {
            throw new Error("Missing Params: " + " UserID " + userID + " message " + message + " taskID " + taskID);
        } else {
            // All params valid
        }
        return this.SendUpstreamPayloadViaFirebaseQueue(
            Constants.TYPE_SERVICE_MESSAGE_PAYLOAD_V1,
            <ISilverBrickTaskMessagePayloadV1>{
                SenderID: userID,
                TaskID: taskID,
                AuthorName: userName,
                Message: message,
                PostType: type,
                BuildingID: buildingID,
                OrgID: orgID
            },
            userID
        );
    }

    public static async SendUpstreamPayloadViaFirebaseQueue(payloadType: string, payload: ISilverBrickTaskMessagePayloadV1 | ISilverBrickServiceAssignPayloadV1 | ISilverBrickPayloadV1 | ISilverBrickMessagesPayloadV1, userID: string): Promise<void> {
        try {
            const deviceID: string = await this.GetSenderDeviceToken(userID);
            const msgID: string = StringUtil.GenerateUUID();
            const platformPayload: IUpstreamFCMPayload = {
                MessageID: msgID,
                FromDeviceID: deviceID,
                Data: payload,
                Type: payloadType,
                Timestamp: firebaseAdmin.database.ServerValue.TIMESTAMP,
                UserID: userID,
                _state: Constants.TYPE_UPSTREAM_START
            };

            console.log("Sending Payload - Type: " + payloadType + " DeviceID: " + deviceID + " msgID: " + msgID + " Payload: " + JSON.stringify(payload));

            const path: string = Constants.STR_QUEUE_FSLASH_PUSH_FSLASH_TASKS_FSLASH;
            await FirebaseAdminWrapper.GetInstance().PushValue(path, platformPayload);
        }
        catch (error) {
            throw new Error("Error SendUpstreamPayloadViaFirebaseQueue: " + JSON.stringify(error));
        }
    }

    private _sortNotifications(nots: INotificationData[]): INotificationData[] {
        let notifications: INotificationData[] = [];
        if (null != nots) {
            notifications = nots.sort((not1: INotificationData, not2: INotificationData) => {
                if (not1.date > not2.date) {
                    return -1;
                }
                if (not1.date < not2.date) {
                    return 1;
                }
                return 0;
            });
        }

        return notifications;
    }
}

