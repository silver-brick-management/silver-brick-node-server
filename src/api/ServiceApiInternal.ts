// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file "LICENSE.txt", which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as firebaseAdmin from "firebase-admin";


// Local Modules
import { FirebaseAdminWrapper } from "../wrappers/FirebaseAdminWrapper";
import { GCloudStorageWrapper } from "../wrappers/GCloudStorageWrapper";

// Util modules
import { JSONUtil } from "../common/JsonUtil";
import { TimeUtil } from "../common/TimeUtil";
import { StringUtil } from "../common/StringUtil";
import { ArrayUtil } from "../common/ArrayUtil";

// Api Modules
import { UserApiInternal } from "./UserApiInternal";
import { SilverBrickAdminApiInternal } from "./SilverBrickAdminApiInternal";
import { UserAdminApiInternal } from "./UserAdminApiInternal";

// Factory Modules
import { NormalizedUser } from "../factories/users/NormalizedUser";

// Type Related Modules
import { Constants } from "../common/Constants";
import {
    IUser,
    IAssignee,
    IUserBuilding,
    IUserOrg,
    IHistory,
    IBuildingSimple,
    IBuildingBasic,
    ITaskFeed,
    IUserAudience,
    IOrgInfo,
    IBuildingInfo,
    IBuilding,
    IOrgBasic,
    INote,
    ITask
} from "../shared/SilverBrickTypes";
// Util Modules


// Logger Modules
import { Logger, SessionLogBuilder, LogLevel } from "../common/Logger";


export class ServiceApiInternal {
    static DurationOptions: string[] = ["day(s)", "week(s)", "month(s)", "year(s)", "forever"];
    /*
        TASK RELATED
    */
    static async GetTasksMultiBldg(): Promise<ITask[]> {
        let path: string = Constants.STR_ALL_TASKS_FSLASH;
        let tasks: ITask[] = [];
        const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(path);
        const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("recurring").equalTo(false);
        const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(path)) {
            // tasks = [...tasks, ...ArrayUtil.Transform(<ITask[]>taskSnap2.val())];
            taskSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newTask: ITask = <ITask>innerSnapshot.val();
                if ((newTask.active) && (newTask.status !== "Closed")) {
                    newTask.id = innerSnapshot.key;
                    newTask.scheduling = false;
                    tasks.push(newTask);
                }
                return false;
            });
        }
        return tasks;
    }

    static async GetClosedTasksMultiBldg(): Promise<ITask[]> {
        let path: string = Constants.STR_ALL_TASKS_FSLASH;
        let tasks: ITask[] = [];
        const taskSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(path);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(path)) {
            // tasks = [...tasks, ...ArrayUtil.Transform(<ITask[]>taskSnap2.val())];
            taskSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newTask: ITask = <ITask>innerSnapshot.val();
                if (((newTask.status === "Closed") || newTask.status === "Completed")) {
                    newTask.id = innerSnapshot.key;
                    newTask.scheduling = false;
                    tasks.push(newTask);
                }
                return false;
            });
        }
        return tasks;
    }

    static async GetRecurringTasksMultiBldg(): Promise<ITask[]> {
        let path: string = Constants.STR_ALL_TASKS_FSLASH;
        let bookings: ITask[] = [];
        // const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + org.id + Constants.STR_FSLASH_TASKS_FSLASH + bldg.id + Constants.STR_FSLASH;
        const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(path);
        const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("recurring").equalTo(true);
        const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(path)) {
            // tasks = [...tasks, ...ArrayUtil.Transform(<ITask[]>taskSnap2.val())];
            taskSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newBook: ITask = <ITask>innerSnapshot.val();
                newBook.id = innerSnapshot.key;
                if (null == newBook.startTime) {
                    newBook.startTime = "16:00";
                    newBook.endTime = "16:01";
                }
                if ((newBook.active) && (newBook.status !== "Closed")) {
                    switch (newBook.durationAmount) {
                        case this.DurationOptions[0]:
                        {
                            let endDate: Date = new Date(newBook.startDate);
                            endDate.setDate(endDate.getDate() + Number(newBook.durationQuantity));
                            endDate.setHours(endDate.getHours() - 4);
                            newBook.endDate = endDate.getTime();
                            bookings.push(newBook);
                            break;
                        }

                        case this.DurationOptions[1]:
                        {
                            let endDate: Date = new Date(newBook.startDate);
                            endDate.setDate(endDate.getDate() + (Number(newBook.durationQuantity) * 7));
                            endDate.setHours(endDate.getHours() - 4);
                            newBook.endDate = endDate.getTime();
                            bookings.push(newBook);
                            break;
                        }

                        case this.DurationOptions[2]:
                        {
                            let date: Date = new Date(newBook.startDate);
                            let currentDate = date.getDate();
                            // Set to day 1 to avoid forward
                            date.setDate(1);
                            // Increase month by 1
                            date.setMonth(date.getMonth() + Number(newBook.durationQuantity) + 1);
                            // Get max # of days in this new month
                            let daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
                            // Set the date to the minimum of current date of days in month
                            date.setDate(Math.min(currentDate, daysInMonth));
                            date.setHours(date.getHours() - 4);
                            newBook.endDate = date.getTime();
                            bookings.push(newBook);
                            break;
                        }

                        case this.DurationOptions[3]:
                        {
                            let endDate: Date = new Date(newBook.startDate);
                            endDate.setFullYear(endDate.getFullYear() + Number(newBook.durationQuantity));
                            endDate.setHours(endDate.getHours() - 4);
                            newBook.endDate = endDate.getTime();
                            bookings.push(newBook);
                            break;
                        }

                        case this.DurationOptions[4]:
                        {
                            let endDate: Date = new Date(newBook.startDate);
                            endDate.setFullYear(endDate.getFullYear() + Number(20));
                            endDate.setHours(endDate.getHours() - 4);
                            newBook.endDate = endDate.getTime();
                            bookings.push(newBook);
                            break;
                        }
                        
                        default:
                        {
                            bookings.push(newBook);
                            break;
                        }
                    }
                }
                return false;
            });
        }
        return bookings;
    }

    static async GetTasksSomeBldg(orgs: IUserOrg[]): Promise<ITask[]> {
        let tasks: ITask[] = [];
        for (let org of orgs) {
            for (let bldg of org.buildings) {
                const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + org.id + Constants.STR_FSLASH_TASKS_FSLASH + bldg.id + Constants.STR_FSLASH;
                // // console.log("taskPath", taskPath);
                const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(taskPath);
                const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("scheduled").equalTo(false);
                const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
                    // tasks = [...tasks, ...ArrayUtil.Transform(<ITask[]>taskSnap2.val())];
                    taskSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                        let newTask: ITask = <ITask>innerSnapshot.val();
                        newTask.id = innerSnapshot.key;
                        if (null == newTask.startTime) {
                            newTask.startTime = "16:00";
                            newTask.endTime = "16:01";
                        }
                        tasks.push(newTask);
                        return false;
                    });
                }
            }
        }
        return tasks;
    }

    static async GetTasksSomeBldgLandlord(orgs: IUserOrg[]): Promise<ITask[]> {
        let tasks: ITask[] = [];
        for (let org of orgs) {
            for (let bldg of org.buildings) {
                const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + org.id + Constants.STR_FSLASH_TASKS_FSLASH + bldg.id + Constants.STR_FSLASH;
                // // console.log("taskPath", taskPath);
                const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(taskPath);
                const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("recurring").equalTo(false);
                const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
                    // tasks = [...tasks, ...ArrayUtil.Transform(<ITask[]>taskSnap2.val())];
                    taskSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                        let newTask: ITask = <ITask>innerSnapshot.val();
                        // // console.log("taskPath", newTask);
                        newTask.id = innerSnapshot.key;
                        if (null == newTask.startTime) {
                            newTask.startTime = "16:00";
                            newTask.endTime = "16:01";
                        }
                        tasks.push(newTask);
                        return false;
                    });
                }
            }
        }
        return tasks;
    }

    static async GetCompletedTasksSomeBldgLandlord(orgs: IUserOrg[]): Promise<ITask[]> {
        let tasks: ITask[] = [];
        for (let org of orgs) {
            for (let bldg of org.buildings) {
                const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + org.id + Constants.STR_FSLASH_TASKS_FSLASH + bldg.id + Constants.STR_FSLASH;
                // // console.log("taskPath", taskPath);
                const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(taskPath);
                const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("recurring").equalTo(true);
                const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
                    // tasks = [...tasks, ...ArrayUtil.Transform(<ITask[]>taskSnap2.val())];
                    taskSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                        let newTask: ITask = <ITask>innerSnapshot.val();
                        // console.log("taskPath", newTask);
                        if ((null != newTask.completedDates)) {
                            newTask.id = innerSnapshot.key;
                            if (null == newTask.startTime) {
                                newTask.startTime = "16:00";
                                newTask.endTime = "16:01";
                            }
                            tasks.push(newTask);
                            return false;
                        }
                    });
                }
            }
        }
        return tasks;
    }

    static async GetRecurringTasksSomeBldg(orgs: IUserOrg[]): Promise<ITask[]> {
        let bookings: ITask[] = [];
        for (let org of orgs) {
            for (let bldg of org.buildings) {
                const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + org.id + Constants.STR_FSLASH_TASKS_FSLASH + bldg.id + Constants.STR_FSLASH;
                // // console.log("taskPath", taskPath);
                const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(taskPath);
                const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("scheduled").equalTo(true);
                const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
                    // tasks = [...tasks, ...ArrayUtil.Transform(<ITask[]>taskSnap2.val())];
                    taskSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                        let newBook: ITask = <ITask>innerSnapshot.val();
                        newBook.id = innerSnapshot.key;
                        if ((newBook.active) && (newBook.status !== "Closed")) {
                            if (null == newBook.startTime) {
                                newBook.startTime = "16:00";
                                newBook.endTime = "16:01";
                            }
                            switch (newBook.durationAmount) {
                                case this.DurationOptions[0]:
                                {
                                    let endDate: Date = new Date(newBook.startDate);
                                    endDate.setDate(endDate.getDate() + Number(newBook.durationQuantity));
                                    endDate.setHours(endDate.getHours() - 4);
                                    newBook.endDate = endDate.getTime();
                                    bookings.push(newBook);
                                    break;
                                }

                                case this.DurationOptions[1]:
                                {
                                    let endDate: Date = new Date(newBook.startDate);
                                    endDate.setDate(endDate.getDate() + (Number(newBook.durationQuantity) * 7));
                                    endDate.setHours(endDate.getHours() - 4);
                                    newBook.endDate = endDate.getTime();
                                    bookings.push(newBook);
                                    break;
                                }

                                case this.DurationOptions[2]:
                                {
                                    let date: Date = new Date(newBook.startDate);
                                    let currentDate = date.getDate();
                                    // Set to day 1 to avoid forward
                                    date.setDate(1);
                                    // Increase month by 1
                                    date.setMonth(date.getMonth() + Number(newBook.durationQuantity) + 1);
                                    // Get max # of days in this new month
                                    let daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
                                    // Set the date to the minimum of current date of days in month
                                    date.setDate(Math.min(currentDate, daysInMonth));
                                    date.setHours(date.getHours() - 4);
                                    newBook.endDate = date.getTime();
                                    bookings.push(newBook);
                                    break;
                                }

                                case this.DurationOptions[3]:
                                {
                                    let endDate: Date = new Date(newBook.startDate);
                                    endDate.setFullYear(endDate.getFullYear() + Number(newBook.durationQuantity));
                                    endDate.setHours(endDate.getHours() - 4);
                                    newBook.endDate = endDate.getTime();
                                    bookings.push(newBook);
                                    break;
                                }

                                case this.DurationOptions[4]:
                                {
                                    let endDate: Date = new Date(newBook.startDate);
                                    endDate.setFullYear(endDate.getFullYear() + Number(20));
                                    endDate.setHours(endDate.getHours() - 4);
                                    newBook.endDate = endDate.getTime();
                                    bookings.push(newBook);
                                    break;
                                }
                                
                                default:
                                {
                                    break;
                                }
                            }
                        }
                        return false;
                    });
                }
            }
        }
        return bookings;
    }

    static async GetTasks(orgID: string, buildingID: string): Promise<ITask[]> {
        let tasks: ITask[] = [];
        const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_TASKS_FSLASH + buildingID + Constants.STR_FSLASH;
        // const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(taskPath);
        const taskSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(taskPath);
        // const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
            taskSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newTask: ITask = <ITask>innerSnapshot.val();
                newTask.id = innerSnapshot.key;
                tasks.push(newTask);
                return false;
            });
        }
        return tasks;
    }

    static async GetRecurringTasks(orgID: string, buildingID: string): Promise<ITask[]> {
        let bookings: ITask[] = [];
        const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_TASKS_FSLASH + buildingID + Constants.STR_FSLASH;
        const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(taskPath);
        const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("scheduled").equalTo(true);
        const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
            taskSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newBook: ITask = <ITask>innerSnapshot.val();
                newBook.id = innerSnapshot.key;
                if ((newBook.active) && (newBook.status !== "Closed")) {
                    if (null == newBook.startTime) {
                        newBook.startTime = "16:00";
                        newBook.endTime = "16:01";
                    }
                    switch (newBook.durationAmount) {
                        case this.DurationOptions[0]:
                        {
                            let endDate: Date = new Date(newBook.startDate);
                            endDate.setDate(endDate.getDate() + Number(newBook.durationQuantity));
                            endDate.setHours(endDate.getHours() - 4);
                            newBook.endDate = endDate.getTime();
                            bookings.push(newBook);
                            break;
                        }

                        case this.DurationOptions[1]:
                        {
                            let endDate: Date = new Date(newBook.startDate);
                            endDate.setDate(endDate.getDate() + (Number(newBook.durationQuantity) * 7));
                            endDate.setHours(endDate.getHours() - 4);
                            newBook.endDate = endDate.getTime();
                            bookings.push(newBook);
                            break;
                        }

                        case this.DurationOptions[2]:
                        {
                            let date: Date = new Date(newBook.startDate);
                            let currentDate = date.getDate();
                            // Set to day 1 to avoid forward
                            date.setDate(1);
                            // Increase month by 1
                            date.setMonth(date.getMonth() + Number(newBook.durationQuantity) + 1);
                            // Get max # of days in this new month
                            let daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
                            // Set the date to the minimum of current date of days in month
                            date.setDate(Math.min(currentDate, daysInMonth));
                            date.setHours(date.getHours() - 4);
                            newBook.endDate = date.getTime();
                            bookings.push(newBook);
                            break;
                        }

                        case this.DurationOptions[3]:
                        {
                            let endDate: Date = new Date(newBook.startDate);
                            endDate.setFullYear(endDate.getFullYear() + Number(newBook.durationQuantity));
                            endDate.setHours(endDate.getHours() - 4);
                            newBook.endDate = endDate.getTime();
                            bookings.push(newBook);
                            break;
                        }

                        case this.DurationOptions[4]:
                        {
                            let endDate: Date = new Date(newBook.startDate);
                            endDate.setFullYear(endDate.getFullYear() + Number(20));
                            endDate.setHours(endDate.getHours() - 4);
                            newBook.endDate = endDate.getTime();
                            bookings.push(newBook);
                            break;
                        }
                        
                        default:
                        {
                            break;
                        }
                    }
                }
                return false;
            });
        }
        return bookings;
    }

    static async GetTask(taskID: string): Promise<ITask> {
        let tasks: ITask = null;
        const taskPath: string = Constants.STR_ALL_TASKS_FSLASH + taskID + Constants.STR_FSLASH;
        const taskSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(taskPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
            tasks = <ITask>taskSnap.val();
            tasks.id = taskSnap.key;
        }
        return tasks;
    }

    static async AddTask(orgID: string, buildingID: string, userID: string, task: ITask): Promise<ITask> {
        const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_TASKS_FSLASH + buildingID + Constants.STR_FSLASH;
        const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(taskPath, task);
        const taskID: string = taskRef.key;
        task.id = taskID;
        const servIDPath: string = taskPath + taskID + Constants.STR_FSLASH + "id/";
        await FirebaseAdminWrapper.GetInstance().SetValue(servIDPath, taskID);
        const schedulesPath: string = Constants.STR_SCHEDULES_FSLASH + task.clientID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (task.clientID.length > 20) {
            await FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath, task);
        }
        const path: string = Constants.STR_ALL_TASKS_FSLASH + taskID + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(path, task);
        let userRef: IUser = await UserAdminApiInternal.GetUser(userID);
        let newFeed: ITaskFeed = {
            type: "new",
            taskID: task.id,
            message: `${ task.description }`,
            timestamp: new Date(Date.now()).getTime(),
            orgID: orgID,
            category: task.category,
            subCategory: task.category,
            buildingID: task.buildingID,
            buildingName: task.buildingName,
            audience: (task.recurring) ? Constants.STR_AUDIENCE_LANDLORD_ONLY : Constants.STR_AUDIENCE_TENANT_AND_LANDLORD,
            role: task.updatedRole,
            clientUnit: task.clientUnit,
            authorName: task.authorName,
            authorID: task.authorID,
            status: "New",
            assignees: (null != task.assignees) ? task.assignees : [],
            approved: ((null != task.updatedRole) && (task.updatedRole === "Staff") && (!userRef.isAdmin)) ? false : true
        };
        await this.RecordActivity(task.orgID, task.buildingID, newFeed);
        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
        if (null != landlords) {
            for (let landlord of landlords) {
                let landSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + landlord.uid + Constants.STR_FSLASH + taskID + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(landSchedulesPath, task);
            }
        }

        if (null != task.assignees) {
            for (let newAssignee of task.assignees) {
                let newSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + newAssignee.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(newSchedulesPath, task);
            }
        }
        return task;
    }

    static async UpdateTask(orgID: string, buildingID: string, userID: string, task: ITask, authorID: string, authorName: string): Promise<ITask> {
        // // console.log("UpdateTask", task);
        const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_TASKS_FSLASH + buildingID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(taskPath, task);
        const schedulesPath: string = Constants.STR_SCHEDULES_FSLASH + task.clientID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (task.clientID.length > 20) {
            await FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath, task);
        }
        if (null != task.assignees) {
            for (let newAssignee of task.assignees) {
                let newSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + newAssignee.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(newSchedulesPath, task);
            }
        }
        const path: string = Constants.STR_ALL_TASKS_FSLASH + task.id + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(path, task);
        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
        if (null != landlords) {
            for (let landlord of landlords) {
                let landSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + landlord.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(landSchedulesPath, task);
            }
        }
        return task;
    }

    static async UpdateTaskLandlord(orgID: string, buildingID: string, userID: string, task: ITask, authorID: string, authorName: string): Promise<ITask> {
        // // console.log("UpdateTask", task);
        const schedulesPath: string = Constants.STR_SCHEDULES_FSLASH + task.clientID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (task.clientID.length > 20) {
            await FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath, task);
        }
        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
        if (null != landlords) {
            for (let landlord of landlords) {
                let landSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + landlord.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(landSchedulesPath, task);
            }
        }
        return task;
    }

    static async DeleteTask(orgID: string, buildingID: string, task: ITask): Promise<ITask> {
        // console.log("DeleteTask", task);
        const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + task.orgID + Constants.STR_FSLASH_TASKS_FSLASH + task.buildingID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
            await FirebaseAdminWrapper.GetInstance().DeleteValue(taskPath);
        }
        const schedulesPath: string = Constants.STR_SCHEDULES_FSLASH + task.clientID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (task.clientID.length > 20) {
            if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(schedulesPath)) {
                await FirebaseAdminWrapper.GetInstance().DeleteValue(schedulesPath);
            }
        }
        if (null != task.assignees) {
            for (let newAssignee of task.assignees) {
                let newSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + newAssignee.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(newSchedulesPath)) {
                    await FirebaseAdminWrapper.GetInstance().DeleteValue(newSchedulesPath);
                }
            }
        }
        const path: string = Constants.STR_ALL_TASKS_FSLASH + task.id + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(path)) {
            await FirebaseAdminWrapper.GetInstance().DeleteValue(path);
        }
        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
        if (null != landlords) {
            for (let landlord of landlords) {
                let landSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + landlord.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(landSchedulesPath)) {
                    await FirebaseAdminWrapper.GetInstance().DeleteValue(landSchedulesPath);
                }
            }
        }
        return task;
    }

    static async UpdateTaskStatus(orgID: string, buildingID: string, userID: string, task: ITaskFeed, clientID: string, completedDates: string[] = null): Promise<ITaskFeed> {
        const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + task.orgID + Constants.STR_FSLASH_TASKS_FSLASH + task.buildingID + Constants.STR_FSLASH + task.taskID + Constants.STR_FSLASH;
        let taskRef: ITask = await this.GetTask(task.taskID);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
            FirebaseAdminWrapper.GetInstance().SetValue(taskPath + "status/", task.status);
            if (null != task.assignees) {
                FirebaseAdminWrapper.GetInstance().SetValue(taskPath + "assignees/", task.assignees);
            }
            FirebaseAdminWrapper.GetInstance().SetValue(taskPath + "updateAuthor/", task.authorName);
            FirebaseAdminWrapper.GetInstance().SetValue(taskPath + "updatedRole/", task.role);
            FirebaseAdminWrapper.GetInstance().SetValue(taskPath + "lastUpdated/", task.timestamp);
            await FirebaseAdminWrapper.GetInstance().SetValue(taskPath + "active/", ((!taskRef.recurring) && (task.status === "Completed")) ? false : true);
            if (null != completedDates) {
                FirebaseAdminWrapper.GetInstance().SetValue(taskPath + "completedDates/", completedDates);
            }
        }
        let schedulesPath: string = Constants.STR_SCHEDULES_FSLASH + clientID + Constants.STR_FSLASH + task.taskID + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(schedulesPath)) {
            FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath + "status/", task.status);
            if (null != task.assignees) {
                FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath + "assignees/", task.assignees);
            }
            FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath + "updateAuthor/", task.authorName);
            FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath + "updatedRole/", task.role);
            FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath + "lastUpdated/", task.timestamp);
            await FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath + "active/", ((!taskRef.recurring) && (task.status === "Completed")) ? false : (task.status === "Closed") ? false : true);
            if (null != completedDates) {
                FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath + "completedDates/", completedDates);
            }
        }
        let path: string = Constants.STR_ALL_TASKS_FSLASH + task.taskID + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(path)) {
            FirebaseAdminWrapper.GetInstance().SetValue(path + "status/", task.status);
            if (null != task.assignees) {
                FirebaseAdminWrapper.GetInstance().SetValue(path + "assignees/", task.assignees);
            }
            FirebaseAdminWrapper.GetInstance().SetValue(path + "updateAuthor/", task.authorName);
            FirebaseAdminWrapper.GetInstance().SetValue(path + "updatedRole/", task.role);
            FirebaseAdminWrapper.GetInstance().SetValue(path + "lastUpdated/", task.timestamp);
            await FirebaseAdminWrapper.GetInstance().SetValue(path + "active/", ((!taskRef.recurring) && (task.status === "Completed")) ? false : (task.status === "Closed") ? false : true);
            if (null != completedDates) {
                FirebaseAdminWrapper.GetInstance().SetValue(path + "completedDates/", completedDates);
            }
        }
        if (null != task.assignees) {
            task.assignees.forEach(async (assignee: IAssignee) => {
                let schedulesAssignPath: string = Constants.STR_SCHEDULES_FSLASH + assignee.uid + Constants.STR_FSLASH + task.taskID + Constants.STR_FSLASH;
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(schedulesPath)) {
                    await FirebaseAdminWrapper.GetInstance().SetValue(schedulesAssignPath + "status/", task.status);
                    await FirebaseAdminWrapper.GetInstance().SetValue(schedulesAssignPath + "assignees/", task.assignees);
                    await FirebaseAdminWrapper.GetInstance().SetValue(schedulesAssignPath + "updateAuthor/", task.authorName);
                    await FirebaseAdminWrapper.GetInstance().SetValue(schedulesAssignPath + "updatedRole/", task.role);
                    await FirebaseAdminWrapper.GetInstance().SetValue(schedulesAssignPath + "lastUpdated/", task.timestamp);
                    await FirebaseAdminWrapper.GetInstance().SetValue(schedulesAssignPath + "active/", ((!taskRef.recurring) && (task.status === "Completed")) ? false : (task.status === "Closed") ? false : true);
                    if (null != completedDates) {
                        FirebaseAdminWrapper.GetInstance().SetValue(schedulesAssignPath + "completedDates/", completedDates);
                    }
                }
            });
        }
        return task;
    }

    static async AssignTask(orgID: string, buildingID: string, task: ITask, authorID: string, authorName: string, message: string = null): Promise<void> {
        let oldTask: ITask = null;
        oldTask = await this.GetTask(task.id);
        if (null != oldTask.assignees) {
            for (let assignee of oldTask.assignees) {
                let schedulesPath: string = Constants.STR_SCHEDULES_FSLASH + assignee.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(schedulesPath)) {
                    await FirebaseAdminWrapper.GetInstance().DeleteValue(schedulesPath);
                }
            }
        }
        if (null != task.assignees) {
            for (let newAssignee of task.assignees) {
                let newSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + newAssignee.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(newSchedulesPath, task);
            }
        }
        const schedulesPath: string = Constants.STR_SCHEDULES_FSLASH + task.clientID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (task.clientID.length > 20) {
            await FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath, task);
        }
        const schedulePath: string = Constants.STR_FSLASH_ORGS_FSLASH + task.orgID + Constants.STR_FSLASH_TASKS_FSLASH + task.buildingID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(schedulePath)) {
            await FirebaseAdminWrapper.GetInstance().SetValue(schedulePath, task);
        }
        const path: string = Constants.STR_ALL_TASKS_FSLASH + task.id + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(path, task);
        let userRef = await UserAdminApiInternal.GetUser(authorID);
        if (!task.recurring) {
            let newFeed: ITaskFeed = {
                type: "assign",
                taskID: task.id,
                message: (null != message) ? message : `This task was assigned to a Silver Brick team member on ${ new Date(Date.now()).toDateString() }`,
                timestamp: new Date(Date.now()).getTime(),
                orgID: task.orgID,
                category: task.category,
                subCategory: task.category,
                buildingID: task.buildingID,
                buildingName: task.buildingName,
                clientUnit: task.clientUnit,
                authorName: authorName,
                role: task.updatedRole,
                audience: Constants.STR_AUDIENCE_STAFF_ONLY,
                authorID: authorID,
                status: "Assigned",
                approved: ((null != task.updatedRole) && (task.updatedRole === "Staff") && (!userRef.isAdmin)) ? false : true,
                assignees: (null != task.assignees) ? task.assignees : []
            };
            await this.RecordActivity(task.orgID, task.buildingID, newFeed);
        }
        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
        if (null != landlords) {
            for (let landlord of landlords) {
                let landSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + landlord.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(landSchedulesPath, task);
            }
        }
    }

    static async ProgressTask(orgID: string, buildingID: string, task: ITask, authorName: string, authorID: string): Promise<ITask> {
        const bookingPath: string = Constants.STR_FSLASH_ORGS_FSLASH + task.orgID + Constants.STR_FSLASH_TASKS_FSLASH + task.buildingID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingPath)) {
            await FirebaseAdminWrapper.GetInstance().SetValue(bookingPath, task);
        }
        const schedulesPath: string = Constants.STR_SCHEDULES_FSLASH + task.clientID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (task.clientID.length > 20) {
            await FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath, task);
        }
        if (null != task.assignees) {
            for (let newAssignee of task.assignees) {
                let newSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + newAssignee.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(newSchedulesPath, task);
            }
        }
        const path: string = Constants.STR_ALL_TASKS_FSLASH + task.id + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(path, task);
        let userRef = await UserAdminApiInternal.GetUser(authorID);
        if (!task.recurring) {
            let newFeed: ITaskFeed = {
                type: "update",
                taskID: task.id,
                message: `${ task.updateAuthor } marked this task as in progress`,
                timestamp: new Date(Date.now()).getTime(),
                category: task.category,
                subCategory: task.category,
                orgID: task.orgID,
                buildingID: task.buildingID,
                buildingName: task.buildingName,
                clientUnit: task.clientUnit,
                audience: Constants.STR_AUDIENCE_STAFF_ONLY,
                authorName: authorName,
                role: task.updatedRole,
                approved: ((null != task.updatedRole) && (task.updatedRole === "Staff") && (!userRef.isAdmin)) ? false : true,
                authorID: authorID,
                status: "In Progress",
                assignees: (null != task.assignees) ? task.assignees : []
            };
            await this.RecordActivity(task.orgID, task.buildingID, newFeed);
        }
        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
        if (null != landlords) {
            for (let landlord of landlords) {
                let landSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + landlord.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(landSchedulesPath, task);
            }
        }
        return task;
    }

    static async CompleteTask(orgID: string, buildingID: string, task: ITask, authorName: string, authorID: string): Promise<ITask> {
        task.status = "Completed";
        const bookingPath: string = Constants.STR_FSLASH_ORGS_FSLASH + task.orgID + Constants.STR_FSLASH_TASKS_FSLASH + task.buildingID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingPath)) {
            await FirebaseAdminWrapper.GetInstance().SetValue(bookingPath, task);
        }
        const schedulesPath: string = Constants.STR_SCHEDULES_FSLASH + task.clientID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (task.clientID.length > 20) {
            await FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath, task);
        }
        if (null != task.assignees) {
            for (let newAssignee of task.assignees) {
                let newSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + newAssignee.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(newSchedulesPath, task);
            }
        }
        const path: string = Constants.STR_ALL_TASKS_FSLASH + task.id + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(path, task);
        let userRef: IUser = await UserAdminApiInternal.GetUser(authorID);
        if (!task.recurring) {
            let newFeed: ITaskFeed = {
                type: "complete",
                taskID: task.id,
                message: `${ authorName } marked this task as complete`,
                timestamp: new Date(Date.now()).getTime(),
                category: task.category,
                subCategory: task.category,
                orgID: task.orgID,
                buildingID: task.buildingID,
                buildingName: task.buildingName,
                clientUnit: task.clientUnit,
                audience: Constants.STR_AUDIENCE_STAFF_ONLY,
                authorName: authorName,
                role: task.updatedRole,
                authorID: authorID,
                approved: ((null != task.updatedRole) && (task.updatedRole === "Staff") && (!userRef.isAdmin)) ? false : true,
                status: "Completed",
                assignees: (null != task.assignees) ? task.assignees : []
            };
            await this.RecordActivity(task.orgID, task.buildingID, newFeed);
        }
        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
        if (null != landlords) {
            for (let landlord of landlords) {
                let landSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + landlord.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(landSchedulesPath, task);
            }
        }
        return task;
    }

    static async UnCompleteTask(orgID: string, buildingID: string, task: ITask, authorName: string, authorID: string): Promise<ITask> {
        task.status = "Assigned";
        const bookingPath: string = Constants.STR_FSLASH_ORGS_FSLASH + task.orgID + Constants.STR_FSLASH_TASKS_FSLASH + task.buildingID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingPath)) {
            await FirebaseAdminWrapper.GetInstance().SetValue(bookingPath, task);
        }
        const schedulesPath: string = Constants.STR_SCHEDULES_FSLASH + task.clientID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
        if (task.clientID.length > 20) {
            await FirebaseAdminWrapper.GetInstance().SetValue(schedulesPath, task);
        }
        if (null != task.assignees) {
            for (let newAssignee of task.assignees) {
                let newSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + newAssignee.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(newSchedulesPath, task);
            }
        }
        const path: string = Constants.STR_ALL_TASKS_FSLASH + task.id + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(path, task);
        // let newFeed: ITaskFeed = {
        //     type: "update",
        //     taskID: task.id,
        //     message: `${ authorName } marked this task as complete`,
        //     timestamp: new Date(Date.now()).getTime(),
        //     category: task.category,
        //     subCategory: task.category,
        //     orgID: task.orgID,
        //     buildingID: task.buildingID,
        //     buildingName: task.buildingName,
        //     clientUnit: task.clientUnit,
        //     approved: ((null != task.updatedRole) && (task.updatedRole === "Staff")) ? false : true,
        //     audience: Constants.STR_AUDIENCE_STAFF_ONLY,
        //     authorName: authorName,
        //     role: task.updatedRole,
        //     authorID: authorID,
        //     status: "In Progress",
        //     assignees: (null != task.assignees) ? task.assignees : []
        // };
        // await this.RecordActivity(task.orgID, task.buildingID, newFeed);
        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(task.orgID, task.buildingID);
        if (null != landlords) {
            for (let landlord of landlords) {
                let landSchedulesPath: string = Constants.STR_SCHEDULES_FSLASH + landlord.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(landSchedulesPath, task);
            }
        }
        return task;
    }

    static async GetTenantSchedule(orgID: string, buildingID: string, userID: string): Promise<ITask[]> {
        let bookings: ITask[] = [];
        const bookingsPath: string = Constants.STR_SCHEDULES_FSLASH + userID + Constants.STR_FSLASH;
        const bookingsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(bookingsPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingsPath)) {
            bookingsSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newBook: ITask = <ITask>innerSnapshot.val();
                newBook.id = innerSnapshot.key;
                bookings.push(newBook);
                return false;
            });
        }
        return bookings;
    }

    static async GetLandlordSchedule(userID: string): Promise<ITask[]> {
        let tasks: ITask[] = [];
        const taskPath: string = Constants.STR_SCHEDULES_FSLASH + userID + Constants.STR_FSLASH;
        const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(taskPath);
        const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("recurring").equalTo(false);
        const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
            taskSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newTask: ITask = <ITask>innerSnapshot.val();
                // console.log("taskPath", newTask);
                if (newTask.scheduled) {
                    newTask.id = innerSnapshot.key;
                    tasks.push(newTask);
                    return false;
                }
            });
        }
        return ArrayUtil.SortTasksByDate(tasks);
    }

    static async GetLandlordScheduleCompleted(userID: string): Promise<ITask[]> {
        let tasks: ITask[] = [];
        const taskPath: string = Constants.STR_SCHEDULES_FSLASH + userID + Constants.STR_FSLASH;
        const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(taskPath);
        const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("recurring").equalTo(false);
        const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
            taskSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newTask: ITask = <ITask>innerSnapshot.val();
                // console.log("taskPath", newTask);
                if (newTask.scheduled) {
                    newTask.id = innerSnapshot.key;
                    tasks.push(newTask);
                    return false;
                }
            });
        }
        return ArrayUtil.SortTasksByDate(tasks);
    }

    static async GetStaffSchedule(orgID: string, buildingID: string, userID: string): Promise<ITask[]> {
        let bookings: ITask[] = [];
        const bookingsPath: string = Constants.STR_SCHEDULES_FSLASH + userID + Constants.STR_FSLASH;
        const bookingsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(bookingsPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingsPath)) {
            bookingsSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newBook: ITask = <ITask>innerSnapshot.val();
                newBook.id = innerSnapshot.key;
                bookings.push(newBook);
                return false;
            });
        }
        return bookings;
    }

    static async GetStaffRecurringSchedule(userID: string): Promise<ITask[]> {
        let bookings: ITask[] = [];
        const bookingsPath: string = Constants.STR_SCHEDULES_FSLASH + userID + Constants.STR_FSLASH;
        const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(bookingsPath);
        const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("scheduled").equalTo(true);
        const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingsPath)) {
            taskSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newBook: ITask = <ITask>innerSnapshot.val();
                newBook.id = innerSnapshot.key;
                switch (newBook.durationAmount) {
                    case this.DurationOptions[0]:
                    {
                        let endDate: Date = new Date(newBook.startDate);
                        endDate.setDate(endDate.getDate() + Number(newBook.durationQuantity));
                        endDate.setHours(endDate.getHours() - 4);
                        newBook.endDate = endDate.getTime();
                        bookings.push(newBook);
                        break;
                    }

                    case this.DurationOptions[1]:
                    {
                        let endDate: Date = new Date(newBook.startDate);
                        endDate.setDate(endDate.getDate() + (Number(newBook.durationQuantity) * 7));
                        endDate.setHours(endDate.getHours() - 4);
                        newBook.endDate = endDate.getTime();
                        bookings.push(newBook);
                        break;
                    }

                    case this.DurationOptions[2]:
                    {
                        let date: Date = new Date(newBook.startDate);
                        let currentDate = date.getDate();
                        // Set to day 1 to avoid forward
                        date.setDate(1);
                        // Increase month by 1
                        date.setMonth(date.getMonth() + Number(newBook.durationQuantity) + 1);
                        // Get max # of days in this new month
                        let daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
                        // Set the date to the minimum of current date of days in month
                        date.setDate(Math.min(currentDate, daysInMonth));
                        date.setHours(date.getHours() - 4);
                        newBook.endDate = date.getTime();
                        bookings.push(newBook);
                        break;
                    }

                    case this.DurationOptions[3]:
                    {
                        let endDate: Date = new Date(newBook.startDate);
                        endDate.setFullYear(endDate.getFullYear() + Number(newBook.durationQuantity));
                        endDate.setHours(endDate.getHours() - 4);
                        newBook.endDate = endDate.getTime();
                        bookings.push(newBook);
                        break;
                    }

                    case this.DurationOptions[4]:
                    {
                        let endDate: Date = new Date(newBook.startDate);
                        endDate.setFullYear(endDate.getFullYear() + Number(20));
                        endDate.setHours(endDate.getHours() - 4);
                        newBook.endDate = endDate.getTime();
                        bookings.push(newBook);
                        break;
                    }
                    
                    default:
                    {
                        break;
                    }
                }
                return false;
            });
        }
        return bookings;
    }

    /*
        HISTORY/ACTIVITY METHODS
    */

    static async GetSchedule(orgID: string, buildingID: string): Promise<ITask[]> {
        let tasks: ITask[] = [];
        const bookingsPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_SCHEDULES_FSLASH + buildingID + Constants.STR_FSLASH;
        const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(bookingsPath);
        const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("active").equalTo(true);
        const bookingsSnap: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingsPath)) {
            bookingsSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newBook: ITask = <ITask>innerSnapshot.val();
                newBook.id = innerSnapshot.key;
                tasks.push(newBook);
                return false;
            });
        }
        return tasks;
    }

    static async GetAllHistory(): Promise<ITaskFeed[]> {
        let orgs: IOrgBasic[] = await SilverBrickAdminApiInternal.GetOrgs();
        let tasks: ITaskFeed[] = [];
        for (let org of orgs) {
            let tasksPath: string = Constants.STR_FSLASH_ORGS_FSLASH + org.id + Constants.STR_FSLASH_HISTORY_FSLASH;
            let tasksSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(tasksPath);
            if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(tasksPath)) {
                const newBuildings: string[] = Object.keys(tasksSnap.val());
                // // console.log("newBuildings", newBuildings);
                for (let bldg of newBuildings) {
                    if (tasks.length < 10) {
                        const taskPath: string = Constants.STR_FSLASH_ORGS_FSLASH + org.id + Constants.STR_FSLASH_HISTORY_FSLASH + bldg + Constants.STR_FSLASH;
                        const taskRef: firebaseAdmin.database.Query = await FirebaseAdminWrapper.GetInstance().GetRef(taskPath).orderByChild("timestamp");
                        const taskSnap: firebaseAdmin.database.DataSnapshot = await taskRef.once(Constants.STR_VALUE);
                        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
                            tasks = [...tasks, ...ArrayUtil.Transform(<ITaskFeed[]>taskSnap.val())];
                        }
                    }
                }
            }
        }
        return ArrayUtil.SortByDate(tasks);
    }

    static async ApproveHistory(orgID: string, buildingID: string, feed: ITaskFeed): Promise<ITaskFeed> {
        const bookingsPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_HISTORY_FSLASH + feed.taskID + Constants.STR_FSLASH + feed.id + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(bookingsPath + Constants.STR_FSLASH + "approved" + Constants.STR_FSLASH, true);
        await FirebaseAdminWrapper.GetInstance().SetValue(bookingsPath + Constants.STR_FSLASH + "approveDate" + Constants.STR_FSLASH, feed.approveDate);
        await FirebaseAdminWrapper.GetInstance().SetValue(bookingsPath + Constants.STR_FSLASH + "approveAuthor" + Constants.STR_FSLASH, feed.approveAuthor);
        await FirebaseAdminWrapper.GetInstance().SetValue(bookingsPath + Constants.STR_FSLASH + "approveAuthorID" + Constants.STR_FSLASH, feed.approveAuthorID);
        return feed;
    }

    static async UpdateHistory(orgID: string, buildingID: string, feed: ITaskFeed): Promise<ITaskFeed> {
        const bookingsPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_HISTORY_FSLASH + feed.taskID + Constants.STR_FSLASH + feed.id + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(bookingsPath + Constants.STR_FSLASH, feed);
        return feed;
    }

    static async GetHistory(orgID: string, taskID: string, role: string, userID: string): Promise<ITaskFeed[]> {
        let tasks: ITaskFeed[] = [];
        const bookingsPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_HISTORY_FSLASH + taskID + Constants.STR_FSLASH;
        const bookingsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(bookingsPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingsPath)) {
            let newFeed: ITaskFeed[] = ArrayUtil.Transform(<ITaskFeed[]>bookingsSnap.val());
            if (role === "landlord") {
                newFeed = newFeed.filter((feed: ITaskFeed) => {
                    return (((null != feed.approved) && (feed.approved)) && ((feed.audience === Constants.STR_AUDIENCE_LANDLORD_ONLY) || (feed.audience === Constants.STR_AUDIENCE_TENANT_AND_LANDLORD))) || (feed.authorID === userID);
                });
                tasks = [...tasks, ...newFeed];
            }
            else if (role === "super_admin") {
                tasks = newFeed;
            }
            else if (role === "staff") {
                newFeed = newFeed.filter((task: ITaskFeed) => {
                    return (((null != task.approved) && (task.approved)) || (task.authorID === userID));
                });
                tasks = [...tasks, ...newFeed];
            }
            else {
                newFeed = newFeed.filter((feed: ITaskFeed) => {
                    return (((null != feed.approved) && (feed.approved)) && (feed.audience === Constants.STR_AUDIENCE_TENANT_ONLY) || (feed.audience === Constants.STR_AUDIENCE_TENANT_AND_LANDLORD) || (feed.audience === Constants.STR_AUDIENCE_TENANT_AND_STAFF)) || (feed.authorID === userID);
                });
                tasks = [...tasks, ...newFeed];
            }
        }
        return ArrayUtil.SortByDate(tasks);
    }

    static async GetStaffHistory(userID: string): Promise<ITaskFeed[]> {
        let tasksFeed: ITaskFeed[] = [];
        const bookingsPath: string = Constants.STR_FSLASH_SCHEDULES_FSLASH + userID + Constants.STR_FSLASH;
        const bookingsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(bookingsPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingsPath)) {
            let tasks: ITask[] = <ITask[]>ArrayUtil.Transform(bookingsSnap.val());
            for (let task of tasks) {
                let feedPath: string = Constants.STR_FSLASH_ORGS_FSLASH + task.orgID + Constants.STR_FSLASH_HISTORY_FSLASH + task.id + Constants.STR_FSLASH;
                let feedSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(feedPath);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(feedPath)) {
                    let newFeed: ITaskFeed[] = ArrayUtil.Transform(<ITaskFeed[]>feedSnap.val());
                    tasksFeed = [...tasksFeed, ...newFeed];
                    tasksFeed = tasksFeed.filter((task: ITaskFeed) => {
                        return (((null != task.approved) && (task.approved)) || (task.authorID === userID));
                    });
                    // // console.log("tasksFeed", tasksFeed);
                }
            }
        }
        return ArrayUtil.SortByDate(tasksFeed);
    }

    static async GetTenantHistory(userID: string): Promise<ITaskFeed[]> {
        let tasksFeed: ITaskFeed[] = [];
        const bookingsPath: string = Constants.STR_FSLASH_SCHEDULES_FSLASH + userID + Constants.STR_FSLASH;
        const bookingsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(bookingsPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingsPath)) {
            let tasks: ITask[] = <ITask[]>ArrayUtil.Transform(bookingsSnap.val());
            for (let task of tasks) {
                let feedPath: string = Constants.STR_FSLASH_ORGS_FSLASH + task.orgID + Constants.STR_FSLASH_HISTORY_FSLASH + task.id + Constants.STR_FSLASH;
                let feedSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(feedPath);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(feedPath)) {
                    let newFeed: ITaskFeed[] = ArrayUtil.Transform(<ITaskFeed[]>feedSnap.val());
                    newFeed = newFeed.filter((feed: ITaskFeed) => {
                        return ((feed.audience === Constants.STR_AUDIENCE_TENANT_ONLY) || (feed.audience === Constants.STR_AUDIENCE_TENANT_AND_LANDLORD) || (feed.audience === Constants.STR_AUDIENCE_TENANT_AND_STAFF));
                    });
                    newFeed = newFeed.filter((task: ITaskFeed) => {
                        return ((null != task.approved) && (task.approved));
                    });
                    tasksFeed = [...tasksFeed, ...newFeed];
                    // // console.log("tasksFeed", tasksFeed);
                }
            }
        }
        return ArrayUtil.SortByDate(tasksFeed);
    }

    static async GetLandlordHistory(userID: string): Promise<ITaskFeed[]> {
        let tasksFeed: ITaskFeed[] = [];
        const bookingsPath: string = Constants.STR_FSLASH_SCHEDULES_FSLASH + userID + Constants.STR_FSLASH;
        const bookingsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(bookingsPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingsPath)) {
            let tasks: ITask[] = <ITask[]>ArrayUtil.Transform(bookingsSnap.val());
            for (let task of tasks) {
                let feedPath: string = Constants.STR_FSLASH_ORGS_FSLASH + task.orgID + Constants.STR_FSLASH_HISTORY_FSLASH + task.id + Constants.STR_FSLASH;
                let feedSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(feedPath);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(feedPath)) {
                    let newFeed: ITaskFeed[] = ArrayUtil.Transform(<ITaskFeed[]>feedSnap.val());
                    newFeed = newFeed.filter((feed: ITaskFeed) => {
                        return ((feed.audience === Constants.STR_AUDIENCE_LANDLORD_ONLY) || (feed.audience === Constants.STR_AUDIENCE_TENANT_AND_LANDLORD));
                    });
                    newFeed = newFeed.filter((task: ITaskFeed) => {
                        return ((null != task.approved) && (task.approved));
                    });
                    tasksFeed = [...tasksFeed, ...newFeed];
                }
            }
        }
        return ArrayUtil.SortByDate(tasksFeed);
    }

    static async RecordActivity(orgID: string, buildingID: string, task: ITaskFeed): Promise<ITaskFeed> {
        if (!task || !orgID || !buildingID) {
            throw new Error("Parameters cannot be null! task: " + task + " orgID: " + orgID + " buildingID: " + buildingID);
        }
        const historyPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH + Constants.STR_HISTORY_FSLASH + task.taskID + Constants.STR_FSLASH;
        let historyRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(historyPath, task);
        let historyID: string = historyRef.key;
        task.id = historyID;
        await FirebaseAdminWrapper.GetInstance().SetValue(historyPath + historyID + Constants.STR_FSLASH + "id/", historyID);
        return task;
    }

    static async CheckHistory(orgID: string, buildingID: string, task: ITask): Promise<boolean> {
        const historyPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH + Constants.STR_HISTORY_FSLASH + task.id + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(historyPath)) {
            return true;
        }
        else {
            return false;
        }
    }

    static async GetNotes(orgID: string, buildingID: string): Promise<INote[]> {
        let notes: INote[] = [];
        const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_NOTES_FSLASH + buildingID + Constants.STR_FSLASH;
        const notesRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(profileUserPath);
        const notesSnap: firebaseAdmin.database.Query = notesRef.orderByChild("deleted").equalTo(false);
        const notesSnap2: firebaseAdmin.database.DataSnapshot = await notesSnap.once(Constants.STR_VALUE);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(profileUserPath)) {
            notesSnap2.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newNotes: INote = <INote>innerSnapshot.val();
                newNotes.id = innerSnapshot.key;
                notes.push(newNotes);
                return false;
            });
        }
        return notes;
    }

    static async AddNote(orgID: string, buildingID: string, note: INote): Promise<INote> {
        const notePath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_NOTES_FSLASH + buildingID + Constants.STR_FSLASH;
        const noteRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(notePath, note);
        const noteID: string = noteRef.key;
        note.id = noteID;
        const servIDPath: string = notePath + noteID + Constants.STR_FSLASH + "id/";
        await FirebaseAdminWrapper.GetInstance().SetValue(servIDPath, noteID);
        return note;
    }

    static async UpdateNote(orgID: string, buildingID: string, note: INote): Promise<INote> {
        note.updated = new Date(Date.now()).getTime();
        const notePath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_NOTES_FSLASH + buildingID + Constants.STR_FSLASH + note.id + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(notePath)) {
            await FirebaseAdminWrapper.GetInstance().SetValue(notePath, note);
        }
        return note;
    }

    static async DeleteNote(orgID: string, buildingID: string, noteID: string): Promise<void> {
        const notePath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_NOTES_FSLASH + buildingID + Constants.STR_FSLASH + noteID + Constants.STR_FSLASH + "deleted/";
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(notePath)) {
            await FirebaseAdminWrapper.GetInstance().SetValue(notePath, true);
        }
    }
}