// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.text', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as firebaseAdmin from "firebase-admin";

// Local Modules
import { FirebaseAdminWrapper } from "../wrappers/FirebaseAdminWrapper";
import { GCloudStorageWrapper } from "../wrappers/GCloudStorageWrapper";
// Logger Modules
import { IMessage } from "../shared/SilverBrickTypes";
import { Constants } from "../common/Constants";
import { AddChatHelper, NewChatRoomHelper, UserChatRoom, MessageParticipant } from "../shared/ResponseTypes";
import { UserApiInternal } from "./UserApiInternal";
import { StatusOrchestrator } from "../common/StatusOrchestrator";
import { ArrayUtil } from "../common/ArrayUtil";

export class MessageApiInternal {
    static async NewChatRoom(helper: NewChatRoomHelper): Promise<NewChatRoomHelper> {
        const roomPath: string = Constants.STR_MESSAGES_FSLASH + Constants.STR_ROOMS_FSLASH;
        try {
            const newRoomRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(roomPath, " ");
            const roomID: string = newRoomRef.key;
            helper.roomID = roomID;
            const newRoomPath: string = roomPath + roomID + Constants.STR_FSLASH;
            const roomRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(newRoomPath, helper.message);
            let messageID: string = roomRef.key;
            helper.message.id = messageID;
            const messageMap: string = Constants.STR_MESSAGES_FSLASH + Constants.STR_MAP_FSLASH;
            const byRoomPath: string = messageMap + Constants.STR_BY_ROOM_FSLASH + roomID + Constants.STR_FSLASH;
            let user: MessageParticipant = helper.participants[0];
            if (null == user.role) {
                user.role = "Tenant";
            }
            await FirebaseAdminWrapper.GetInstance().SetValue(byRoomPath + user.uid + Constants.STR_FSLASH, user);
            let messageAction = helper.participants.forEach((user: MessageParticipant) => {
                let byUserPath: string = messageMap + Constants.STR_BY_USER_FSLASH + user.uid + Constants.STR_FSLASH + roomID;
                FirebaseAdminWrapper.GetInstance().SetValue(byUserPath + "/roomID/", roomID);
                FirebaseAdminWrapper.GetInstance().SetValue(byUserPath + "/authorID/", helper.authorID);
                FirebaseAdminWrapper.GetInstance().SetValue(byUserPath + Constants.STR_FSLASH_LAST_MESSAGE_FSLASH, helper.message);
                if (helper.message.type === "photo") {
                    FirebaseAdminWrapper.GetInstance().SetValue(byUserPath + Constants.STR_FSLASH_LAST_MESSAGE_FSLASH + "text/", `${helper.message.authour} added a photo`);
                    // helper.message.text = await this.GetMessageImage(helper.message.text);
                }
                for (let nextUser of helper.participants) {
                    FirebaseAdminWrapper.GetInstance().SetValue(byUserPath + Constants.STR_FSLASH + "users/" + nextUser.uid, nextUser);
                }
            });
            await messageAction;
        } catch (error) {
            console.log("Error New Chat", error);
            await StatusOrchestrator.GetInstance().SendStatus(`NewChatRoom: ${helper.roomID} - ${error.message}`, "errors");
            throw new Error("Error in New Chat " + error);
        }
        return helper;
    }

    static async AddMessage(helper: AddChatHelper, roomID: string): Promise<AddChatHelper> {
        try {
            const roomPath: string = Constants.STR_MESSAGES_FSLASH + Constants.STR_ROOMS_FSLASH + roomID + Constants.STR_FSLASH;
            let messageParticipants: MessageParticipant[] = await this.GetRoomUsers(roomID, helper.authorID);
            const roomRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(roomPath, helper.message);
            const messageID: string = roomRef.key;
            helper.message.id = messageID;
            const messageMap: string = Constants.STR_MESSAGES_FSLASH + Constants.STR_MAP_FSLASH;
            for (let user of messageParticipants) {
                let byUserPath: string = messageMap + Constants.STR_BY_USER_FSLASH + user.uid + Constants.STR_FSLASH + roomID;
                await FirebaseAdminWrapper.GetInstance().SetValue(byUserPath + Constants.STR_FSLASH_LAST_MESSAGE_FSLASH, helper.message);
                if (helper.message.type === "photo") {
                    FirebaseAdminWrapper.GetInstance().SetValue(byUserPath + Constants.STR_FSLASH_LAST_MESSAGE_FSLASH + "text/", `${helper.message.authour} added a photo`);
                }
            }
            const newMessage: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(roomPath + messageID);
            // helper.message = newMessage.val();
            // if (helper.message.type === "photo") {
            //     helper.message.text = await this.GetMessageImage(helper.message.text);
            // }
            return helper;
        } catch (error) {
            // await StatusOrchestrator.GetInstance().SendStatus(`AddMessage: ${helper.roomID} - ${error.message}`, "errors");
            throw new Error(error);
        }
    }

    static async GetRoomUsers(roomID: string, userID: string): Promise<MessageParticipant[]> {
        let users: MessageParticipant[] = [];
        const roomPath: string = Constants.STR_MESSAGES_FSLASH + Constants.STR_MAP_FSLASH + Constants.STR_BY_USER_FSLASH + userID + Constants.STR_FSLASH + roomID + Constants.STR_FSLASH_USERS_FSLASH;
        const roomSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(roomPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(roomPath)) {
            roomSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newUser: MessageParticipant = <MessageParticipant>innerSnapshot.val();
                if (null == newUser.role) {
                    newUser.role = "N/A";
                }
                users.push(newUser);
                return false;
            });
        }
        return users;
    }

    static async GetChatRoomsDashboard(): Promise<UserChatRoom[]> {
        let rooms: UserChatRoom[] = [];
        const byUserPath: string = Constants.STR_MESSAGES_FSLASH + Constants.STR_MAP_FSLASH + Constants.STR_BY_USER_FSLASH;
        const userRoomsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(byUserPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(byUserPath)) {
            let roomKeys: string[] = <string[]>Object.keys(userRoomsSnap.val());
            let keys: UserChatRoom[] = <UserChatRoom[]>ArrayUtil.Transform(userRoomsSnap.val());
            let roomSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(byUserPath);
            try {
                for (let key of roomKeys) {
                    let index: number = roomKeys.findIndex((val: string) => {
                        return val === key;
                    });
                    let userRoomPath: string = Constants.STR_MESSAGES_FSLASH + Constants.STR_MAP_FSLASH + Constants.STR_BY_USER_FSLASH + roomKeys[index] + Constants.STR_FSLASH;
                    let userRoomSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(userRoomPath);
                    if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(userRoomPath)) {
                        userRoomSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                            let userRoom: UserChatRoom = <UserChatRoom>innerSnapshot.val();
                            let match: UserChatRoom = rooms.find((rom: UserChatRoom) => {
                                return rom.roomID === userRoom.roomID;
                            });
                            if (null == match) {
                                rooms.push(userRoom);
                            }
                            return false;
                        });
                    }
                }
                // if (roomSnap.val()) {
                //   roomSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                //     let newRoom: UserChatRoom = <UserChatRoom>innerSnapshot.val();
                //     newRoom.roomID = innerSnapshot.key;
                //     rooms.push(newRoom);
                //     return false;
                //   });
                // }
                // for (let room of rooms) {
                //     room.lastMessage.photoURL = await UserApiInternal.GetAvatar(room.lastMessage.authorID);
                //     for (let uid in room.users) {
                //         room.users[uid].photoURL = await UserApiInternal.GetAvatar(uid);
                //     }
                // }
                return rooms;
            } catch (error) {
                console.log("Error GetChatRooms", error);
                throw new Error("Error in GetChatRooms " + error);
            }
        }
        else {
            return rooms;
        }
    }

    static async GetChatRooms(userID: string): Promise<UserChatRoom[]> {
        let rooms: UserChatRoom[] = [];
        const byUserPath: string = Constants.STR_MESSAGES_FSLASH + Constants.STR_MAP_FSLASH + Constants.STR_BY_USER_FSLASH + userID + Constants.STR_FSLASH;
        const roomSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(byUserPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(byUserPath)) {
            try {
                if (roomSnap.val()) {
                    roomSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                        let newRoom: UserChatRoom = <UserChatRoom>innerSnapshot.val();
                        newRoom.roomID = innerSnapshot.key;
                        rooms.push(newRoom);
                        return false;
                    });
                }

                // for (let room of rooms) {
                //     room.lastMessage.photoURL = await UserApiInternal.GetAvatar(room.lastMessage.authorID);
                //     for (let uid in room.users) {
                //         room.users[uid].photoURL = await UserApiInternal.GetAvatar(uid);
                //     }
                // }
                return rooms;
            } catch (error) {
                console.log("Error GetChatRooms", error);
                await StatusOrchestrator.GetInstance().SendStatus(`GetChatRooms: ${userID} - ${error.message}`, "errors");
                throw new Error("Error in GetChatRooms " + error);
            }
        }
        else {
            return rooms;
        }
    }

    static async GetMessageHistory(roomID: string, cursor: string = null): Promise<IMessage[]> {
        try {
            if (!roomID) {
                throw new Error("GetMessageHistory " + roomID);
            }
            let messages: IMessage[] = [];
            const messagePath: string = Constants.STR_MESSAGES_FSLASH + Constants.STR_ROOMS_FSLASH + roomID;
            let messageQuery: firebaseAdmin.database.Query = await FirebaseAdminWrapper.GetInstance()
                .GetRef(messagePath)
                .orderByKey()
                .limitToLast(Constants.NUM_PAGINATION_LIMIT + Constants.NUM_LIMIT_BUFFER_ONE);
            if (cursor) {
                messageQuery = messageQuery.startAt(cursor);
            }
            const messageSnap: firebaseAdmin.database.DataSnapshot = await messageQuery.once(Constants.STR_VALUE);
            if (messageSnap) {
                messageSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                    let newMessage: IMessage = <IMessage>innerSnapshot.val();
                    if (null == newMessage.role) {
                        newMessage.role = "Tenant";
                    }
                    newMessage.id = innerSnapshot.key;
                    messages.push(newMessage);
                    return false;
                });
            } else {
                // no snapshot!
            }
            // Get proper photoURL of chats with photos
            // for (let message of messages) {
            //     // Check for images and get download URL
            //     if (message.type === Constants.STR_PHOTO) {
            //         message.text = await this.GetMessageImage(message.text);
            //     } else {
            //         // Msg type is not an image... continue
            //     }
            //     let avatar: string = await UserApiInternal.GetAvatar(message.authorID);
            //     message.photoURL = avatar;
            // }
            const sortedMessages: IMessage[] = this._sortMessages(messages);
            return sortedMessages;
        } catch (error) {
            await StatusOrchestrator.GetInstance().SendStatus(`GetMessageHistory: ${roomID} - ${cursor} - ${error.message}`, "errors");
            throw new Error("Error in GetMessageHistory " + error);
        }
    }

    /* HELPERS */

    public static async GetMessageImage(message: string): Promise<string> {
        try {
            // Check for images and get download URL
            let photoURL: string = null;
            try {
                // Go get real URL from firebase storage as we save the reference path instead
                const downloadURL: string = await GCloudStorageWrapper.GetInstance().GetDownloadUrl(message.trim());
                if (downloadURL) {
                    photoURL = downloadURL.trim();
                } else {
                    // Download url is empty for some reason...
                    // Set broken image (this should be stored locally on device)
                    photoURL = "n/a";
                }
            } catch (error) {
                // Error getting the download url...
                // Set broken image (this should be stored locally on device)
                photoURL = "n/a";
            }

            return photoURL;
        } catch (error) {
            await StatusOrchestrator.GetInstance().SendStatus(`GetMessageImage: ${message} - ${error.message}`, "errors");
            throw new Error("Error in GetMessageImage " + error);
        }
    }

    private static _sortMessages(messages: IMessage[]): IMessage[] {
        let messagesList: IMessage[] = messages.sort((message1, message2) => {
            if (message1.date < message2.date) {
                return -1;
            }

            if (message1.date > message2.date) {
                return 1;
            }

            return 0;
        });
        return messagesList;
    }
}