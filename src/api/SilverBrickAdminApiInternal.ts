// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as firebaseAdmin from "firebase-admin";
import { IShippingInformation, customers, ICard, cards, subscriptions } from "stripe";

// Local Modules
import { FirebaseAdminWrapper } from "../wrappers/FirebaseAdminWrapper";
import { GCloudStorageWrapper } from "../wrappers/GCloudStorageWrapper";

// Util modules
import { JSONUtil } from "../common/JsonUtil";
import { TimeUtil } from "../common/TimeUtil";
import { StringUtil } from "../common/StringUtil";

// Api Modules
import { UserApiInternal } from "./UserApiInternal";
import { UserAdminApiInternal } from "./UserAdminApiInternal";
import { ServiceApiInternal } from "./ServiceApiInternal";
import { StripeWrapper } from "../wrappers/StripeWrapper";

// Factory Modules
import { NormalizedUser } from "../factories/users/NormalizedUser";

// Type Related Modules
import { Constants } from "../common/Constants";
import {
    IUser,
    IUserBuilding,
    IBuildingBasic,
    IUserOrg,
    IUserAudience,
    IOrgInfo,
    IBuildingInfo,
    IBuildingSearch,
    IProfile,
    ITask,
    IBuilding,
    IVendor,
    IOrgBasic,
    IBuildingBasicInfo,
    IStaffProfile
} from "../shared/SilverBrickTypes";
import { StatusOrchestrator } from "../common/StatusOrchestrator";
import { ArrayUtil } from "../common/ArrayUtil";


// Util Modules


// Logger Modules
import { Logger, SessionLogBuilder, LogLevel } from "../common/Logger";


export class SilverBrickAdminApiInternal {
    /* DBA APIs */

    static async GetOrgs(): Promise<IOrgBasic[]> {
        const orgPath: string = Constants.STR_FSLASH_ORGS_FSLASH + Constants.STR_SUMMARY_FSLASH;
        let orgs: IOrgBasic[] = [];
        const orgsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(orgPath);

        orgsSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
            const org: IOrgBasic = <IOrgBasic>innerSnapshot.val();
            if (null != org.buildings) {
                org.buildings = ArrayUtil.Transform(org.buildings);
            }
            org.id = innerSnapshot.key;
            orgs.push(org);
            return false;
        });
        return orgs;
    }

    static async GetOrgsInfo(): Promise<IOrgInfo[]> {
        const orgPath: string = Constants.STR_FSLASH_ORGS_FSLASH + Constants.STR_SUMMARY_FSLASH;
        let orgs: IOrgInfo[] = [];
        const orgsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(orgPath);
        let dataPayload: string[] = Object.keys(orgsSnap.val());
        for (let orgID of dataPayload) {
            let newPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_INFO;
            const orgInfoSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(newPath);
            const org: IOrgInfo = <IOrgInfo>orgInfoSnap.val();
            org.id = orgID;
            orgs.push(org);
        }
        return orgs;
    }

    static async GetOrgsAllBuildings(): Promise<any[]> {
        const orgPath: string = Constants.STR_FSLASH_ORGS_FSLASH + Constants.STR_SUMMARY_FSLASH;
        let orgs: IOrgBasic[] = [];
        let orgKeys: string[] = [];
        let finalOrgs: any[] = [];
        let count: number = 0;
        const orgsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(orgPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(orgPath)) {
            orgsSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newOrg: any = <any>innerSnapshot.val();
                let newBuilding: any[] = [];
                if (null != newOrg.buildings) {
                    newBuilding = <any[]>ArrayUtil.Transform(newOrg.buildings);
                }
                finalOrgs = [ ...finalOrgs, ...newBuilding ];
                return false;
            });
            // console.log("orgKeys", orgKeys);
            // orgs = ArrayUtil.Transform(<IOrgBasic[]>orgsSnap.val());
        }
        // for (let org of orgs) {
            
            // for (let building in org.buildings) {
            //     let newBuilding: IBuilding = await this.GetBuilding(orgKeys[count], building);
            //     finalOrgs.push(newBuilding);
            // }
            // count = count + 1;
        // }
        return finalOrgs;
    }

    static async GetBuildingConfigProperty<T>(orgID: string, buildingID: string, propertyName: string): Promise<T> {
        if (!orgID || !buildingID || !propertyName) {
            throw new Error("Parameters cannot be null! orgID: " + orgID + " buildingID: " + buildingID);
        }

        let propertyPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH + buildingID + Constants.STR_FSLASH;

        return (await FirebaseAdminWrapper.GetInstance().GetValue(propertyPath)).val()[propertyName];
    }


    // static async GetAllBuildings(): Promise<IBuildingBasicInfo[]> {
    //     let buildings: IBuildingBasicInfo[] = [];
    //     const orgPath: string = Constants.STR_FSLASH_ORGS_FSLASH + Constants.STR_SUMMARY_FSLASH;
    //     const orgsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(orgPath);
    //     if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(orgPath)) {
    //         let orgKeys: string[] = <string[]>Object.keys(orgsSnap.val());
    //         let roomSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(orgPath);
    //         try {
    //             for (let key of orgKeys) {
    //                 let index: number = orgKeys.findIndex((val: string) => {
    //                     return (val === key);
    //                 });
    //                 let buildingInfo: IBuildingBasicInfo[] = await this.GetBuildingInfosInOrg(key);
    //                 buildings = [...buildings, ...buildingInfo];
    //             }
    //         }
    //         catch (error) {
    //             console.log("Error GetChatRooms", error);
    //             throw new Error("Error in GetChatRooms " + error);
    //         }
    //     }
    //     // console.log("Buildings", buildings);
    //     return buildings;
    // }

    static async CreateOrg(info: IOrgInfo): Promise<IOrgBasic> {
        const orgPath: string = Constants.STR_FSLASH_ORGS_FSLASH;

        // Since it starts from having nothing at all there, we push an empty value to generate the orgID
        const orgRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(orgPath, " ");
        const orgID: string = orgRef.key;

        // Once done we can go back and populate the /info node
        const orgInfoPath: string = orgPath + orgID + Constants.STR_FSLASH_INFO;
        await FirebaseAdminWrapper.GetInstance().SetValue(orgInfoPath, info);

        // Then the summary node
        const orgSumPath: string = orgPath + Constants.STR_SUMMARY_FSLASH + orgID + Constants.STR_FSLASH_NAME_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(orgSumPath, info.name);
        const newOrg: IOrgBasic = {
            name: info.name,
            id: orgID,
            buildings: []
        };
        return newOrg;
    }

    static async CreateBuilding(orgID: string, building: IBuilding): Promise<IBuilding> {
        if (!orgID || (null == building)) {
            throw new Error("Parameters cannot be null! Org ID: " + orgID + " building: " + building);
        }
        // creating building and basic configurations
        const buildingPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH;
        let buildingRef: firebase.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(buildingPath, "");
        let buildingID: string = buildingRef.key;
        let buildingInfoPath: string = buildingPath + buildingID;
        await FirebaseAdminWrapper.GetInstance().SetValue(buildingInfoPath, building);
        const orgSumPath: string = Constants.STR_FSLASH_ORGS_FSLASH + Constants.STR_SUMMARY_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH + buildingID;
        await FirebaseAdminWrapper.GetInstance().SetValue(orgSumPath, { name: building.info.name, id: buildingID, orgID: orgID });
        await FirebaseAdminWrapper.GetInstance().SetValue(orgSumPath + "/info/", { name: building.info.name, id: buildingID });
        let val: number = Math.floor(100000 + Math.random() * 90000);
        const orgCodePath: string = Constants.STR_PROPERTY_CODES_FSLASH + String(val) + Constants.STR_FSLASH;
        const codeOrgPath: string = buildingInfoPath + Constants.STR_FSLASH_CONFIG + Constants.STR_FSLASH + Constants.STR_PROPERTY_CODES_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(orgCodePath)) {
            val = Math.floor(100000 + Math.random() * 90000);
            if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(orgCodePath)) {
                val = Math.floor(100000 + Math.random() * 90000);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(orgCodePath)) {
                    val = Math.floor(100000 + Math.random() * 90000);

                    // TODO's - alert us if it doesn't pass past this.  we'll prob have to manually create on our side.

                }
                else {
                    await FirebaseAdminWrapper.GetInstance().SetValue(orgCodePath + Constants.STR_FSLASH, `${orgID}@@${buildingID}`);
                    await FirebaseAdminWrapper.GetInstance().SetValue(codeOrgPath, String(val));
                }
            }
            else {
                await FirebaseAdminWrapper.GetInstance().SetValue(orgCodePath + Constants.STR_FSLASH, `${orgID}@@${buildingID}`);
                await FirebaseAdminWrapper.GetInstance().SetValue(codeOrgPath, String(val));
            }
        }
        else {
            await FirebaseAdminWrapper.GetInstance().SetValue(orgCodePath + Constants.STR_FSLASH, `${orgID}@@${buildingID}`);
            await FirebaseAdminWrapper.GetInstance().SetValue(codeOrgPath, String(val));
        }
        let buildingItem: IBuildingSearch = {
            propertyCode: String(val),
            orgID: orgID,
            buildingID: buildingID,
            name: building.info.name,
            address: building.info.address
        };
        const propIndexPath: string = Constants.STR_PROPERTY_INDEX_FSLASH + `${orgID}@@${buildingID}` + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(propIndexPath, buildingItem);
        await FirebaseAdminWrapper.GetInstance().SetValue(buildingPath + buildingID + "/id/", buildingID);
        return building;
    }

    static async GetOrgIDFromCode(code: string): Promise<string> {
        let orgID: string = null;
        const orgPath: string = Constants.STR_PROPERTY_CODES_FSLASH + code + Constants.STR_FSLASH;
        const orgSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(orgPath);
        if (orgSnap) {
            orgID = <string>orgSnap.val();
        }
        return orgID;
    }

    static async GetOrgInfo(orgID: string): Promise<IOrgInfo> {
        if (!orgID) {
            throw new Error("Parameters cannot be null! orgID: " + orgID);
        }

        let orgInfoPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_INFO;

        let snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(orgInfoPath);
        let dataPayload: IOrgInfo = snapshot.val();

        if (null == dataPayload) {
            throw new Error("DB value payload is null! Please check that the following DB path exists: " + orgInfoPath);
        }

        return <IOrgInfo>dataPayload;
    }

    static async UpdateOrgInfo(orgID: string, orgInfo: IOrgInfo): Promise<IOrgBasic> {
        if (!orgID || (null == orgInfo)) {
            throw new Error("Parameters cannot be null! orgID: " + orgID + " orgInfo: " + orgInfo);
        }
        const newOrg: IOrgBasic = {
            name: orgInfo.name,
            id: orgID,
            buildings: []
        };
        try {
            let orgInfoPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_INFO;
            await FirebaseAdminWrapper.GetInstance().UpdateValue(orgInfoPath, orgInfo);
            const orgSumPath: string = Constants.STR_FSLASH_ORGS_FSLASH + Constants.STR_SUMMARY_FSLASH + orgID + Constants.STR_FSLASH_NAME_FSLASH;
            await FirebaseAdminWrapper.GetInstance().SetValue(orgSumPath, orgInfo.name);
        }
        catch (error) {
            await StatusOrchestrator.GetInstance().SendStatus("UpdateOrgInfo Error: " + JSON.stringify(error), Constants.SLACK_CHANNEL_MISC_LOGS);
            console.log("Error", error);
            throw error;
        }
        return newOrg;
    }

    static async DeleteOrg(orgID: string): Promise<void> {
        if (!orgID) {
            throw new Error("Parameters cannot be null! orgID: " + orgID);
        }
        try {
            let orgPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH;
            if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(orgPath)) {
                let orgRef: Map<string, any> = await this.GetBuildingsInOrg(orgID);
                if (null != orgRef) {
                    orgRef.forEach(async (value: string, key: string) => {
                        let tasks: ITask[] = await ServiceApiInternal.GetTasks(orgID, key);
                        if ((null != tasks)) {
                            for (let task of tasks) {
                                await ServiceApiInternal.DeleteTask(task.orgID, task.buildingID, task);
                            }
                        }
                        let landlords: IUser[] = await UserApiInternal.GetAllLandlords(orgID, key);
                        if (null != landlords) {
                            for (let landlord of landlords) {
                                await UserAdminApiInternal.DeleteUser(landlord.uid);
                            }
                        }
                        let tenants: IUser[] = await UserApiInternal.GetAllTenants(orgID, key);
                        if (null != tenants) {
                            for (let tenant of tenants) {
                                await UserAdminApiInternal.DeleteUser(tenant.uid);
                            }
                        }
                        let orgPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH;
                        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(orgPath)) {
                            await FirebaseAdminWrapper.GetInstance().DeleteValue(orgPath);
                            const orgSumPath: string = orgPath + Constants.STR_SUMMARY_FSLASH + orgID + Constants.STR_FSLASH;
                            await FirebaseAdminWrapper.GetInstance().DeleteValue(orgSumPath);
                        }
                    });
                }
            }
            else {
                await StatusOrchestrator.GetInstance().SendStatus("Error Deleting Org: " + orgID, Constants.SLACK_CHANNEL_MISC_LOGS);
                console.log("Error Deleting Org: " + orgID);
                throw new Error("Error Deleting Org: " + orgID);
            }
        }
        catch (error) {
            await StatusOrchestrator.GetInstance().SendStatus("Error Deleting Org: " + error, Constants.SLACK_CHANNEL_MISC_LOGS);
            console.log("Error", error);
            throw new Error("Error Deleting Org: " + error);
        }
    }

    static async GetBuildingsInOrg(orgID: string): Promise<Map<string, string>> {
        if (!orgID) {
            throw new Error("Parameters cannot be null! Org ID: " + orgID);
        }

        const buildingPath: string = Constants.STR_FSLASH_ORGS_FSLASH + Constants.STR_SUMMARY_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH;
        const buildings: any = (await FirebaseAdminWrapper.GetInstance().GetValue(buildingPath)).val();

        const buildingMap: Map<string, string> = new Map<string, string>();
        // for-in automatically loops over object keys
        // console.log("Buildings", buildings);
        for (let buildingID in buildings) {
            buildingMap.set(buildingID, buildings[buildingID]);
            // console.log("Buildings", buildingMap, buildings[buildingID]);
        }

        return buildingMap;
    }

    static async GetBuildingInfosInOrg(orgID: string): Promise<IBuildingBasicInfo[]> {
        if (!orgID) {
            throw new Error("Parameters cannot be null! Org ID: " + orgID);
        }

        const buildingPath: string = Constants.STR_FSLASH_ORGS_FSLASH + Constants.STR_SUMMARY_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH;
        const buildings: IBuildingBasicInfo[] = (await FirebaseAdminWrapper.GetInstance().GetValue(buildingPath)).val();
        return buildings;
    }

    static async GetBuildingInfo(orgID: string, buildingID: string): Promise<IBuildingInfo> {
        if (!orgID || !buildingID) {
            throw new Error("Parameters cannot be null! orgID: " + orgID + " buildingID: " + buildingID);
        }

        let buildingInfoPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH + buildingID + Constants.STR_FSLASH_INFO;

        let snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(buildingInfoPath);
        let dataPayload: any = snapshot.val();

        if (null == dataPayload) {
            throw new Error("DB value payload is null! Please check that the following DB path exists: " + buildingInfoPath);
        }

        return <IBuildingInfo>dataPayload;
    }

    static async SetBuildingInfo(orgID: string, buildingID: string, info: IBuildingInfo): Promise<IBuildingInfo> {
        if (!orgID || !buildingID) {
            throw new Error("Parameters cannot be null! orgID: " + orgID + " buildingID: " + buildingID);
        }

        let buildingInfoPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH + buildingID + Constants.STR_FSLASH_INFO;

        await FirebaseAdminWrapper.GetInstance().SetValue(buildingInfoPath, info);

        return info;
    }

    static async GetBuilding(orgID: string, buildingID: string): Promise<IBuilding> {
        if (!orgID || !buildingID) {
            throw new Error("Parameters cannot be null! Org ID: " + orgID + " buildingID: " + buildingID);
        }
        let building: IBuilding = null;
        const buildingPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH + buildingID;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(buildingPath)) {
            let snapShot: firebase.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(buildingPath);
            building = <IBuilding>snapShot.val();
            building.id = snapShot.key;
        }
        return building;
    }

    static async UpdateBuilding(orgID: string, building: IBuilding): Promise<IBuilding> {
        if (!orgID || (null == building)) {
            throw new Error("Parameters cannot be null! Org ID: " + orgID + " building: " + building);
        }
        const buildingPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH + building.id;
        await FirebaseAdminWrapper.GetInstance().UpdateValue(buildingPath, building);
        const orgSumPath: string = Constants.STR_FSLASH_ORGS_FSLASH + Constants.STR_SUMMARY_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH + building.id;
        // await FirebaseAdminWrapper.GetInstance().SetValue(orgSumPath, building.info.name);
        await FirebaseAdminWrapper.GetInstance().SetValue(orgSumPath, { name: building.info.name, id: building.id, orgID: orgID });
        await FirebaseAdminWrapper.GetInstance().SetValue(orgSumPath + "/info/", { name: building.info.name, id: building.id });
        return building;
    }

    static async GetUserProfile(orgID: string, buildingID: string, userID: string): Promise<IStaffProfile> {
        let staffProfile: IStaffProfile = null;
        const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_USERPROFILES_FSLASH + buildingID + Constants.STR_FSLASH + userID;
        const userSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(profileUserPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(profileUserPath)) {
            let newUser: IStaffProfile = <IStaffProfile>userSnap.val();
            staffProfile = newUser;
        }
        return staffProfile;
    }

    static async AddUserProfile(user: NormalizedUser): Promise<void> {
        let profile: IStaffProfile = {
            firstName: user.firstName,
            lastName: user.lastName,
            uid: user.uid,
            role: user.role,
            email: user.email,
            phone: ((null != user.phone) && (user.phone !== "") ? user.phone : ""),
            isVendor: (user.isVendor) ? user.isVendor : false
        };
        if (user.orgs.isAdmin) {
            const buildings: Map<string, string> = await this.GetBuildingsInOrg(user.orgs.id);
            if (buildings) {
                // Iterate through all buildings and update the userProfile node
                for (let buildingID of buildings.keys()) {
                    const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_USERPROFILES_FSLASH + buildingID + Constants.STR_FSLASH + user.uid;
                    await FirebaseAdminWrapper.GetInstance().SetValue(profileUserPath, profile);
                }
            }
            else {
                throw new Error("Building list cannot be resolved for org: " + user.orgs.id);
            }
        }
        else {
            for (let building of user.orgs.buildings) {
                const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_USERPROFILES_FSLASH + building.id + Constants.STR_FSLASH + user.uid;
                await FirebaseAdminWrapper.GetInstance().SetValue(profileUserPath, profile);
            }
        }
    }

    static async DeleteUserProfile(user: NormalizedUser): Promise<void> {
        if (user.orgs.isAdmin) {
            const buildings: Map<string, string> = await this.GetBuildingsInOrg(user.orgs.id);

            if (buildings) {
                // Iterate through all buildings and update the userProfile node
                for (let buildingID of buildings.keys()) {
                    const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_USERPROFILES_FSLASH + buildingID + Constants.STR_FSLASH + user.uid;
                    if (FirebaseAdminWrapper.GetInstance().DoesRefExist(profileUserPath)) {
                        await FirebaseAdminWrapper.GetInstance().DeleteValue(profileUserPath);
                    }
                }
            }
            else {
                throw new Error("Building list cannot be resolved for org: " + user.orgs.id);
            }
        }
        else {
            for (let building of user.orgs.buildings) {
                const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_USERPROFILES_FSLASH + building.id + Constants.STR_FSLASH + user.uid;
                if (FirebaseAdminWrapper.GetInstance().DoesRefExist(profileUserPath)) {
                    await FirebaseAdminWrapper.GetInstance().DeleteValue(profileUserPath);
                }
            }
        }
    }

    static async AddTenantProfile(user: NormalizedUser): Promise<void> {
        for (let building of user.orgs.buildings) {
            const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_TENANTS_FSLASH + building.id + Constants.STR_FSLASH + user.uid;
            let tenant: IStaffProfile = {
                firstName: user.firstName,
                lastName: user.lastName,
                uid: user.uid,
                role: user.role,
                email: user.email,
                phone: ((null != user.phone) && (user.phone !== "") ? user.phone : ""),
                isVendor: (user.isVendor) ? user.isVendor : false
            };
            await FirebaseAdminWrapper.GetInstance().SetValue(profileUserPath, tenant);
        }
    }

    static async DeleteTenantProfile(user: NormalizedUser): Promise<void> {
        for (let building of user.orgs.buildings) {
            const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_TENANTS_FSLASH + building.id + Constants.STR_FSLASH + user.uid;
            if (FirebaseAdminWrapper.GetInstance().DoesRefExist(profileUserPath)) {
                await FirebaseAdminWrapper.GetInstance().DeleteValue(profileUserPath);
            }
        }
    }

    static async AddLandlordProfile(user: NormalizedUser): Promise<void> {
        for (let building of user.orgs.buildings) {
            const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_LANDLORDS_FSLASH + building.id + Constants.STR_FSLASH + user.uid;
            let tenant: IStaffProfile = {
                firstName: user.firstName,
                lastName: user.lastName,
                uid: user.uid,
                role: user.role,
                email: user.email,
                phone: ((null != user.phone) && (user.phone !== "") ? user.phone : ""),
                isVendor: (user.isVendor) ? user.isVendor : false
            };
            await FirebaseAdminWrapper.GetInstance().SetValue(profileUserPath, tenant);
        }
    }

    static async DeleteLandlordProfile(user: NormalizedUser): Promise<void> {
        for (let building of user.orgs.buildings) {
            const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_LANDLORDS_FSLASH + building.id + Constants.STR_FSLASH + user.uid;
            if (FirebaseAdminWrapper.GetInstance().DoesRefExist(profileUserPath)) {
                await FirebaseAdminWrapper.GetInstance().DeleteValue(profileUserPath);
            }
        }
    }

    static async AddVendor(orgID: string, buildingID: string, vendor: IVendor): Promise<IVendor> {
        const vendorsPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_VENDORS_FSLASH + buildingID + Constants.STR_FSLASH;
        const vendorRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(vendorsPath, vendor);
        const vendorID: string = vendorRef.key;
        vendor.id = vendorID;
        let venPath: string = vendorsPath + vendorID + "/id/";
        await FirebaseAdminWrapper.GetInstance().SetValue(venPath, vendorID);
        return vendor;
    }

    static async GetVendors(orgID: string, buildingID: string): Promise<IVendor[]> {
        let vendors: IVendor[] = [];
        const vendorsPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_VENDORS_FSLASH + buildingID + Constants.STR_FSLASH;
        const vendorSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(vendorsPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(vendorsPath)) {
            vendorSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newVendor: IVendor = <IVendor>innerSnapshot.val();
                if (null != newVendor.employees) {
                    newVendor.employees = ArrayUtil.Transform(newVendor.employees);
                }
                vendors.push(newVendor);
                return false;
            });
        }
        else {
            return vendors;
        }
        return vendors;
    }

    static async UpdateBillingContact(orgID: string, buildingID: string, contact: IProfile): Promise<IProfile> {
        const contactPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_BUILDINGS_FSLASH + buildingID + Constants.STR_FSLASH_INFO + Constants.STR_FSLASH_BILLING_CONTACT_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(contactPath, contact);
        return contact;
    }
}