// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as firebaseAdmin from "firebase-admin";
import { IShippingInformation, customers } from "stripe";

// Local Modules
import { FirebaseAdminWrapper } from "../wrappers/FirebaseAdminWrapper";
import { StripeWrapper } from "../wrappers/StripeWrapper";
import { GCloudStorageWrapper } from "../wrappers/GCloudStorageWrapper";

// Util modules
import { ArrayUtil } from "../common/ArrayUtil";
import { StringUtil } from "../common/StringUtil";

// Api Modules
import { SilverBrickAdminApiInternal } from "./SilverBrickAdminApiInternal";
import { ServiceApiInternal } from "./ServiceApiInternal";
// Factory Modules

// Type Related Modules
import { Constants } from "../common/Constants";
import { IFirebaseUser } from "../interfaces/IFirebase";
import { PushApiInternal } from "./PushApiInternal";
import {
    ISilverBrickUser,
    IUser,
    IUserOrg,
    ITask,
    IOrgInfo,
    IBuildingBasicInfo,
    IAssignee,
    IUserAudience,
    IUserBuilding,
    IBuildingInfo,
    IProfile,
    IContactPerson
} from "../shared/SilverBrickTypes";
import { ICredentials } from "../shared/IParams";
import { IJwtUser } from "../interfaces/IAuth";
import { UserApiInternal } from "./UserApiInternal";
// Logger Modules
import {
    Logger,
    SessionLogBuilder,
    LogLevel
} from "../common/Logger";
import { PushWrapper, IPushResponseResult } from "../wrappers/PushWrapper";
import { StatusOrchestrator } from "../common/StatusOrchestrator";
import { NormalizedUser } from "../factories/users/NormalizedUser";
import {
    NormalizedUserInfo,
    PushPriority,
    PushSound,
    SilverBrickPushType,
    IEmailPredicate
} from "../common/ServerInternalTypes";
import { MailGunWrapper } from "../wrappers/MailGunWrapper";
import {
    EmailTemplateFactory,
    MailType
} from "../factories/EmailTemplateFactory";

export class UserAdminApiInternal {
    static async GetUser(uidOrEmail: string, logBuilder: SessionLogBuilder = null): Promise<NormalizedUser> {
        if (!uidOrEmail) {
            throw new Error("Parameters cannot be null! uid_or_email: " + uidOrEmail);
        }

        try {
            let authSnapshot: firebaseAdmin.auth.UserRecord;
            // Determine if param is uid or email
            if (StringUtil.IsValidEmail(uidOrEmail)) {
                // Get user data from the firebase auth db by email search
                authSnapshot = await FirebaseAdminWrapper.GetInstance().GetUserByEmail(uidOrEmail);
            }
            else {
                // Get user data from the firebase auth db by uid search
                authSnapshot = await FirebaseAdminWrapper.GetInstance().GetUserByUid(uidOrEmail);
            }

            if (!authSnapshot) {
                throw new Error("Firebase auth payload is null! Please check that the following user email or uid exists: " + uidOrEmail);
            }

            // Get user data from the custom db
            const map: string = await UserApiInternal.GetUserMap(authSnapshot.uid);
            const uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + authSnapshot.uid;
            const dbSnapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(uidPath);
            let userVal: any = <any>dbSnapshot.val();
            if (!userVal) {
                throw new Error("User payload is null! Please check that the following DB path exists: " + uidPath);
            }
            let org: IUserOrg = null;
            // if ((userVal.role.toLowerCase() !== "tenant") || ((userVal.role.toLowerCase() === "landlord") && (null != userVal.orgs)) || ((userVal.role.toLowerCase() === "tenant") && (null != userVal.orgs))) {
                const orgID: string = Object.keys(userVal.orgs)[0];
                const buildingMap: Map<string, any> = await SilverBrickAdminApiInternal.GetBuildingsInOrg(orgID);
                let buildings: IUserBuilding[] = null;
                if (orgID) {
                    // OPTIMIZE: Use for loop over map for-in performance gain
                    buildings = Object.keys(userVal.orgs[orgID].buildings).map((bKey: string) => {
                        return {
                            id: bKey,
                            name: buildingMap.get(bKey).name,
                            isAdmin: userVal.orgs[orgID].buildings[bKey].isAdmin || false
                        };
                    });
                    // console.log("buildings", buildings);
                }

                const orgInfo: IOrgInfo = await SilverBrickAdminApiInternal.GetOrgInfo(orgID);
                org = {
                    id: orgID,
                    name: orgInfo.name,
                    isAdmin: userVal.orgs[orgID].isAdmin || false,
                    buildings: ArrayUtil.Transform(buildings)
                };
                // console.log("org", org);
            // }
            const fbUser: firebaseAdmin.auth.UserRecord = await FirebaseAdminWrapper.GetInstance().GetUserByEmail(userVal.email);
            let userObj: NormalizedUser = <NormalizedUser>userVal;
            // let userObj: NormalizedUser = new NormalizedUser(userVal.uid,
            // userVal.firstName,
            // userVal.lastName,
            // (null != userVal.photoURL) ? userVal.photoURL : Constants.DEFAULT_NEW_USER_PHOTO_URL_PATH,
            // userVal.email,
            // userVal.phone,
            // userVal.isAdmin,
            // userVal.role,
            // org,
            // fbUser.metadata.lastSignedInAt,
            // fbUser.metadata.createdAt,
            // userVal.fcmRegToken,
            // userVal.address,
            // userVal.clients,
            // userVal.osType,
            // userVal.stripeID,
            // userVal.propertyCodes,
            // userVal.preferences,
            // (userVal.isVendor) ? userVal.isVendor : false);
            if (null != org) {
                userObj.orgs = org;
            }
            if (null != authSnapshot) {
                userObj.lastSignedInAt = authSnapshot.metadata.lastSignedInAt;
                userObj.createdAt = authSnapshot.metadata.createdAt;
            }
            return userObj;
        }
        catch (error) {
            throw new Error("Error GetUser: " + uidOrEmail + " " + error);
        }
    }

    static async UpdateToken(token: string, userID: string): Promise<boolean> {
        let userPath: string = Constants.STR_FSLASH_USERS_FSLASH + userID + Constants.STR_FSLASH_FCMTOKEN_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(userPath, token);
        return true;
    }

    static async CreateUser(user: IUser): Promise<IUser> {
        const fbUser: IFirebaseUser = {
            email: user.email,
            password: user.password,
            displayName: user.firstName + " " + user.lastName
        };
        const fbUserNew: firebaseAdmin.auth.UserRecord = await FirebaseAdminWrapper.GetInstance().CreateUser(fbUser);
        let userPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((user.role.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (user.role.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + fbUserNew.uid;
        user.uid = fbUserNew.uid;
        let userObj: NormalizedUser = <NormalizedUser>user;
        userObj.uid = fbUserNew.uid;
        userObj.photoURL = Constants.DEFAULT_NEW_USER_PHOTO_URL_PATH;
        userObj.createdAt = fbUserNew.metadata.createdAt;
        userObj.lastSignedInAt = fbUserNew.metadata.lastSignedInAt;
        userObj.isFirstTime = true;
        let configVal: IBuildingInfo = await SilverBrickAdminApiInternal.GetBuildingConfigProperty<IBuildingInfo>(
        user.orgs.id,
        user.orgs.buildings[0].id,
        "info");
        let userObj2: NormalizedUser = new NormalizedUser(fbUserNew.uid,
            user.firstName,
            user.lastName,
            Constants.DEFAULT_NEW_USER_PHOTO_URL_PATH,
            user.email,
            user.phone,
            user.isAdmin,
            user.role,
            user.orgs,
            fbUserNew.metadata.lastSignedInAt,
            fbUserNew.metadata.createdAt,
            user.fcmRegToken,
            configVal.address,
            user.clients,
            user.osType,
            user.stripeID,
            user.propertyCodes,
            user.preferences,
            (user.isVendor) ? user.isVendor : false,
            true,
            (user.lastActivity) ? user.lastActivity : null,
            (user.isClockedIn) ? user.isClockedIn : false,
            (user.unit) ? user.unit : null,
            (user.calendarColor) ? user.calendarColor : null,
            (user.calendarTextColor) ? user.calendarTextColor : null);
        if (user.role.toLowerCase() === "tenant") {
            const address: IShippingInformation = {
                address: {
                    line1: configVal.address.line1,
                    line2: configVal.address.line2,
                    city: configVal.address.city,
                    state: configVal.address.state,
                    postal_code: configVal.address.zip,
                    country: "US"
                },
                name: "Home Address"
            };
            const stripeID: string = await StripeWrapper.GetInstance().CreateCustomer(fbUserNew.email, address);
            user.stripeID = stripeID;
            userObj2.stripeID = stripeID;
            userObj2.propertyCodes = 123456;
            user.propertyCodes = 123456;
            userObj.propertyCodes = 123456;
            await SilverBrickAdminApiInternal.AddTenantProfile(userObj);
        }
        else if (user.role.toLowerCase() === "landlord") {
            const address: IShippingInformation = {
                address: {
                    line1: configVal.address.line1,
                    line2: configVal.address.line2,
                    city: configVal.address.city,
                    state: configVal.address.state,
                    postal_code: configVal.address.zip,
                    country: "US"
                },
                name: "Home Address"
            };
            const stripeID: string = await StripeWrapper.GetInstance().CreateCustomer(fbUserNew.email, address);
            user.stripeID = stripeID;
            userObj2.stripeID = stripeID;
            userObj2.propertyCodes = 123456;
            user.propertyCodes = 123456;
            userObj.propertyCodes = 123456;
            if (null != userObj.orgs.buildings) {
                for (let building of userObj.orgs.buildings) {
                    if ((null != building.isBillingContact) && (building.isBillingContact)) {
                        let contactPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_BUILDINGS_FSLASH + building.id + Constants.STR_FSLASH_INFO + Constants.STR_FSLASH_BILLING_CONTACT_FSLASH;
                        let profile: IProfile = {
                            uid: user.uid,
                            firstName: user.firstName,
                            lastName: user.lastName,
                            email: user.email,
                            phone: user.phone
                        };
                        await FirebaseAdminWrapper.GetInstance().SetValue(contactPath, profile);
                    }
                }
            }
            await SilverBrickAdminApiInternal.AddLandlordProfile(userObj);
        }
        else {
            const staffPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_USERPROFILES_FSLASH + user.orgs.buildings[0].id + Constants.STR_FSLASH;
            const userSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(staffPath);
            if (null == userSnap.val()) {
                let contactPerson: IContactPerson = {
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    phone: (null != user.phone) ? user.phone : "",
                    role: user.role
                };
                let contactPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_BUILDINGS_FSLASH + user.orgs.buildings[0].id + Constants.STR_FSLASH_INFO + Constants.STR_FSLASH + Constants.STR_CONTACT_PERSON_FSLASH;
                await FirebaseAdminWrapper.GetInstance().SetValue(contactPath, contactPerson);
                await SilverBrickAdminApiInternal.AddUserProfile(userObj);
            }
            else {
                await SilverBrickAdminApiInternal.AddUserProfile(userObj);
            }
        }
        const mapPath: string = Constants.STR_USERS_FSLASH + Constants.STR_MAP_FSLASH + user.uid;
        await FirebaseAdminWrapper.GetInstance().SetValue(mapPath, user.role);

        try {
            userObj.isFirstTime = true;
            userObj2.isFirstTime = true;
            FirebaseAdminWrapper.GetInstance().SetValue(userPath, userObj2.GetDatabaseData());
            FirebaseAdminWrapper.GetInstance().DeleteValue(userPath + "/password/");
        }
        catch (error) {
            console.log("Error making square customer", error);
            throw new Error(error);
        }
        console.log("new user: ", userObj);
        return user;
    }

    static async CreateUserGoogleSignIn(user: IUser): Promise<IUser> {
        const fbUser: firebaseAdmin.auth.UserRecord = await FirebaseAdminWrapper.GetInstance().GetUserByEmail(user.email);
        let userPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((user.role.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (user.role.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + fbUser.uid;
        user.uid = fbUser.uid;
        let userObj: NormalizedUser = <NormalizedUser>user;
        userObj.uid = fbUser.uid;
        userObj.photoURL = Constants.DEFAULT_NEW_USER_PHOTO_URL_PATH;
        userObj.createdAt = fbUser.metadata.createdAt;
        userObj.lastSignedInAt = fbUser.metadata.lastSignedInAt;
        let configVal: IBuildingInfo = await SilverBrickAdminApiInternal.GetBuildingConfigProperty<IBuildingInfo>(
        user.orgs.id,
        user.orgs.buildings[0].id,
        "info");
        let userObj2: NormalizedUser = new NormalizedUser(fbUser.uid,
            user.firstName,
            user.lastName,
            Constants.DEFAULT_NEW_USER_PHOTO_URL_PATH,
            user.email,
            user.phone,
            user.isAdmin,
            user.role,
            user.orgs,
            fbUser.metadata.lastSignedInAt,
            fbUser.metadata.createdAt,
            user.fcmRegToken,
            configVal.address,
            user.clients,
            user.osType,
            user.stripeID,
            user.propertyCodes,
            user.preferences,
            (user.isVendor) ? user.isVendor : false,
            (user.isFirstTime) ? user.isFirstTime : false,
            (user.lastActivity) ? user.lastActivity : null,
            (user.isClockedIn) ? user.isClockedIn : false,
            (user.unit) ? user.unit : null,
            (user.calendarColor) ? user.calendarColor : null,
            (user.calendarTextColor) ? user.calendarTextColor : null);
        if (user.role.toLowerCase() === "tenant") {
            const address: IShippingInformation = {
                address: {
                    line1: configVal.address.line1,
                    line2: configVal.address.line2,
                    city: configVal.address.city,
                    state: configVal.address.state,
                    postal_code: configVal.address.zip,
                    country: "US"
                },
                name: "Home Address"
            };
            const stripeID: string = await StripeWrapper.GetInstance().CreateCustomer(fbUser.email, address);
            user.stripeID = stripeID;
            userObj2.stripeID = stripeID;
            await SilverBrickAdminApiInternal.AddTenantProfile(userObj);
        }
        else if (user.role.toLowerCase() === "landlord") {
            const address: IShippingInformation = {
                address: {
                    line1: configVal.address.line1,
                    line2: configVal.address.line2,
                    city: configVal.address.city,
                    state: configVal.address.state,
                    postal_code: configVal.address.zip,
                    country: "US"
                },
                name: "Home Address"
            };
            const stripeID: string = await StripeWrapper.GetInstance().CreateCustomer(fbUser.email, address);
            user.stripeID = stripeID;
            userObj2.stripeID = stripeID;
            await SilverBrickAdminApiInternal.AddLandlordProfile(userObj);
        }
        else {
            await SilverBrickAdminApiInternal.AddUserProfile(userObj);
        }
        const mapPath: string = Constants.STR_USERS_FSLASH + Constants.STR_MAP_FSLASH + user.uid;
        await FirebaseAdminWrapper.GetInstance().SetValue(mapPath, user.role);

        try {
            FirebaseAdminWrapper.GetInstance().SetValue(userPath, userObj2.GetDatabaseData());
            FirebaseAdminWrapper.GetInstance().DeleteValue(userPath + "/password/");
        }
        catch (error) {
            console.log("Error making square customer", error);
            throw new Error(error);
        }
        console.log("new user: ", userObj);
        return user;
    }

     static async GetAllUsers(cursor: string = null, limit: number = Constants.NUM_PAGINATION_LIMIT, isForward: boolean = true): Promise<any[]> {
        try {
            // Limit sanity check
            if (limit < 1) {
                limit = Constants.NUM_PAGINATION_LIMIT;
            }

            let normalizedUserArray: any[] = [];

            const userPath: string = Constants.STR_FSLASH_USERS_FSLASH + Constants.STR_STAFF_FSLASH;

            let fbRef: firebaseAdmin.database.Reference = FirebaseAdminWrapper.GetInstance().GetRef(userPath);
            let fbQuery: firebaseAdmin.database.Query = fbRef.orderByKey();

            if (isForward) {
                // Limit to first
                fbQuery = fbQuery.limitToFirst(limit + Constants.NUM_LIMIT_BUFFER_ONE);
                if (cursor) {
                    // Start at
                    fbQuery = fbQuery.startAt(cursor);
                }
            }
            else {
                // Limit to last
                fbQuery = fbQuery.limitToLast(limit + Constants.NUM_LIMIT_BUFFER_TWO);
                if (cursor) {
                    // End at
                    fbQuery = fbQuery.endAt(cursor);
                }
            }

            const snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(userPath);
            if (snapshot.val()) {
                let allUids: string[] = Object.keys(snapshot.val());
                for (let uid of allUids) {
                    const map: string = await UserApiInternal.GetUserMap(uid);
                    const uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + "staff/" + uid;
                    const dbSnapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(uidPath);
                    let userVal: any = <any>dbSnapshot.val();
                    // Extract org information from the snapshot
                    // Assuming a user will only ever have 1 org
                    // if ((userVal.role.toLowerCase() !== "tenant") || ((userVal.role.toLowerCase() === "tenant") && (null != userVal.orgs)) || ((userVal.role.toLowerCase() === "landlord") && (null != userVal.orgs))) {
                        const orgID: string = Object.keys(userVal.orgs)[0];
                        const buildingMap: Map<string, any> = await SilverBrickAdminApiInternal.GetBuildingsInOrg(orgID);
                        let buildings: IUserBuilding[] = null;
                        if (orgID) {
                            // OPTIMIZE: Use for loop over map for-in performance gain
                            buildings = Object.keys(userVal.orgs[orgID].buildings).map((bKey: string) => {
                                return {
                                    id: bKey,
                                    name: buildingMap.get(bKey).name,
                                    isAdmin: userVal.orgs[orgID].buildings[bKey].isAdmin || false
                                };
                            });
                            // console.log("buildings", buildings);
                        }

                        const orgInfo: IOrgInfo = await SilverBrickAdminApiInternal.GetOrgInfo(orgID);
                        let org: IUserOrg = {
                            id: orgID,
                            name: orgInfo.name,
                            isAdmin: userVal.orgs[orgID].isAdmin || false,
                            buildings: ArrayUtil.Transform(buildings)
                        };
                        let userObj: NormalizedUser = <NormalizedUser>userVal;
                        userObj.orgs = org;
                        normalizedUserArray.push(userObj);
                    // }
                    // else {
                    //     let userObj: NormalizedUser = <NormalizedUser>userVal;
                    //     normalizedUserArray.push(userObj);
                    // }
                }
            }
            else {
                // snapshot val is empty
            }

            // Fetch extra metadata from firebase auth db
            // for (let user of normalizedUserArray) {
            //     // Get user data from the firebase auth db
            //     try {
            //         const authSnapshot: firebaseAdmin.auth.UserRecord = await FirebaseAdminWrapper.GetInstance().GetUserByUid(user.uid);
            //         if (null != authSnapshot) {
            //             user.lastSignedInAt = authSnapshot.metadata.lastSignedInAt;
            //             user.createdAt = authSnapshot.metadata.createdAt;
            //         }
            //         else {
            //             normalizedUserArray = normalizedUserArray.filter((badUser: NormalizedUser) => {
            //                 return (user.uid !== badUser.uid);
            //             });
            //         }
            //     }
            //     catch (error) {
            //         console.log("Error from firebase Auth", error);
            //         normalizedUserArray = normalizedUserArray.filter((badUser: NormalizedUser) => {
            //             return (user.uid !== badUser.uid);
            //         });
            //     }
            // }
            return normalizedUserArray;
        }
        catch (error) {
            throw error;
        }
    }

    static async GetAllNonOrgAdminsUnderBuilding(orgID: string, buildingID: string): Promise<NormalizedUser[]> {
        if (!orgID || !buildingID) {
            throw new Error("Parameters cannot be null! orgID: " + orgID + " buildingID: " + buildingID);
        }

        let profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_USERPROFILES_FSLASH + buildingID;

        Logger.Info("Getting all non org admins under the building from: " + profileUserPath);

        let snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(profileUserPath);
        let dataPayload: any = snapshot.val();
        let userBuildings: Array<NormalizedUser> = [];
        if (null == dataPayload) {
            throw new Error("DB value payload is null! Please check that the following DB path exists: " + profileUserPath);
        }

        let logBuilder = SessionLogBuilder.CreateInstance("#### Retrieved users ####", LogLevel.INFO);

        for (let uid in dataPayload) {
            try {
                const newUser: NormalizedUser = await this.GetUser(uid);
                // if (!newUser.orgs.isAdmin) {
                newUser.orgs.buildings = ArrayUtil.Transform(newUser.orgs.buildings);
                userBuildings.push(newUser);
                // }
            }
            catch (error) {
                Logger.Info("We hit an issue with getting the user '" + uid + "'. Best effort'ing. " + error);
            }
        }

        // Retrieving the snapshot of registered user
        return userBuildings;
    }

    static async GetAllAdmins(): Promise<NormalizedUser[]> {
        let profileUserPath: string = Constants.STR_USERS_FSLASH + "staff/";
        Logger.Info("Getting all non org admins under the building from: " + profileUserPath);
        
        const taskRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().GetRef(profileUserPath);
        const taskSnap: firebaseAdmin.database.Query = taskRef.orderByChild("isAdmin").equalTo(true);
        const taskSnap2: firebaseAdmin.database.DataSnapshot = await taskSnap.once(Constants.STR_VALUE);
        let userBuildings: Array<any> = [];
        let dataPayload: string[] = Object.keys(taskSnap2.val());
        console.log("dataPayload", dataPayload);
        for (let uid of dataPayload) {
            try {
                const newUser: NormalizedUser = await this.GetUser(uid);
                newUser.orgs.buildings = ArrayUtil.Transform(newUser.orgs.buildings);
                userBuildings.push(newUser);
            }
            catch (error) {
                Logger.Info("We hit an issue with getting the user '" + uid + "'. Best effort'ing. " + error);
            }
        }
        // Retrieving the snapshot of registered user
        return userBuildings;
    }

    static async UpdateUser(user: IUser): Promise<NormalizedUser> {
        // Input validation
        if (!user.uid || !user.firstName || !user.lastName || !user.email) {
            throw new Error("Parameters cannot be null! User ID: " +
                user.uid +
                ", First Name: " +
                user.firstName +
                ", Last Name: " +
                user.lastName +
                ", Email: " +
                user.email);
        }

        try {
            // TODO: Check to make sure orgID and buildingID are all valid
            // Set up change detection flags
            let updateFbAuth: boolean = false;
            let updateProfileNode: boolean = false;
            // TODO: Add a change detection flag for audience when we implement that

            // Set up firebase auth object map for potential updating
            let fbUserObj: any = {};

            // Get the old user object for change comparison
            let oldUserObj: NormalizedUser = await this.GetUser(user.uid);

            // Check for password change
            if (user.password) {
                updateFbAuth = true;
                fbUserObj.password = user.password;
            }

            // Check for name change
            if ((user.firstName !== oldUserObj.firstName) ||
                (user.lastName !== oldUserObj.lastName)) {
                updateFbAuth = true;
                fbUserObj.displayName = `${user.firstName} ${user.lastName}`;
                updateProfileNode = true;
                // TODO: Update audience node also when that is ready
            }

            if ((user.email !== oldUserObj.email)) {
                updateFbAuth = true;
                fbUserObj.email = user.email;
                updateProfileNode = true;
                // TODO: Update audience node also when that is ready
            }
            // console.log("oldUserObj", oldUserObj);
            // Check for position change
            if (user.role.toLowerCase() !== oldUserObj.role.toLowerCase()) {
                updateProfileNode = true;
            }
            // Check for org change
            else if (user.orgs.id.toLowerCase() !== oldUserObj.orgs.id.toLowerCase()) {
                updateProfileNode = true;
            }
            // Check for building change
            else if (await this._isBuildingsEqual(user.orgs.buildings, oldUserObj)) {
                console.log("this._isBuildingsEqual(user.orgs.buildings, oldUserObj)", this._isBuildingsEqual(user.orgs.buildings, oldUserObj));
                updateProfileNode = true;
                StatusOrchestrator.GetInstance().SendStatus("Updating User (IsBuildingsEqual): " + JSON.stringify(user.orgs.buildings) + JSON.stringify(oldUserObj.orgs.buildings) + this._isBuildingsEqual(user.orgs.buildings, oldUserObj), Constants.SLACK_CHANNEL_MISC_LOGS);
            }

            /*
            Start the updates
            */

            // Update firebase auth db
            let newUser: firebaseAdmin.auth.UserRecord = undefined;
            if (updateFbAuth) {
                // Update the firebase auth db
                newUser = await FirebaseAdminWrapper.GetInstance().UpdateUser(user.uid, fbUserObj);
            }

            // Update the user db
            let userObj: NormalizedUser = new NormalizedUser(oldUserObj.uid,
                user.firstName,
                user.lastName,
                oldUserObj.photoURL,
                oldUserObj.email,
                user.phone,
                user.isAdmin,
                user.role,
                user.orgs,
                null,
                null,
                oldUserObj.fcmRegToken,
                user.address,
                oldUserObj.clients,
                oldUserObj.osType,
                oldUserObj.stripeID,
                oldUserObj.propertyCodes,
                oldUserObj.preferences,
                (oldUserObj.isVendor) ? oldUserObj.isVendor : false,
                (oldUserObj.isFirstTime) ? oldUserObj.isFirstTime : false,
                (oldUserObj.lastActivity) ? oldUserObj.lastActivity : null,
                (oldUserObj.isClockedIn) ? oldUserObj.isClockedIn : false,
                (user.unit) ? user.unit : null,
                (user.calendarColor) ? user.calendarColor : null,
                (user.calendarTextColor) ? user.calendarTextColor : null);

            let uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((user.role.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (user.role.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + user.uid;
            await FirebaseAdminWrapper.GetInstance().SetValue(uidPath, userObj.GetDatabaseData());
            // if (updateProfileNode) {
                // let uidPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + ((user.role.toLowerCase() === "staff") ? Constants.STR_FSLASH_USERPROFILES_FSLASH : (user.role.toLowerCase() !== "landlord" ? Constants.STR_FSLASH_TENANTS_FSLASH : Constants.STR_FSLASH_LANDLORDS_FSLASH)) + user.uid;
            switch(user.role) {
                case "Staff":
                {
                    console.log("updateProfileNode", user.role, user.email);
                    await SilverBrickAdminApiInternal.DeleteUserProfile(oldUserObj);
                    await SilverBrickAdminApiInternal.AddUserProfile(userObj);
                    break;
                }

                case "Landlord":
                {
                    console.log("updateProfileNode", user.role, user.email);
                    await SilverBrickAdminApiInternal.DeleteLandlordProfile(oldUserObj);
                    await SilverBrickAdminApiInternal.AddLandlordProfile(userObj);
                    break;
                }

                case "Tenant":
                {
                    console.log("updateProfileNode", user.role, user.email);
                    await SilverBrickAdminApiInternal.DeleteTenantProfile(oldUserObj);
                    await SilverBrickAdminApiInternal.AddTenantProfile(userObj);
                    break;
                }

                default:
                {
                    break;
                }
            }
            // }
            if (userObj.role.toLowerCase() === "landlord") {
                if (null != userObj.orgs.buildings) {
                    for (let building of userObj.orgs.buildings) {
                        if ((null != building.isBillingContact) && (building.isBillingContact)) {
                            let contactPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_BUILDINGS_FSLASH + building.id + Constants.STR_FSLASH_INFO + Constants.STR_FSLASH_BILLING_CONTACT_FSLASH;
                            let profile: IProfile = {
                                uid: user.uid,
                                firstName: user.firstName,
                                lastName: user.lastName,
                                email: user.email,
                                phone: user.phone
                            };
                            await FirebaseAdminWrapper.GetInstance().SetValue(contactPath, profile);
                        }
                    }
                }
            }

            return userObj;
        }
        catch (error) {
            throw error;
        }
    }

    static async ChangePassword(uid: string, userData: ICredentials): Promise<boolean> {
        if (!uid || (userData == null)) {
            throw new Error("ChangePassword params null: " + uid + userData);
        }
        let result: boolean = false;
        try {
            const changeSucces: firebaseAdmin.auth.UserRecord = await firebaseAdmin.auth().updateUser(uid, userData);
            const map: string = await UserApiInternal.GetUserMap(uid);
            let uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + uid + Constants.STR_FSLASH_IS_FIRSTIME_FSLASH;
            await FirebaseAdminWrapper.GetInstance().SetValue(uidPath, false);
            result = true;
        } catch (error) {
            console.log("Error", error);
            result = false;
        }

        return result;
    }

    static async GetImageUrl(refPath: string): Promise<string> {
        // Check for images and get download URL
        let photoURL: string = null;
        try {
            // Go get real URL from firebase storage as we save the reference path instead
            const downloadURL: string = await GCloudStorageWrapper.GetInstance().GetDownloadUrl(refPath.trim());
            // console.log("downloadURL", downloadURL, refPath);
            if (downloadURL) {
                photoURL = downloadURL.trim();
            }
            else {
                // Download url is empty for some reason...
            }
        }
        catch (error) {
            // Error getting the download url...
        }

        return photoURL;
    }

    /* HELPERS */

    static async ActivateUser(uid: string, logBuilder: SessionLogBuilder = null): Promise<boolean> {
        return this._changeUserActivation(uid, true, logBuilder);
    }

    static async DeactivateUser(uid: string, logBuilder: SessionLogBuilder = null): Promise<boolean> {
        return this._changeUserActivation(uid, false, logBuilder);
    }

    static async DeleteUser(uid: string, logBuilder: SessionLogBuilder = null): Promise<boolean> {
        if (!uid) {
            throw new Error("Parameters cannot be null! uid: " + uid);
        }

        try {
            // validate user path

            const mapPath: string = Constants.STR_USERS_FSLASH + Constants.STR_MAP_FSLASH + uid;
            const map: string = await UserApiInternal.GetUserMap(uid);
            let uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + uid + Constants.STR_FSLASH;
            let userExsist: boolean = await FirebaseAdminWrapper.GetInstance().DoesRefExist(uidPath);
            if (!userExsist) {
                throw new Error("DB value payload is null! Please check that the following user exists: " + uid);
            }


            // Get the user object for its org/building information
            let userObj: NormalizedUser = await this.GetUser(uid, logBuilder);
            let userBldgs: IUserBuilding[] = userObj.orgs.buildings;
            const bookingsPath: string = Constants.STR_SCHEDULES_FSLASH + uid + Constants.STR_FSLASH;
            let tasks: ITask[] = await ServiceApiInternal.GetStaffSchedule(userObj.orgs.id, userObj.orgs.buildings[0].id, uid);
            if (null != tasks) {
                for (let task of tasks) {
                    let newAssignees: IAssignee[] = task.assignees.filter((assign: IAssignee) => {
                        return assign.uid !== uid;
                    });
                    let taskPath: string = Constants.STR_ALL_TASKS_FSLASH + task.id + "/assignees/";
                    if (null != newAssignees) {
                        task.assignees = newAssignees;
                        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(taskPath)) {
                            await FirebaseAdminWrapper.GetInstance().SetValue(taskPath, newAssignees);
                        }
                        let assignTask = newAssignees.forEach(async (ass: IAssignee) => {
                            let newSchedulePath: string = Constants.STR_SCHEDULES_FSLASH + ass.uid + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                            if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(newSchedulePath)) {
                                await FirebaseAdminWrapper.GetInstance().SetValue(newSchedulePath + "assignees/", newAssignees);
                            }
                            let orgPath: string = Constants.STR_FSLASH_ORGS_FSLASH + task.orgID + Constants.STR_FSLASH_TASKS_FSLASH + task.buildingID + Constants.STR_FSLASH + task.id + Constants.STR_FSLASH;
                            if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(orgPath)) {
                                await FirebaseAdminWrapper.GetInstance().SetValue(orgPath + "assignees/", newAssignees);
                            }
                        });
                        await assignTask;
                    }
                    else {
                        await FirebaseAdminWrapper.GetInstance().DeleteValue(taskPath);
                    }
                }
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(bookingsPath)) {
                    await FirebaseAdminWrapper.GetInstance().DeleteValue(bookingsPath);
                }
            }
            await FirebaseAdminWrapper.GetInstance().DeleteValue(bookingsPath);
            // Remove user from userProfiles
            if (userObj.role === "Tenant") {
                await SilverBrickAdminApiInternal.DeleteTenantProfile(userObj);
            }
            else if (userObj.role === "Landlord") {
                await SilverBrickAdminApiInternal.DeleteLandlordProfile(userObj);
            }
            else if (userObj.role === "Staff") {
                await SilverBrickAdminApiInternal.DeleteUserProfile(userObj);
            }

            // Delete from user node in the DB
            await FirebaseAdminWrapper.GetInstance().DeleteValue(uidPath);
            await FirebaseAdminWrapper.GetInstance().DeleteValue(mapPath);

            // Delete user from the firebase auth db
            await FirebaseAdminWrapper.GetInstance().DeleteUser(uid);


            if (logBuilder) {
                logBuilder.Log("Delete user '" + userObj.firstName + " " + userObj.lastName);
            }

            return true;
        }
        catch (error) {
            throw error;
        }
    }

    /* HELPERS */

    private static async _changeUserActivation(uid: string, activate: boolean, logBuilder: SessionLogBuilder = null): Promise<boolean> {
        if (!uid || (typeof (activate) !== "boolean")) {
            throw new Error("Parameters cannot be null! uid: " + uid +
                ", Activation Status: " +
                activate);
        }

        try {
            // validate user path
            let uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + Constants.STR_STAFF_FSLASH + uid;
            let userExsist: boolean = await FirebaseAdminWrapper.GetInstance().DoesRefExist(uidPath);

            if (!userExsist) {
                throw new Error("DB value payload is null! Please check that the following user exists: " + uid);
            }

            await FirebaseAdminWrapper.GetInstance().UpdateValue(uidPath, { isActive: activate });

            // Update the disabled flag in the firebase auth db
            await FirebaseAdminWrapper.GetInstance().UpdateUser(uid, { disabled: !activate });

            // Get the user object for its org/building information
            let userObj: NormalizedUser = await this.GetUser(uid, logBuilder);

            if (activate) {
                // Add user to the userprofile nodes
                await SilverBrickAdminApiInternal.AddUserProfile(userObj);
            }
            else {
                // Remove user from userProfiles
                await SilverBrickAdminApiInternal.DeleteUserProfile(userObj);
            }

            if (logBuilder) {
                logBuilder.Log("Deactivated user '" + userObj.firstName + " " + userObj.lastName);
            }

            return true;
        }
        catch (error) {
            throw error;
        }
    }

    private static async _isBuildingsEqual(newBuildings: IUserBuilding[], oldUserObj: NormalizedUser): Promise<boolean> {
        if (!newBuildings) {
            return false;
        }

        if (newBuildings.length !== oldUserObj.orgs.buildings.length) {
            return false;
        }

        if (!(newBuildings instanceof Array)) {
            return false;
        }

        // Extract building ids from the org in this object
        let oldBuildingIDs: string[] = oldUserObj.orgs.buildings.map((oldBuilding: IUserBuilding, index: number) => {
            return oldBuilding.id;
        });

        return newBuildings.every((newBuilding, index) => {
            console.log("IsBuildingsEqual: ", newBuilding.id, oldBuildingIDs.indexOf(newBuilding.id) > - 1);
            return (oldBuildingIDs.indexOf(newBuilding.id) > - 1);
        });
    }
}
