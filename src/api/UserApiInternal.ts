// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import * as firebaseAdmin from "firebase-admin";
import * as firebase from "firebase";
import { IShippingInformation, customers, ICard, cards, subscriptions, charges } from "stripe";

// Local modules
import { IJwtUser } from "../interfaces/IAuth";
import { Constants } from "../common/Constants";
import { IReportBuilder } from "../interfaces/IReportBuilder";
import { Logger, SessionLogBuilder, LogLevel, ReportBuilder } from "../common/Logger";
import { FirebaseAdminWrapper } from "../wrappers/FirebaseAdminWrapper";
import { PushApiInternal } from "./PushApiInternal";
import { SilverBrickAdminApiInternal } from "./SilverBrickAdminApiInternal";
import { ServiceApiInternal } from "./ServiceApiInternal";
import { StripeWrapper } from "../wrappers/StripeWrapper";
import { NormalizedUserInfo, OperatingSystemType, DeviceInfoTuple } from "../common/ServerInternalTypes";
import { NormalizedUser } from "../factories/users/NormalizedUser";
import {
    ISilverBrickUser,
    IStaffProfile,
    NotificationType,
    INotificationData,
    IBuildingInfo,
    IPreferences,
    IUserBuilding,
    ILocation,
    IUserAudience,
    IOrgInfo,
    IUserOrg,
    ITimeCard,
    IUser,
    IBuildingSearch,
    IDeviceInfo,
    IProfile
} from "../shared/SilverBrickTypes";
import { UserAdminApiInternal } from "./UserAdminApiInternal";
import { ArrayUtil } from "../common/ArrayUtil";
import { GCloudStorageWrapper } from "../wrappers/GCloudStorageWrapper";
import { EmailTemplateFactory, MailType, ISignUpEmailTemplateParams } from "../factories/EmailTemplateFactory";
import { IEmailPredicate } from "../common/ServerInternalTypes";
import { StatusOrchestrator } from "../common/StatusOrchestrator";
import { MailGunWrapper } from "../wrappers/MailGunWrapper";
import { INewSubscription, ITimeCardToday, ITimeCardRoster } from "../shared/ResponseTypes";
import { TimeUtil } from "../common/TimeUtil";

export class UserApiInternal {
    static async CreateSubscription(stripeID: string, cardID: string, type: string, orgID: string, cvc: string, numbers: string, uid: string, referralCode: string = null): Promise<subscriptions.ISubscription> {
        let subscription: subscriptions.ISubscription = null;
        const map: string = await this.GetUserMap(uid);
        let userPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + uid + "/preferences";
        const orgSubscriptionPath: string = userPath + Constants.STR_FSLASH_SUBSCRIPTION_DATE_FSLASH;
        const orgPlanPath: string = userPath + "/cardID/";
        await FirebaseAdminWrapper.GetInstance().SetValue(orgPlanPath, cardID);
        await FirebaseAdminWrapper.GetInstance().SetValue(orgSubscriptionPath, new Date(Date.now()).getTime());
        subscription = await StripeWrapper.GetInstance().CreateSubscription(stripeID, type, cardID, cvc, numbers, referralCode);
        return subscription;
    }

    static async CreateCharge(stripeID: string, cardID: string, email: string, orgID: string, price: number, uid: string, referralCode: string = null): Promise<charges.ICharge> {
        let charge: charges.ICharge = null;
        const map: string = await this.GetUserMap(uid);
        let userPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + uid + "/preferences";
        const orgSubscriptionPath: string = userPath + Constants.STR_FSLASH_SUBSCRIPTION_DATE_FSLASH;
        const orgPlanPath: string = userPath + "/cardID/";
        await FirebaseAdminWrapper.GetInstance().SetValue(orgPlanPath, cardID);
        await FirebaseAdminWrapper.GetInstance().SetValue(orgSubscriptionPath, new Date(Date.now()).getTime());
        charge = await StripeWrapper.GetInstance().ChargeCard(stripeID, email, cardID, price, this._getApartmentSize(price));
        return charge;
    }

    static async GetSenderDeviceToken(userID: string): Promise<string> {
        if (!userID) {
            throw new Error("No User ID!");
        } else {
            // UserID exists
        }
        let fcmRegToken: string = "";
        const map: string = await this.GetUserMap(userID);
        let userPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + userID + Constants.STR_FSLASH_FCMTOKEN_FSLASH;
        if (FirebaseAdminWrapper.GetInstance().DoesRefExist(userPath)) {
            let snapShot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(userPath);
            fcmRegToken = snapShot.val();
        } else {
            fcmRegToken = "No FCM Token!";
        }

        return fcmRegToken;
    }

    static SilverBrickUsertoNormalizedUserInfo(userID: string, user: ISilverBrickUser): NormalizedUserInfo {
        console.log("Got Info ", userID);
        let osType: OperatingSystemType = OperatingSystemType.OTHER;

        switch (user.osType) {
            case Constants.OS_TYPE_ANDROID: {
                osType = OperatingSystemType.ANDROID;
                break;
            }

            case Constants.OS_TYPE_IOS: {
                osType = OperatingSystemType.IOS;
                break;
            }

            default: {
                osType = OperatingSystemType.OTHER;
                break;
            }
        }
        return NormalizedUserInfo.CreateInstance(userID, user, [user.email], [DeviceInfoTuple.CreateInstance(user.fcmRegToken, osType)]);
    }

    static async GetDevices(uidList: string[], uidListToAdd: string[], reportBuilder: IReportBuilder = null): Promise<NormalizedUserInfo[]> {
        // console.log("Getting Devices", uidList, uidListToAdd);
        let normalizedUserInfo: NormalizedUserInfo[] = [];
        if (uidList.length > 0) {
            for (let uid of uidList) {
                let user: ISilverBrickUser = await this.GetUserInfo(uid);
                normalizedUserInfo.push(this.SilverBrickUsertoNormalizedUserInfo(uid, user));
            }
        }
        if (uidListToAdd.length > 0) {
            for (let uid of uidList) {
                let user: ISilverBrickUser = await this.GetUserInfo(uid);
                normalizedUserInfo.push(this.SilverBrickUsertoNormalizedUserInfo(uid, user));
            }
        } else {
            console.log("No one to add!");
            reportBuilder.Add("No one to add!", uidListToAdd);
        }
        console.log("@@@@");
        return normalizedUserInfo;
    }

    public static async GetAllTenantsAllBuildings(): Promise<IUser[]> {
        // console.log("GetAllTenantsAllBuildings");
        let profileUserPath: string = Constants.STR_USERS_FSLASH + Constants.STR_TENANTS_FSLASH;
        let userBuildings: Array<IUser> = [];
        let snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(profileUserPath);
        // console.log("dataValues", dataValues, dataPayload);
        if (snapshot.val()) {
            let userVals: any[] = ArrayUtil.Transform(<any[]>snapshot.val());
            for (let userVal of userVals) {
                const orgID: string = Object.keys(userVal.orgs)[0];
                const buildingMap: Map<string, any> = await SilverBrickAdminApiInternal.GetBuildingsInOrg(orgID);
                let buildings: IUserBuilding[] = null;
                if (orgID) {
                    // OPTIMIZE: Use for loop over map for-in performance gain
                    buildings = Object.keys(userVal.orgs[orgID].buildings).map((bKey: string) => {
                        return {
                            id: bKey,
                            name: buildingMap.get(bKey).name,
                            isAdmin: userVal.orgs[orgID].buildings[bKey].isAdmin || false
                        };
                    });
                }
                const orgInfo: IOrgInfo = await SilverBrickAdminApiInternal.GetOrgInfo(orgID);
                let org: IUserOrg = {
                    id: orgID,
                    name: orgInfo.name,
                    isAdmin: userVal.orgs[orgID].isAdmin || false,
                    buildings: ArrayUtil.Transform(buildings)
                };
                let userObj: NormalizedUser = <NormalizedUser>userVal;
                userObj.orgs = org;
                userBuildings.push(userObj);
            }
        }
        return userBuildings;
    }

    public static async GetAllLandlordsAllBuildings(): Promise<IUser[]> {
        let profileUserPath: string = Constants.STR_USERS_FSLASH + Constants.STR_LANDLORDS_FSLASH;
        let userBuildings: Array<IUser> = [];
        let snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(profileUserPath);
        // console.log("dataValues", dataValues, dataPayload);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(profileUserPath)) {
            let userVals: any[] = ArrayUtil.Transform(<any[]>snapshot.val());
            for (let userVal of userVals) {
                const orgID: string = Object.keys(userVal.orgs)[0];
                const buildingMap: Map<string, any> = await SilverBrickAdminApiInternal.GetBuildingsInOrg(orgID);
                let buildings: IUserBuilding[] = null;
                if (orgID) {
                    // OPTIMIZE: Use for loop over map for-in performance gain
                    // console.log("orgID", orgID);
                    buildings = Object.keys(userVal.orgs[orgID].buildings).map((bKey: string) => {
                        // console.log("bKey", bKey, buildingMap);
                        return {
                            id: bKey,
                            name: buildingMap.get(bKey).name,
                            isAdmin: userVal.orgs[orgID].buildings[bKey].isAdmin || false
                        };
                    });
                }
                const orgInfo: IOrgInfo = await SilverBrickAdminApiInternal.GetOrgInfo(orgID);
                let org: IUserOrg = {
                    id: orgID,
                    name: orgInfo.name,
                    isAdmin: userVal.orgs[orgID].isAdmin || false,
                    buildings: ArrayUtil.Transform(buildings)
                };
                let userObj: NormalizedUser = <NormalizedUser>userVal;
                userObj.orgs = org;
                userBuildings.push(userObj);
            }
        }
        return userBuildings;
    }

    public static async GetAllTenants(orgID: string, buildingID: string): Promise<IUser[]> {
        console.log("GetAllTenants");
        let profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_TENANTS_FSLASH + buildingID;
        let userBuildings: Array<NormalizedUser> = [];
        // Logger.Info("GetAllTenants under the building from: " + profileUserPath);
        let snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(profileUserPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(profileUserPath)) {
            let dataPayload: string[] = Object.keys(snapshot.val());
            if (null == dataPayload) {
                throw new Error("DB value payload is null! Please check that the following DB path exists: " + profileUserPath);
            }

            let logBuilder = SessionLogBuilder.CreateInstance("#### Retrieved users ####", LogLevel.INFO);
            for (let uid of dataPayload) {
                const map: string = await UserApiInternal.GetUserMap(uid);
                const uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + Constants.STR_TENANTS_FSLASH + uid;
                const dbSnapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(uidPath);
                let userVal: any = <any>dbSnapshot.val();
                // Extract org information from the snapshot
                // Assuming a user will only ever have 1 org
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(uidPath)) {
                    const orgID: string = Object.keys(userVal.orgs)[0];
                    const buildingMap: Map<string, any> = await SilverBrickAdminApiInternal.GetBuildingsInOrg(orgID);
                    let buildings: IUserBuilding[] = null;
                    if (orgID) {
                        // OPTIMIZE: Use for loop over map for-in performance gain
                        buildings = Object.keys(userVal.orgs[orgID].buildings).map((bKey: string) => {
                            return {
                                id: bKey,
                                name: buildingMap.get(bKey).name,
                                isAdmin: userVal.orgs[orgID].buildings[bKey].isAdmin || false
                            };
                        });
                        // console.log("buildings", buildings);
                    }

                    const orgInfo: IOrgInfo = await SilverBrickAdminApiInternal.GetOrgInfo(orgID);
                    let org: IUserOrg = {
                        id: orgID,
                        name: orgInfo.name,
                        isAdmin: userVal.orgs[orgID].isAdmin || false,
                        buildings: ArrayUtil.Transform(buildings)
                    };
                    // console.log("org", org);
                    let userObj: NormalizedUser = <NormalizedUser>userVal;
                    userObj.orgs = org;
                    userBuildings.push(userObj);
                }
            }
        }
        return userBuildings;
    }

    public static async GetAllTenantsSomeBldg(orgs: IUserOrg[]): Promise<IUser[]> {
        console.log("GetAllTenantsSomeBldg", orgs);
        let userBuildings: Array<NormalizedUser> = [];
        let logBuilder = SessionLogBuilder.CreateInstance("#### Retrieved users ####", LogLevel.INFO);
        for (let org of orgs) {
            for (let bldg of org.buildings) {
                const tenantPath: string = Constants.STR_FSLASH_ORGS_FSLASH + org.id + Constants.STR_FSLASH_TENANTS_FSLASH + bldg.id + Constants.STR_FSLASH;
                let snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(tenantPath);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(tenantPath)) {
                    let dataPayload: string[] = Object.keys(snapshot.val());
                    console.log("dataPayload", dataPayload);
                    for (let uid of dataPayload) {
                        console.log("dataPayload", dataPayload, uid);
                        const uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + Constants.STR_TENANTS_FSLASH + uid + Constants.STR_FSLASH;
                        const dbSnapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(uidPath);
                        let userVal: any = <any>dbSnapshot.val();
                        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(uidPath)) {
                            const orgID: string = Object.keys(userVal.orgs)[0];
                            let buildings: IUserBuilding[] = null;
                            buildings = [{
                                id: bldg.id,
                                name: (null != bldg.name) ? bldg.name : "",
                                isAdmin: userVal.orgs[org.id].buildings[bldg.id].isAdmin || false
                            }];
                            const orgInfo: IOrgInfo = await SilverBrickAdminApiInternal.GetOrgInfo(org.id);
                            let userOrg: IUserOrg = {
                                id: orgID,
                                name: orgInfo.name,
                                isAdmin: userVal.orgs[orgID].isAdmin || false,
                                buildings: ArrayUtil.Transform(buildings)
                            };
                            let userObj: NormalizedUser = <NormalizedUser>userVal;
                            userObj.orgs = userOrg;
                            console.log("userObj", userObj);
                            userBuildings.push(userObj);
                        }
                    }
                }
                else {
                    return userBuildings;
                }
            }
        }
        return userBuildings;
    }

    public static async GetAllLandlords(orgID: string, buildingID: string): Promise<IUser[]> {
        let profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + orgID + Constants.STR_FSLASH_LANDLORDS_FSLASH + buildingID;
        let userBuildings: Array<NormalizedUser> = [];
        try {
            let snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(profileUserPath);
            if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(profileUserPath)) {
                let dataPayload: string[] = Object.keys(snapshot.val());
                if (null == dataPayload) {
                    throw new Error("DB value payload is null! Please check that the following DB path exists: " + profileUserPath);
                }

                let logBuilder = SessionLogBuilder.CreateInstance("#### Retrieved users ####", LogLevel.INFO);

                for (let uid of dataPayload) {
                    const map: string = await UserApiInternal.GetUserMap(uid);
                    const uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + Constants.STR_LANDLORDS_FSLASH + uid;
                    const dbSnapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(uidPath);
                    if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(uidPath)) {
                        let userVal: any = <any>dbSnapshot.val();
                        // Extract org information from the snapshot
                        // Assuming a user will only ever have 1 org
                        if (((userVal.role.toLowerCase() === "landlord") && (null != userVal.orgs))) {
                            const orgID: string = Object.keys(userVal.orgs)[0];
                            const buildingMap: Map<string, any> = await SilverBrickAdminApiInternal.GetBuildingsInOrg(orgID);
                            let buildings: IUserBuilding[] = null;
                            if (orgID) {
                                // OPTIMIZE: Use for loop over map for-in performance gain
                                buildings = Object.keys(userVal.orgs[orgID].buildings).map((bKey: string) => {
                                    return {
                                        id: bKey,
                                        name: buildingMap.get(bKey).name,
                                        isAdmin: userVal.orgs[orgID].buildings[bKey].isAdmin || false
                                    };
                                });
                                // console.log("buildings", buildings);
                            }

                            const orgInfo: IOrgInfo = await SilverBrickAdminApiInternal.GetOrgInfo(orgID);
                            let org: IUserOrg = {
                                id: orgID,
                                name: orgInfo.name,
                                isAdmin: userVal.orgs[orgID].isAdmin || false,
                                buildings: ArrayUtil.Transform(buildings)
                            };
                            // console.log("org", org);
                            let userObj: NormalizedUser = <NormalizedUser>userVal;
                            userObj.orgs = org;
                            userBuildings.push(userObj);
                        }
                        else {
                            let userObj: NormalizedUser = <NormalizedUser>userVal;
                            userBuildings.push(userObj);
                        }
                    }
                    else {
                        
                    }
                }
            }
        }
        catch (error) {
            console.log("Error GetAllLandlords: " + error);
            throw new Error("Error GetAllLandlords: " + error);
        }
        return userBuildings;
    }

    public static async UpdateUserDeviceInfo(userID: string, deviceInfo: IDeviceInfo): Promise<void> {
        if (!userID || null == deviceInfo) {
            throw new Error("Parameters cannot be null! userID: " + userID + " deviceInfo: " + deviceInfo);
        }

        if (!deviceInfo.fcmRegToken || !deviceInfo.osType) {
            Logger.Error("Device info passed in does not have either the fcmRegToken or osType! Uid - " + userID + " Device info - " + JSON.stringify(deviceInfo));
        }

        console.log("UpdateUserDeviceInfo", JSON.stringify(deviceInfo));
        const map: string = await this.GetUserMap(userID);
        let userPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + userID;

        let userPathExist: boolean = await FirebaseAdminWrapper.GetInstance().DoesRefExist(userPath);

        if (!userPathExist) {
            throw new Error("UID not recognized!!!" + userID);
        }

        await FirebaseAdminWrapper.GetInstance().UpdateValue(userPath, deviceInfo);

        Logger.Info("Updated " + userID + " with the following info - " + JSON.stringify(deviceInfo));
    }

    public static async DeleteUserDeviceInfo(userID: string, deviceInfo: IDeviceInfo): Promise<void> {
        if (!userID || null == deviceInfo) {
            throw new Error("Parameters cannot be null! userID: " + userID + " deviceInfo: " + deviceInfo);
        }

        if (!deviceInfo.fcmRegToken || !deviceInfo.osType) {
            Logger.Error("Device info passed in does not have either the fcmRegToken or osType! Uid - " + userID + " Device info - " + JSON.stringify(deviceInfo));
        }

        const map: string = await this.GetUserMap(userID);
        let userPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + userID;

        let userPathExist: boolean = await FirebaseAdminWrapper.GetInstance().DoesRefExist(userPath);

        if (!userPathExist) {
            throw new Error("UID not recognized!!!" + userID);
        }

        // Since we only have one device pair, we can just clear out the single fields. In the future, we'll
        // need to do a device ID lookup to decide, which device ID to delete.

        deviceInfo.fcmRegToken = null;
        deviceInfo.osType = null;

        await FirebaseAdminWrapper.GetInstance().UpdateValue(userPath, deviceInfo);

        Logger.Info("Deleted device info for - " + userID);
    }

    static async GetUserInfo(uid: string, logBuilder: SessionLogBuilder = null): Promise<ISilverBrickUser> {
        if (!uid) {
            throw new Error("Parameters cannot be null! uid: " + uid);
        }
        const map: string = await this.GetUserMap(uid);
        let uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + uid;
        let snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(uidPath);
        let returnVal: ISilverBrickUser = <ISilverBrickUser>snapshot.val();

        if (null == returnVal) {
            throw new Error("DB value payload is null! Please check that the following DB path exists: " + uidPath);
        }

        if (logBuilder) {
            logBuilder.Log("Got user info for '" + returnVal.firstName + " " + returnVal.lastName + "' from path: " + uidPath);
        }

        return returnVal;
    }

    static async AddTimeCard(card: ITimeCard): Promise<ITimeCard> {
        if (!card) {
            throw new Error("Parameters cannot be null! card: " + card);
        }
        let userPath: string = Constants.STR_USERS_FSLASH + Constants.STR_STAFF_FSLASH + card.authorID + Constants.STR_FSLASH_ISCLOCKEDIN_FSLASH;
        // console.log("userPath", userPath);
        await FirebaseAdminWrapper.GetInstance().SetValue(userPath, true);
        let timeCardPath: string = Constants.STR_TIME_CARDS_FSLASH;
        let byMonthPath: string = timeCardPath + Constants.STR_BY_MONTH_FSLASH + card.authorID + Constants.STR_FSLASH + `${ card.year }_${ card.month }_${ card.day }` + Constants.STR_FSLASH;
        let byMonthRef: firebaseAdmin.database.Reference = await FirebaseAdminWrapper.GetInstance().PushValue(byMonthPath, card);
        let cardID: string = byMonthRef.key;
        card.id = cardID;
        await FirebaseAdminWrapper.GetInstance().SetValue(byMonthPath + cardID + "/id/", cardID);
        return card;
    }

    static async UpdateTimeCard(card: ITimeCard): Promise<ITimeCard> {
        if (!card) {
            throw new Error("Parameters cannot be null! card: " + card);
        }
        let userPath: string = Constants.STR_USERS_FSLASH + Constants.STR_STAFF_FSLASH + card.authorID + Constants.STR_FSLASH_ISCLOCKEDIN_FSLASH;
        // console.log("userPath", userPath);
        await FirebaseAdminWrapper.GetInstance().SetValue(userPath, false);
        let todayDate: Date = new Date(card.endTime);
        let timeCardPath: string = Constants.STR_TIME_CARDS_FSLASH;
        let byMonthPath: string = timeCardPath + Constants.STR_BY_MONTH_FSLASH + card.authorID + Constants.STR_FSLASH + `${ card.year }_${ card.month }_${ card.day }` + Constants.STR_FSLASH + card.id + Constants.STR_FSLASH;
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(byMonthPath)) {
            await FirebaseAdminWrapper.GetInstance().SetValue(byMonthPath, card);
        }
        else {
            let newByMonthPath: string = timeCardPath + Constants.STR_BY_MONTH_FSLASH + card.authorID + Constants.STR_FSLASH + `${ card.year }_${ card.month }_${ (card.day - 1) }` + Constants.STR_FSLASH + card.id + Constants.STR_FSLASH;
            if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(newByMonthPath)) {
                await FirebaseAdminWrapper.GetInstance().SetValue(byMonthPath, card);
            }
            else {
                let newMonth = card.month - 1;
                let year = card.year;
                if (card.month === 0) {
                    newMonth = 11;
                    year = card.year - 1;
                }
                let d = new Date(card.year, newMonth + 1, 0);
                let newerByMonthPath: string = timeCardPath + Constants.STR_BY_MONTH_FSLASH + card.authorID + Constants.STR_FSLASH + `${ year }_${ newMonth }_${ d.getDate() }` + Constants.STR_FSLASH + card.id + Constants.STR_FSLASH;
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(newerByMonthPath)) {
                    await FirebaseAdminWrapper.GetInstance().SetValue(newerByMonthPath, card);
                }
            }
        }
        return card;
    }

    static async GetTimeCards(date: number, uid: string, year: number, month: number, day: number): Promise<ITimeCard[]> {
        if (!date) {
            throw new Error("Parameters cannot be null! date: " + date);
        }
        let cards: ITimeCard[] = [];
        let todayDate: Date = new Date(Date.now());
        let timeCardPath: string = Constants.STR_TIME_CARDS_FSLASH;
        let byMonthPath: string = timeCardPath + Constants.STR_BY_MONTH_FSLASH + uid + Constants.STR_FSLASH + `${ year }_${ month }_${ day }` + Constants.STR_FSLASH;
        let byMonthSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(byMonthPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(byMonthPath)) {
            byMonthSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newCard: ITimeCard = <ITimeCard>innerSnapshot.val();
                newCard.id = innerSnapshot.key;
                cards.push(newCard);
                return false;
            });
        }
        else {
            let newByMonthPath: string = timeCardPath + Constants.STR_BY_MONTH_FSLASH + uid + Constants.STR_FSLASH + `${ year }_${ month }_${ (day - 1) }` + Constants.STR_FSLASH;
            let newByMonthSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(newByMonthPath);
            if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(newByMonthPath)) {
                newByMonthSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                    let newCard: ITimeCard = <ITimeCard>innerSnapshot.val();
                    newCard.id = innerSnapshot.key;
                    cards.push(newCard);
                    return false;
                });
            }
            else {
                let newMonth = month - 1;
                let newYear = year;
                if (month === 0) {
                    newMonth = 11;
                    newYear = year - 1;
                }
                let d = new Date(year, newMonth + 1, 0);
                let newerByMonthPath: string = timeCardPath + Constants.STR_BY_MONTH_FSLASH + uid + Constants.STR_FSLASH + `${ year }_${ newMonth }_${ d.getDate() }` + Constants.STR_FSLASH;
                let newerByMonthSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(newerByMonthPath);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(newerByMonthPath)) {
                    newerByMonthSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                    let newCard: ITimeCard = <ITimeCard>innerSnapshot.val();
                    newCard.id = innerSnapshot.key;
                    cards.push(newCard);
                    return false;
                });
                }
            }
        }
        return cards;
    }

    static async GetTimeCardsForToday(year: number, month: number, day: number): Promise<ITimeCardRoster> {
        let todayDate: Date = new Date(Date.now());
        let cards: ITimeCardRoster = {};
        const timeCard: string = Constants.STR_TIME_CARDS_FSLASH + Constants.STR_BY_MONTH_FSLASH;
        let snapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(timeCard);
        if (snapshot.val()) {
            let dataPayload: string[] = Object.keys(snapshot.val());
            if (null == dataPayload) {
                throw new Error("DB value payload is null! Please check that the following DB path exists: " + timeCard);
            }
            for (let uid of dataPayload) {
                let byMonthPath: string = timeCard + uid + Constants.STR_FSLASH + `${ year }_${ month }_${ day }` + Constants.STR_FSLASH;
                // console.log("uid", uid, byMonthPath);
                let byMonthSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(byMonthPath);
                if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(byMonthPath)) {
                    byMonthSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                        let newCard: ITimeCard = <ITimeCard>innerSnapshot.val();
                        newCard.id = innerSnapshot.key;
                        if (null == cards[uid]) {
                            cards[uid] = {
                                name: newCard.authorName,
                                uid: newCard.authorID,
                                cards: []
                            };
                            cards[uid].cards.push(newCard);
                        }
                        else {
                            cards[uid].cards.push(newCard);
                        }
                        return false;
                    });
                }
            }
        }
        return cards;
    }

    static async GetTimeCard(uid: string): Promise<ITimeCard[]> {
        let cards: ITimeCard[] = [];
        let todayDate: Date = new Date(Date.now());
        let timeCardPath: string = Constants.STR_TIME_CARDS_FSLASH;
        let byMonthPath: string = timeCardPath + Constants.STR_BY_MONTH_FSLASH + uid + Constants.STR_FSLASH + `${ todayDate.getFullYear() }_${ todayDate.getMonth() + 1 }_${ todayDate.getDate() }` + Constants.STR_FSLASH;
        let byMonthSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(byMonthPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(byMonthPath)) {
            byMonthSnap.forEach((innerSnapshot: firebaseAdmin.database.DataSnapshot) => {
                let newCard: ITimeCard = <ITimeCard>innerSnapshot.val();
                newCard.id = innerSnapshot.key;
                cards.push(newCard);
                return false;
            });
        }
        return cards;
    }

    static async GetCalendarColors(): Promise<any> {
        let colors: any = null;
        let colorsPath: string = Constants.STR_USERS_FSLASH + Constants.STR_CALENDAR_COLORS_FSLASH;
        let colorsSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(colorsPath);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(colorsPath)) {
            colors = <any>colorsSnap.val();
        }
        return colors;
    }

    static async UpdateProfile(user: IProfile): Promise<NormalizedUser> {
        // Input validation
        if (!user.uid || !user.firstName || !user.lastName) {
            throw new Error("Parameters cannot be null! User ID: " + user.uid + ", First Name: " + user.firstName + ", Last Name: " + user.lastName);
        }

        try {
            // Set up change detection flags
            let updateFbAuth: boolean = false;
            let updateProfileNode: boolean = false;
            // TODO: Add a change detection flag for audience when we implement that

            // Set up firebase auth object map for potential updating
            let fbUserObj: any = {};

            // Get the old user object for change comparison
            let oldUserObj: IUser = await UserAdminApiInternal.GetUser(user.uid);

            if (!oldUserObj) {
                throw new Error("User payload is null! Please check that the following user id exists: " + user.uid);
            }

            // Check for name change
            if (user.firstName !== oldUserObj.firstName || user.lastName !== oldUserObj.lastName) {
                updateFbAuth = true;
                fbUserObj.displayName = `${user.firstName} ${user.lastName}`;

                updateProfileNode = true;
                // TODO: Update audience node also when that is ready
            }

            /*
            Start the updates
            */

            // Update firebase auth db
            let newUser: firebaseAdmin.auth.UserRecord = undefined;
            if (updateFbAuth) {
                // Update the firebase auth db
                newUser = await FirebaseAdminWrapper.GetInstance().UpdateUser(user.uid, fbUserObj);
            }

            // Update the user db
            let userObj: NormalizedUser = new NormalizedUser(
                oldUserObj.uid,
                user.firstName,
                user.lastName,
                user.photoURL,
                oldUserObj.email,
                user.phone,
                oldUserObj.isAdmin,
                oldUserObj.role,
                oldUserObj.orgs,
                newUser ? newUser.metadata.lastSignedInAt : oldUserObj.lastSignedInAt, 
                newUser ? newUser.metadata.createdAt : oldUserObj.createdAt,
                oldUserObj.fcmRegToken, user.address,
                oldUserObj.clients,
                oldUserObj.osType,
                oldUserObj.stripeID,
                oldUserObj.propertyCodes,
                oldUserObj.preferences,
                (oldUserObj.isVendor) ? oldUserObj.isVendor : false,
                (oldUserObj.isFirstTime) ? oldUserObj.isFirstTime : false,
                (oldUserObj.lastActivity) ? oldUserObj.lastActivity : null,
                (oldUserObj.isClockedIn) ? oldUserObj.isClockedIn : false,
                (oldUserObj.unit) ? oldUserObj.unit : null,
                (oldUserObj.calendarColor) ? oldUserObj.calendarColor : null,
                (oldUserObj.calendarTextColor) ? oldUserObj.calendarTextColor : null);

            let uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((userObj.role.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (userObj.role.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + user.uid;
            await FirebaseAdminWrapper.GetInstance().SetValue(uidPath, userObj.GetDatabaseData());
            if (user.address) {
                const updates: customers.ICustomerUpdateOptions = {
                    shipping: {
                        address: {
                            line1: user.address.line1,
                            line2: user.address.line2,
                            city: user.address.city,
                            state: user.address.state,
                            postal_code: user.address.zip,
                            country: "US"
                        },
                        name: "Home Address"
                    }
                };
                const stripeID: customers.ICustomer = await StripeWrapper.GetInstance().UpdateCustomer(userObj.stripeID, updates);
            }
            return userObj;
        } catch (error) {
            throw error;
        }
    }

    static async UpdateUserPreferences(uid: string, preferences: IPreferences, shipping: IShippingInformation, cardInfo: cards.ISourceCreationOptionsExtended, user: IUser): Promise<void> {
        const userPath: string = Constants.STR_USERS_FSLASH + Constants.STR_TENANTS_FSLASH + uid;
        let oldUser: NormalizedUser = await UserAdminApiInternal.GetUser(uid);
        if (await FirebaseAdminWrapper.GetInstance().DoesRefExist(userPath)) {
            if (oldUser.preferences.isFirstTime) {
                try {
                    oldUser.preferences.isFirstTime = false;
                    preferences.isFirstTime = false;
                    const profileUserPath: string = Constants.STR_FSLASH_ORGS_FSLASH + user.orgs.id + Constants.STR_FSLASH_TENANTS_FSLASH + user.orgs.buildings[0].id + Constants.STR_FSLASH + user.uid;
                    let tenant: IStaffProfile = {
                        firstName: user.firstName,
                        lastName: user.lastName,
                        uid: user.uid,
                        role: user.role,
                        email: user.email,
                        isVendor: (user.isVendor) ? user.isVendor : false
                    };
                    await FirebaseAdminWrapper.GetInstance().SetValue(profileUserPath, tenant);
                    const usersPath2: string = Constants.STR_USERS_FSLASH + Constants.STR_FSLASH_TENANTS_FSLASH + uid + Constants.STR_FSLASH;
                    await FirebaseAdminWrapper.GetInstance().SetValue(usersPath2 + "firstName/", user.firstName);
                    await FirebaseAdminWrapper.GetInstance().SetValue(usersPath2 + "lastName/", user.lastName);
                    await FirebaseAdminWrapper.GetInstance().SetValue(usersPath2 + "address/", user.address);
                    const updates: customers.ICustomerUpdateOptions = {
                        shipping: {
                            address: {
                                line1: shipping.address.line1,
                                line2: shipping.address.line2,
                                city: shipping.address.city,
                                state: shipping.address.state,
                                postal_code: shipping.address.postal_code,
                                country: "US"
                            },
                            name: "Home Address"
                        }
                    };
                    const stripeID: customers.ICustomer = await StripeWrapper.GetInstance().UpdateCustomer(oldUser.stripeID, updates);
                    const newCardIinfo: ICard = await StripeWrapper.GetInstance().AddPaymentCard(oldUser.stripeID, oldUser.email, cardInfo);
                    preferences.cardID = newCardIinfo.id;
                    preferences.subscriptionNewDate = new Date(Date.now()).getTime();
                    // if (booking.isCleaning) {
                    //     const newChage: charges.ICharge = await this.CreateCharge(stripeID.id, newCardIinfo.id, user.email, user.orgs.id, booking.price, user.uid);
                    // }
                    // else {
                    //     const newSub: subscriptions.ISubscription = await this.CreateSubscription(oldUser.stripeID, newCardIinfo.id, preferences.planID, user.orgs.id, cardInfo.cvc, cardInfo.number, oldUser.uid);
                    //     const helper: INewSubscription = {
                    //         planID: newSub.id,
                    //         date: new Date(Date.now()).getDate(),
                    //         price: 0.5
                    //     };
                    // }
                    // await ServiceApiInternal.AddService(user.orgs.id, user.orgs.buildings[0].id, oldUser.uid, booking);
                    const emailPredicate: IEmailPredicate = {
                        Email: "leo@silverbrickmanagement.com",
                        FirstName: "Leo"
                    };
                    const userEmailPredicate: IEmailPredicate = {
                        Email: user.email,
                        FirstName: user.firstName
                    };
                    const adamEmailPredicate: IEmailPredicate = {
                        Email: "adam@adamwolfson.com",
                        FirstName: "Adam"
                    };
                    let date: string = new Date().toDateString();
                    let newDate = new Date(TimeUtil.GetMonday(new Date(Date.now()), TimeUtil.GetWeekDay(preferences.preferredDay)));
                    let parts: string[] = date.split(" ", 4);
                    let newMonth: number = new Date(Date.now()).getTime() + Constants.A_DAY_IN_MILLISECONDS * 31;
                    console.log("newMonth", new Date(newMonth));
                    let month: Date = new Date(newMonth);
                    let day: string = parts[2];
                    let year: string = String(new Date(Date.now()).getFullYear());
                    let renewal: string = month.toDateString();
                    // const emailParams: ISignUpEmailTemplateParams = EmailTemplateFactory.CreateSignUpEmailTemplateParams(`A new user signed up ${user.firstName} ${user.lastName} from property ${user.orgs.name} (${user.orgs.buildings[0].name})`, `A new user sign up ${oldUser.email} from property ${user.orgs.name} (${user.orgs.buildings[0].name}`, `A new user sign up ${oldUser.firstName} ${oldUser.lastName} has signed up for SilverBrick on ${date}`, preferences, user.orgs.name, user.orgs.buildings[0].name, (booking.isCleaning) ? String(booking.price) : preferences.planID === Constants.BASE_PLAN_ID ? "$399" : "$799", null);
                    // const userEmailParams: ISignUpEmailTemplateParams = EmailTemplateFactory.CreateSignUpEmailTemplateParams(`Thank you for signing up with SilverBrick`, "Welcome to SilverBrick", `We’re looking forward to service you. We’ll come by${(booking.isCleaning) ? "" : " for the first time" } on ${newDate.toString()}. `, preferences, user.orgs.name, user.orgs.buildings[0].name, (booking.isCleaning) ? String(booking.price) : preferences.planID === Constants.BASE_PLAN_ID ? "$399" : "$799", null);
                    // const adamEmailParams: ISignUpEmailTemplateParams = EmailTemplateFactory.CreateSignUpEmailTemplateParams(`A new user signed up ${user.firstName} ${user.lastName} from property ${user.orgs.name} (${user.orgs.buildings[0].name})`, `A new user sign up ${oldUser.email} from property ${user.orgs.name} (${user.orgs.buildings[0].name}`, `A new user sign up ${oldUser.firstName} ${oldUser.lastName} has signed up for SilverBrick on ${date}`, preferences, user.orgs.name, user.orgs.buildings[0].name, (booking.isCleaning) ? String(booking.price) : preferences.planID === Constants.BASE_PLAN_ID ? "$399" : "$799", null);
                    // await MailGunWrapper.GetInstance().Send([emailPredicate], `A new user signed up ${user.firstName} ${user.lastName} from property ${user.orgs.name} (${user.orgs.buildings[0].name})`, userEmailParams, MailType.SIGNUP);
                    // await MailGunWrapper.GetInstance().Send([userEmailPredicate], `Thank you for signing up with SilverBrick`, userEmailParams, MailType.SIGNUP);
                    // await MailGunWrapper.GetInstance().Send([adamEmailPredicate], `A new user signed up ${user.firstName} ${user.lastName} from property ${user.orgs.name} (${user.orgs.buildings[0].name})`, userEmailParams, MailType.SIGNUP);
                    await FirebaseAdminWrapper.GetInstance().SetValue(userPath + "/preferences/", preferences);
                    let message: string = `New user ${user.firstName} ${user.lastName} from property ${user.orgs.name} (${user.orgs.buildings[0].name}) signed up.`;
                    const notificationData: INotificationData = {
                        authorName: `${user.firstName} ${user.lastName}`,
                        title: `New User(${user.orgs.name} | ${user.orgs.buildings[0].name})`,
                        type: NotificationType.NEW_USER,
                        date: firebaseAdmin.database.ServerValue.TIMESTAMP,
                        message: message,
                        authorID: user.uid
                    };
                    PushApiInternal.AddNotification(user.orgs.id, user.orgs.buildings[0].id, notificationData);
                    // if (booking.isCleaning) {
                    //     await StatusOrchestrator.GetInstance().SendStatus(`A new user signed up ${user.firstName} ${user.lastName} from property ${user.orgs.name} (${user.orgs.buildings[0].name}). \nThey have paid for a one time cleaning for a`, "one-offs");
                    // }
                    // else {
                    //     await StatusOrchestrator.GetInstance().SendStatus(`A new user signed up ${user.firstName} ${user.lastName} from property ${user.orgs.name} (${user.orgs.buildings[0].name}). \nTheir plan is the ${preferences.planID === Constants.BASE_PLAN_ID ? "Basic Plan" : "Premium Plan"} for ${preferences.planID === Constants.BASE_PLAN_ID ? "$399" : "$799"}.\nTheir next billing date is ${renewal}.`, "sign-ups");
                    // }
                } catch (error) {
                    console.log("Error Preferences! " + JSON.stringify(error));
                    throw new Error("Error Preferences! " + JSON.stringify(error));
                }
            }
        }
    }

    public static async UpdateUserLocation(userID: string, location: ILocation): Promise <ILocation> {
        if (!userID || !location) {
            throw new Error("Parameters: " + userID + " location: " + location);
        }
        const userLocationPath: string = Constants.STR_USER_LOCATIONS_FSLASH + userID + Constants.STR_FSLASH;
        await FirebaseAdminWrapper.GetInstance().UpdateValue(userLocationPath, location);
        return location;
    }

    static async SaveLastActivityDate(userID: string): Promise<void> {
        const map: string = await this.GetUserMap(userID);
        let uidPath: string = Constants.STR_FSLASH_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + userID + Constants.STR_FSLASH_LAST_ACTIVITY_FSLASH;
        await FirebaseAdminWrapper.GetInstance().SetValue(uidPath, new Date(Date.now()).getTime());
    }

    /**
     * Returns "artist" or "fan" depending on user's account type
     *
     * @param  {string} userId
     * @returns Promise<string> - either "artist" or "fan"
     */
    static async GetUserMap(userId: string): Promise<string> {
        try {
            const mapPath: string = Constants.STR_USERS_FSLASH + Constants.STR_MAP_FSLASH + userId;
            const mapSnapshot: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(mapPath);
            const map: string = mapSnapshot.val();
            return map;
        } catch (error) {
            console.log(`Error: ${error}`);
            await StatusOrchestrator.GetInstance().SendStatus(`GetUserMap: ${userId} - ${error.message}`, "errors");
            throw new Error(error);
        }
    }

    /**
     * Get's a user's avatar
     *
     * @param  {string} uid userID to get avatar for
     *
     * @returns Promise<string> url for user's avatar
     */
    static async GetAvatar(uid: string): Promise<string> {
        try {
            const map: string = await this.GetUserMap(uid);
            const customAvatarPath: string = Constants.STR_USERS_FSLASH + ((map.toLowerCase() === "staff") ? Constants.STR_STAFF_FSLASH : (map.toLowerCase() !== "landlord" ? Constants.STR_TENANTS_FSLASH : Constants.STR_LANDLORDS_FSLASH)) + `${uid}/photoURL`;
            const customSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(customAvatarPath);
            let avatarPath: string = `/users/${uid}/${uid}.png`;
            if (null != map && map.toLowerCase() !== "tenant") {
                avatarPath = "Defaults/icon.png";
            }
            if (null == map) {
                avatarPath = "Defaults/icon.png";
            }
            if (customSnap.val()) {
                return await UserAdminApiInternal.GetImageUrl(avatarPath);
            } else {
                return Constants.DEFAULT_PROFILE_PHOTO_PATH;
            }
        } catch (error) {
            console.log(`Error: ${error}`);
            await StatusOrchestrator.GetInstance().SendStatus(`GetAvatar: ${uid} - ${error.message}`, "errors");
            throw new Error(error);
        }
    }

    static async CheckEmail(searchString: string): Promise<boolean> {
        let searchResults: boolean = false;
        const userMapPath: string = Constants.STR_USERS_FSLASH + Constants.STR_FSLASH_TENANTS_FSLASH;

        try {
            const mapSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(userMapPath);
            const mapVal: ISilverBrickUser[] = <ISilverBrickUser[]>ArrayUtil.Transform(mapSnap.val());
            const search: ISilverBrickUser[] = mapVal.filter((map: ISilverBrickUser) => {
                return map.email.toLowerCase() === searchString.toLowerCase().trim();
            });
            // console.log("Is Email Available? ", search.length === 0);

            return search.length === 0;
        } catch (error) {
            console.log("Error", error);
            throw new Error(error);
        }
    }

    static async CheckAddress(searchString: string): Promise<IBuildingSearch[]> {
        let searchResults: IBuildingSearch[] = [];
        const propertyCodePath: string = Constants.STR_PROPERTY_INDEX_FSLASH;

        try {
            const mapSnap: firebaseAdmin.database.DataSnapshot = await FirebaseAdminWrapper.GetInstance().GetValue(propertyCodePath);
            const mapVal: IBuildingSearch[] = <IBuildingSearch[]>ArrayUtil.Transform(mapSnap.val());
            const search: IBuildingSearch[] = mapVal.filter((map: IBuildingSearch) => {
                return (map.address.line1.toLowerCase().indexOf(searchString.toLowerCase()) > - 1);
                // return map.address.line1.toLowerCase().trim() === searchString.toLowerCase().trim();
            });
            const search2: IBuildingSearch[] = mapVal.filter((map: IBuildingSearch) => {
                return (map.name.toLowerCase().indexOf(searchString.toLowerCase()) > - 1);
                // return map.name.toLowerCase() === searchString.toLowerCase().trim();
            });
            if (null != search) {
                searchResults = [...searchResults, ...search];
            }
            if (null != search2) {
                searchResults = [...searchResults, ...search2];
            }
            // console.log("Search", searchResults);
            // console.log("Is Email Available? ", search.length === 0);
            searchResults = [...new Set(searchResults)];
            return searchResults;
        } catch (error) {
            console.log("Error", error);
            throw new Error(error);
        }
    }

    static  _getApartmentSize(price: number): string {
        let size: string = "One Bedroom";
        switch (price) {
            case 125:
            {
                return "Studio";
            }

            case 150:
            {
                return "One Bedroom";
            }

            case 175:
            {
                return "Two Bedroom";
            }

            case 200:
            {
                return "Three Bedroom";
            }

            default:
            {
                return "One Bedroom";
            }
        }
    }
}

