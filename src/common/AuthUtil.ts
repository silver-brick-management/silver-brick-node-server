// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import { sign, verify, SignOptions, VerifyOptions } from "jsonwebtoken";
import * as crypto from "crypto";

// Local modules
import { Constants } from "../common/Constants";
import { IJwtUser, IJwtBuilding, IJwtOrg } from "../interfaces/IAuth";
import { NormalizedUser } from "../factories/users/NormalizedUser";
import { IUser, IUserAudience } from "../shared/SilverBrickTypes";

export class AuthUtil {
    static GenerateJWTAccessToken(user: NormalizedUser, jwtSecret: string, jwtExpire: string): string {
        let buildings: IJwtBuilding[] = [];
        let jwtOrgData: IJwtOrg = null;
        buildings = user.orgs.buildings.map((building: any) => {
            let audiences: string[] = [];
            let audienceNames: string[] = [];
            if (building.audiences) {
                audiences = building.audiences.map((audience: IUserAudience) => {
                    return audience.id;
                });

                building.audiences.forEach((aud: IUserAudience) => {
                    audienceNames.push(aud.name);
                });
            }
            // Add all to the audience list as all users are in the all audience by default
            audiences.push(Constants.DEFAULT_AUDIENCE);
            audienceNames.push(Constants.DEFAULT_AUDIENCE);
            const jwtBuildingData: IJwtBuilding = {
                id: building.id,
                isAdmin: building.isAdmin,
                audiences: audiences,
                audienceNames: audienceNames
            };
            return jwtBuildingData;
        });

        jwtOrgData = {
            id: user.orgs.id,
            isAdmin: user.orgs.isAdmin
        };

        let jwtData: IJwtUser = {
            uid: user.uid,
            org: jwtOrgData,
            buildings: buildings,
            email: user.email,
            stripeID: user.stripeID,
            phone: (null != user.phone) ? user.phone : "",
            role: user.role,
            isAdmin: user.isAdmin,
            customerName: `${user.firstName} ${user.lastName}`
        };

        const jwtOptions: SignOptions = {
            expiresIn: jwtExpire,
            algorithm: "HS512"
        };
        return sign(jwtData, jwtSecret, jwtOptions);
    }

    static GenerateJWTApplessAccessToken(user: NormalizedUser, jwtSecret: string, jwtExpire: string): string {
        let buildings: IJwtBuilding[] = [];
        let jwtOrgData: IJwtOrg = null;
        buildings = user.orgs.buildings.map((building: any) => {
            let audiences: string[] = [];
            let audienceNames: string[] = [];
            if (building.audiences) {
                audiences = building.audiences.map((audience: IUserAudience) => {
                    return audience.id;
                });

                building.audiences.forEach((aud: IUserAudience) => {
                    audienceNames.push(aud.name);
                });
            }
            // Add all to the audience list as all users are in the all audience by default
            audiences.push(Constants.DEFAULT_AUDIENCE);
            audienceNames.push(Constants.DEFAULT_AUDIENCE);
            const jwtBuildingData: IJwtBuilding = {
                id: building.id,
                isAdmin: building.isAdmin,
                audiences: audiences,
                audienceNames: audienceNames
            };
            return jwtBuildingData;
        });

        jwtOrgData = {
            id: user.orgs.id,
            isAdmin: user.orgs.isAdmin
        };

        let jwtData: IJwtUser = {
            uid: user.uid,
            org: jwtOrgData,
            buildings: buildings,
            email: user.email,
            role: user.role,
            isAdmin: user.isAdmin,
            stripeID: user.stripeID,
            customerName: `${user.firstName} ${user.lastName}`
        };

        const jwtOptions: SignOptions = {
            expiresIn: Math.floor(Date.now() / 1000) + (30 * 60),
            algorithm: "HS512"
        };
        return sign(jwtData, jwtSecret, jwtOptions);
    }

    static VerifyJWTAccessToken(token: string, jwtSecret: string): IJwtUser {
        const jwtOptions: VerifyOptions = {
            algorithms: ["HS512"]
        };
        return <IJwtUser>verify(token, jwtSecret, jwtOptions);
    }

    static async VerifyWebJWTAccessToken(token: string, jwtSecret: string): Promise<IJwtUser> {
        const jwtOptions: VerifyOptions = {
            algorithms: ["HS512"]
        };
        return <IJwtUser>verify(token, jwtSecret, jwtOptions);
    }

    static async CreateLoginHint(msisdn: string, orgID: string, buildingID: string, bookingID: string, serviceID: string, userID: string): Promise<string> {
        console.log("CreateLoginHint", orgID, buildingID, bookingID, serviceID, userID);
        let loginHint = String(msisdn);
        let timestamp: string = new Date().toISOString();
        let iv: Buffer = crypto.randomBytes(16);
        let key: Buffer = crypto.createHash("sha256").update(loginHint).digest();
        console.log("key=" + key.toString("hex"));
        console.log("iv=" + iv.toString("hex"));
        let cryptoCipher: crypto.Cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
        let cypheredLoginHint: string = cryptoCipher.update(orgID + "+___" + buildingID + "+___" + bookingID + "+___" + serviceID + "+___" + userID + "+___" + orgID + "+___" + buildingID + "+___" + userID, "utf8", "base64") + cryptoCipher.final("base64");
        let finalcypheredLoginHint: string = encodeURIComponent(iv.toString("hex") + "+___" + cypheredLoginHint);
        console.log("cypheredLoginHint = " + cypheredLoginHint);
        console.log("finalcypheredLoginHint = " + finalcypheredLoginHint);
        return finalcypheredLoginHint;
    }

    static async VerifyLoginHint(loginHint: string, hint: string): Promise<string> {
        console.log("VerifyLoginHint", loginHint, hint);
        let token: string = decodeURIComponent(hint);
        let parts: string[] = token.split("+___");
        let finalPass: string = null;
        let key: Buffer = crypto.createHash("sha256").update(loginHint).digest();
        console.log(parts, key);
        try {
            let decipher: crypto.Decipher = crypto.createDecipheriv("aes-256-cbc", key, parts[0].slice(0, 16));
            let decryptedPassword = decipher.update(parts[1], "base64", "utf8");
            finalPass = decryptedPassword + decipher.final("utf8");
            console.log("finalPass", finalPass);
            return finalPass;
        }
        catch (error) {
            console.log("error", error);
            throw new Error("error" + error);
        }
    }

}
