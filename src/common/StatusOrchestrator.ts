// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { IStatusReporter, IncomingMessageCallbackType } from "../interfaces/IStatusReporter";
import { StatusReporterType, StatusReporterFactory } from "../factories/StatusReporterFactory";
import { IConfigContext } from "../interfaces/IConfigContext";

export class StatusOrchestrator implements IStatusReporter {

    private static _statusOrchestrator: StatusOrchestrator = null;

    private _isInitialized: boolean;

    private _statusReporters: IStatusReporter[];

    constructor() {
        this._isInitialized = false;
        this._statusReporters = [];
    }

    Initialize(configContext: IConfigContext) {
        if (!this._isInitialized) {
            this._statusReporters.push(
                StatusReporterFactory.CreateInstance(StatusReporterType.SLACK, configContext));

            this._isInitialized = true;
        }
        else {
            // Already initialized, nothing to do
        }
    }

    static GetInstance() {
        if (null == this._statusOrchestrator) {
            this._statusOrchestrator = new StatusOrchestrator();
        }

        return this._statusOrchestrator;
    }

    SendStatus(message: string, channel?: string): void {
        // Iterate all status reporters and send a message
        for (let statusReporter of this._statusReporters) {
            statusReporter.SendStatus(message, channel);
        }
    }

    RegisterIncomingMessageCallback(callback: IncomingMessageCallbackType) {
        for (let statusReporter of this._statusReporters) {
            statusReporter.RegisterIncomingMessageCallback(callback);
        }
    }

}