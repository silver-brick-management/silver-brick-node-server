// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// *********************************************************

// Local modules
import { IUnauthorizedError } from "../interfaces/IError";

export class UnauthorizedError extends Error implements IUnauthorizedError {

    private _name: string;
    private _code: string;
    private _status: number;
    private _message: string;

    get Name(): string {
        return this._name;
    }

    get Code(): string {
        return this._code;
    }

    get Status(): number {
        return this._status;
    }

    get Message(): string {
        return this._message;
    }

    constructor(code: string, obj: any) {
        super(obj.message);

        this._name = "UnauthorizedError";
        this._code = code;
        this._status = 401;
        this._message = obj.message;
    }

    toJSON(): IUnauthorizedError {
        return {
            Name: this.Name,
            Code: this.Code,
            Status: this.Status,
            Message: this.Message
        };
    }
}