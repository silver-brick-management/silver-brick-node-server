// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { StatusOrchestrator } from "../common/StatusOrchestrator";
import { IConfigContext, ExecutionType } from "../interfaces/IConfigContext";
import { IReportBuilder } from "../interfaces/IReportBuilder";
import { IPushResponseResult } from "../wrappers/PushWrapper";
import { NormalizedUserInfo } from "../common/ServerInternalTypes";

export enum LogLevel {
    STACKTRACE,
    VERBOSE,
    INFO,
    ERROR,
    STATUS
}

export type ReportMap = {
    [key: string]: any[];
};

export class ReportBuilder implements IReportBuilder {
    private _reportObj: ReportMap;
    private _header: string;

    private constructor(header: string) {
        this._reportObj = {};
        this._header = header;
    }

    public Add(tag: string, obj: any): void {
        if (null == this._reportObj[tag]) {
            this._reportObj[tag] = obj;
        }
        else {
            if (!Array.isArray(this._reportObj[tag])) {
                // Assign the old value to an array, then push the new
                this._reportObj[tag] = [this._reportObj[tag]];
            }

            this._reportObj[tag].push(obj);
        }
    }

    public static CreateInstance(header: string): ReportBuilder {
        return new ReportBuilder(header);
    }

    public toString(): string {
        return "\nSTART: " + this._header + "\n" + JSON.stringify(this._reportObj, null, 2) + "\nEND: " + this._header + "\n";
    }
}

export class SessionLogBuilder {
    private _tag: string;
    private _isInitialized: boolean;
    private _logLevel: LogLevel;
    private _content: string;

    private constructor(tag: string, logLevel: LogLevel) {
        this._tag = tag;
        this._isInitialized = false;
        this._logLevel = logLevel;
        this._content = "";
    }

    private _initialize(): void {
        if (!this._isInitialized) {
            this._content += "\nENTRY: " + this._tag + "\n\n";
            this._isInitialized = true;
        }
        else {
            Logger.Error("ScopedLogBuilder: Invalid call pattern! _Initialize cannot be called twice");
        }
    }

    static CreateInstance(message: string, logLevel: LogLevel = LogLevel.VERBOSE): SessionLogBuilder {
        let scopedLogger: SessionLogBuilder = new SessionLogBuilder(message, logLevel);

        scopedLogger._initialize();

        return scopedLogger;
    }

    Log(message: string) {
        this._content += "    - " + message + "\n";
    }

    Destroy(): void {
        // Since typescript/javascript has no destructors,
        // destroy should be called explicitly when using ScopedLogger
        if (this._isInitialized) {
            this._content += "\nEXIT: " + this._tag + "\n\n";

            this._flush();

            this._isInitialized = false;
        }
        else {
            Logger.Error("ScopedLogger: Invalid call pattern! destroy cannot be called twice");
        }
    }

    private _flush(): void {
        Logger.log(this._content, this._logLevel);

        // resetting the content message
        this._content = null;
    }
}

export class Logger {
    private _configContext: IConfigContext;
    private static _loggerInstance: Logger = null;

    constructor() {
        this._configContext = null;
    }

    static GetInstance(): Logger {
        if (null == this._loggerInstance) {
            this._loggerInstance = new Logger();
        }

        return this._loggerInstance;
    }

    static SetConfigContext(configContext: IConfigContext) {
        this.GetInstance()._setConfigContext(configContext);
    }

    private _setConfigContext(configContext: IConfigContext) {
        this._configContext = configContext;
    }

    static log(message: string, logLevel: LogLevel): void {
        this.GetInstance()._log(message, logLevel);
    }

    private shouldSendStatusReport(): Boolean {
        return (null !== this._configContext) && (ExecutionType.CREATE_WORKER === this._configContext.ExecutionType);
    }

    private _log(message: string, logLevel: LogLevel): void {
        if (null == message) {
            message = "";
        }

        switch (logLevel) {
            case LogLevel.STATUS:
                if (this.shouldSendStatusReport()) {
                    StatusOrchestrator.GetInstance().SendStatus(
                        this._configContext.WorkerID + ": " + message);
                }

                console.info(message);
                break;

            case LogLevel.ERROR:
                if (this.shouldSendStatusReport()) {
                    StatusOrchestrator.GetInstance().SendStatus(
                        this._configContext.WorkerID + ": " + message);
                }

                console.error(message);
                break;

            case LogLevel.VERBOSE:
            case undefined: /* Default to verbose logging for this case*/
                console.log(message);
                break;

            case LogLevel.STACKTRACE:
                StatusOrchestrator.GetInstance().SendStatus(
                    this._configContext.WorkerID + ": " + message);
                console.trace(message);
                break;

            case LogLevel.INFO:
                console.info(message);
                break;

            default:
                console.error("Logger: LogLevel out of range!");
                console.trace();
                break;
        }
    }

    static Status(message: string) {
        this.log(message, LogLevel.STATUS);
    }

    static ErrorWithPayload(tag: string, payloadData: string) {
        this.PayloadData(tag, payloadData, LogLevel.ERROR);
    }

    static Error(message: string) {
        this.log(message, LogLevel.ERROR);
    }

    static Info(message: string) {
        this.log(message, LogLevel.INFO);
    }

    static Verbose(message: string) {
        this.log(message, LogLevel.VERBOSE);
    }

    static StackTrace(message: string) {
        this.log(message, LogLevel.STACKTRACE);
    }

    static PayloadData(tag: string, payloadData: string, logLevel: LogLevel = LogLevel.VERBOSE) {
        let message = "__ start:" + tag + " __\n" + payloadData + "\n__ end:" + tag + " __";
        this.log(message, logLevel);
    }

    static PushResponseResult(responseBody: IPushResponseResult) {
        Logger.Info("Reponse: " + JSON.stringify(responseBody));
    }

    static CatchError(error: any) {
        if (error instanceof Error) {
            Logger.Error("    --> Name: " + error.name);
            Logger.Error("    --> Message: " + error.message);
            Logger.ErrorWithPayload("Stack: ", error.stack);
        }
        else {
            Logger.Error("Reason: " + error);
        }
    }

    static UserDevicePairs(userDevicePair: NormalizedUserInfo[]) {
        let logBuilder: SessionLogBuilder = SessionLogBuilder.CreateInstance("Sending push to " + userDevicePair.length + " users!", LogLevel.INFO);

        for (let user of userDevicePair) {
            if (null != user.User) {
                logBuilder.Log("+ User: " + user.User.firstName + " " + user.User.lastName);
            }

            if (null != user.DeviceTuple) {
                for (let tuple of user.DeviceTuple) {
                    logBuilder.Log("    ---> Device ID: " + tuple.DeviceID);
                }
            }
        }

        logBuilder.Destroy();
    }

}