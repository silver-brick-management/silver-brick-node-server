// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

export class Constants {
    /*
        Used by NodeJS's process module
    */
    static readonly EACCES: string = "EACCES";
    static readonly EADDRINUSE: string = "EADDRINUSE";
    static readonly SIGTERM: string = "SIGTERM";
    static readonly SIGINT: string = "SIGINT";

    static readonly ONE_MINUTE_IN_MILLISECONDS: Number = 60 /* seconds */ * 1000 /* 1 second in milliseconds */;
    static readonly LEO_UID = "I01PnAW9pqSXTYm8trfw6pwB45l2";
    static readonly NATE_UID = "qfD6dkLEyrUf3JzXfIScK68XaBh2";
    static readonly FIREBASE_QUEUE_PATH = "queue";
    static readonly TWILLIO_NUMBER = "+19143506680";

    /*
        Firebase Queue spec creation related constants
    */
    static readonly SPEC_UPSTREAM_WORKER_V1: string = "spec_upstream_worker_v1";
    static readonly UPSTREAM_START: string = "upstream_start";
    static readonly UPSTREAM_IN_PROGRESS: string = "upstream_in_progress";

    static readonly SPEC_DOWNSTREAM_WORKER_V1: string = "spec_downstream_worker_v1";
    static readonly DOWNSTREAM_START: string = "downstream_start";
    static readonly DOWNSTREAM_IN_PROGRESS: string = "downstream_in_progress";

    static readonly SPEC_FATAL_ERROR_V1: string = "spec_error_worker_v1";
    static readonly FATAL_ERROR_START: string = "fatal_error_start";
    static readonly FATAL_IN_PROGRESS: string = "fatal_in_progress";

    static readonly SPEC_RPC_WORKER_V1: string = "spec_rpc_worker_v1";
    static readonly RPC_START: string = "rpc_start";
    static readonly RPC_IN_PROGRESS: string = "rpc_in_progress";
    static readonly RPC_FINISHED: string = "rpc_finished";

    static readonly SPEC_FEEDBACK_WORKER_V1: string = "spec_feedback_worker_v1";
    static readonly SPEC_API_WORKER_V1: string = "spec_api_worker_v1";

    /*
        Enviroment Types
    */
    static readonly ENV_DEVELOPMENT: string = "dev";
    static readonly ENV_PRODUCTION: string = "prod";
    static readonly MORGAN_DEV: string = "morganloggingdev";
    static readonly MORGAN_PROD: string = "morganlogging";

    /*
        Slack specific constants
    */
    static readonly SLACK_EVENT_MESSAGE_NAME = "message";
    static readonly SLACK_CHANNEL_MISC_LOGS = "misc_logs";
    static readonly SLACK_SIGNUP_CHANNEL_NAME = "signups";

    /*
        Common constant strings throughout the project
    */
    static readonly STR_AUDIENCE_TENANT_ONLY: string = "tenant";
    static readonly STR_AUDIENCE_LANDLORD_ONLY: string = "landlord";
    static readonly STR_AUDIENCE_TENANT_AND_LANDLORD: string = "tenant_landlord";
    static readonly STR_AUDIENCE_TENANT_AND_STAFF: string = "tenant_staff";
    static readonly STR_AUDIENCE_STAFF_ONLY: string = "staff";
    static readonly STR_CP: string = "user-cp";
    static readonly STR_SPECS: string = "specs";
    static readonly STR_PHOTO: string = "photo";
    static readonly STR_TASKS: string = "tasks";
    static readonly STR_FEEDBACK_FSLASH: string = "feedback/";
    static readonly STR_CALENDAR_COLORS_FSLASH = "calendarColors/";
    static readonly STR_QUEUE_FSLASH_PUSH_FSLASH_TASKS_FSLASH = "queue/push/tasks/";
    static readonly STR_SUMMARY_FSLASH = "summary/";
    static readonly STR_FSLASH_AUDIENCES_FSLASH = "/audiences/";
    static readonly STR_FSLASH_NAME_FSLASH = "/name/";
    static readonly STR_FSLASH_ORGS_FSLASH = "/orgs/";
    static readonly STR_CODES_FSLASH: string = "codes/";
    static readonly STR_MAP_FSLASH: string = "map/";
    static readonly STR_FSLASH_BUILDINGS_FSLASH = "/buildings/";
    static readonly STR_CONTACT_PERSON_FSLASH = "contactPerson/";
    static readonly STR_FSLASH_USERPROFILES_FSLASH = "/userProfiles/";
    static readonly STR_FSLASH = "/";
    static readonly STR_ROOMS_FSLASH = "rooms/";
    static readonly STR_BY_ROOM_FSLASH = "byRoom/";
    static readonly STR_BY_USER_FSLASH: string = "byUser/";
    static readonly STR_FSLASH_INFO = "/info";
    static readonly STR_HISTORY_FSLASH: string = "history/";
    static readonly STR_FSLASH_ORDERS_FSLASH = "/orders/";
    static readonly STR_MESSAGES_FSLASH = "messages/";
    static readonly STR_FSLASH_LAST_MESSAGE_FSLASH = "/lastMessage/";
    static readonly STR_FSLASH_PAYMENT_INFO = "/paymentInfo/";
    static readonly STR_FSLASH_NOTIFICATIONS_FSLASH = "/notifications/";
    static readonly STR_FSLASH_CONFIG = "/config";
    static readonly STR_NOTIFICATIONS_FSLASH: string = "notifications/";
    static readonly STR_FSLASH_IS_FIRSTIME_FSLASH = "/isFirstTime/";
    static readonly STR_FSLASH_PLAN_ID_FSLASH = "/planID/";
    static readonly STR_FSLASH_SUBSCRIPTION_DATE_FSLASH = "/subscriptionNewDate/";
    static readonly STR_FSLASH_PROFILES = "/profiles";
    static readonly STR_FSLASH_HISTORY_FSLASH = "/history/";
    static readonly STR_FSLASH_USERS_FSLASH = "/users/";
    static readonly STR_FSLASH_FCMTOKEN_FSLASH = "/fcmRegToken/";
    static readonly STR_ALL_TASKS_FSLASH = "allTasks/";
    static readonly STR_FSLAH_TIMESTAMP_FSLASH = "/timestamp/";
    static readonly STR_FSLASH_SUMMARY_FSLASH = "/summary/";
    static readonly STR_FSLASH_CLEANING_FSLASH = "/cleanings/";
    static readonly STR_FSLASH_ISCLOCKEDIN_FSLASH = "/isClockedIn/";
    static readonly STR_FSLASH_SERVICES_FSLASH = "/services/";
    static readonly STR_FSLASH_TASKS_FSLASH = "/tasks/";
    static readonly STR_FSLASH_VENDORS_FSLASH = "/vendors/";
    static readonly STR_TENANTS_FSLASH = "tenants/";
    static readonly STR_LANDLORDS_FSLASH = "landlords/";
    static readonly STR_OWNERS_FSLASH = "owners/";
    static readonly STR_STAFF_FSLASH = "staff/";
    static readonly STR_FSLASH_TENANTS_FSLASH = "/tenants/";
    static readonly STR_FSLASH_LANDLORDS_FSLASH = "/landlords/";
    static readonly STR_FSLASH_OWNERS_FSLASH = "/owners/";
    static readonly STR_FSLASH_STAFF_FSLASH = "/staff/";
    static readonly STR_FSLASH_SCHEDULES_FSLASH = "/schedules/";
    static readonly STR_FSLASH_NOTES_FSLASH = "/buildingNotes/";    
    static readonly STR_SCHEDULES_FSLASH = "schedules/";
    static readonly STR_FSLASH_LAST_ACTIVITY_FSLASH = "/lastActivity/";
    static readonly STR_USERS_FSLASH = "users/";
    static readonly STR_USER_LOCATIONS_FSLASH = "userLocations/";
    static readonly STR_PROPERTY_CODES_FSLASH = "propertyCodes/";
    static readonly STR_PROPERTY_INDEX_FSLASH = "propertyIndex/";
    static readonly STR_TIME_CARDS_FSLASH = "timeCards/";
    static readonly STR_FSLASH_BILLING_CONTACT_FSLASH = "/billingContact/";
    static readonly STR_BY_MONTH_FSLASH = "byMonth/";
    static readonly STR_BY_WEEK_FSLASH = "byWeek/";
    static readonly STR_VALUE = "value";
    static readonly STR_DBA = "dba";
    static readonly STR_PORTAL = "portal";
    static readonly STR_LIVE = "live";

    /*
        Defaults
    */
    static readonly DEFAULT_AUDIENCE = "all";
    static readonly DEFAULT_USER_POSITION = "User";
    static readonly DEFAULT_JWT_EXPIRATION = "1h";
    static readonly DEFAULT_JWT_PORTAL_EXPIRATION = "1h";
    static readonly DEFAULT_JWT_CP_EXPIRATION = "10h";
    static readonly DEFAULT_PROFILE_PHOTO_PATH = "https://firebasestorage.googleapis.com/v0/b/silver-brick.appspot.com/o/Defaults%2Fdefault-user.png?alt=media&token=864efc3d-7b31-4f68-8401-965e5c33f734";
    static readonly DEFAULT_NEW_USER_PHOTO_URL_PATH = "https://firebasestorage.googleapis.com/v0/b/silver-brick.appspot.com/o/Defaults%2Fdefault-user.png?alt=media&token=864efc3d-7b31-4f68-8401-965e5c33f734";
    static readonly PREMIUM_PLAN_ID = "plan_FRmVjmCAUVw4Ic";
    static readonly BASE_PLAN_ID = "plan_FRmUqX9sRf3fdX";
    static readonly TEST_PLAN_ID = "plan_Fbup3JUdc7lfx3";
    static readonly PRE_PROD_PLAN_ID = "plan_FhVXctyvITDn3r";
    static readonly BASE_TEST_PLAN_ID = "plan_FnOlNjSlUwwhZA";
    static readonly PREMIUM_TEST_PLAN_ID = "plan_FnOkaeCRu8ksh9";
    // Though this is 512 bit, the config versions are 1024 bit
    static readonly DEFAULT_JWT_TOKEN = "s5v8y/B?E(H+MbQeThWmYq3t6w9z$C&F)J@NcRfUjXn2r4u7x!A%D*G-KaPdSgVk";
    static readonly DEFAULT_HTTP_PORT = 3000;
    static readonly DEFAULT_SOCKET_HANDSHAKE_TIMEOUT = 20000;
    static readonly EXTENDED_SOCKET_HANDSHAKE_TIMEOUT = 120000;
    static readonly SLACK_BOT_TOKEN = "xoxb-1829915228566-1860525943216-e4HgATurfeVS3G59TYZ8UGdn";
    static readonly DEFAULT_SLACK_BOT_ICON = "https://www.adamwolfson.com/silver.jpg";
    /*
        Pagination constants
    */
    static readonly STR_LIMIT = "limit";
    static readonly STR_PREV_CURSOR = "prev_cursor";
    static readonly STR_NEXT_CURSOR = "next_cursor";
    static readonly STR_IS_FORWARD = "is_forward=true";
    static readonly STR_IS_BACKWARD = "is_forward=false";
    static readonly STR_QUESTION = "?";
    static readonly STR_AND = "&";
    static readonly STR_EQUAL = "=";

    /*
        Numbers used in the project
    */
    static readonly NUM_PAGINATION_LIMIT = 25;
    static readonly NUM_LIMIT_BUFFER_ONE = 1;
    static readonly NUM_LIMIT_BUFFER_TWO = 2;
    static readonly NUM_EMAIL_MIN_LENGTH = 5;
    static readonly NUM_EMAIL_MAX_LENGTH = 256;

    /*
        Payload types
    */
    static readonly TYPE_SilverBrick_V1 = "SilverBrickPayload_V1";
    static readonly TYPE_MESSAGE_PAYLOAD_V1 = "SilverBrickMessagePayload_V1";
    static readonly TYPE_SERVICE_ASSIGN_PAYLOAD_V1 = "SilverBrickServiceAssignPayload_V1";
    static readonly TYPE_SERVICE_PROGRESS_PAYLOAD_V1 = "SilverBrickServiceProgressPayload_V1";
    static readonly TYPE_SERVICE_UPDATED_PAYLOAD_V1 = "SilverBrickServiceUpdatedPayload_V1";
    static readonly TYPE_SERVICE_COMPLETE_PAYLOAD_V1 = "SilverBrickServiceCompletePayload_V1";
    static readonly TYPE_SERVICE_REOPENED_PAYLOAD_V1 = "SilverBrickServiceReOpenedPayload_V1";
    static readonly TYPE_SERVICE_NEW_PAYLOAD_V1 = "SilverBrickNewServicePayloadV1";
    static readonly TYPE_SERVICE_MESSAGE_PAYLOAD_V1 = "SilverBrickTaskMessagePayload_V1";
    static readonly TYPE_UPSTREAM_START = "upstream_start";

    /*
        API Logger Types
    */

    static readonly TYPE_API_LIVE = "Live API";
    static readonly TYPE_API_LIVE_AUTH = "Live Auth Api";
    static readonly TYPE_SOCKET_LIVE = "Live Socket Base";

    /*
        Import Constants
    */
    static readonly UPLOAD_IMAGE_PROPERTY_INPUT_FILE = "inputFilePath";

    /*
        Express constants
    */
    static readonly ERROR_MESSAGE_BAD_DATA = "BadData";
    static readonly ERROR_MESSAGE_UNAUTHORIZED = "Unauthorized";
    static readonly ERROR_MESSAGE_FORBIDDEN = "Forbidden";
    static readonly ERROR_MESSAGE_INACTIVE = "InactiveAccount";
    static readonly ERROR_MESSAGE_SERVER_ERROR = "ServerError";
    static readonly ERROR_MESSAGE_ORDER_ERROR = "OrderError";
    static readonly ERROR_MESSAGE_BAD_EMAIL = "BadEmail";
    static readonly ERROR_MESSAGE_BAD_EMAIL_LENGTH = "BadEmailLength";
    static readonly ERROR_MESSAGE_NO_USER = "NoUser";
    static readonly ERROR_MESSAGE_NO_TOKEN = "NoToken";

    static readonly ERROR_DESCRIPTION_MISSING_UID = "The user id URL parameter is missing or invalid";
    static readonly ERROR_DESCRIPTION_MISSING_REQS = "Some of the required parameters are missing or invalid";
    static readonly ERROR_DESCRIPTION_MISMATCH_UID = "The user id in the URL parameter does not match the user id in the user object";
    static readonly ERROR_DESCRIPTION_BAD_PATCH_ACTION = "The patch action must either be 'activate' or 'deactivate'.";

    static readonly SUCCESS_MESSAGE_DEACTIVATE = "UserDeactivated";
    static readonly SUCCESS_MESSAGE_ACTIVATE = "UserActivated";
    static readonly SUCCESS_MESSAGE_DELETE = "DeletedSuccessful";

    static readonly PATCH_ACTIVATE_ACTION = "activate";
    static readonly PATCH_DEACTIVATE_ACTION = "deactivate";

    static readonly PATCH_ADD_ACTION = "add";
    static readonly PATCH_REMOVE_ACTION = "remove";

    static readonly MESSAGE_TITLE_NEW_USER = "Welcome to Verb Your Career";
    static readonly MESSAGE_DESCRIPTION_NEW_USER = "Thank you for joining Verb Your Career";

    /*
        Report Constants
    */
    static readonly REPORT_POST_TITLE = "Post Title";
    static readonly REPORT_USER_NAME = "User Name";
    static readonly REPORT_UPSTREAM_PAYLOAD_TYPE = "Upstream Payload Type";
    static readonly REPORT_SilverBrick_PUSH_TYPE = "SilverBrick Push Type";
    static readonly REPORT_SUBSTATUS = "SubStatus";
    static readonly REPORT_PAYLOAD_AUTHOR_ID = "Payload Author ID";
    static readonly REPORT_PAYLOAD_TYPE = "Payload Type";
    static readonly REPORT_PUSH_SUCCESSES_COUNT = "Push Successes (Count)";
    static readonly REPORT_PUSH_FAILURE_COUNT = "Push Failures (Count)";
    static readonly REPORT_FAILED_PUSHES_DESCRIPTIVE = "Failed Pushes (Descriptive)";
    static readonly REPORT_SUCCESSFUL_PUSHES_DESCRIPTIVE = "Successful Pushes (Descriptive)";
    static readonly REPORT_PUSH_SOUND = "Push Sound";
    static readonly REPORT_SHOULD_NOTIFY = "Should Notify";
    static readonly REPORT_EMAILS_SENT = "Emails Sent";
    static readonly REPORT_EMAIL_SUBJECT = "Email Subject";
    static readonly REPORT_MESSAGE_ID = "Message ID";
    static readonly REPORT_PAYLOAD_AUTHOR_NAME = "Payload Author Name";
    static readonly REPORT_SHOULD_SEND_PUSH = "Should Send Push";
    static readonly REPORT_ATTEMPT_PUSH = "Attempt Push?";
    static readonly REPORT_FOLKS_RECEIVING_PUSH = "Folks Receiving Push";
    static readonly REPORT_ATTEMPTING_TO_SEND_PUSH_TO = "Attempting To Send Push To";
    static readonly REPORT_ATTEMPT_TO_SEND_EMAIL = "Attempting To Send Email To";
    static readonly REPORT_CRITICAL_ERROR = "Critical Error";
    static readonly REPORT_TIMESTAMP = "Report Timestamp";
    static readonly REPORT_ACTION_TYPE = "Action Type";
    static readonly REPORT_ID = "Report ID";

    /*
        Socket io
        Socket channel actions should be defined
        in their respective files as enums
    */
    static readonly SOCKET_SilverBrick_CHANNEL = "/SilverBrick";
    static readonly SOCKET_ACTIVITY_FEED_CHANNEL = "/activity";
    static readonly SOCKET_MESSAGES_CHANNEL = "/messages";
    static readonly SOCKET_NOTIFICATIONS_CHANNEL = "/notifications";
    static readonly SOCKET_CONNECTION = "connection";
    static readonly SOCKET_CONNECT = "connect";
    static readonly SOCKET_DISCONNECT = "disconnect";
    static readonly SOCKET_DISCONNECTING = "disconnecting";
    static readonly SOCKET_RECONNECT = "reconnect";
    static readonly SOCKET_ERROR = "error";
    static readonly SOCKET_AUTHENTICATE = "authenticate";
    static readonly SOCKET_AUTHENTICATED = "authenticated";
    static readonly SOCKET_UNAUTHORIZED = "unauthorized";
    static readonly SOCKET_JOIN = "join";
    static readonly SOCKET_JOINED = "joined";
    static readonly SOCKET_LEAVE = "leave";
    static readonly SOCKET_REFRESH = "refresh";
    static readonly SOCKET_LOGOUT = "logout";

    /*
        Message Constants
    */
    static readonly REQUEST_CHECKIN_PERSON_MSG = "cannot be found. Does anyone have any updates on their location?";
    static readonly USER_NO_FCM_TOKEN = "The user has no fcm token.";

    /*
        OS
    */
    static readonly OS_TYPE_ANDROID = "android";
    static readonly OS_TYPE_IOS = "ios";

    /*
        TIME
    */

    // Note! The granularity of time in javascript is milliseconds!
    static readonly A_SECOND_IN_MILLISECONDS = 1000;
    static readonly AN_HOUR_IN_MINUTES = 60;
    static readonly AN_HOUR_IN_SECONDS = Constants.AN_HOUR_IN_MINUTES * 60; // 60 * 60 = 3600
    static readonly AN_HOUR_IN_MILLISECONDS = Constants.AN_HOUR_IN_SECONDS * Constants.A_SECOND_IN_MILLISECONDS; // 3600 * 1000 = 3,600,000
    static readonly A_DAY_IN_MILLISECONDS = Constants.AN_HOUR_IN_MILLISECONDS * 24 /* 24 hours in a day */; // 3,600,000 * 24 = 86,400,000
    static readonly END_OF_DAY_IN_MILLISECONDS = Constants.A_DAY_IN_MILLISECONDS - 1;
    static readonly START_OF_DAY_IN_MILLISECONDS = 0 /* 12 AM in epoch milliseconds*/;
    static readonly A_WEEK_IN_MILLISECONDS = Constants.A_DAY_IN_MILLISECONDS * 7;
}

export const staffColors: any = {
    "ZG4D0TFYG2VY0FuQaoBmQ5by5vw2": {
        primary: "#ff885c",
        secondary: "#000"
    },
    "UBAkHLKTuGUFnSwmrZXi37MzUPD2": {
        primary: "#a9efa9",
        secondary: "#000"
    },
    "Zk8kplp7R1hISEVsfm2WfescaJ12": {
        primary: "#c0b359",
        secondary: "#000"
    },
    "I41o6F08A7cClZh8sIqXFs39Rk02": {
        primary: "#87cefa",
        secondary: "#000"
    },
    "SDp6u7LC0zR2T6Y8BrwCSV8sp8Z2": {
        primary: "#deb887",
        secondary: "#000"
    },
    "qfD6dkLEyrUf3JzXfIScK68XaBh2": {
        primary: "#660099",
        secondary: "#fff"
    },
    "HaJsDQiiWDglxcAWcIcQ4xD1dJ42": {
        primary: "#deb887",
        secondary: "#000"
    },
    "notFound": {
        primary: "rgb(217, 217, 217)",
        secondary: "#000"
    },
    "sCL3x6K4vqeNMn1XH1BwVtarE1T2": {
        primary: "rgb(0, 0, 0)",
        secondary: "#fff"
    },
    "lEkZQcZNgVe54yCQGWCelx3qPRF3": {
        primary: "rgb(229, 230, 46)",
        secondary: "#000"
    },
    "hb7m6CJ91ydD6VbJIlIiGY5shu52": {
        primary: "rgb(230, 162, 46)",
        secondary: "#000"
    },
    "aAEzjUa46bSzVWwQOd5cWe5KYCH2": {
        primary: "rgb(18, 228, 233)",
        secondary: "#000"
    },
    "ZvY8ck96v2XID3CFoxvVbjsoWib2": {
        primary: "rgb(18, 228, 233)",
        secondary: "#000"
    },
    "YXCHgSu3gQXaLezWhsrxenhtdqr1": {
        primary: "rgb(251, 100, 100)",
        secondary: "#fff"
    },
    "JcnB7Git6zUrqS4gfDgfcScFwSR2": {
        primary: "rgb(20, 86, 143)",
        secondary: "#fff"
    },
    "EE0vk3d483XV7njC9ycdQnrRlhp2": {
        primary: "rgb(20, 86, 143)",
        secondary: "#fff"
    },
    "7VUXqqjXP8ZvsoPTOpRdlDxwwqH2": {
        primary: "rgb(143, 33, 20)",
        secondary: "#fff"
    },
    "0gQOWQmcq0ZBJ8WTU7Yx3bqmxAu2": {
        primary: "rgb(170, 131, 14)",
        secondary: "#fff"
    },
    "0K2swEAq2vTQrPkrYFvADQFqHcs2": {
        primary: "rgb(21, 134, 15)",
        secondary: "#fff"
    }
    
};

export const WeekDays: any[] = ["SU", "MO", "TU", "WE", "TH", "FR", "SA"];