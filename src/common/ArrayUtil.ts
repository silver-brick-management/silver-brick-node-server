import { IBuildingBasic, IBuildingSimple, ITask, ITaskFeed } from "../shared/SilverBrickTypes";

export class ArrayUtil {

    private static getRandomNumber(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static SortByDate(users: ITaskFeed[]): ITaskFeed[] {
        let newTasks: ITaskFeed[] = users.sort((task1, task2) => {
            if (task1.timestamp > task2.timestamp) {
                return -1;
            }

            if (task1.timestamp < task2.timestamp) {
                return 1;
            }

            return 0;
        });
        return newTasks;
    }

    static SortTasksByDate(users: ITask[]): ITask[] {
        let newTasks: ITask[] = users.sort((task1, task2) => {
            if (task1.lastUpdated < task2.lastUpdated) {
                return -1;
            }

            if (task1.lastUpdated > task2.lastUpdated) {
                return 1;
            }

            return 0;
        });
        // console.log("newTasks", newTasks);
        return newTasks;
    }

    static TransformSimple(value: any): IBuildingSimple[] {
        let keys = Object.keys(value);
        let bldgArray: IBuildingSimple[] = [];
       // let newKeys: IBuildingBasic[] = keys.map(k => value[k]);
        keys.forEach((key: string) => {
            let newBldg: IBuildingSimple = {
                id: key,
                name: value[key]
            };
            bldgArray.push(newBldg);
        });

        return bldgArray;
    }

    static TransformBasic(value: any): IBuildingBasic[] {
        if (!value || (null == value)) {
            const newBldg: IBuildingBasic[] = [];
            return newBldg;
        }
        let keys = Object.keys(value);
        let newKeys: IBuildingBasic[] = keys.map(k => value[k]);
        
        return newKeys;
    }

    static ShuffleInPlace<T>(array: T[]): T[] {
        // if it's 1 or 0 items, just return
        if (array.length <= 1) return array;

        // For each index in array
        for (let i = 0; i < array.length; i++) {

            // choose a random not-yet-placed item to place there
            // must be an item AFTER the current item, because the stuff
            // before has all already been placed
            const randomChoiceIndex = this.getRandomNumber(i, array.length - 1);

            // place our random choice in the spot by swapping
            [array[i], array[randomChoiceIndex]] = [array[randomChoiceIndex], array[i]];
        }

        return array;
    }

    static DivideIntoChunks<T>(arr: T[], chunkSize: number): T[][]  {
        let groups: T[][] = [];

        for (let i = 0; i < arr.length; i += chunkSize) {
            groups.push(arr.slice(i, i + chunkSize));
        }
        return groups;
    }

    static Transform(value: any): any[] {
        let keys = Object.keys(value);
        let newKeys: any[] = keys.map(k => value[k]);
        return newKeys;
    }
}