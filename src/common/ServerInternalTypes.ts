// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { ISilverBrickUser } from  "../shared/SilverBrickTypes";
import { UserChatRoom, MessageParticipant } from "../shared/ResponseTypes";

// Details of the actual spec,
// that will pushed/updated to firebase
export class FirebaseSpecEntryData {
    readonly start_state: string;

    readonly in_progress_state: string;

    readonly finished_state: string;

    readonly error_state: string;

    readonly timeout: Number;

    readonly retries: Number;

    constructor(
        start_state: string,
        in_progress_state: string,
        finished_state: string,
        error_state: string,
        timeout: Number,
        retries: Number) {

        this.start_state = start_state;
        this.in_progress_state = in_progress_state;
        this.finished_state = finished_state;
        this.error_state = error_state;
        this.timeout = timeout;
        this.retries = retries;
    }

    toString(): string {
        return JSON.stringify(this);
    }
}

export interface IEmailPredicate {
    FirstName: string;
    Email: string;
}

export class SpecEntry {
    readonly Name: string;
    readonly Data: FirebaseSpecEntryData;

    constructor(name: string, data: FirebaseSpecEntryData) {
        this.Name = name;
        this.Data = data;
    }
}

export class FirebaseQueueTaskPayload {
    readonly SpecType: string;
    readonly Data: any;
    readonly _state: string;

    constructor(specType: string, data: any, state: string) {
        this.SpecType = specType;
        this.Data = data;
        this._state = state;
    }
}

export enum PushSound {
    NUKE,
    DEFAULT
}

export enum PushPriority {
    HIGH
}

export enum SilverBrickPushType {
    NONE,
    WEEKLY,
    NEW_ACCOUNT,
    NEW_MESSAGE,
    SERVICE_ASSIGN,
    NEW_REQUEST,
    SERVICE_COMPLETE,
    REQUEST_UPDATED,
    SERVICE_REOPENED,
    REQUEST_MESSAGE,
    SERVICE_PROGRESS
}

export class SilverBrickPushPredicate {
    readonly UserDevicePair: NormalizedUserInfo[];
    readonly Title: string;
    readonly PushType: SilverBrickPushType;
    readonly AllowSound: boolean;
    readonly CreatedTimestamp?: number;
    readonly Description: string;

    constructor(
        userDevicePair: NormalizedUserInfo[],
        title: string,
        pushType: SilverBrickPushType,
        allowSound: boolean,
        createdTimestamp: number,
        description: string) {

        this.UserDevicePair = userDevicePair;
        this.Title = title;
        this.PushType = pushType;
        this.AllowSound = allowSound;
        this.CreatedTimestamp = createdTimestamp;
        this.Description = description;
    }
}

export class NewMessagePushPredicate {
    readonly UserDevicePair: NormalizedUserInfo[];
    readonly Title: string;
    readonly Message: string;
    readonly PushType: SilverBrickPushType;
    readonly AllowSound: boolean;
    readonly CreatedTimestamp: number;
    readonly Description: string;

    constructor(
        userDevicePair: NormalizedUserInfo[],
        title: string,
        pushType: SilverBrickPushType,
        allowSound: boolean,
        createdTimestamp: number,
        description: string) {

        this.UserDevicePair = userDevicePair;
        this.Title = title;
        this.PushType = pushType;
        this.AllowSound = allowSound;
        this.CreatedTimestamp = createdTimestamp;
        this.Description = description;
    }
}

export class ServiceAssignPushPredicate {
    readonly UserDevicePair: NormalizedUserInfo[];
    readonly Title: string;
    readonly Message: string;
    readonly PushType: SilverBrickPushType;
    readonly AllowSound: boolean;
    readonly CreatedTimestamp: number;
    readonly Description: string;

    constructor(
        userDevicePair: NormalizedUserInfo[],
        title: string,
        pushType: SilverBrickPushType,
        allowSound: boolean,
        createdTimestamp: number,
        description: string) {

        this.UserDevicePair = userDevicePair;
        this.Title = title;
        this.PushType = pushType;
        this.AllowSound = allowSound;
        this.CreatedTimestamp = createdTimestamp;
        this.Description = description;
    }
}

export class ServiceMessagePushPredicate {
    readonly UserDevicePair: NormalizedUserInfo[];
    readonly Title: string;
    readonly Message: string;
    readonly PushType: SilverBrickPushType;
    readonly AllowSound: boolean;
    readonly CreatedTimestamp: number;
    readonly Description: string;

    constructor(
        userDevicePair: NormalizedUserInfo[],
        title: string,
        pushType: SilverBrickPushType,
        allowSound: boolean,
        createdTimestamp: number,
        description: string) {

        this.UserDevicePair = userDevicePair;
        this.Title = title;
        this.PushType = pushType;
        this.AllowSound = allowSound;
        this.CreatedTimestamp = createdTimestamp;
        this.Description = description;
    }
}

export interface ISilverBrickMessagesPayloadV1 {
    readonly SenderID: string;
    readonly AuthorID: string;
    readonly RoomID: string;
    readonly Message?: string;
    readonly AuthorName: string;
    readonly MessageType: string;
    readonly BuildingID: string;
    readonly OrgID: string;
    readonly PostType: SilverBrickPushType;
}

export interface ISilverBrickNewServicePayloadV1 {
    readonly SenderID: string;
    readonly TaskID: string;
    readonly Message?: string;
    readonly AuthorName: string;
    readonly BuildingID: string;
    readonly OrgID: string;
    readonly PostType: SilverBrickPushType;
}

export interface ISilverBrickServiceAssignPayloadV1 {
    readonly SenderID: string;
    readonly TaskID: string;
    readonly Message?: string;
    readonly AuthorName: string;
    readonly BuildingID: string;
    readonly OrgID: string;
    readonly PostType: SilverBrickPushType;
}

export interface ISilverBrickTaskMessagePayloadV1 {
    readonly SenderID: string;
    readonly TaskID: string;
    readonly Message?: string;
    readonly AuthorName: string;
    readonly BuildingID: string;
    readonly OrgID: string;
    readonly PostType: SilverBrickPushType;
}

export interface ISilverBrickPayloadV1 {
    readonly UserID: string;
    readonly Message?: string;
    readonly Type: SilverBrickPushType;
    readonly id: string;
}

export interface IUpstreamFCMPayload {
    readonly MessageID: string;

    readonly FromDeviceID: string;

    readonly Data: any;

    readonly Timestamp: any;

    readonly Type: string;

    readonly UserID: string;

    readonly _state?: string;
}

export enum OperatingSystemType {
    NONE,
    ANDROID,
    IOS,
    OTHER
}

export class DeviceInfoTuple {
    readonly DeviceID: string;
    readonly OsType: OperatingSystemType;

    private constructor (deviceID: string, osType: OperatingSystemType) {
        this.DeviceID = deviceID;
        this.OsType = osType;
    }

    static CreateInstance(deviceID: string, osType: OperatingSystemType): DeviceInfoTuple {
        return new DeviceInfoTuple(deviceID, osType);
    }
}

export class NormalizedUserInfo {
    readonly DeviceTuple: DeviceInfoTuple[];
    readonly User: ISilverBrickUser;
    readonly Emails: string[];
    readonly UserID: string;

    private constructor(userID: string, user: ISilverBrickUser, emails: string[], deviceTuples: DeviceInfoTuple[]) {
        this.UserID = userID;
        this.User = user;
        this.Emails = emails;
        this.DeviceTuple = deviceTuples;
    }

    static CreateInstance(userID: string, user: ISilverBrickUser, emails: string[], deviceTuples: DeviceInfoTuple[]): NormalizedUserInfo {
        return new NormalizedUserInfo(userID, user, emails, deviceTuples);
    }
}