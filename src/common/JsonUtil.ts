// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import * as fs from "fs";

export class JSONUtil {
    static SanitizeObject(obj: any): any {
        // Strips out any undefines
        return JSON.parse(JSON.stringify(obj));
    }

    static ExpandJsonObject(obj: any): any {
        if (null == obj) {
            return null;
        }

        // Check if it has the 'Include' property, which is what we use
        // to include a base JSON file before assigning properties in the target
        // file
        let includePath: string = obj["Include"];

        if (!includePath) {
            // Return the obj back, since no 'Include' property is defined.
            // In otherwords, there's nothing to expand
            return obj;
        }


        // Load the JSON config file
        let includeJsonData: any = JSON.parse(fs.readFileSync(includePath, "utf8"));

        // We use a map here, since a HashSet doesn't exist in JS currently
        type OverridablePropertyMap = {
            // [Property] => boolean
            [key: string]: boolean;
        };

        let overridablePropertyMap: OverridablePropertyMap = {};

        // Check for overrides
        let overridablePropsStr: string = obj["OverridableProperties"];
        if (overridablePropsStr) {
            let overridablePropsEntry: string[] = overridablePropsStr.split(";");
            for (let property of overridablePropsEntry) {
                // true, means it exists.
                overridablePropertyMap[property] = true;
            }
        }

        for (let entry in obj) {
            if (includeJsonData[entry]) {
                if (overridablePropertyMap[entry]) {
                    // Override/overlap has been detected, and it's a valid overrides
                    // continue safely and add data
                    includeJsonData[entry] = obj[entry];
                }
                else {
                    throw new Error("An undocumented override has been detected! Key - " + entry);
                }
            }
            else {
                // No overlap, add safely
                includeJsonData[entry] = obj[entry];
            }

        }

        return includeJsonData;
    }
}