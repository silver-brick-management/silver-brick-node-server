// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

import { Constants } from "./Constants";

export enum TimeZoneType {
    EST
}

export enum Months {
    JAN,
    FEB,
    MAR,
    APR,
    MAY,
    JUN,
    JUL,
    AUG,
    SEP,
    OCT,
    NOV,
    DEC
}

export class TimeUtil {
    static IsTimeInRange(targetTime: Date, startTime: Date, endTime: Date): boolean {
        let normalizedTargetTime: number = targetTime.getTime() % Constants.A_DAY_IN_MILLISECONDS;
        let normalizedStartTime: number = startTime.getTime() % Constants.A_DAY_IN_MILLISECONDS;
        let normalizedEndTime: number = endTime.getTime() % Constants.A_DAY_IN_MILLISECONDS;
        // console.log("normalizedTargetTime: ", normalizedTargetTime, " normalizedStartTime: ", normalizedStartTime, " normalizedEndTime: ", normalizedEndTime);
        return (normalizedTargetTime <= normalizedEndTime) && (normalizedTargetTime >= normalizedStartTime);
    }

    static ConvertLocalTimeToUtc(localTimeInSeconds: any | string, timeZone: TimeZoneType): number {
        let adjustmentHours: number = 0;
        switch (timeZone) {
            case TimeZoneType.EST:
                adjustmentHours = 5;
                break;

            default:
            {
                throw new Error("Invalid Time Zone!");
            }

        }
        return (localTimeInSeconds * Constants.A_SECOND_IN_MILLISECONDS) + (adjustmentHours * Constants.AN_HOUR_IN_MILLISECONDS);
    }

    static GetMonthDayYear(): string {
        let newDate: string = "";
        const curDate: Date = new Date(Date.now());
        newDate = Months[curDate.getMonth()] + " " + curDate.getDay().toString() + ", " + curDate.getFullYear().toString();
        return newDate;
    }

    static BuildISODate(date: string, time: string, start: boolean): number {
        let dateArray = date && date.split("-");
        let timeArray = time && time.split(":");
        let timeNumber: number = start ? parseInt(timeArray[1]) : parseInt(timeArray[1]) + 30;
        let normalDate = new Date(parseInt(dateArray[0]), parseInt(dateArray[1]) - 1, parseInt(dateArray[2]), parseInt(timeArray[0]), timeNumber, 0, 0);
        // console.log("normalDate", normalDate);
        return normalDate.getTime();
    }

    public static GetMonday(day: Date, date: number): number {
        let d = new Date();
        d.setDate(d.getDate() + (day.getDay() === date ? 7 : (date + 7 - d.getDay()) % 7));
        console.log(d);
        return d.getTime();
    }

    static GetReportMonday(day: Date): number {
        day = new Date(day);
        let d: number = day.getDay();
        let diff: number = day.getDate() - d + (d === 0 ? - 6 : 1); // adjust when day is sunday
        // console.log("Get Monday", new Date().setDate(diff));
        return new Date().setDate(diff);
    }

    static GetWeekDay(day: string): number {
        switch (day) {
            case "Sunday": {
                return 0;
            }

            case "Monday": {
                return 1;
            }

            case "Tuesday": {
                return 2;
            }

            case "Wednesday": {
                return 3;
            }

            case "Thursday": {
                return 4;
            }

            case "Friday": {
                return 5;
            }

            case "Saturday": {
                return 6;
            }

            default: {
                return 0;
            }
        }
    }
}