// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************

// Node modules
import { Request } from "express";
import { parse, format } from "url";

export class HTTPUtil {
    static NormalizePort(val: number | string): number | string | boolean {
        let port: number = (typeof val === "string") ? parseInt(val, 10) : val;
        if (isNaN(port)) {
            return val;
        } else if (port >= 0) {
            return port;
        } else {
            return false;
        }
    }

    static GetFullUrl(req: Request, query: string = null): string {
        // Gets the ful url for a given route and builds a new one
        // with queries if included
        let urlObj = parse(req.originalUrl);
        urlObj.protocol = req.protocol;
        urlObj.host = req.get("host");
        urlObj.search = query;
        return format(urlObj);
    }
}