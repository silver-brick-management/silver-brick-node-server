FROM node:8-onbuild
ARG MAX_OLD_SPACE_SIZE=4096
ENV NODE_OPTIONS=--max-old-space-size=${MAX_OLD_SPACE_SIZE}
MAINTAINER adam@adamwolfson.com

# Ports exposed in the container
EXPOSE 3000

# node on Docker defaults to normal events only, therefore
# we escalate the log level to warn
ENV NPM_CONFIG_LOGLEVEL warn

RUN npm install -g ts-node typescript 
