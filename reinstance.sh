#!/bin/bash

if [ "$1" == "prod" ]
then
    echo "Detected Production flag!"
    IsProduction=true
    IsPreProd=false
elif [ "$1" == "preprod" ]
then
    echo "Detected PreProd flag!"
    IsProduction=false
    IsPreProd=true
else
    echo "Usage Info: ./reinstance.sh prod"
    exit 1
fi

# Delete all containers
docker rm $(docker ps -a -q) -f

# Delete all built images
docker rmi $(docker images -q)

# Building the 'notification-server' image
docker build -t notification-server .

if [ "$IsProduction" == true ]
then
    echo "Creating 'production' docker workers!"

    # Run the upstream and downstream containers
    docker run -d --name=downstream_production_0 --env-file docker/downstream.production.env --log-driver=awslogs --log-opt awslogs-group=production-server-log-group --log-opt awslogs-stream=downstream_prod_0 --restart always notification-server
    docker run -d --name=upstream_production_0 --env-file docker/upstream.production.env --log-driver=awslogs --log-opt awslogs-group=production-server-log-group --log-opt awslogs-stream=upstream_prod_0 --restart always notification-server
    docker run -d --name=error_production_0 --env-file docker/error.production.env --log-driver=awslogs --log-opt awslogs-group=production-server-log-group --log-opt awslogs-stream=error_prod_0 --restart always notification-server
    docker run -p 3000:3000 -d --name=httpstack_production_0 --env-file docker/httpstack.production.env --log-driver=awslogs --log-opt awslogs-group=production-server-log-group --log-opt awslogs-stream=httpstack_prod_0 --restart always notification-server
elif [ "$IsPreProd" == true ]
then
    echo "Creating 'pre-production' docker workers!"

    # Run the upstream and downstream containers
    docker run -d --name=downstream_preprod_0 --env-file docker/downstream.preproduction.env --log-driver=awslogs --log-opt awslogs-group=preprod-server-log-group --log-opt awslogs-stream=downstream_preprod_0 --restart always notification-server
    docker run -d --name=upstream_preprod_0 --env-file docker/upstream.preproduction.env --log-driver=awslogs --log-opt awslogs-group=preprod-server-log-group --log-opt awslogs-stream=upstream_preprod_0 --restart always notification-server
    docker run -d --name=error_preprod_0 --env-file docker/error.preproduction.env --log-driver=awslogs --log-opt awslogs-group=preprod-server-log-group --log-opt awslogs-stream=error_preprod_0 --restart always notification-server
    docker run -p 3000:3000 -d --name=httpstack_preprod_0 --env-file docker/httpstack.preproduction.env --log-driver=awslogs --log-opt awslogs-group=preprod-server-log-group --log-opt awslogs-stream=httpstack_preprod_0 --restart always notification-server

fi
